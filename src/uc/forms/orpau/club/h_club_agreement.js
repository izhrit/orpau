define([
	'forms/base/h_msgbox'
  , 'forms/orpau/club/c_club_agreement'
],
function (h_msgbox, c_club_agreement)
{
	var helper = {};

	helper.Вступить_в_профсоюз = function(ау, base_url, on_signed)
	{
		var self = this;
		var club_agreement_url = base_url + '?action=club.agreement';
		var url = club_agreement_url + '&cmd=get-union-application-to-sign';
		var v_ajax = h_msgbox.ShowAjaxRequest('Подготовка заявления о вступлении в профсоюз', url);
		v_ajax.ajax
		({
			dataType: "json",
			type: 'GET',
			cache: false,
			success: function (txt, textStatus)
			{
				if (null == txt)
				{
					v_ajax.ShowAjaxError(txt, textStatus);
				}
				else
				{
					self.Подписать_заявление_о_вступлении_в_профсоюз(ау, base_url, txt, on_signed);
				}
			}
		});
	}

	helper.Подписать_заявление_о_вступлении_в_профсоюз = function (ау, base_url, txt, on_signed)
	{
		var cc_club_agreement= c_club_agreement({ base_url: base_url });
		cc_club_agreement.SetFormContent({ ау: ау, text: txt });

		var id_div = 'cpw-form-orpau-club-agreement';
		cc_club_agreement.CloseSelf= function()
		{
			h_msgbox.Close({id_div: id_div});
			on_signed();
		}

		h_msgbox.ShowModal({
			title:'Подписание заявления о вступлении в профсоюз'
			,buttons:['Закрыть, не подписывая']
			,id_div:id_div, controller:cc_club_agreement
		});
	}

	helper.AlertAsp = function(on_sign_asp)
	{
		var btn_cancel= 'Отмена';
		var btn_sign_asp= 'Перейти к подписанию соглашения';
		var buttons= !on_sign_asp ? ['OK'] : [btn_sign_asp,btn_cancel];
		h_msgbox.ShowModal({
			title: "Внимание", width: 600
			, html:'<span>Для вступления в профсоюз Вам нужно подписать заявление о присоединении</span><br/>'
				+ 'к <a href="' + app.url_offer_asp
				+ '">cоглашению об использовании аналога собственноручной подписи</a>!'
			, buttons:buttons
			, onclose: function (btn)
			{
				if (btn == btn_sign_asp)
				{
					on_sign_asp();
				}
			}
		});
	}

	return helper;
});