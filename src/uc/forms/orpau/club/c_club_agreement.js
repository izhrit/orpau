define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/club/e_club_agreement.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/base/h_check_mobile'
],
function(c_fastened, tpl, h_msgbox, h_check_mobile)
{
	return function(options_arg)
	{

		var isMobile = h_check_mobile.isMobile;
		var options = { isMobile: isMobile };
		var controller = c_fastened(tpl, options);

		controller.size = !isMobile() 
			? { width: 700, height: 600 } 
			: { width: window.innerWidth, height: window.innerHeight };

		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.club_agreement_url = controller.base_url + '?action=club.agreement';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self= this;

			if (!this.model)
			{
				//$(sel + ' div.send-key-first').hide();
				$(sel + ' div.key-sent').hide();
			}
			else
			{
				$(sel + ' div.no-sign').hide();
				$(sel + ' div.key-sent').hide();
			}

			$(sel + ' button.send-code').click(function (e) { self.OnSendCode(); });
			$(sel + ' button.resend-code').click(function (e) { self.OnSendCode(); });
			$(sel + ' button.sign').click(function (e) { self.OnSign(); });
		}

		controller.OnSendCode= function()
		{
			var self = this;

			var url = this.club_agreement_url + '&cmd=throw-code';
			var v_ajax = h_msgbox.ShowAjaxRequest("Запроса на отправку кода подтверждения", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.OnCodeSent(data);
					}
				}
			});
		}

		controller.OnCodeSent= function(sent)
		{
			var sel = this.fastening.selector;
			$(sel + ' div.send-key-first').hide();
			$(sel + ' div.key-sent').show();
			$(sel + ' input.code').focus();

			$(sel + ' span.sms.time').text(sent.sms.time);
			$(sel + ' span.sms-number').text(sent.sms.number);

			$(sel + ' button.resend-code').hide();

			this.code_sent_now = (new Date()).getTime();
			this.OnTimer();

			$(sel + ' .resend-after').show();

			$(sel + ' .document-on-sign').height('240px')

			if (!sent.test_code)
			{
				$(sel + ' span.test-code').hide();
			}
			else
			{
				$(sel + ' span.test-code').show();
				$(sel + ' span.test-code span.code').text(sent.test_code);
			}

			if (!this.timer_id)
			{
				var self = this;
				var each_second = function () { self.OnTimer(); }
				this.timer_id = setInterval(each_second, 900)
			}
		}

		controller.OnTimer= function()
		{
			var sel = this.fastening.selector;
			var now = (new Date()).getTime();
			var diff = now - this.code_sent_now;
			var diff_seconds = diff / 1000;
			var to_resend = Math.floor(60 - diff_seconds);
			if (to_resend > 0)
			{
				$(sel + ' .resend-after span.seconds').text(to_resend);
			}
			else
			{
				$(sel + ' .resend-after').hide();
				$(sel + ' button.resend-code').show();
			}
		}

		controller.OnSign = function ()
		{
			var sel = this.fastening.selector;
			var code = $(sel + ' input.code').val().trim();

			if ('' == code)
			{
				h_msgbox.ShowModal({
					title: "Действия при подписании", width: 380
					, html: "Для подписания Вам нужно указать полученный код подтверждения!"
				});
				$(sel + ' input.code').focus();
			}
			else
			{
				var self = this;
				var url = this.club_agreement_url + '&cmd=sign';
				if (this.model && this.model.ау && this.model.ау.id_Manager)
					url+= '&id_Manager=' + this.model.ау.id_Manager;
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка подписи", url);
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, data: { code: code }
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.OnSigned(data);
						}
					}
				});
			}
		}

		controller.OnSigned= function(signed)
		{
			var self = this;
			if (!signed.ok)
			{
				h_msgbox.ShowModal({
					title: "Подписание заявления не произошло!", width: 380
					, html: "Подписание заявления не произошло по причине:<br/>"
					+ "<b>" + signed.why + "</b>"
					, onclose: function ()
					{
						self.StartSigningAgain();
					}
				});
			}
			else
			{
				h_msgbox.ShowModal({
					title: "Заявление подписано", width: 380
					, html: "Подписание заявления о вступлении в профсоюз и регистрация его на сервере завершились успешно!"
					, onclose: function()
					{
						if (self.CloseSelf)
							self.CloseSelf();
					}
				});
			}
		}

		controller.StartSigningAgain= function()
		{
			var sel = this.fastening.selector;
			$(sel + ' div.key-sent').hide();
			$(sel + ' div.send-key-first').show();
			$(sel + ' input.code').val('');
			$(sel + ' span.test-code').hide();
			$(sel + ' .document-on-sign').height('340px')
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy= function()
		{
			base_Destroy.call(this);
			if (this.timer_id && null!=this.timer_id)
			{
				clearInterval(this.timer_id);
				delete this.timer_id;
			}
		}

		return controller;
	}
})