﻿define([
	'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service = ajax_jqGrid('orpau?action=manager.jqgrid');

	service.rows_all = function ()
	{
		var result = ql.select(function (r) { return {
				"id_Manager": r.m.id_Manager
				, "ArbitrManagerID": r.m.ArbitrManagerID
				, "FirstName": r.m.FirstName
				, "LastName": r.m.LastName
				, "MiddleName": r.m.MiddleName
				, "INN": r.m.INN
				, "SRO": (!r.sro || null==r.sro) ? '' : r.sro.Name
				, "Region": !r.region ? '' : r.region.Name
				, "id_Group": (function(){
					return ql.select(function (t) { return t.mgm.id_Manager_group })
					.from(db.manager_group_manager, 'mgm')
					.where(function(t) { return r.m.id_Manager == t.mgm.id_Manager })
					.exec().join(', ')
				})()
			}})
			.from(db.manager, 'm')
			.left_join(db.sro,'sro',function (r) { return r.m.SRORegNum == r.sro.RegNum })
			.left_join(db.region,'region',function (r) { return r.m.id_Region == r.region.id_Region })
			.exec();
		return result;
	}

	return service;
});