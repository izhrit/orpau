define([
	  'forms/base/ql'
	, 'forms/d_orpau'
	, 'forms/base/ajax/ajax-service-base'
	, 'forms/orpau/base/h_cryptoapi'
]
, function (ql, db, ajax_service_base, h_ctyptoapi)
{
	var service = ajax_service_base("orpau?action=manager.auth");

	service.GetTokenToSignForManager = function (manager)
	{
		if (null == manager)
		{
			return null;
		}
		else
		{
			var token= {
				with_datamart:'test_auth_with_datamart_token_to_sign_for_id_Manager_' + manager.id_Manager
				,without_datamart:'test_auth_without_datamart_token_to_sign_for_id_Manager_' + manager.id_Manager
			};
			console.log('prepared token:');
			console.log(token);
			return token;
		}
	}

	service.GetTokenToSign = function(data)
	{
		if (!data.Subject)
		{
			var manager = ql.find_first_or_null(db.manager, function (m) { return m.INN==data.inn; });
			return this.GetTokenToSignForManager(manager);
		}
		else
		{
			var subject= h_ctyptoapi.GetDNFields(data.Subject);
			var manager = ql.find_first_or_null(db.manager, function (m) { return m.INN==subject.ИНН; });
			return this.GetTokenToSignForManager(manager);
		}
	}
	
	// здесь проверки не происходит, т.к. браузер блокирует запросы к другим доменным именам
	service.AuthByTokenSignature = function(parsed_args,data)
	{
		var id_Manager= parsed_args.id_Manager;

		var manager = ql.find_first_or_null(db.manager, function (m) { return m.id_Manager==id_Manager; });

		var auth_info = {
			id_Manager: id_Manager
			,ArbitrManagerID: manager.ArbitrManagerID

			,Фамилия: manager.LastName
			,Имя: manager.FirstName
			,Отчество: manager.MiddleName
			,ИНН: manager.INN
			,efrsbNumber: manager.RegNum
		}

		if ('with_datamart' == parsed_args.variant)
		{
			auth_info.datamart = {id_ManagerCertAuth:"128",auth:"uc_test_auth"};
		}

		console.log('authorized:');
		console.log(auth_info);

		return auth_info;
	}

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		switch (parsed_args.cmd)
		{
			case 'get-token-to-sign':
				var token= this.GetTokenToSign(cx.originalOptions.data);
				if (null != token)
				{
					cx.completeCallback(200, 'success', { text: JSON.stringify({ token: token }) });
				}
				else
				{
					cx.completeCallback(404, 'not found', { text: JSON.stringify({ token: token }) });
				}
				return;
			case 'auth-by-token-signature':
				var res = this.AuthByTokenSignature(parsed_args,cx.originalOptions.data);
				cx.completeCallback(200, 'success', { text: JSON.stringify(res, null, '\t') });
				return;
			case 'logout':
				cx.completeCallback(200, 'success', { text: JSON.stringify({ok:true}, null, '\t') });
				return;
		}
		var error_msg= 'unknown cmd=' + parsed_args.cmd + '!';
		console.log(error_msg);
		completeCallback(400, 'bad request', { text: error_msg });
	};

	return service;
});