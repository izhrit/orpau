define([
	  'forms/base/ql'
	, 'forms/d_orpau'
	, 'forms/base/ajax/ajax-service-base'
	, 'forms/orpau/base/h_cryptoapi'
]
, function (ql, db, ajax_service_base, h_ctyptoapi)
{
	var service = ajax_service_base("orpau?action=manager.authPassword");

	service.LoginManager = function(data) {
		var params = JSON.parse('{"' + decodeURIComponent(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');

		var manager = ql.find_first_or_null(db.manager, function (m) { return m.EMail==params.Login && m.Password==params.Password; });

		var auth_info = null;

		if (manager) {
			auth_info = {
				id_Manager: manager.id_Manager
				,ArbitrManagerID: manager.ArbitrManagerID

				,Фамилия: manager.LastName
				,Имя: manager.FirstName
				,Отчество: manager.MiddleName
				,ИНН: manager.INN
				,efrsbNumber: manager.RegNum
			}
		}

		return auth_info;
	}

	service.action= function (cx)
	{
		var result = this.LoginManager(cx.originalOptions.data);
		if(result){
			cx.completeCallback(200, 'success', { text: JSON.stringify(result, null, '\t') });
		} else {
			cx.completeCallback(200, 'success', null);
		}
	};

	return service;
});