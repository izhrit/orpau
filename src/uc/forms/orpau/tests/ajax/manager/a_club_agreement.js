define([
    'forms/base/ql'
  , 'forms/d_orpau'
  , 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
	var service = ajax_service_base("orpau?action=club.agreement");

	var catch_code= function(id_Manager,c)
	{
		switch (c.code)
		{
			case '1':
				return { ok: false, why: 'Указанный код подтверждения не соответствует высланному!' };
			case '2':
				return { ok: false, why: 'С момента отправки кода подтверждения прошло слишком много времени!' };
			default:
				if (id_Manager)
				{
					var Manager = ql.find_first_or_null(db.manager, function (m) { return m.id_Manager == id_Manager; });
					Manager.ClubAgreementText= 'да';
					Manager.ClubAgreementSignature= '1';
				}
				return { ok: true };
		}
	}

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		var res = null;
		switch (parsed_args.cmd)
		{
			case 'get-union-application-to-sign':
				var txt= "да, я вступаю, бла бла бла..";
				var line= txt;
				for (var i= 0; i<5; i++)
					line+= ' ' + txt;
				res= line;
				for (var i= 0; i<25; i++)
					res+= '\r\n' + line;
				break;
			case 'throw-code':
				res = { sms: {time:'01.01.2021 10:00:00',number:'79128556745'} };
				break;
			case 'get-signed-agreement':
				res = "что то там подписали про профсоюз..";
				break;
			case 'sign':
				res = catch_code(parsed_args.id_Manager,cx.data_args());
				break;
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	};

	return service;
});