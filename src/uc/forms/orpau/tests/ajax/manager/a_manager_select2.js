define([
	  'forms/base/ajax/ajax-select2'
	, 'forms/d_orpau'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service= ajax_select2('orpau?action=manager.select2');

	service.query = function (q, page, args)
	{
		var items = ql.select(function (r) { return {
			"id": r.m.id_Manager
			, "firstName": r.m.FirstName
			, "lastName": r.m.LastName
			, "middleName": r.m.MiddleName
			, "text": r.m.LastName + ' ' + r.m.FirstName + ' ' + r.m.MiddleName
			, "inn": r.m.INN
			, "sro": (!r.sro || null==r.sro) ? '' : r.sro.Name
		}})
		.from(db.manager, 'm')
		.left_join(db.sro,'sro',function (r) { return r.m.SRORegNum == r.sro.RegNum })
		.where(function (r) { return -1!=r.m.LastName.indexOf(q); })
		.exec();
		return { results: items };
	}

	return service;
});