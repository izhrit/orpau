define([
	'forms/d_orpau',
	'forms/base/ajax/ajax-CRUD',
	'forms/base/ql'
], function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('orpau?action=manager.crud');

	service.read = function (id_Manager)
	{
		var rows = ql.select(function (r) { return {
				id_Manager: r.m.id_Manager
				, FirstName: r.m.FirstName
				, LastName: r.m.LastName
				, MiddleName: r.m.MiddleName
				, RegNum: r.m.RegNum
				, INN: r.m.INN
				, CorrespondenceAddress: r.m.CorrespondenceAddress
				, Phone: r.m.Phone
				, EMail: r.m.EMail
				, Password: r.m.Password
				, ArbitrManagerID: r.m.ArbitrManagerID
				, SROName: (!r.sro || null==r.sro) ? '' : r.sro.Name
				, SRORegNum: (!r.sro || null==r.sro) ? null : r.sro.RegNum
				, Region: !r.region ? '' : r.region.Name
				, ИспользуетАСП: (r.m.AgreementSignature && null!=r.m.AgreementSignature)
				, СостоитВПрофсоюзе: (r.m.ClubAgreementSignature && null!=r.m.ClubAgreementSignature)
			}; })
			.from(db.manager, 'm')
			.left_join(db.sro,'sro',function (r) { return r.m.SRORegNum == r.sro.RegNum })
			.left_join(db.region,'region',function (r) { return r.m.id_Region == r.region.id_Region })
			.where(function (r) { return r.m.id_Manager == id_Manager; })
			.exec();
		var row = rows[0];
		console.log(JSON.stringify(row));
		return row;
	};

	service.update = function(id_Manager, data)
	{
		var Manager = ql.find_first_or_null(db.manager, function (m) { return m.id_Manager == id_Manager; });
		Manager.EMail = data.EMail;
		Manager.Phone = data.Phone;
		return true;
	}

	return service;
});
