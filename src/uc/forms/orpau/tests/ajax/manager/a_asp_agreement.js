define([
    'forms/base/ql'
  , 'forms/d_orpau'
  , 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
	var service = ajax_service_base("orpau?action=asp.agreement");

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		var res = null;
		switch (parsed_args.cmd)
		{
			case 'get-application-for-accession-to-sign':
				var txt= "да, я согласен, бла бла бла..";
				var line= txt;
				for (var i= 0; i<5; i++)
					line+= ' ' + txt;
				res= line;
				for (var i= 0; i<25; i++)
					res+= '\r\n' + line;
				break;
			case 'sign':
				var Manager = ql.find_first_or_null(db.manager, function (m) { return m.id_Manager == parsed_args.id_Manager; });
				Manager.AgreementText= 'да';
				Manager.AgreementSignature= '1';
				res = {ok:true};
				break;
			case 'verify-signature':
				res = {};
				break;
			case 'get-signed-application-for-accession':
				res = "что то там подписали..";
				break;
		}
		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	};

	return service;
});