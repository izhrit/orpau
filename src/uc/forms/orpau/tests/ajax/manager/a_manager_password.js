define([
    'forms/base/ql'
  , 'forms/d_orpau'
  , 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
  var service = ajax_service_base("orpau?action=manager.password");

  service.action= function (cx)
  {
    var parsed_args= cx.url_args();
    var res = false;
    var Manager = ql.find_first_or_null(db.manager, function (m) { return m.id_Manager == parsed_args.id_Manager; });
    if(Manager != null) {
      switch (parsed_args.cmd)
      {
          case 'change':
              Manager.EMail = parsed_args.email;
              Manager.Password = parsed_args.id_Manager;
              res = true;
              break;
          case 'block':
              Manager.EMail = parsed_args.email;
              Manager.Password = null;
              res = true;
              break;
      }
		  cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: res }) });
    }
  };

  return service;
});