define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/d_orpau'
	, 'forms/base/ql'
]
, function (ajax_service_base, db, ql)
{
	var service= ajax_service_base('orpau?action=manager.for-inn');

	service.action= function (cx)
	{
		var data_args= cx.data_args();
		var inns= data_args.inn.split(',');
		var inns_hash = {};
		for (var i = 0; i < inns.length; i++)
			inns_hash[inns[i]]= true;

		var managers= ql.select(function (r) { return {
			"id": r.m.id_Manager
			, "firstName": r.m.FirstName
			, "lastName": r.m.LastName
			, "middleName": r.m.MiddleName
			, "text": r.m.LastName + ' ' + r.m.FirstName + ' ' + r.m.MiddleName
			, "inn": r.m.INN
			, "sro": (!r.sro || null==r.sro) ? '' : r.sro.Name
		}})
		.from(db.manager, 'm')
		.left_join(db.sro,'sro',function (r) { return r.m.SRORegNum == r.sro.RegNum })
		.where(function (r) { return inns_hash[r.m.INN]; })
		.exec();

		cx.completeCallback(200, 'success', { text: JSON.stringify(managers) });
	}

	return service;
});