﻿define
(
	[
		'forms/base/ajax/ajax-collector'

		, 'forms/orpau/tests/ajax/debtor/a_debtor_jqgrid'
		, 'forms/orpau/tests/ajax/debtor/a_debtor_CRUD'

		, 'forms/orpau/tests/ajax/manager/a_asp_agreement'
		, 'forms/orpau/tests/ajax/manager/a_club_agreement'

		, 'forms/orpau/tests/ajax/manager/a_manager_jqgrid'
		, 'forms/orpau/tests/ajax/manager/a_manager_CRUD'
		, 'forms/orpau/tests/ajax/manager/a_manager_password'
		, 'forms/orpau/tests/ajax/manager/a_manager_authPassword'
		, 'forms/orpau/tests/ajax/manager/a_manager_auth'
		, 'forms/orpau/tests/ajax/manager/a_manager_select2'
		, 'forms/orpau/tests/ajax/manager/a_manager_for_inn'

		, 'forms/orpau/tests/ajax/events_news/a_news_jqgrid'
		, 'forms/orpau/tests/ajax/events_news/a_schedule_info'

		, 'forms/orpau/tests/ajax/confirmation/a_confirmation_jqgrid'
		, 'forms/orpau/tests/ajax/confirmation/a_confirmation_CRUD'
		, 'forms/orpau/tests/ajax/confirmation/a_confirmation_printable'
		, 'forms/orpau/tests/ajax/confirmation/a_confirmation_generate'

		, 'forms/orpau/tests/ajax/award/a_award_managers'
		, 'forms/orpau/tests/ajax/award/a_award_manager'
		, 'forms/orpau/tests/ajax/award/a_award_nominees'
		, 'forms/orpau/tests/ajax/award/a_award_vote'
		, 'forms/orpau/tests/ajax/award/a_award_votes'
		, 'forms/orpau/tests/ajax/award/a_award_votes_jqgrid'
		, 'forms/orpau/tests/ajax/award/a_award_polls'
		, 'forms/orpau/tests/ajax/award/a_award_poll'
		, 'forms/orpau/tests/ajax/award/a_award_login'

		, 'forms/orpau/tests/ajax/voting/a_log_jqgrid'
		, 'forms/orpau/tests/ajax/voting/a_questions_jqgrid'
		, 'forms/orpau/tests/ajax/voting/a_documents_jqgrid'
		, 'forms/orpau/tests/ajax/voting/a_question_crud'
		, 'forms/orpau/tests/ajax/voting/a_voting_sms'
		, 'forms/orpau/tests/ajax/voting/a_documents_main'
		
		, 'forms/orpau/tests/ajax/admin/a_admin_auth'

		, 'forms/orpau/tests/ajax/group/a_group_jqgrid'
		, 'forms/orpau/tests/ajax/group/a_group_CRUD'
		, 'forms/orpau/tests/ajax/group/a_group_select2'

		, 'forms/orpau/tests/ajax/admin/a_poll_jqgrid'
		, 'forms/orpau/tests/ajax/admin/a_poll_CRUD'
		, 'forms/orpau/tests/ajax/admin/a_poll_select2'
		, 'forms/orpau/tests/ajax/admin/a_poll_last_for_manager'

		, 'forms/orpau/tests/ajax/admin/a_sro_select2'
		, 'forms/orpau/tests/ajax/admin/a_region_select2'

		//, 'forms/orpau/tests/ajax/cryptoapi/a_client_cryptoapi'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);