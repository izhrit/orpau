define([
	'forms/d_orpau',
	'forms/base/ajax/ajax-CRUD',
	'forms/base/ql'
], function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('orpau?action=group.crud');

	var fill_row= function (group, row_group)
	{
		if (!row_group)
			row_group = {};
		row_group.Name= group.Name;
		row_group.Description= group.Description;
		return row_group;
	}

	service.create = function(group)
	{
		var new_row_group= ql.insert_with_next_id(db.manager_group, 'id_Manager_group', fill_row(group));
		var id_Manager_group= new_row_group.id_Manager_group;

		for (var i = 0; i < group.Состав_АУ.length; i++)
		{
			var au= group.Состав_АУ[i];
			db.manager_group_manager.push({id_Manager_group:id_Manager_group, id_Manager:au.id});
		}

		return id_Manager_group;
	};

	service.read = function (id_Group)
	{
		var rows = ql.select(function(r){return{
			Name: r.g.Name
			,Description: r.g.Description
			,Состав_АУ: (function(){
				return ql.select(function (r) { return {
					"id": r.m.id_Manager
					, "firstName": r.m.FirstName
					, "lastName": r.m.LastName
					, "middleName": r.m.MiddleName
					, "text": r.m.LastName + ' ' + r.m.LastName + ' ' + r.m.MiddleName
					, "inn": r.m.INN
					, "sro": (!r.sro || null==r.sro) ? '' : r.sro.Name
					, "region": !r.region ? '' : r.region.Name
				}})
				.from(db.manager, 'm')
				.left_join(db.sro,'sro',function (r) { return r.m.SRORegNum == r.sro.RegNum })
				.left_join(db.region,'region',function (r) { return r.m.id_Region == r.region.id_Region })
				.inner_join(db.manager_group_manager, 'mg', function(r){ return r.mg.id_Manager == r.m.id_Manager})
				.where(function(r){ return r.mg.id_Manager_group == id_Group })
				.exec();
			})()
		};})
		.from(db.manager_group, 'g')
		.where(function(r) { return r.g.id_Manager_group == id_Group; })
		.exec();

		return rows[0];
	};

	service.update = function(id_Group, group)
	{
		var row_group = ql.find_first_or_null(db.manager_group, function (g) { return id_Group==g.id_Manager_group;});
		fill_row(group, row_group);

		db.manager_group_manager= ql._delete(db.manager_group_manager,function (mgm) { return id_Group==mgm.id_Manager_group;})

		for (var i = 0; i < group.Состав_АУ.length; i++)
		{
			var au= group.Состав_АУ[i];
			db.manager_group_manager.push({id_Manager_group:id_Group, id_Manager:au.id});
		}

		return true;
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id = id_arr[i];
			db.manager_group_manager= ql._delete(db.manager_group_manager,function (mgm) { return id==mgm.id_Manager_group;})
			db.manager_group= ql._delete(db.manager_group, function (mg) { return id == mg.id_Manager_group; });
		}
	}

	return service;
});