define([
	  'forms/base/ajax/ajax-select2'
	, 'forms/d_orpau'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service= ajax_select2('orpau?action=group.select2');

	service.query = function (q, page, args)
	{
		var items = ql.select(function (r) {
			return {
				id:r.mg.id_Manager_group
				, text:r.mg.Name
			};
		})
		.from(db.manager_group,'mg')
		.where(function (r) { return -1!=r.mg.Name.indexOf(q); })
		.exec();
		return { results: items };
	}

	return service;
});