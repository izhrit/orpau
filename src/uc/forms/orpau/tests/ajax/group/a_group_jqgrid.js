define([
	'forms/base/ajax/ajax-jqGrid'
	, 'forms/d_orpau'
	, 'forms/base/ql'
], function (ajax_jqGrid, db, ql)
{
	var service = ajax_jqGrid('orpau?action=group.jqgrid');

	service.rows_all = function ()
	{
		var items = ql.select(function (r) {
			return {
				id_Group:r.g.id_Manager_group
				, Name:r.g.Name
			};
		})
		.from(db.manager_group,'g')
		.exec();
		return items;
	}

	return service;
});