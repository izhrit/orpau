define([
	'forms/d_orpau',
	'forms/base/ajax/ajax-CRUD',
	'forms/base/ql'
], function (db, ajax_CRUD, ql)
{
	var srevice = ajax_CRUD('orpau?action=debtor.crud');

	srevice.read = function (id_Debtor)
	{
		var rows = ql.select(function (rr) { return {
				id_Debtor: rr.d.id_Debtor
				, Name: rr.d.Name
				, OGRN: rr.d.OGRN
				, SNILS: rr.d.SNILS
				, INN: rr.d.INN
				, Bankruptid: rr.d.Bankruptid
				, casenumber: rr.d.casenumber
				, DateStart: rr.d.DateStart
				, DateFinish: rr.d.DateFinish
				, Procedure: rr.d.Procedure
				, Manager: ql.find_first_or_null(db.manager, function (m) { return m.ArbitrManagerID==rr.d.ArbitrManagerID; })
				, Confirmation: ql.find_first_or_null(db.confirmation, function (c) { return c.id_Debtor==id_Debtor; })
			}; })
			.from(db.debtor, 'd')
			.where(function (r){return r.d.id_Debtor == id_Debtor; })
			.exec();

		var row = rows[0];

		var sro= ql.find_first_or_null(db.sro, function (sro) { return sro.id_SRO==row.Manager.id_SRO; })
		row.Manager.SROName = null==sro ? null : sro.Name;

		return row;
	};

	return srevice;
});
