﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service= ajax_jqGrid('orpau?action=debtor.jqgrid');

	service.rows_all = function (url, args)
	{
		return ql.select(function(r) { return {
				"title": r.d.Name
				, "ogrn": r.d.OGRN
				, "inn": r.d.INN
				, "snils": r.d.SNILS
				, "id_Debtor": r.d.id_Debtor
			} })
			.from(db.debtor, 'd')
			.where(function (r) { return !args.ArbitrManagerID || r.d.ArbitrManagerID == args.ArbitrManagerID; })
			.exec();
	}

	return service.prepare_try_to_prepare_send_abort();
});