define([
	'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service = ajax_jqGrid('orpau?action=questions.jqgrid');

	service.rows_all = function (url, args)
	{
		var id_Manager = args.id_Manager;

		var rows= ql.select(function (r) {
			return {
				id_Question:r.q.id_Question
				, VoteExtra: r.v ? r.v.VoteExtra : {}
				, Time: r.v ? r.v.VoteTime : ''
				, Title: r.q.Title
				, QuestionExtra: r.q.Extra
			};
		})
		.from(db.question,'q')
		.left_join(db.poll,'p',function(r) { return r.p.id_Poll = r.q.id_Poll })
		.left_join(db.manager_group_manager, 'mgm',
			function(r) { 
				return r.mgm.id_Manager_group == r.p.id_Manager_group
			}
		)
		.left_join(db.sro,'sro',function (r) { return r.p.id_SRO == r.sro.id_SRO })
		.left_join(db.manager,'m',
			function(r) {
				return r.m.id_Region == r.p.id_Region ||
					(r.sro && r.m.SRORegNum == r.sro.RegNum) ||
					(r.mgm && r.m.id_Manager == r.mgm.id_Manager) ||
					(
						!r.p.id_Manager_group && !r.p.id_SRO && !r.p.id_Region
					)
			}
		)
		.left_join(db.vote,'v',
			function(r) { return r.q.id_Question == r.v.id_Question && r.v.id_Manager == r.m.id_Manager }
		)
		.where(function(r) { return r.m.id_Manager = id_Manager})
		.exec();

		var uniq = [];
		rows.forEach(function(item){
			var itemHas = true;
			uniq.forEach(function(uniqItem){
				if(item.id_Question == uniqItem.id_Question){
					itemHas = false;
				}
			});
			if(itemHas) uniq.push(item);
		})

		return uniq;
	}

	return service;
});