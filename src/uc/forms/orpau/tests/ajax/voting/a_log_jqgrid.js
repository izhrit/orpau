define([
	'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service = ajax_jqGrid('orpau?action=log.jqgrid');

	service.rows_all = function ()
	{
		return [
			{
				'id_Vote_log': 1,
				'time': '10.10.2019 00:00:00',
				'event': 'Уведомление о заседании на адрес'
			},
			{
				'id_Vote_log': 2,
				'time': '31.01.2020 00:00:00',
				'event': 'Подпись бюллетеня на адрес'
			}
		];
	}

	return service;
});