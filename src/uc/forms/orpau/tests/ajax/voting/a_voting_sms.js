define([
    'forms/base/ql'
  , 'forms/d_orpau'
  , 'forms/base/ajax/ajax-service-base'
  , 'forms/base/codec/datetime/h_codec.datetime'
  , 'forms/base/h_times'
]
, function (ql, db, ajax_service_base, h_codec_datetime, h_times)
{
  var service = ajax_service_base("orpau?action=vote.sms");

  service.action= function (cx)
  {
    var dt_codec = h_codec_datetime.ru_txt2dt();
    var время = dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()));

    var parsed_args= cx.url_args();

    switch (parsed_args.cmd)
    {
        case 'throw-code':
            cx.completeCallback(200, 'success', 
            { 
                text: JSON.stringify({ 
                    sms: {
                        time: 'test',
                        number: 'test'
                    },
                    email: {
                        address: 'test',
                        time: 'test'
                    },
                    test_code: '1234'
                })
            });
            return;
        case 'catch-code':
            var parsed_data = cx.data_args();
            if(parsed_data.code == '1234') {
            cx.completeCallback(200, 'success', { text: JSON.stringify({ok: true, Подписано: время, Документы: 'Документы', Журнал: 'Журнал'})});
            }
            cx.completeCallback(200, 'success', { text: JSON.stringify({ok: false, why: 'неверный код'}) });
            return;
        case 'sign':
            cx.completeCallback(200, 'success', { ok: 'true' });
            return;
    }
    var error_msg= 'unknown cmd=' + parsed_args.cmd + '!';
    console.log(error_msg);
    completeCallback(400, 'bad request', { text: error_msg });
  };

  return service;
});