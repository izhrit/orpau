define([
    'forms/base/ql'
  , 'forms/d_orpau'
  , 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
  var service = ajax_service_base("orpau?action=documents.main");

  service.GetDocument = function(args)
  {
        return {
            Subject: 'Тема ' + args.name,
            Body: 'Текст'
        };
  }

  service.action= function (cx)
  {
      var parsed_args= cx.url_args();

      switch (parsed_args.cmd)
      {
          case 'get':
                var data = this.GetDocument(parsed_args)
                cx.completeCallback(200, 'success', { text: JSON.stringify(data) });
                return;
          case 'download':
            cx.completeCallback(200, 'success', { text: 'file.txt' });
            return;
      }
      var error_msg= 'unknown cmd=' + parsed_args.cmd + '!';
      console.log(error_msg);
      completeCallback(400, 'bad request', { text: error_msg });
  };

  return service;
});