define([
	'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service = ajax_jqGrid('orpau?action=documents.jqgrid');

	service.rows_all = function ()
	{
		return [
			{
				"id_Vote_document": 1,
				"id_Vote": 1,
				"Vote_document_type": 'x',
				"Time": "10.10.2019 00:00:00",
				"Document": "x",
				"FileName": "x.txt",
				"Size": "1",
				"doc_md5": "9dd4e461268c8034f5c8564e155c67a6",
			  },
			  {
				"id_Vote_document": 1,
				"id_Vote": 1,
				"Vote_document_type": 'x',
				"Time": "10.10.2019 00:00:00",
				"Document": "x2",
				"FileName": "x2.txt",
				"Size": "12",
				"doc_md5": "9dd4e461268c8034f5c8564e155c67a6",
			  }
		];
	}

	return service;
});