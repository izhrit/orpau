define([
	'forms/d_orpau',
	'forms/base/ajax/ajax-CRUD',
	'forms/base/ql'
], function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('orpau?action=question.crud');

	service.read = function (id_Question, args)
	{
		var id_Manager = args.id_Manager;
		var rows= ql.select(function (r) {
			var status = 'Голосование';
			if(r.p.ExtraColumns.Состояние) status = r.p.ExtraColumns.Состояние;
			var dateFinishStr = new Date(r.p.DateFinish).getTime();
			if(new Date().getTime() > dateFinishStr) status = 'Время для голосования прошло';
			return {
				id_Question:r.q.id_Question
				, VoteExtra: r.v ? r.v.VoteExtra: null
				, Time: r.v ? r.v.VoteTime : null
				, Title: r.q.Title
				, QuestionExtra: r.q.Extra
				, Status: status
				, id_Vote: r.v ? r.v.id_Vote : null
				, DateFinish: r.p.DateFinish
			};
		})
		.from(db.question,'q')
		.left_join(db.poll,'p',function(r) { return r.p.id_Poll = r.q.id_Poll })
		.left_join(db.vote,'v',
			function(r) { return r.q.id_Question == r.v.id_Question && r.v.id_Manager == id_Manager }
		)
		.where(function (r) { return r.q.id_Question==id_Question; })
		.exec();
		return rows[0];
	};

	service.update = function(id, data)
	{
		console.log('update', id, data)
		return true;
	}

	return service;
});
