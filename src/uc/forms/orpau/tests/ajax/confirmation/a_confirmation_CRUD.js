define([
	  'forms/d_orpau'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/ql'
], function (db, ajax_CRUD, ql)
{
	var service = ajax_CRUD('orpau?action=confirmation.crud');

	service.read = function (id_Confirmation)
	{
		var rows = ql.select(function (rr) { return {
				id_Confirmation: rr.c.id_Confirmation,
				id_Manager: rr.c.id_Manager,
				id_Debtor: rr.c.id_Debtor,
				TimeOfCreation: rr.c.TimeOfCreation,
				Text: rr.c.Text,
				Number: rr.c.Number,
				Signature: rr.c.Signature,
				EfrsbMessageNumber: rr.c.EfrsbMessageNumber,
				JudicalActId: rr.c.JudicalActId
			}; })
			.from(db.confirmation, 'c')
			.where(function (r) { return r.c.id_Confirmation == id_Confirmation; })
			.exec();
		return rows[0];
	};

	service.create = function(carg)
	{
		var carg= JSON.parse(carg);
		var cexisted= ql.find_first_or_null(db.confirmation, 
			function (c) { return c.id_Debtor==carg.id_Debtor && c.id_Manager==carg.id_Manager; });
		if (null!=cexisted)
		{
			return null;
		}
		else
		{
			var cnew= ql.insert_with_next_id(db.confirmation, 'id_Confirmation', 
			{
				id_Manager: carg.id_Manager
				, id_Debtor: carg.id_Debtor
				, Text: carg.Text
				, TimeOfCreation: new Date().toLocaleDateString()
				, Number: null
				, Signature: null
				, EfrsbMessageNumber: null
				, JudicalActId: null
			});
			return cnew.id_Confirmation;
		}
	}

	return service;
});
