﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service = ajax_jqGrid('orpau?action=confirmation.jqgrid');

	service.rows_all = function (url, args)
	{
		return ql.select(function(rr) { return {
			"number":rr.c.id_Confirmation
			, "date":rr.c.TimeOfCreation
			, "manager": rr.m.LastName + " " + rr.m.FirstName.charAt(0) + ". " + rr.m.MiddleName.charAt(0) + '.'
			, "debtor": rr.d.Name
			, "ogrn": rr.d.OGRN
			, "inn": rr.d.INN
			, "snils": rr.d.SNILS
			, "id_Confirmation": rr.c.id_Confirmation
		} })
		.from(db.confirmation, 'c')
		.inner_join(db.manager, 'm', function(r){ return r.m.id_Manager == r.c.id_Manager})
		.inner_join(db.debtor, 'd', function(r){ return r.d.id_Debtor == r.c.id_Debtor})
		.where(function (r) { return !args.id_Manager || r.c.id_Manager == args.id_Manager; })
		.exec();
	}

	return service;
});