define([
	  'forms/base/ql'
	, 'forms/d_orpau'
	, 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
	var service = ajax_service_base("orpau?action=confirmation.generate");

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		switch (parsed_args.cmd)
		{
			case 'get':
				var row = this.generate(parsed_args.id_Debtor, parsed_args.id_Manager);
				res = {Text: row}
				cx.completeCallback(200, 'success', res);
				return;
		}
		cx.completeCallback(400, 'bad request', { text: 'unknown cmd=' + parsed_args.cmd + '!' });
	}

	service.generate = function(id_Debtor, id_Manager)
	{
		var manager = ql.find_first_or_null(db.manager,function(m){return m.id_Manager == id_Manager; });
		var debtor = ql.find_first_or_null(db.debtor,function(d){return d.id_Debtor == id_Debtor; });

		var managerFullName= manager.lastName + ' ' + manager.firstName + ' ' + manager.middleName;

		var result = 'Справка о полномочиях Арбитражного управляющего № ' + manager.efrsbNumber +
			"\n\nПодготовлена в отношении:" +
			'\n-*' + managerFullName + '*, действующий с атрибутами:' +
			'\n\tИНН '+manager.INN +
			'\n\tПочтовый адрес' + manager.PostAdress +
			'\n-*'+debtor.Name+'*, действующий с атрибутами:' +
			'\n\tИНН ' + debtor.INN +
			'\n\tОГРН ' + debtor.OGRN +
			'\n\nВышеозначенный АУ действует в рамках ЗОБ как арбитражный управляющий в процедуре "Н" над Должником (далее - Процедура).' +
			'\nОбщероссийский профсоюз арбитражных управляющих (далее - ОРПАУ) подтверждает полномочия АУ в Процедуре в соответствии с Законодательством РФ и пп. 11, 12, 13 ст. 4 устава ОРПАУ.' +
			'\n\nОРПАУ выражает готовность защищать интересы АУ в рамках Процедуры.' +
			'\n\nОРПАУ при необходимости готов сообщить или подтвердить контактные данные АУ.';

		return result;
	}

	return service;
});