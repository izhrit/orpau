define([
	  'forms/base/ql'
	, 'forms/d_orpau'
	, 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
	var service = ajax_service_base("orpau?action=confirmation.printable");

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		var confirmation= ql.find_first_or_default(db.confirmation,function(c){ return c.id_Confirmation==parsed_args.id_Confirmation});
		res = { 
		text:"<div>"+
		"<pre>"+confirmation.Text+"</pre>"+
		'<div style="border:3px solid #002197; border-radius: 30px; color: #002197; width: 350px; height: 150px;">'+
			'<b><img src="http://localhost/orpau-srv/img/rus.jpg" width="75px", height="75px" style="margin-top: 5px;margin-left: 5px; border-radius: 30px;">'+
				'<div style="text-align: center; margin-left: 10px; font-size: 12px; line-height: 1.5; display: inline;">'+
					'<div style="margin-top: -70px; margin-left: 70px;">ДОКУМЕНТ ПОДПИСАН<br>УСИЛЕННОЙ КВАЛИФИЦИРОВАННОЙ<br>ЭЛЕКТРОННОЙ ПОДПИСЬЮ</div>'+
				'</div>'+
				'<div style="font-size: 12px; margin-left: 15px; margin-top: 25px;">'+
					'Сертификат: 982476528347569879326504234912'+
				'</div>'+
				'<div style="font-size: 12px; margin-left: 15px; margin-top: 5px;">'+
					'Владелец: МИ ФИС России по ПОД'+
				'</div>'+
				'<div style="font-size: 12px; margin-left: 15px; margin-top: 5px;">'+
					'Действителен: с 01.01.2020 по 01.01.2040'+
				'</div>'+
			'<b>'+
		'</div>'+
		'<div>'
		};
		cx.completeCallback(200, 'success', res);
	}

	return service;
});