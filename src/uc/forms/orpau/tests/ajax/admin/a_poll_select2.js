define([
	  'forms/base/ajax/ajax-select2'
	, 'forms/d_orpau'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service= ajax_select2('orpau?action=poll.select2');

	service.query = function (q, page, args)
	{
		var items = ql.select(function (r) {
			return {
				id:r.p.id_Poll
				, text:r.p.Name
			};
		})
		.from(db.poll,'p')
		.exec();
		return { results: items };
	}

	return service;
});