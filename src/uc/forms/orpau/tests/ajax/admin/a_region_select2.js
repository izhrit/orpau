define([
	  'forms/base/ajax/ajax-select2'
	, 'forms/d_orpau'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service= ajax_select2('orpau?action=region.select2');

	service.query = function (q, page, args)
	{
		var items = ql.select(function (r) {
			return {
				id:r.rg.id_Region
				, text:r.rg.Name
			};
		})
		.from(db.region,'rg')
		.exec();
		return { results: items };
	}

	return service;
});