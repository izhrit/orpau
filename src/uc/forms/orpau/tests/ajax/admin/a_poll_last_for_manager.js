define([
	'forms/d_orpau'
	,'forms/base/ajax/ajax-service-base'
	,'forms/base/ql'
], function (db, ajax_service_base, ql)
{
	var service = ajax_service_base('orpau?action=poll.last-for-manager');

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		var id_Manager= parsed_args.id_Manager;
		var rows = ql.select(function (r) { return { 
			id: r.p.id_Poll
			, text: r.p.Name
		};})
		.from(db.poll, 'p')
		.left_join(db.manager_group_manager, 'mgm',function (r){return r.mgm.id_Manager_group == r.p.id_Manager_group;})
		.left_join(db.sro, 'sro', function (r) { return r.p.id_SRO == r.sro.id_SRO })
		.left_join(db.manager, 'm',
			function (r)
			{
				return r.m.id_Region == r.p.id_Region ||
					(r.sro && r.m.SRORegNum == r.sro.RegNum) ||
					(r.mgm && r.m.id_Manager == r.mgm.id_Manager) ||
					(
						!r.p.id_Manager_group && !r.p.id_SRO && !r.p.id_Region
					)
			}
		)
		.where(function (r) { return r.m.id_Manager == id_Manager })
		.order(function (a, b) { return Number(a.id_Poll) - Number(b.id_Poll); })
		.limit(1)
		.exec();
		var poll = rows[0];
		cx.completeCallback(200, 'success', { text: JSON.stringify(poll, null, '\t') });
	}

	return service;
});