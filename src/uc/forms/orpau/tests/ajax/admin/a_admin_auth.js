define([
	  'forms/base/ql'
	, 'forms/d_orpau'
	, 'forms/base/ajax/ajax-service-base'
]
, function (ql, db, ajax_service_base)
{
	var service = ajax_service_base("orpau?action=admin.auth");

	service.Login = function(args)
	{
		if ('v' != args.login || '1' != args.password)
		{
			console.log(args);
			console.log('args.login=' + args.login);
			console.log('args.password=' + args.password);
			console.log('bad auth!');
			return null;
		}
		else
		{
			console.log('ok auth!');
			return { id_Admin:1, login: args.login };
		}
	}

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		switch (parsed_args.cmd)
		{
			case 'login':
				var auth_info= this.Login(cx.data_args());
				cx.completeCallback(200, 'success', { text: JSON.stringify(auth_info) });
				return;
		}
		var error_msg= 'unknown cmd=' + parsed_args.cmd + '!';
		console.log(error_msg);
		completeCallback(400, 'bad request', { text: error_msg });
	};

	return service;
});