define([
	'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
	, 'forms/d_orpau'
], function (ajax_jqGrid, ql, db)
{
	var service = ajax_jqGrid('orpau?action=poll.jqgrid');

	service.rows_all = function ()
	{
		return ql.select(function (r) { 
			return {
				id_Poll: r.p.id_Poll
				,Name: r.p.Name
			};
		})
		.from(db.poll,'p')
		.exec();
	}

	return service;
});