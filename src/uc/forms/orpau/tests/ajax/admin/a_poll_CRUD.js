define([
	'forms/d_orpau'
	,'forms/base/ajax/ajax-CRUD'
	,'forms/base/ql'
	,'forms/base/codec/datetime/h_codec.datetime'
], function (db, ajax_CRUD, ql, h_codec_datetime)
{
	var service = ajax_CRUD('orpau?action=poll.crud');

	var fill_row= function (poll, row_poll)
	{
		if (!row_poll)
			row_poll = {};
		var txt_date= poll.Завершение.Дата + ' ' + poll.Завершение.Время;
		row_poll.Name= poll.Название;
		row_poll.Status= 'n';
		row_poll.DateFinish= h_codec_datetime.ru_legal_txt2txt_mysql().Encode(txt_date);
		row_poll.ExtraColumns= { Вопросы: poll.Вопросы };
		return row_poll;
	}

	service.create = function(poll)
	{
		var new_row_poll= ql.insert_with_next_id(db.poll, 'id_Poll', fill_row(poll));
		return new_row_poll.id_Poll;
	}

	service.read = function (id_Poll, args)
	{
		var rows = ql.select(function (r)
		{
			var txt_date = h_codec_datetime.ru_legal_txt2txt_mysql().Decode(r.p.DateFinish);
			var txt_date_parts = txt_date.split(' ');
			return {
				id: r.p.id_Poll
				, Название: r.p.Name
				, Завершение: {
					Дата: txt_date_parts[0]
					, Время: txt_date_parts[1]
				}
				, Вопросы: r.p.ExtraColumns.Вопросы
				, Состояние: 'n'==r.p.Status ? 'Создано' : 'Голосование'
			};
		})
		.from(db.poll, 'p')
		.where(function (r) { return r.p.id_Poll == id_Poll; })
		.exec();
		return rows[0];
	}

	service.update = function(id_Poll, poll)
	{
		var row_poll = ql.find_first_or_null(db.poll, function (p) { return id_Poll==p.id_Poll;});
		fill_row(poll, row_poll);
	}

	service._delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id = id_arr[i];
			db.poll = ql._delete(db.poll, function (p) { return id == p.id_Poll; });
		}
	}

	return service;
});