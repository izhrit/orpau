﻿define([
	  'forms/d_orpau'
	, 'forms/base/ql'
	, 'forms/base/ajax/ajax-service-base'
],
function (db, ql, ajax_service_base)
{
	var service = ajax_service_base('orpau?action=award.manager');

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		var manager = ql.select(function (r) { return {
			inn: r.manager.INN
			, VoteTime: r.manager.VoteTime
			, Signed: (null != r.manager.BulletinSignature)
		}; })
		.from(db.manager, 'm')
		.where(function (r) { return r.m.INN == parsed_args.inn; })
		.exec();
		cx.completeCallback(200, 'success', { text: JSON.stringify(manager, null, '\t') });
	}

	return service;
});