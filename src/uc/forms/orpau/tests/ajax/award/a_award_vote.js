﻿define(['forms/base/ajax/ajax-service-base'],
function (ajax_service_base)
{
	var service = ajax_service_base('orpau?action=award.vote');

	service.action= function (cx)
	{
		cx.completeCallback(200, 'success', { text: JSON.stringify({}, null, '\t') });
	}

	return service;
});