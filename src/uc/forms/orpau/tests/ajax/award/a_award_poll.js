define([
	'forms/d_orpau'
	, 'forms/base/ql'
	, 'forms/base/ajax/ajax-service-base'
],
function (db, ql, ajax_service_base)
{
	var service = ajax_service_base('orpau?action=award.poll');

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();
		var rows= ql.select(function(r) { return {
			id_Poll:r.p.id_Poll
			, Name:r.p.Name
			, ExtraColumns:r.p.ExtraColumns
		} })
		.from(db.poll, 'p')
		.where(function(r){return parseInt(parsed_args.id_Poll)==r.p.id_Poll})
		.exec();
		var poll = rows[0];
		cx.completeCallback(200, 'success', { text: JSON.stringify(poll, null, '\t') });
	}

	return service;
});