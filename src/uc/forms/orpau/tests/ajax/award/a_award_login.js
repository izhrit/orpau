define([
	  'forms/base/ajax/ajax-service-base'
	, 'forms/base/codec/url/codec.url.url-args'
],
function (ajax_service_base, codec_url_urlargs)
{
	var service = ajax_service_base('orpau?action=award.login');

	service.action= function (cx)
	{
		var args = cx.data_args();
		var password = args.password;
		cx.completeCallback(200, 'success', { text: JSON.stringify({ ok: (1==password) }) });
	}

	return service;
});