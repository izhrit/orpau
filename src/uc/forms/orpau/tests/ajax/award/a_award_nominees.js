﻿define([
	'forms/d_orpau'
	, 'forms/base/ql'
	, 'forms/base/ajax/ajax-service-base'
],
function (db, ql, ajax_service_base)
{
	var service = ajax_service_base('orpau?action=award.nominees');

	service.action= function (cx)
	{
		var rows = ql.select(function (r) { return { ExtraColumns: r.p.ExtraColumns }; })
			.from(db.poll, "p")
			.where(function(r){ return r.p.id_Poll==1})
			.exec();
		var nominees = rows[0].ExtraColumns;
		cx.completeCallback(200, 'success', { text: JSON.stringify(nominees, null, '\t') });
	}

	return service;
});