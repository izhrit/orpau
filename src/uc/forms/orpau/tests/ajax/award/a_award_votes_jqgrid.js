﻿define([
	  'forms/base/ajax/ajax-jqGrid'
], function (ajax_jqGrid)
{
	var transport = ajax_jqGrid('orpau?action=award.jqgrid');

	transport.rows_all = function ()
	{
		var result = 
		[
			{
				id_Nominee:1
				, Name:"Иванов Иван Иванович"
				, votes_count:100
			},
			{
				id_Nominee:2
				, Name:"Серёгин Серёга Сергеевич"
				, votes_count:67
			}
		]
		return result;
	}

	return transport;
});