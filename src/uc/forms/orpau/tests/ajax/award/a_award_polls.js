define([
	  'forms/base/ql'
	, 'forms/d_orpau'
	, 'forms/base/ajax/ajax-service-base'
],
function (ql, db, ajax_service_base)
{
	var service = ajax_service_base('orpau?action=award.polls');

	service.action= function (cx)
	{
		var polls = ql.select(function(r){ return { id_Poll:r.p.id_Poll, Name:r.p.Name}; })
		.from(db.poll, "p")
		.exec();
		cx.completeCallback(200, 'success', { text: JSON.stringify(polls, null, '\t') });
	}

	return service;
});