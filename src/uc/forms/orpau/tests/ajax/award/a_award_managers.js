﻿define
([
	  'forms/base/ajax/ajax-select2'
	, 'forms/d_orpau'
	, 'forms/base/ql'
]
, function (ajax_select2, db, ql)
{
	var service = ajax_select2('orpau?action=award.managers');

	service.query = function ()
	{
		var maxSize = 6;
		var items = ql.select(function (r) { return {
			id:r.m.id_Manager
			, text:r.m.LastName+' '+r.m.FirstName+' '+r.m.MiddleName
			, data:{
				inn: r.m.INN
				, Имя: r.m.FirstName
				, Фамилия: r.m.LastName
				, Отчество: r.m.MiddleName
				, efrsbNumber: r.m.RegNum
				, СРО: (function () { var sro= ql.find_first_or_null(db.sro, function (sro) { return r.m.SRORegNum==sro.RegNum; }); return null==sro ? '' : sro.Name; })()
			}
		}; })
		.from(db.manager, 'm')
		.exec();

		var more = items.length == maxSize;
		return { results: items, pagination: { more: more } };
	}

	return service;
});