﻿define(function ()
{
	var events = [
		{
			messageType: 'Meeting',
			options: {
				BankruptFirm_v2: 'ООО "СТРОИТЕЛЬ"',
				MeetingDateBegin: '2020-09-03',
				MeetingForm: 'Заочная (электронная)',
				MeetingTimeBegin: '18:00:00.0000000+03:00',
				efrsb_id: '5341037'
			},
			title: 'Собрание кредиторов',
			start: '2020-09-03 18:00:00',
			url: 'https://bankrot.fedresurs.ru/MessageWindow.aspx?ID=3994D6E9D082CC2A6EE4B00C317FF5E3'
		},
		{
			messageType: 'CourtDecision',
			options: {
				BankruptFirm_v2: 'ООО "СТРОИТЕЛЬ"',
				MeetingDateBegin: '2020-09-06',
				MeetingForm: 'Очная',
				MeetingTimeBegin: '00:00:00.0000000+03:00',
				efrsb_id: '5341033'
			},
			title: 'Судебное заседание',
			start: '2020-09-06 00:00:00',
			url: 'https://bankrot.fedresurs.ru/MessageWindow.aspx?ID=3994D6E9D082CC2A6EE4B00C317FF5E3'
		}
	];

	window.cpw_test_datamart_schedule_AddEvent = function (testEvent) { events.push(testEvent); }

	var url_prefix = 'orpau?action=schedule.events';

	var transport = { options: { url_prefix: url_prefix } };

	transport.prepare_try_to_prepare_send_abort= function()
	{
		var self= this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback) {
						var txt_res = JSON.stringify(events);
						completeCallback(200, 'success', { result: txt_res });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});