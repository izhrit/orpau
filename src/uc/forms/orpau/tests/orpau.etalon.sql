set names utf8;

select 'insert into sro' as '';
insert into `sro` set `id_SRO`= 1, `Name`= 'ААУ \'Гарантия\'', `RegNum`= '100500', `Revision`= -2, `INN`= '7491772104';
insert into `sro` set `id_SRO`= 2, `Name`= 'ААУ \'Инициатива\'', `RegNum`= '100501', `Revision`= -1, `INN`= '5426643024';

select 'insert into region' as '';
insert into `region` set `id_Region`= 1, `Name`= 'Удмуртия';
insert into `region` set `id_Region`= 2, `Name`= 'Башкирия';

select 'insert into manager_group' as '';
insert into `manager_group` set `id_Manager_group`= 1, `Name`= 'Синие', `Description`= 'Описание синих';
insert into `manager_group` set `id_Manager_group`= 2, `Name`= 'Зелёные', `Description`= 'Описание зелёных';

select 'insert into manager' as '';
insert into `manager` set `id_Manager`= 1, `ArbitrManagerID`= '3', `SRORegNum`= '100500', `Revision`= -3, `EMail`= 'IvanovII@someMail.com', `Password`= 'Password', `LastName`= 'Иванов', `FirstName`= 'Иван', `MiddleName`= 'Иванович', `RegNum`= '123', `INN`= '364484332544', `Phone`= '89889889988', `CorrespondenceAddress`= 'ул. Вадима-Сивкова 172', `id_Region`= 1, `AgreementText`= 'да', `AgreementSignature`= '1';
insert into `manager` set `id_Manager`= 2, `ArbitrManagerID`= '4321', `SRORegNum`= '100501', `Revision`= -2, `EMail`= 'Sem1972@someMail.com', `Password`= 'Password', `LastName`= 'Семёнов', `FirstName`= 'Семён', `MiddleName`= 'Семёнович', `RegNum`= '987654321', `INN`= '987654321', `Phone`= '89876543231', `CorrespondenceAddress`= 'ул. Кирова 131', `id_Region`= 2;
insert into `manager` set `id_Manager`= 3, `ArbitrManagerID`= '4322', `Revision`= -1, `EMail`= 'Sem1972@someMail.com', `Password`= 'Password', `LastName`= 'Афанасьев', `FirstName`= 'Григорий', `MiddleName`= 'Григорьевич', `RegNum`= '987654321', `INN`= '111111111', `Phone`= '89876543231', `CorrespondenceAddress`= 'ул. Кирова 131', `AgreementText`= 'да', `AgreementSignature`= '1', `ClubAgreementText`= 'да';

select 'insert into manager_group_manager' as '';
insert into `manager_group_manager` set `id_Manager_group`= 1, `id_Manager`= 1;
insert into `manager_group_manager` set `id_Manager_group`= 1, `id_Manager`= 2;
insert into `manager_group_manager` set `id_Manager_group`= 2, `id_Manager`= 3;

select 'insert into debtor' as '';
insert into `debtor` set `id_Debtor`= 1, `Bankruptid`= '42', `ArbitrManagerID`= '4321', `Name`= 'Гальперов Василий Васильевич', `INN`= '465422652901', `OGRN`= '311275365969330', `SNILS`= '76427634429';
insert into `debtor` set `id_Debtor`= 2, `Bankruptid`= '32', `ArbitrManagerID`= '3', `Name`= 'Долгов Афанасий Петрович', `INN`= '978675', `OGRN`= '978675', `SNILS`= '978675';
insert into `debtor` set `id_Debtor`= 3, `Bankruptid`= '33', `ArbitrManagerID`= '4322', `Name`= 'Ковалёв Михаил Петрович', `INN`= '978675', `OGRN`= '978675', `SNILS`= '978675';

select 'insert into confirmation' as '';
insert into `confirmation` set `id_Confirmation`= 1, `id_Manager`= 1, `id_Debtor`= 2, `Number`= '9999999999', `TimeOfCreation`= '26.01.2019', `Text`= 'Справка о полномочиях Арбитражного управляющего № 3\n\nПодготовлена 26.01.2019 в отношении лиц:\n- *Иванов Иван Иванович* (далее - АУ), действующий с атрибутами:\n      ИНН 123456789,\n      почтовый ул. Кирова 131\n      сертификат ЭП №3458345876345876 выдан УЦ Инфотраст\n- *Долгов Афанасий Петрович* (далее - Должник) с атрибутами:\n      ИНН 97867564534231\n      ОГРН 97867564534231\n\nВышеозначенный АУ действует в рамках ЗОБ как арбитражный управляющий в процедуре Наблюдения над Должником (далее - Процедура).\nОбщероссийский профсоюз арбитражных управляющих (далее - ОРПАУ) подтверждает полномочия АУ в Процедуре в соответствии с Законодательством РФ и пп. 11, 12, 13 ст. 4 устава ОРПАУ.\n\nОРПАУ выражает готовность защищать интересы АУ в рамках Процедуры.\n\nОРПАУ при необходимости готов сообщить или подтвердить контактные данные АУ.\n\nСправка подготовлена на основании следующих данных:\n1) АУ принят в СРО “Лихие АУ” 10/09/2015, сведений о его исключении или дисквалификации не обнаружено\n2) На сайте арбитражного суда опубликовано определение о назначении АУ от 01/01/2011, определений о его отстранении или завершении процедуре не обнаружено\n3) На сайте единого федерального реестра сведений о банкротстве опубликовано сообщение №35345 в отношении должника за подписью АУ, сообщений от других АУ после этой даты не обнаружено', `Signature`= 'ЭП №3458345876345876 выдан УЦ Инфотраст', `EfrsbMessageNumber`= '35345', `JudicalActId`= '23764';
insert into `confirmation` set `id_Confirmation`= 2, `id_Manager`= 2, `id_Debtor`= 1, `Number`= '2057239542', `TimeOfCreation`= '26.03.2020', `Text`= 'Справка о полномочиях Арбитражного управляющего № 2057239542\n\nПодготовлена 26.01.2019 в отношении лиц:\n- *Семёнов Семён Семёнович* (далее - АУ), действующий с атрибутами:\n      ИНН 987654321, \n      почтовый ул. Вадима-Сивкова 172\n      сертификат ЭП №2892470238274 выдан УЦ Инфотраст\n- *Гальперов Василий Васильевич* (далее - Должник) с атрибутами:\n      ИНН 13243546576879\n      ОГРН 13243546576879\n\nВышеозначенный АУ действует в рамках ЗОБ как арбитражный управляющий в процедуре Наблюдения над Должником (далее - Процедура).\nОбщероссийский профсоюз арбитражных управляющих (далее - ОРПАУ) подтверждает полномочия АУ в Процедуре в соответствии с Законодательством РФ и пп. 11, 12, 13 ст. 4 устава ОРПАУ.\n\nОРПАУ выражает готовность защищать интересы АУ в рамках Процедуры.\n\nОРПАУ при необходимости готов сообщить или подтвердить контактные данные АУ.\n\nСправка подготовлена на основании следующих данных:\n1) АУ принят в СРО “Лихие АУ” 10/09/2015, сведений о его исключении или дисквалификации не обнаружено\n2) На сайте арбитражного суда опубликовано определение о назначении АУ от 01/01/2011, определений о его отстранении или завершении процедуре не обнаружено\n3) На сайте единого федерального реестра сведений о банкротстве опубликовано сообщение №26822 в отношении должника за подписью АУ, сообщений от других АУ после этой даты не обнаружено', `Signature`= 'ЭП №2892470238274 выдан УЦ Инфотраст', `EfrsbMessageNumber`= '26822', `JudicalActId`= '7293857';

select 'insert into vote' as '';

select 'insert into poll' as '';
insert into `poll` set `id_Poll`= 1, `Name`= 'Арбитражный управляющий года', `DateFinish`= '2021-05-01T10:00:01', `Status`= 'n', `ExtraColumns`= compress('{\n  \"id_Poll\": \"1\",\n  \"Название\": \"Арбитражный управляющий года\",\n  \"Вопросы\": [\n    {\n      \"В_повестке\": \"Выбор арбитражного управляющего года\",\n      \"На_голосование\": {\n        \"Формулировка\": \"Выбрать арбитражного управляющего года\",\n        \"Форма_бюллетеня\": \"f2\",\n        \"Варианты\": [\n          \"Иванов И.И.\",\n          \"Петров В.В.\",\n          \"Сидоров А.А.\"\n        ]\n      }\n    }\n  ],\n  \"Завершение\": {\n    \"Дата\": \"12.12.2021\",\n    \"Время\": \"00:00\"\n  },\n  \"Голосуют\": \"все\",\n  \"Состояние\": \"Голосование\"\n}');
insert into `poll` set `id_Poll`= 2, `Name`= 'СРО Года', `DateFinish`= '2021-05-01T10:00:01', `Status`= 'n', `ExtraColumns`= compress('{\n  \"id_Poll\": \"2\",\n  \"Название\": \"СРО Года\",\n  \"Вопросы\": [\n    {\n      \"В_повестке\": \"Выбор СРО Года\",\n      \"На_голосование\": {\n        \"Формулировка\": \"Выбрать СРО Года\",\n        \"Форма_бюллетеня\": \"f2\",\n        \"Варианты\": [\n          \"ААУ \\"Гарантия\\"\",\n          \"ААУ \\"Инициатива\\"\"\n        ]\n      }\n    }\n  ],\n  \"Завершение\": {\n    \"Дата\": \"12.12.2021\",\n    \"Время\": \"00:00\"\n  },\n  \"Голосуют\": \"все\",\n  \"Состояние\": \"Голосование\"\n}');
insert into `poll` set `id_Poll`= 3, `id_Region`= 1, `Name`= 'Выборы канидатов от Удмуртии', `DateFinish`= '2021-05-01T10:00:01', `Status`= 'n', `ExtraColumns`= compress('{\n  \"id_Poll\": \"3\",\n  \"Название\": \"СРО Года\",\n  \"Вопросы\": [\n    {\n      \"В_повестке\": \"Выбор СРО Года\",\n      \"На_голосование\": {\n        \"Формулировка\": \"Выбрать СРО Года\",\n        \"Форма_бюллетеня\": \"f2\",\n        \"Варианты\": [\n          \"ААУ \\"Гарантия\\"\",\n          \"ААУ \\"Инициатива\\"\"\n        ]\n      }\n    }\n  ],\n  \"Завершение\": {\n    \"Дата\": \"12.12.2021\",\n    \"Время\": \"00:00\"\n  },\n  \"Голосуют\": \"регион\",\n  \"Из\": {\n    \"региона\": {\n      \"id\": \"1\",\n      \"text\": \"Удмуртия\"\n    }\n  },\n  \"Состояние\": \"Голосование\"\n}');

select 'insert into question' as '';
insert into `question` set `id_Question`= 1, `id_Poll`= 1, `Title`= 'Выбор арбитражного управляющего года', `Extra`= compress('{\n  \"Формулировка\": \"Выбрать арбитражного управляющего года\",\n  \"Форма_бюллетеня\": \"f2\",\n  \"Варианты\": [\n    \"Иванов И.И.\",\n    \"Петров В.В.\",\n    \"Сидоров А.А.\"\n  ]\n}');
insert into `question` set `id_Question`= 2, `id_Poll`= 2, `Title`= 'Выбор СРО Года', `Extra`= compress('{\n  \"Формулировка\": \"Выбрать СРО Года\",\n  \"Форма_бюллетеня\": \"f2\",\n  \"Варианты\": [\n    \"ААУ \\"Гарантия\\"\",\n    \"ААУ \\"Инициатива\\"\"\n  ]\n}');

select 'insert into email_message' as '';
insert into `email_message` set `id_Email_Message`= 1, `EmailType`= 'a', `TimeDispatch`= '2019-10-15T10:01', `RecipientEmail`= 'a@a.a', `RecipientType`= 'd', `RecipientId`= 1, `Details`= compress('{\n  \"Subject\": \"subject1\",\n  \"Receiver\": \"Иванов Иван Иванович\",\n  \"Body\": \"body1\"\n}');
insert into `email_message` set `id_Email_Message`= 2, `EmailType`= 'a', `TimeDispatch`= '2019-10-16T10:01', `RecipientEmail`= 'b@b.b', `RecipientType`= 'd', `RecipientId`= 2, `Details`= compress('{\n  \"Subject\": \"subject2\",\n  \"Receiver\": \"Пётр Петрович Петров\",\n  \"Body\": \"body2\"\n}');
insert into `email_message` set `id_Email_Message`= 3, `EmailType`= 'a', `TimeDispatch`= '2019-10-17T10:01', `RecipientEmail`= 'c@c.c', `RecipientType`= 'd', `RecipientId`= 3, `Details`= compress('{\n  \"Subject\": \"subject3\",\n  \"Receiver\": \"Сидоров С С\",\n  \"Body\": \"body3\"\n}');

select 'insert into email_attachment' as '';
insert into `email_attachment` set `id_Email_Attachment`= 1, `id_Email_Message`= 2, `FileName`= 'a.txt', `Content`= 'content1';
insert into `email_attachment` set `id_Email_Attachment`= 2, `id_Email_Message`= 3, `FileName`= 'b.txt', `Content`= 'content2';
insert into `email_attachment` set `id_Email_Attachment`= 3, `id_Email_Message`= 3, `FileName`= 'c.txt', `Content`= 'content3';

select 'insert into email_sender' as '';
insert into `email_sender` set `id_Email_Sender`= 1, `SenderServer`= 'server1', `SenderUser`= 'user1';
insert into `email_sender` set `id_Email_Sender`= 2, `SenderServer`= 'server2', `SenderUser`= 'user2';

