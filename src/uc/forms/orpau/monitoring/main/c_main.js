define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/monitoring/main/e_main.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_grid_url = base_url + "?action=monitoring.jqgrid";
		var base_url_select2 = base_url + '?action=poll.select2';

		var options = {
			field_spec:
				{
					Голосования: { ajax: { url: base_url_select2 , dataType: 'json' }, allowClear: true, placeholder: 'Голосование не выбрано', width: '100%' }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			this.RenderGrid();

			$(sel + ' [data-fc-selector="Голосования"]').on('change', function (e) { self.OnSelect2Changed(e); });

			$(sel + ' .refresh-btn').click(function(){
				$(sel + ' .grid').trigger('reloadGrid');
			});
		}

		controller.OnSelect2Changed = function (e)
		{
			var sel= this.fastening.selector;
			$(sel + ' input#gs_id_Poll').val(e.val).trigger('keydown');
		}

		var formatter =   {
			sqlDate: function(sqlDate) {
				var dateAndTimeSplit = sqlDate.split(' ');
				var dateSplit = dateAndTimeSplit[0].split('-');
				var timeSplit = dateAndTimeSplit[1].split(':')

				return dateSplit[2] + '.' + dateSplit[1] + '.' + dateSplit[0] + ' ' 
				+ timeSplit[0] + ':' + timeSplit[1];
			}
			,date: function(cellvalue, options, rowObject) {
				return cellvalue && cellvalue.length > 0? '<span class="q-time">'+formatter.sqlDate(cellvalue)+'</span>': ''
			}
			,answer: function(voteExtra, options, rowObject) {
				if(typeof voteExtra == 'string') {
					voteExtra = JSON.parse(voteExtra);
				}
				return voteExtra && voteExtra.Ответ ? voteExtra.Ответ : '';
			}
			,signDate: function(voteExtra, options, rowObject) {
				if(typeof voteExtra == 'string') {
					voteExtra = JSON.parse(voteExtra);
				}
				return voteExtra && voteExtra.Подписано && voteExtra.Подписано.length > 0 ? 
					('<span class="q-sign-time">'+voteExtra.Подписано+'</span>'): ''
			}
			,result: function(questionExtra, options, rowObject) {
				if(typeof questionExtra == 'string') {
					questionExtra = JSON.parse(questionExtra);
				}
				if(questionExtra)
				{
					if(questionExtra.Решение_НЕ_принято)
					{
						return 'решение не принято';
					}
					if(questionExtra.Принято_решение)
					{
						return 'принято решение: «' + questionExtra.Принято_решение + '»'
					}
				}
				return '';
			}
		}

		controller.colModel =
		[
			{ name: 'id_Question', hidden: true }
			, { name: 'id_Poll', hidden: true }
			, { label: 'АУ', name: 'FullName', width: 200 }
			, { label: 'ИНН', name: 'INN', width: 80 }
			, { label: 'СРО', name: 'SRO', width: 80 }
			, { label: 'Регион', name: 'Region', width: 80 }
			, { label: 'Группа', name: 'Group', width: 80 }

			, { label: 'Вопрос', name: 'Title', width: 100 }
			, { label: 'Время голосования', name: 'Time', width: 80, search: false, formatter: formatter.date }
			, { label: 'Ответ АУ', name: 'VoteExtra', width: 60, 
				search: false, sortable: false, formatter: formatter.answer }
			, { label: 'Подписано', name: 'VoteExtra', width: 80, 
				search: false, sortable: false, formatter: formatter.signDate }
			, { label: 'Результат голосования', name: 'QuestionExtra', width: 100, 
				search: false, sortable: false, formatter: formatter.result }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var url= base_grid_url;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны ответы: {1} из {2}'
				, emptyText: 'Нет ответов для отображения'
				, emptyrecords: 'Нет ответов для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10
				, rowList: [10, 30, 60, 100, 300, 500]
				, pager: '#cpw-orpau-voting-questions-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) {  }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка ответов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		return controller;
	}
})