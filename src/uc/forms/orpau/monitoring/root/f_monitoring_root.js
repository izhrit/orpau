define(['forms/orpau/monitoring/root/c_root'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'monitoring'
		, Title: 'Добро пожаловать в административную часть orpau'
	};
	return form_spec;
});