﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/debtor/e_debtor.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/confirmation/c_confirmation'
],
function (c_fastened, tpl, h_msgbox, c_confirmation)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.confirmation_url = controller.base_url + '?action=confirmation.generate&cmd=get';

		controller.size = { width: 1000, height: 550 };

		var base_render = controller.Render;
		controller.Render = function (sel)
		{
			base_render.call(this, sel);
			var self= this;
			$(sel + ' button.open-confirmation-btn').click(function () { self.onConfirmation(); });
		}

		var au_name = function (manager)
		{
			if (!manager || null==manager)
				return '';
			var res= manager.LastName;
			if (manager.FirstName && ''!=manager.FirstName)
				res+= ' ' + manager.FirstName.charAt(0);
			if (manager.MiddleName && ''!=manager.MiddleName)
				res+= ' ' + manager.MiddleName.charAt(0);
			return res;
		}

		controller.onConfirmation = function ()
		{
			var model= this.model;
			var managerName = au_name(model.Manager);

			var debtorName = model.Name;

			if (model.Confirmation) 
			{
				this.OnExistingConfirmation(managerName, debtorName);
			}
			else 
			{
				this.OnNotExistingConfirmation(managerName, debtorName);
			}
		}

		controller.OnExistingConfirmation = function(managerName, debtorName)
		{
			var id_Confirmation = this.model.Confirmation.id_Confirmation;
			var confirmation_url = this.base_url+'?action=confirmation.crud&cmd=get&id=' + id_Confirmation;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", confirmation_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data) 
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cconfirmation = c_confirmation({ base_url: controller.base_url });
							data.Manager = controller.model.Manager;
							data.Debtor = controller.model;
							cconfirmation.SetFormContent(data);
							h_msgbox.ShowModal
							({
								title: 'Справка №'+id_Confirmation+' о полномочиях АУ "' + managerName + '" в отношении "' + debtorName + '"'
								, controller: cconfirmation
								, buttons: ['Закрыть справку']
								, id_div: "cpw-form-orpau-confirmation"
							});

						}
					}
				});
		}

		controller.OnNotExistingConfirmation = function(managerName, debtorName)
		{
			var model= this.model;
			var url= this.base_url + '?action=confirmation.generate&cmd=get&id_Debtor=' + model.id_Debtor + '&id_Manager=' + model.Manager.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Подготовка справки", url);
			v_ajax.ajax({
				dataType: "text"
				, cache: false
				, success: function (confirmation_text, textStatus)
				{
					if (null == confirmation_text)
					{
						v_ajax.ShowAjaxError(confirmation_text, textStatus);
					}
					else
					{
						var cconfirmation = c_confirmation({ base_url: controller.base_url });
						cconfirmation.SetFormContent({Text:confirmation_text});
						h_msgbox.ShowModal
						({
							title: 'АУ "' + managerName + '" в отношении "' + debtorName + '"'
							, controller: cconfirmation
							, buttons: ['Закрыть справку']
							, id_div: "cpw-form-orpau-confirmation"
						});
					}
				}
			});
		}

		return controller;
	}
});