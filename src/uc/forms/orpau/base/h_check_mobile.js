define(function ()
{
	var helper = {};

	helper.isMobileByDevice = function() {
        var isMobile = false;
        var toMatch = [
            /Android/i,
            /webOS/i,
            /iPhone/i,
            /iPad/i,
            /iPod/i,
            /BlackBerry/i,
            /Windows Phone/i
        ];

        for (var i=0; i<toMatch.length;i++){
            var toMatchItem = toMatch[i];
            if(navigator.userAgent.match(toMatchItem))
            {
                isMobile = true;
                break;
            };

        }
        return isMobile;
    }

    helper.isMobile = function() {
        return window.innerWidth < 480;
    }

    helper.lockScroll = function(el) {
        if(el){
            $(el).css({'overflow': 'hidden'});
        } else {
            $('body').css({'overflow': 'hidden'});
        }
    }

    helper.unlockScroll = function(el) {
        if(el){
            $(el).css({'overflow': 'auto'});
        } else {
            $('body').css({'overflow': 'auto'});
        }        
    }

	return helper;
})