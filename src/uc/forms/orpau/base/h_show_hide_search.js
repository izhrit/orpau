define(function ()
{
	var helper = {};

	helper.OnShowHideSearchInputs = function (sel)
	{
		var search_toolbar = $(sel + ' .ui-search-toolbar');
		var display = search_toolbar.css('display');
		search_toolbar.css('display', ('none' == display) ? 'table-row' : 'none');
		var new_text = ('none' == display) ? '������� �����' : '������';
		$(sel + ' .show-hide-search-inputs span.search-button-text').text(new_text);
	}

	helper.BindShowHideSearchButton = function (sel)
	{
		$(sel + ' .show-hide-search-inputs').click(function () { helper.OnShowHideSearchInputs(sel); });
	}

	return helper;
})