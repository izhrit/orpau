define([
	  'forms/base/ajax/ajax-service-base'
],
function (ajax_service_base)
{
	var service = ajax_service_base('client.cryptoapi');

	var formatCapicomDate = function (dd)
	{
		var d= new Date(dd);
		var res= '';
		var day= d.getDate();
		if (day<10)
			day= '0' + day;
		res+= day;
		res+= '.';
		var m= d.getMonth()+1;
		if (m<10)
			m= '0' + m;
		res+= m;
		res+= '.';
		res+= d.getFullYear();
		return res;
	};

	service.getCertificatesForSelect2= function ()
	{
		if (app.getCertificatesForSelect2)
			return app.getCertificatesForSelect2();
		try
		{
			// ������������� ������� CAPICOM.Store: ������������� ������ ��� ������ � ���������� ������������
			var myStore = new ActiveXObject("CAPICOM.Store");
			// ��������� ��������� ������������ ������������
			myStore.Open($.capicom.CAPICOM_CURRENT_USER_STORE, "My", $.capicom.CAPICOM_STORE_OPEN_READ_ONLY);

			// ����� ���� ����������� ������������ (������ �� ����)
			// ��������� �������: http://msdn.microsoft.com/en-us/library/aa375642(v=vs.85).aspx
			var filteredCertificates = myStore.Certificates.Find($.capicom.CAPICOM_CERTIFICATE_FIND_TIME_VALID);
			var result = [];
			for (var i = 1; i <= filteredCertificates.Count; i++)
			{
				var cert = filteredCertificates.Item(i);
				var Subject = cert.GetInfo(/*CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME*/0);
				var Issuer = cert.GetInfo(/*CAPICOM_CERT_INFO_ISSUER_SIMPLE_NAME*/1);
				var res = {
					id: cert.Thumbprint
					, text: Subject
					, data: {
						ValidFromDate: formatCapicomDate(cert.ValidFromDate)
						, ValidToDate: formatCapicomDate(cert.ValidToDate)
						, SerialNumber: cert.SerialNumber
						, Issuer: Issuer
						, SubjectName: cert.SubjectName
					}
				}
				result.push(res);
			}
			return result;
		}
		catch (ex)
		{
			return null;
		}
	}

	service.ExecuteCommand= function(cmd)
	{
		switch (cmd)
		{
			case 'certificates.select2': return this.getCertificatesForSelect2();
		}
	}

	service.action= function (cx)
	{
		var parsed_args= cx.url_args();

		var res= this.ExecuteCommand(parsed_args.cmd);

		cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
	}

	return service;
});