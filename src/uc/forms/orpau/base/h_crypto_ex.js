define([
	'forms/base/h_msgbox'

  , 'tpl!forms/orpau/login/v_capicom_undefined.html'
  , 'tpl!forms/orpau/login/v_cert_canceled_by_user.html'
  , 'forms/orpau/base/h_check_mobile'
],
function (h_msgbox, v_capicom_undefined, v_cert_canceled_by_user, h_check_mobile)
{
	var helper = {};

	helper.handleCryptoApiEx = function(ex) {
        var isMobile = h_check_mobile.isMobile();
        var description= ex.description ? ex.description : ex.message;
        if (!description || null==description)
            throw ex;
        var exMessages = ["ActiveXObject is not defined", "'ActiveXObject' is not defined", "Can't find variable: ActiveXObject"];
        if (-1 != exMessages.indexOf(description))			{
            h_msgbox.ShowModal({width: isMobile ? window.innerWidth-20 : 460, title:'Аутентификация неудачна',html: v_capicom_undefined()});
        }
        else if (-1 != description.indexOf('cancelled by the user'))
        {
            h_msgbox.ShowModal({width: isMobile ? window.innerWidth-20 : 460, title:'Аутентификация неудачна',html: v_cert_canceled_by_user()});
        }
        else
        {
            throw ex;
        }
    }

	return helper;
})