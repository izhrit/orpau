include ..\in.lib.txt quiet
wait_text "Логин"
shot_check_png ..\..\shots\01sav.png

check_stored_lines orpau_profile_fields_1

shot_check_png ..\..\shots\01sav.png

play_stored_lines orpau_profile_fields_2

shot_check_png ..\..\shots\02edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02edt.json.result.txt

wait_click_full_text "Редактировать модель в элементе управления"
wait_text "Логин"
shot_check_png ..\..\shots\02edt.png

check_stored_lines orpau_profile_fields_2

exit