define([
	'forms/base/h_msgbox'
	, 'forms/orpau/profile/c_profile'
	, 'tpl!forms/orpau/profile/v_forgot_password.html'
	, 'forms/orpau/base/h_check_mobile'
	, 'forms/base/h_validate'
],
function (h_msgbox, c_profile, v_forgot_password, h_check_mobile, h_validate)
{
	return {

		ModalEditManagerProfile: function (base_url, id_Manager, on_done)
		{
			var ajaxurl = base_url + '?action=manager.crud' + '&cmd=get&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об АУ с сервера", ajaxurl);
			var self = this;
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false,
				success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.EditProfile(base_url, id_Manager, on_done, data);
					}
				}
			});
		}

		, OkToSave: function (manager)
		{
			function showMessage(html)
			{
				var title = "Проверка перед сохранением";
				var width = 400;
				h_msgbox.ShowModal({ title: title, width: width, html: html });
			}
			var email = manager.EMail;
			var phone = manager.Phone;
			if (''!=phone && !h_validate.ValidatePhoneNumber(phone))
			{
				showMessage('Неправильный номер телефона');
				return false;
			}
			if (''!=email && !h_validate.ValidateEmail(email))
			{
				showMessage('Неправильный e-mail');
				return false;
			}
			return true;
		}

		, EditProfile: function (base_url, id_Manager, on_done, manager)
		{
			var isMobile = h_check_mobile.isMobile();
			if(isMobile)
				h_check_mobile.lockScroll();

			var profile = c_profile({base_url: base_url});
			var self = this;
			var id_div = 'cpw-form-ama-orpau-manager';
			profile.UpdateProfileAgreement = function()
			{
				$('#'+id_div).dialog("close");
				self.ModalEditManagerProfile(base_url, id_Manager, on_done);
			}
			profile.SetFormContent(manager);
			var btnOk = 'Сохранить свойства АУ';
			h_msgbox.ShowModal
			({
				title: 'Настройка свойств АУ',
				controller: profile,
				width: isMobile ? window.innerWidth : 695,
				height: isMobile ? window.innerHeight : 665,
				buttons: [btnOk, 'Отмена'],
				id_div: id_div,
				beforeClose: function() { if (isMobile) h_check_mobile.unlockScroll(); },
				onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var manager = profile.GetFormContent();
						if (!self.OkToSave(manager))
							return false;
						self.UpdateProfile(base_url, id_Manager, on_done, manager);
					}
				}
			});
		}

		, UpdateProfile: function (base_url, id_Manager, on_done, manager)
		{
			var ajaxurl = base_url + '?action=manager.crud&cmd=update&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача информации об АУ на сервер", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json", type: 'POST', data: manager, cache: false,
				success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else if (true == responce.ok)
					{
						if (null != on_done)
							on_done();
					}
					else
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка сохранения свойств АУ', width: 400,
							html: '<span>Не удалось сохранить свойства АУ по причине:</span><br/> <center><b>\"' +
								responce.reason + '\"</b></center>' +
								'<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
				}
			});
		}

            , ForgotPassword: function(base_url) {
                var btnOk = 'Отправить';
                h_msgbox.ShowModal({
                    title: 'Сброс пароля',
                    html: v_forgot_password,
                    width: h_check_mobile.isMobile() ? window.innerWidth-20 : 420,
                    buttons: [btnOk, 'Отмена'],
                    onclose: function (btn, dlg_div) {
                        var login = $('div.cpw-orpau-forgot-password input[name="Login"]').val().trim();
                        if (btn == btnOk && login) {
                            var ajaxurl = base_url + '?action=manager.password' + '&cmd=reset-by-login&login=' + login;
                            var v_ajax = h_msgbox.ShowAjaxRequest("Сброс пароля ", ajaxurl);

                            v_ajax.ajax({
                                dataType: "json",
                                type: 'GET',
                                cache: false,
                                success: function (responce, textStatus) {
                                    if (null == responce) {
                                        v_ajax.ShowAjaxError(responce, textStatus);
                                    } else if (responce.ok) {
                                        h_msgbox.ShowModal({
                                            title: 'Сброс пароля',
                                            html: '<span>Новый пароль успешно отправлен на почту.</small>'
                                        });
                                    } else {
                                        h_msgbox.ShowModal({
                                            title: 'Ошибка сброса пароля',
                                            width: 450,
                                            html: '<span>Не удалось сменить пароль и email АУ по причине:</span><br/> <center><b>\"' +
                                                responce.reason + '\"</b></center>' +
                                                '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }

			, PleaseEditProdile: function(message, onProfile)
			{
				var btn_edit_profile= 'Указать контактные данные в профиле';
				var id_div = 'please-edit-profile';
				h_msgbox.ShowModal({
					width: 500
					, id_div: id_div
					, title: 'Внимание'
					, buttons:[btn_edit_profile,'Отмена']
					, html: message
					, onclose: function (btn)
					{
						if (btn == btn_edit_profile)
						{
							onProfile();
						}
					}
				});
			}

			, ManagerRead: function(base_url, id_Manager, callback)
			{
				var ajaxurl = base_url + '?action=manager.crud' + '&cmd=get&id=' + id_Manager;
				var v_ajax = h_msgbox.ShowAjaxRequest("Запрос информации об АУ с сервера", ajaxurl);
				v_ajax.ajax(
				{
					dataType: "json", type: 'GET',cache: false,
					success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							callback(data);
						}
					}
				});
			}

		};
});