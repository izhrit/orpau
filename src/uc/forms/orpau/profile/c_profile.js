define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/profile/e_profile.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/orpau/profile/v_agreement_signature.html'
	, 'forms/orpau/club/h_club_agreement'
	, 'forms/orpau/base/h_check_mobile'
	, 'forms/orpau/asp/h_asp_agreement'
	, 'forms/base/h_validate'
	, 'tpl!forms/orpau/asp/v_asp_need_contacts.html'
], function (c_fastened, tpl, h_msgbox, v_agreement_signature, h_club_agreement, h_check_mobile, h_asp_agreement, h_validate, v_asp_need_contacts)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);
		var isMobile = h_check_mobile.isMobile;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.PasswordEnabled = false;

			this.PasswordEnabled = this.model && this.model.Password;
			this.id_Manager = !this.model ? null : this.model.id_Manager;
			if (!this.PasswordEnabled)
				this.PasswordEnabled = false;

			var self = this;
			$(sel + ' button.password').click(function () { self.OnChangePassword(); });
			$(sel + ' button.block').click(function () { self.OnBlock(); });
			$(sel + ' button.использовать-асп').click(function(){ self.ЗаявлениеАСП(); });
			$(sel + ' button.асп-данные').click(function(){ self.VerifyAgreementSignature(); })
			$(sel + ' button.асп-документ').click(function(){ self.GetAspAgreementText(); });
			$(sel + ' button.вступить-в-профсоюз-профиль').click(function(){ self.ЗаявлениеВПрофсоюз(); });
			$(sel + ' button.профсоюз-документ').click(function(){ self.GetClubAgreementText(); });

			$("input[name='phone']").inputmask("99999999999");
		}

		controller.OnChangePassword = function ()
		{
			var mobileMsgBoxWidth = window.innerWidth-20;
			this.model = this.GetFormContent();
			if (!this.model || !this.model.EMail || null == this.model.EMail || '' == this.model.EMail)
			{
				h_msgbox.ShowModal({
					width: isMobile() ? mobileMsgBoxWidth : 400
					,title:'Отказ выслать пароль'
					,html: 'Укажите E-mail (адрес электронной почты) АУ на который вы хотите получить письмо с паролем.'
				});
			}
			else
			{
				var self = this;
				var btnOk = this.PasswordEnabled ? 'Да, хочу сменить пароль' : 'Да, хочу открыть доступ через пароль';
				h_msgbox.ShowModal({
					width: isMobile() ? mobileMsgBoxWidth : 520,
					title: "Подтверждение операции с паролем",
					html: true == this.PasswordEnabled ?
						'<span>Вы уверены что хотите сменить пароль</span><br/> и выслать его на адрес <strong>' + this.model.EMail + '</strong>?' :
 						'<span>Вы уверены что хотите открыть доступ через пароль</span><br/> и выслать его на адрес <strong>' + this.model.EMail + '</strong>?',
					buttons: [btnOk, 'Нет, отменить'],
					id_div: "cpw-form-ama-orpau-manager-form-confirm",
					onclose: function (btn)
					{
						if (btn == btnOk)
							self.ChangePassword();
					}
				});
			}
		}

        controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
        controller.base_password_url = controller.base_url + '?action=manager.password';
        controller.base_club_agreement_url = controller.base_url + '?action=club.agreement';
        controller.base_asp_agreement_url = controller.base_url + '?action=asp.agreement';

        controller.ChangePassword = function () {
            var self = this;
            var sel = this.fastening.selector;

            var url = this.base_password_url + '&cmd=change&id_Manager=' + this.id_Manager + '&email=' + this.model.EMail;
            var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на создание пароля", url);

            v_ajax.ajax({
                dataType: "json",
                type: 'GET',
                cache: false,
                success: function (responce, textStatus) {
                    if (null == responce) {
                        v_ajax.ShowAjaxError(responce, textStatus);
                    } else if (true == responce.ok) {
                        self.PasswordEnabled = true;
                        $(sel + ' div.cpw-orpau-profile .settings')
                            .attr('password_enabled', self.PasswordEnabled);
                        h_msgbox.ShowModal({
                            width: 200,
                            html: '<center>Пароль выслан</center>'
                        });
                    } else {
                        h_msgbox.ShowModal({
                            title: 'Ошибка сохранения пароля',
                            width: isMobile() ? window.innerWidth-20 : 450,
                            html: '<span>Не удалось сменить пароль и email АУ по причине:</span><br/> <center><b>\"' +
                                responce.reason + '\"</b></center>' +
                                '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
                        });
                    }
                }
            });
        }

        controller.OnBlock = function () {
            this.model = this.GetFormContent();
            if (true != this.PasswordEnabled) {
                h_msgbox.ShowModal({
					title:'Отказ блокировать доступ'
					,width:350
                    ,html: 'Доступ по логину сейчас итак отсутствует, блокировать нечего.'
                });
            } else {
                var self = this;
                var btnOk = 'Да, хочу заблокировать доступ по логину';
                h_msgbox.ShowModal({
                    width: isMobile() ? window.innerWidth-20 : 520,
                    title: 'Предупреждение',
                    html: 'Вы уверены что хотите заблокировать доступ по логину?',
                    buttons: [btnOk, 'Нет, отменить'],
                    id_div: "cpw-form-ama-orpau-manager-form-confirm",
                    onclose: function (btn) {
                        if (btn == btnOk) {
                            self.BlockAccess();
                        }
                    }
                });
            }
        }

        controller.BlockAccess = function () {
            var self = this;
            var sel = this.fastening.selector;

            var url = this.base_password_url + '&cmd=block&id_Manager=' + this.id_Manager + '&email=' + this.model.EMail;
            var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на блокирование доступа", url);

            v_ajax.ajax({
                dataType: "json",
                type: 'GET',
                cache: false,
                success: function (responce, textStatus) {
                    if (null == responce) {
                        v_ajax.ShowAjaxError(responce, textStatus);
                    } else if (true == responce.ok) {
                        self.PasswordEnabled = false;
                        $(sel + ' div.cpw-orpau-profile .settings')
                            .attr('password_enabled', self.PasswordEnabled);
                        h_msgbox.ShowModal({
                            width: 200,
                            html: '<center>Доступ по логину заблокирован</center>'
                        });
                    } else {
                        h_msgbox.ShowModal({
                            title: 'Ошибка блокировки логина',
                            width: isMobile() ? window.innerWidth-20 : 450,
                            html: '<span>Не удалось заблокировать логин АУ по причине:</span><br/> <center><b>\"' +
                                responce.reason + '\"</b></center>' +
                                '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
                        });
                    }
                }
            });
        }

        controller.VerifyAgreementSignature = function() {
            var self = this;
            var url = self.base_asp_agreement_url + '&cmd=verify-signature&id=' + self.id_Manager;
            var v_ajax = h_msgbox.ShowAjaxRequest('Получение данных о подписи заявления', url);

            v_ajax.ajax({
                dataType: "json",
                type: 'GET',
                cache: false,
                success: function (responce, textStatus) {
                    if (null == responce) {
                        v_ajax.ShowAjaxError(responce, textStatus);
                    } else {
                        if(isMobile()) h_check_mobile.lockScroll();

                        h_msgbox.ShowModal({
                            width: isMobile() ? window.innerWidth : 550,
                            height: isMobile() ? window.innerHeight : 400,
                            title: 'Информация о подписании заявления',
                            html: v_agreement_signature(responce),
                            beforeClose: function() { if(isMobile()) h_check_mobile.unlockScroll(); }
                        });
                    }
                }
            })
        }

		controller.GetAspAgreementText = function()
		{
			var self = this;
			var url = self.base_asp_agreement_url + '&cmd=get-signed-application-for-accession&id=' + self.id_Manager;

			var v_ajax = h_msgbox.ShowAjaxRequest('Получение подписанного заявления о присоединении к АСП', url);
			v_ajax.ajax
			({
				dataType: "json",
				type: 'GET',
				cache: false,
				success: function (txt, textStatus)
				{
					if (null == txt)
					{
						v_ajax.ShowAjaxError(txt, textStatus);
					}
					else
					{
						self.ShowAgreement(txt,'Подписанное заявление о присоединении к АСП');
					}
				}
			})
		}

		controller.GetClubAgreementText = function()
		{
			var self = this;
			var url = self.base_club_agreement_url + '&cmd=get-signed-agreement&id=' + self.id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest('Получение данных о подписанном заявления', url);

			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false,
				success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.ShowAgreement(responce,'Подписанное заявление о вступлении в профсоюз');
					}
				}
			});
		}

		controller.ShowAgreement = function(text,title)
		{
			if(isMobile())
				h_check_mobile.lockScroll();

			h_msgbox.ShowModal
			({
				width: isMobile() ? window.innerWidth : 640,
				height: isMobile() ? window.innerHeight : 545,
				title: title,
				buttons: ['Закрыть'],
				html: '<textarea class="signature-result" readonly="readonly"></textarea>',
				create: function(event, ui)
				{
					$('textarea.signature-result').val(text).css({
						border: '2px solid green', color: 'green',
						width: '100%', height: '100%'
					});
					setTimeout(function(){ $('textarea.signature-result').scrollTop(0); });
				},
				beforeClose: function() { if(isMobile()) h_check_mobile.unlockScroll(); }
			});
		}

		controller.ЗаявлениеВПрофсоюз = function()
		{
			var self= this;
			if (!self.model || !self.model.ИспользуетАСП)
			{
				h_club_agreement.AlertAsp(function () { self.ЗаявлениеАСП(); });
			}
			else
			{
				var ау= self.GetFormContent();
				var phone = ау.Phone;
				var email = ау.EMail;
				ау.id_Manager= self.model.id_Manager;
				if (!h_validate.ValidateEmail(email) ||
					!h_validate.ValidatePhoneNumber(phone))
				{
					h_msgbox.ShowModal({
						title: 'Внимание',width: 400,
						html: 'Заполните и сохраните контактные данные, чтобы перейти к подписанию соглашения'
					});
				}
				else
				{
					h_club_agreement.Вступить_в_профсоюз(
						ау, self.base_url
						, function()
						{
							self.UpdateProfileAgreement();
							$('li.профсоюз').hide();
							$('div.cpw-orpau-manager-cabinet').addClass('СостоитВПрофсоюзе');
						}
					);
				}
			}
		}

		controller.ЗаявлениеАСП = function()
		{
			var self = this;

			var ау= self.GetFormContent();
			var phone = !ау.Phone ? '' : ау.Phone;
			var email = !ау.EMail ? '' : ау.EMail;
			ау.INN= self.model.INN;
			ау.id_Manager= self.model.id_Manager;
			if (!h_validate.ValidateEmail(email) ||
				!h_validate.ValidatePhoneNumber(phone))
			{
				h_msgbox.ShowModal({
					title: 'Укажите контактные данные для АСП',
					html: v_asp_need_contacts(), width: 500, id_div:'cpw-need-contact-data-msg'
				});
			}
			else
			{
				h_asp_agreement.Присоединиться_к_соглашению_об_АСП
				(
					ау, self.base_asp_agreement_url
					, function()
					{
						self.UpdateProfileAgreement();
						$('li.асп').hide();
						$('div.cpw-orpau-manager-cabinet').addClass('ИспользуетАСП');
					}
				);
			}
		}

		return controller;
	}
})