﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/debtors/e_debtors.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/debtor/c_debtor'
],
	function (c_fastened, tpl, h_msgbox, c_debtor)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.ReloadGrid = function()
		{
			var sel = controller.fastening.selector;
			$(sel + ' table.grid').trigger('reloadGrid');
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_grid_url = controller.base_url + '?action=debtor.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=debtor.crud';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			return url;
		}

		controller.colModel =
			[
				{ name: 'id_Debtor', hidden: true }
				, { label: 'Должник', name: 'title', width: 800}
				, { label: 'ИНН', name: 'inn', width: 180 }
				, { label: 'ОГРН', name: 'ogrn', width: 185 }
				, { label: 'СНИЛС', name: 'snils', width: 180 }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = this.base_grid_url;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка должников...'
				, recordtext: 'Показаны должники: {1} из {2}'
				, emptyText: 'Нет должников для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15

				, pager: '#cpw-orpau-debtors-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { self.OnDebtor(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка процедур", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnDebtor = function (rowid)
		{
			var self = this;
			var selectedRow = $(this.fastening.selector + ' table.grid')[0].rows[rowid];
			var debtorName = selectedRow.cells[1].innerHTML;
			var id_Debtor = selectedRow.cells[0].innerHTML;
			var debtor_url = controller.base_crud_url + '&cmd=get&id=' + id_Debtor;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", debtor_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cdebtor = c_debtor({ base_url: self.base_url });
							cdebtor.SetFormContent(data);
							h_msgbox.ShowModal
							({
								title: 'Информация о должнике ' + debtorName
								, controller: cdebtor
								, buttons: ['Закрыть']
								, id_div: "cpw-form-orpau-debtor"
							});

						}
					}
				});
		}

		return controller;
	}
});