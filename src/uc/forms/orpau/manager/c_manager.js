﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/manager/e_manager.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/debtor/c_debtor'
],
function (c_fastened, tpl, h_msgbox, c_debtor)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.size = { width:1000, height: 680 };

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_grid_url = controller.base_url+"?action=debtor.jqgrid";
		controller.debtor_crud_url = controller.base_url + '?action=debtor.crud';

		var base_render = controller.Render;
		controller.Render = function(sel)
		{
			base_render.call(this, sel);
			if (this.model && this.model.ArbitrManagerID)
				this.RenderGrid(this.model.ArbitrManagerID);
		}

		controller.colModel =
			[
				{ name: 'id_Debtor', hidden: true }
				, { label: 'Должник', name: 'title', width: 400 }
				, { label: 'ИНН', name: 'inn', width: 130 }
				, { label: 'ОГРН', name: 'ogrn', width: 135 }
				, { label: 'СНИЛС', name: 'snils', width: 130 }
			];

		controller.RenderGrid = function(ArbitrManagerID)
		{
			var grid = $(".procedure-grid-table");
			var url = controller.base_grid_url+"&ArbitrManagerID="+ArbitrManagerID;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны процедуры: {1} из {2}'
				, emptyText: 'Нет процедур для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 8
				, pager: '#cpw-orpau-procedure-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: "auto"
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { controller.OnDebtor(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка процедур", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnDebtor = function (rowid)
		{
			var self = this;
			var selectedRow = $(".procedure-grid-table")[0].rows[rowid];
			var debtorName = selectedRow.cells[1].innerHTML;
			var id_Debtor = selectedRow.cells[0].innerHTML;
			var debtor_url = controller.debtor_crud_url + '&cmd=get&id=' + id_Debtor;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", debtor_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cdebtor = c_debtor({ base_url: self.base_url });
							cdebtor.SetFormContent(data);
							h_msgbox.ShowModal
								({
									title: 'Информация о должнике ' + debtorName
									, controller: cdebtor
									, buttons: ['Закрыть информацию о должнике']
									, id_div: "cpw-form-orpau-debtor"
								});

						}
					}
				});
		}

		return controller;
	}
});