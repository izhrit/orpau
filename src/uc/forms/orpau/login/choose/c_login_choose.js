define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/login/choose/e_login_choose.html'
	, 'tpl!forms/orpau/login/choose/v_manager_select2.html'
	, 'forms/orpau/base/h_cryptoapi'
	, 'tpl!forms/orpau/login/choose/v_certificate_select2.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/orpau/login/choose/v_no_cert_for_manager.html'
	, 'tpl!forms/orpau/login/choose/v_no_certs_for_managers.html'
]
, function (c_fastened, tpl, v_manager_select2, h_cryptoapi, v_certificate_select2, h_msgbox, v_no_cert_for_manager, v_no_certs_for_managers)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_url_select2 = base_url + '?action=manager.select2';

		var select2Width = '100%';

		var create_Сертификат_select2_options = function (certs)
		{
			return {
				data: certs,
				formatResult: v_certificate_select2,
				formatSelection: function (item) { return 'на имя: ' + item.text; },
				width: select2Width,
				placeholder: 'Сертификат не выбран',
				initSelection : function (element, callback) {}
			}
		}

		var options = {
			field_spec:
				{
					АУ: { 
						ajax: { url: base_url_select2, dataType: 'json' },
						width: select2Width,
						formatResult: v_manager_select2,
						placeholder: 'Арбитражный управляющий не выбран',
						initSelection : function (element, callback) {}
					}
					,Сертификат: create_Сертификат_select2_options([])
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:450,height:510};

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;
			$(sel + ' .certs-select2').on('change', function(e) { self.OnSelectCert(); });
			$(sel + ' .managers-select2').on('change', function(e) { self.OnSelectManager(); });
			$(sel + ' .selected-cert').on('click', function(e) { self.OnCertClick(e); });

			self.TryToPrepareCertsAndManager();
		}

		controller.OnCertClick = function (e)
		{
			var iextra= $(e.target).parents('.extra');
			if (iextra && null != iextra && iextra.length)
			{
				var sel= this.fastening.selector;
				var cert= $(sel + ' .certs-select2').select2('data');
				var signer= $.capicom.findCertificateByHash(cert.id);
				signer.Certificate.Display();
			}
		}

		controller.TryToPrepareCertsAndManager = function ()
		{
			var self = this;
			var url = 'client.cryptoapi?cmd=certificates.select2';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение информации о доступных сертификатах ЭП", url);
			setTimeout(function () { 
				v_ajax.ajax({
					dataType: "json", type: 'GET', cache: false
					, success: function (certs, textStatus)
					{
						if (null!=certs)
							self.OnGotCertificates(certs);
					}
				});
			},20);
		}

		var certs_prepare_INN = function (certs)
		{
			for (var i = 0; i < certs.length; i++)
			{
				var cert= certs[i];
				var dn_fields= h_cryptoapi.GetDNFields(cert.data.SubjectName);
				cert.data.ИНН= dn_fields.ИНН;
			}
		}

		var certs_disable_bad = function (certs)
		{
			for (var i = 0; i < certs.length; i++)
			{
				var cert= certs[i];
				if (!cert.data.ИНН || false==cert.data.isManager)
					cert.disabled= true;
			}
		}

		var cert_cmp = function (cert)
		{
			var res= cert.disabled ? '0' : '1';

			var d= cert.data.ValidFromDate.split('.');
			res+= d[2] + d[1] + d[0];

			return res;
		}

		var certs_sort = function (certs)
		{
			certs.sort(function (c1, c2)
			{
				var sc1= cert_cmp(c1);
				var sc2= cert_cmp(c2);
				return sc1>sc2 ? -1 : (sc1==sc2 ? 0 : 1);
			});
		}

		var certs_get_inns = function (certs)
		{
			var res= [];
			for (var i = 0; i < certs.length; i++)
			{
				var cert= certs[i];
				if (!cert.disabled && cert.data.ИНН)
					res.push(cert.data.ИНН);
			}
			return res;
		}

		controller.OnGotCertificates = function (certs)
		{
			certs_prepare_INN(certs);

			var certs_inn= certs_get_inns(certs);

			var self = this;
			var url = base_url + '?action=manager.for-inn';
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение информации об АУ для доступных сертификатов ЭП", url);
			v_ajax.ajax({
				dataType: "json", type: 'POST', cache: false, data: {inn:certs_inn.join(',')}
				, success: function (managers_for_certs_inn, textStatus)
				{
					if (null != managers_for_certs_inn)
					{
						self.OnGotCertificatesManagers(certs,managers_for_certs_inn);
					}
					else
					{
						self.ShowCertificatesToSelect(certs);
						v_ajax.ShowAjaxError(managers_for_certs_inn, textStatus);
					}
				}
			});
		}

		controller.ShowCertificatesToSelect = function (certs)
		{
			certs_disable_bad(certs);
			certs_sort(certs);

			this.certs= certs;
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-auth-manager-by-cert-step1').attr('data-client-cryptoapi','yes');

			$(sel + ' [data-fc-selector="Сертификат"]').select2('destroy');
			$(sel + ' [data-fc-selector="Сертификат"]').select2(create_Сертификат_select2_options(certs));

			var cert0 = (0 == certs.length) ? null : certs[0];
			if (null == cert0 || cert0.disabled)
			{
				$(sel + ' div.selected-cert').html(v_no_certs_for_managers());
			}
			else
			{
				$(sel + ' [data-fc-selector="Сертификат"]').select2('data', cert0);
				this.OnSelectCert();
			}
		}

		var prep_managers_for_inn = function (managers_for_certs_inn)
		{
			var managers_for_inn = {};
			for (var i = 0; i < managers_for_certs_inn.length; i++)
			{
				var m= managers_for_certs_inn[i];
				managers_for_inn[m.inn]= m;
			}
			return managers_for_inn;
		}

		var certs_mark_for_managers = function (certs, managers_for_inn)
		{
			for (var i = 0; i < certs.length; i++)
			{
				var cert= certs[i];
				var manager= managers_for_inn[cert.data.ИНН];
				if (!manager)
				{
					cert.data.isManager = false;
				}
				else
				{
					cert.data.isManager = true;
					cert.data.manager = manager;
				}
			}
		}

		controller.OnGotCertificatesManagers = function (certs, managers_for_certs_inn)
		{
			var managers_for_inn= prep_managers_for_inn(managers_for_certs_inn);
			certs_mark_for_managers(certs, managers_for_inn);
			this.ShowCertificatesToSelect(certs);
		}

		controller.OnSelectCert = function ()
		{
			var sel= this.fastening.selector;
			var cert= $(sel + ' .certs-select2').select2('data');
			if (null != cert)
			{
				this.ShowSelectedCert(sel,cert);
				$(sel + ' .managers-select2').select2('data',cert.data.manager);
				this.ShowSelectedManager(sel,cert.data.manager);
			}
		}

		controller.ShowSelectedCert = function (sel,cert)
		{
			var cert_html= (!cert || null==cert) ? v_no_cert_for_manager() : v_certificate_select2(cert);
			$(sel + ' div.selected-cert').html(cert_html);
		}

		controller.ShowSelectedManager = function (sel,manager)
		{
			var manager_html= v_manager_select2(manager);
			$(sel + ' div.selected-au').html(manager_html);
		}

		var certs_find_for_inn = function (certs, inn)
		{
			for (var i = 0; i < certs.length; i++)
			{
				var cert= certs[i];
				if (!cert.disabled && cert.data.ИНН==inn)
					return cert;
			}
			return null;
		}

		controller.OnSelectManager = function ()
		{
			var sel= this.fastening.selector;
			var manager= $(sel + ' .managers-select2').select2('data');
			this.ShowSelectedManager(sel,manager);
			if (this.certs)
			{
				var icerts= $(sel + ' .certs-select2');
				var cert= certs_find_for_inn(this.certs, manager.inn);
				icerts.select2('data',cert);
				this.ShowSelectedCert(sel,cert);
			}
		}

		return controller;
	}
})