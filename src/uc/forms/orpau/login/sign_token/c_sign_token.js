define([
    'forms/base/fastened/c_fastened'
    , 'tpl!forms/orpau/login/sign_token/e_sign_token.html'
    , 'forms/base/h_msgbox'
], function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
        var base_url = !options_arg ? 'orpau' : options_arg.base_url;
        var hasActiveX = options_arg && options_arg.hasActiveX ? options_arg.hasActiveX : false;

        var controller = c_fastened(tpl);

        var base_Render = controller.Render;
        controller.Render = function (sel) {
            base_Render.call(this, sel);

			var self= this;
            $(sel + ' input').change(function () { self.OnChangeDM(); });
            
            $(sel + ' button.sign-by-cert').click(function(){ self.SignByCapicom(); });

            $(sel + ' button.download-token').click(function(){
                var checked= $(sel + ' div.cpw-orpau-auth-token-show-to-sign input[type="checkbox"]').attr('checked');
                var token = checked ? self.model.with_datamart : self.model.without_datamart;

                var fileName = 'token.txt';
                if (navigator.msSaveBlob) {
                    var blob = new Blob([token], {type:  "text/plain;charset=utf-8;"});
                    return navigator.msSaveBlob(blob, fileName);
                } else {
                    var contentType = 'data:text/plain;charset=utf-8,';
                    var uriContent = contentType + encodeURIComponent(token);

                    var element = document.createElement('a');
                    element.setAttribute('href', uriContent);
                    element.setAttribute('target', '_blank');
                    element.setAttribute('download', fileName);
                    document.body.appendChild(element);
                    element.click();
                    document.body.removeChild(element);
                }
            });
            $(sel + ' button.upload-sign').click(function(){ $(sel + ' #upload-sign-input').click(); });

            $(sel + ' #upload-sign-input').change(function(e){
                var file = $(this).prop('files')[0];

                var reader = new FileReader();
                reader.readAsText(file, "UTF-8");
                reader.onload = function (evt) {
                    self.UploadAndLogin(evt.target.result);
                }
                reader.onerror = function (evt) {
                    alert("error reading file");
                }
            });

            if(!hasActiveX){ $(sel + ' .sign-by-cert').attr('disabled', true); }
        }

        controller.OnChangeDM = function ()
		{
			var sel= this.fastening.selector;
			var o= this.model;
			if (!o.with_datamart)
			{
				h_msgbox.ShowModal({
					width:400
					, title:'Витрины данных ПАУ недостуна'
					, html: 'Отсутствует отметка сайта витрины данных ПАУ и поэтому<br/>отображение информации из ИС "Витрина данных ПАУ" невозможно.'
				});
				$(sel + ' input').attr('checked',null);
			}
			else
			{
				var with_dm= $(sel + ' input').attr('checked');
				var txt= (with_dm) ? o.with_datamart : o.without_datamart;
				$(sel + ' pre').text(txt);
			}
		}

        return controller;
    }
})