﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/login/e_token_to_sign.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' input').change(function () { self.OnChangeDM(); });
		}

		controller.OnChangeDM = function ()
		{
			var sel= this.fastening.selector;
			var o= this.model;
			if (!o.with_datamart)
			{
				h_msgbox.ShowModal({
					width:500
					, title:'Витрины данных ПАУ недостуна'
					, html: 'Отсутствует отметка сайта витрины данных ПАУ и поэтому<br/>отображение информации из ИС "Витрина данных ПАУ" невозможно.'
				});
				$(sel + ' input').attr('checked',null);
			}
			else
			{
				var with_dm= $(sel + ' input').attr('checked');
				var txt= (with_dm) ? o.with_datamart : o.without_datamart;
				$(sel + ' pre').text(txt);
			}
		}

		return controller;
	}
});