define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/login/sign/e_ext_sign.html'
]
, function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.size = {width:420,height:350};

		controller.GetFormContent = function ()
		{
			var sel= this.fastening.selector;
			var iinput= $(sel + ' input[type="file"]')[0];

			var res= new FormData();

			res.append('signature', iinput.files[0]);

			return iinput.files[0];
		}

		return controller;
	}
})