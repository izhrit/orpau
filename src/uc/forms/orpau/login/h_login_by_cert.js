﻿define([
	  'forms/base/h_msgbox'
	, 'forms/orpau/base/h_cryptoapi'

	, 'tpl!forms/orpau/login/v_choosed_cert_without_inn.html'
	, 'tpl!forms/orpau/login/v_au_not_found.html'
	, 'tpl!forms/orpau/login/v_auth_signature_bad.html'
	, 'tpl!forms/orpau/login/v_trusted_site.html'

	, 'forms/orpau/login/c_login_by_cert'
	, 'forms/orpau/base/h_crypto_ex'

	, 'forms/orpau/login/choose/c_login_choose'
	, 'forms/orpau/login/sign/c_ext_sign'
],
function (h_msgbox, h_cryptoapi
	, v_choosed_cert_without_inn, v_au_not_found, v_auth_signature_bad, v_trusted_site, c_login_by_cert, h_crypto_ex, c_login_choose, c_ext_sign)
{
	var helper = {};

	helper.isIE = function()
	{
		const ua = window.navigator.userAgent;
		const msie = ua.indexOf('MSIE ');
		const trident = ua.indexOf('Trident/');
		return (msie > 0 || trident > 0);
	}

	helper.Login= function (base_url, on_logged)
	{
		var self = this;

		var cc_login_choose = new c_login_choose({base_url:base_url});
		var btn_next= 'Перейти к аутентификации';
		h_msgbox.ShowModal({
			title:'Аутентификация АУ'
			,buttons:[btn_next,'Отмена']
			,id_div:'cpw-form-orpau-choose-au-cert'
			,controller:cc_login_choose
			,onclose: function (btn)
			{
				if (btn_next == btn)
				{
					var man_cert= cc_login_choose.GetFormContent();
					if (!man_cert.АУ || null == man_cert.АУ)
					{
						h_msgbox.ShowModal({
							width: 460, title: 'Аутентификация невозможна'
							, html: 'Необходимо указать АУ, которым вы хотите аутентифицироваться!'
						});
						return false;
					}
					self.Login_for_choosed_man_cert(base_url, man_cert, on_logged);
				}
			}
		});
	}

	helper.Login_for_choosed_man_cert = function (base_url, man_cert, on_logged)
	{
		var self = this;
		var url = base_url + '?action=manager.auth&cmd=get-token-to-sign';
		var v_ajax = h_msgbox.ShowAjaxRequest("Получение токена авторизации на подпись", url);
		v_ajax.ajax
		({
			dataType: "json"
			, type: 'POST'
			, cache: false
			, data: man_cert.АУ
			, success: function (data, textStatus)
			{
				if (null == data || !data.token)
				{
					v_ajax.ShowAjaxError(data, textStatus);
				}
				else
				{
					self.Login_for_choosed_man_cert_tokens(base_url, man_cert, data.token, on_logged);
				}
			}
		});
	}

	helper.Login_for_choosed_man_cert_tokens = function (base_url, man_cert, token, on_logged)
	{
		var self= this;

		var cc_login_by_cert= c_login_by_cert();
		cc_login_by_cert.SetFormContent(token);

		var btn_sign= 'Подписать запрос на доступ и войти в систему';
		h_msgbox.ShowModal({
			title:'Подписание запроса на доступ к информации'
			,buttons:[btn_sign,'Закрыть']
			,id_div:'cpw-form-orpau-auth-token',width:600,height:700
			,controller:cc_login_by_cert
			,onclose: function (btn)
			{
				if (btn_sign == btn)
				{
					var checked = $('div.cpw-orpau-auth-token-show-to-sign input[type="checkbox"]').attr('checked');
					var variant= checked ? 'with_datamart' : 'without_datamart';
					self.Login_for_token_variant(base_url, man_cert, token, variant, on_logged);
				}
			}
		});
	}

	helper.Login_for_token_variant= function (base_url, man_cert, token, variant, on_logged)
	{
		if (man_cert.Сертификат && null!=man_cert.Сертификат)
		{
			var cert= app.cryptoapi.CertificateByThumbprint(man_cert.Сертификат.id);
			var base64_encoded_signature= app.cryptoapi.SignBase64(token[variant],/*detached=*/true, cert);
			var signature_form_data= new FormData();
			signature_form_data.append('base64_encoded_signature', base64_encoded_signature);
			this.Login_for_signature_form_data(base_url, man_cert, token, variant, signature_form_data, on_logged);
		}
		else
		{
			var cc_ext_sign= c_ext_sign();
			cc_ext_sign.SetFormContent(man_cert);

			var self= this;
			var btn_ok= 'Предъявить подпись';
			h_msgbox.ShowModal({
				title:'Предъявление подписи'
				,buttons:[btn_ok,'Закрыть']
				,id_div:'cpw-form-orpau-auth-token-sign'
				,controller:cc_ext_sign
				,onclose: function (btn)
				{
					if (btn_ok == btn)
					{
						var signature= cc_ext_sign.GetFormContent();
						var signature_form_data= new FormData();
						signature_form_data.append('signature', signature);
						self.Login_for_signature_form_data(base_url, man_cert, token, variant, signature_form_data, on_logged);
					}
				}
			});
		}
	}

	helper.Login_for_signature_form_data= function (base_url, man_cert, token, variant, signature_form_data, on_logged)
	{
		var url= base_url + '?action=manager.auth&cmd=auth-by-token-signature&variant=' + variant + '&id_Manager=' + man_cert.АУ.id;
		var v_ajax = h_msgbox.ShowAjaxRequest("Отправка подписи токена авторизации на сервер", url);
		v_ajax.ajax
		({
			dataType: "json"
			, type: 'POST'
			, cache: false
			, data: signature_form_data
			, processData: false
			, success: function (responce, textStatus)
			{
				if (null == responce || !responce.id_Manager)
				{
					v_ajax.ShowAjaxError(responce, textStatus);
				}
				else
				{
					on_logged(man_cert,variant,responce);
				}
			}
		});
	}

	helper.SafeOfferSetupLogin = function (auth_info, func_setup)
	{
		if (!auth_info.Login)
		{
			var btnOk = 'Да, задать логин для входа';
			h_msgbox.ShowModal
			({
				title: 'Вход сертификатом не всегда удобен', width:450
				, html: '<div style="line-height:1em;font-size:22px;font-weight:600:padding:20px;margin-left:100px">Хотите задать логин?</div>'
				, buttons: [btnOk, 'Нет']
				, onclose: function (btn)
				{
					if (btn == btnOk)
						func_setup();
				}
			});
		}
	}

	return helper;
});