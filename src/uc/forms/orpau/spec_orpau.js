define([
	  'forms/base/h_spec'
	, 'forms/orpau/cabinet/spec_orpau_cabinet'
	, 'forms/orpau/admin/spec_orpau_admin'
]
, function (h_spec, spec_orpau_cabinet, spec_orpau_admin)
{
	var orpau_specs = {

		controller: {
			"confirmation":
			{
				path: 'forms/orpau/confirmation/c_confirmation'
				, title: 'Справка о полномочиях АУ'
				, keywords: 'арбитражный управляющий полномочия справка банкротная процедура'
			}
			, "debtor": 
			{
				path: 'forms/orpau/debtor/c_debtor'
				, title: 'Информация о должнике в отоншении которого начата банкротная процедура'
				, keywords: 'должник процедура банкротство'
			}
			, "manager":
			{
				path: 'forms/orpau/manager/c_manager'
				, title: 'Информация об АУ'
				, keywords: 'Арбитражный управляющий'
			}
			, "main":
			{
				path: 'forms/orpau/main/c_main'
				, title: 'Главная форма портала ОРПАУ'
				, keywords: 'Главная страница профсоюз арбитражные управляющие'
			}
			, "root":
			{
				path: 'forms/orpau/root/c_root'
				, title: 'Начальная форма портала ОРПАУ'
				, keywords: 'Начальная страница профсоюз арбитражные управляющие'
			}
			, "debtors":
			{
				path: 'forms/orpau/debtors/c_debtors'
				, title: 'Должники в отоншении которых начаты банкротные процедуры'
				, keywords: 'должники процедуры банкротство'
			}
			, "confirmations":
			{
				path: 'forms/orpau/confirmations/c_confirmations'
				, title: 'Справки о полномочиях АУ'
				, keywords: 'арбитражные управляющий полномочия справки'
			}
			, "managers":
			{
				path: 'forms/orpau/managers/c_managers'
				, title: 'Арбитражные управляющие'
				, keywords: 'АУ'
			}
			, "login":
			{
				path: 'forms/orpau/login/c_login_by_cert'
				, title: 'Авторизация сертификатом АУ'
			}
			, "asp-accession":
			{
				path: 'forms/orpau/asp/c_asp_agreement'
				, title: 'Подписание заявления о присоединении к АСП'
			}
			, "union-application":
			{
				path: 'forms/orpau/club/c_club_agreement'
				, title: 'Подписание заявления о вступлении в профсоюз'
			}
			, "profile":
			{
				path: 'forms/orpau/profile/c_profile'
				, title: 'Профиль АУ'
			}
			, "login_choose":
			{
				path: 'forms/orpau/login/choose/c_login_choose'
				, title: 'Выбор сертификата и/или АУ для аутентификации'
			}
			, "sign":
			{
				path: 'forms/orpau/login/sign/c_ext_sign'
				, title: 'Подписание документа'
			}
		}

		, content: {
			  "orpau_confirmation-1": { 
					path: 'txt!forms/orpau/confirmation/tests/contents/example1.json.txt'
					, title: 'Справка о полномочиях АУ с номером'
				}
			, "orpau_confirmation-line80": {
					path: 'txt!forms/orpau/confirmation/tests/contents/line80.json.txt'
					, title: 'Справка о полномочиях АУ с эталонными строками'
				}
			, "orpau_confirmation-no-number": {
					path: 'txt!forms/orpau/confirmation/tests/contents/example2-no-number.json.txt'
					, title: 'Справка о полномочиях АУ без номера'
				}

			, "login_tokens-example1": {
					path: 'txt!forms/orpau/login/tests/contents/example1.json.txt'
				}
			, "login_tokens-example2": {
					path: 'txt!forms/orpau/login/tests/contents/example2.json.txt'
				}
			, "asp-accession-example1": {
					path: 'txt!forms/orpau/asp/tests/contents/example1.json.txt'
				}

			,"debtor-1":                     { path: 'txt!forms/orpau/debtor/tests/contents/example1.json.txt' }
			, "manager-1":                   { path: 'txt!forms/orpau/manager/tests/contents/example1.json.txt' }
			, "award_voting_example1":       { path: 'txt!forms/award/voting/tests/contents/example1.json.txt' }

			, "profile-01sav": { path: 'txt!forms/orpau/profile/tests/contents/01sav.json.etalon.txt' }
			, "profile-example1": { path: 'txt!forms/orpau/profile/tests/contents/example1.json.txt' }
			, "sign-example1": { path: 'txt!forms/orpau/login/sign/tests/contents/example1.json.txt' }
		}
	};
	return h_spec.combine(orpau_specs, spec_orpau_cabinet, spec_orpau_admin);
});