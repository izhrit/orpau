﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/admin/login/e_admin_login.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;

		var controller = c_fastened(tpl);
		controller.service_url= base_url + '?action=admin.auth'

		controller.rights_to_check= (!options_arg || !options_arg.rights_to_check) ? 'admin' : options_arg.rights_to_check;

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.login').click(function () { self.OnLogin(); });
		}

		controller.OnLogin = function ()
		{
			var login_data= this.GetFormContent();
			var self = this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос авторизации на сервере", this.service_url + '&cmd=login');
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						h_msgbox.ShowModal({
							title: 'Неудачная аутентификация', width: 500
							, html: 'Вы ввели неправильное имя пользователя или пароль!'
						});
					}
					else if (false == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка аутентификации', width: 450
							, html: '<span>Не удалось провести аутентификацию по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
						});
					}
					else if (!responce.rights || !responce.rights[self.rights_to_check])
					{
						h_msgbox.ShowModal({
							title: 'Недостаточно прав', width: 500
							, html: 'У аутентифицированного пользователя нет доступа в административную часть!'
						});
					}
					else
					{
						if (self.OnOkLogin)
							self.OnOkLogin(responce);
					}
				}
			});
		}

		return controller;
	}
});