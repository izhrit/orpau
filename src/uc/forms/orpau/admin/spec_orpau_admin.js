define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var orpau_admin_specs = {

		controller: {
			"admin_login": 
			{
				path: 'forms/orpau/admin/login/c_admin_login'
				, title: 'Вход в административную часть'
			}
			,"admin_main": 
			{
				path: 'forms/orpau/admin/main/c_admin_main'
				, title: 'Главная страница административной части ОРПАУ'
			}
			,"admin": 
			{
				path: 'forms/orpau/admin/root/c_admin'
				, title: 'Административная часть ОРПАУ'
			}
			,"admin_polls": 
			{
				path: 'forms/orpau/admin/polls/c_admin_polls'
				, title: 'Настройки голосований в административной части ОРПАУ'
			}
			,"admin_poll": 
			{
				path: 'forms/orpau/admin/poll/c_admin_poll'
				, title: 'Настройки голосования в административной части ОРПАУ'
			}
			,"admin_group": 
			{
				path: 'forms/orpau/admin/group/c_admin_group'
				, title: 'Настройки группы в административной части ОРПАУ'
			}
			,"admin_groups": 
			{
				path: 'forms/orpau/admin/groups/c_admin_groups'
				, title: 'Настройки групп в административной части ОРПАУ'
			}
			,"admin_question":
			{
				path: 'forms/orpau/admin/question/c_question'
				, title: 'Настройки вопроса голосования'
			}
			,"admin_questions":
			{
				path: 'forms/orpau/admin/questions/c_questions'
				, title: 'Настройки вопросов голосования'
			}
			,"admin_question_variants":
			{
				path: 'forms/orpau/admin/question/variants/c_question_variants'
				, title: 'Варианты ответа на вопрос голосования'
			}
			,"admin_manager":
			{
				path: 'forms/orpau/admin/manager_vote/main/c_admin_manager'
				, title: 'Просмотр информации о голосовании АУ'
			}
			,"admin_managers":
			{
				path: 'forms/orpau/admin/managers/c_admin_managers'
				, title: 'Просмотр АУ'
			}
		}

		, content: {
			"poll-question-variants-01sav":
			{
				path: 'txt!forms/orpau/admin/question/variants/tests/contents/01sav.json.etalon.txt'
			}
			,"poll-question-01sav":
			{
				path: 'txt!forms/orpau/admin/question/tests/contents/01sav.json.etalon.txt'
			}
			,"poll-questions-01sav":
			{
				path: 'txt!forms/orpau/admin/questions/tests/contents/01sav.json.etalon.txt'
			}
			,"poll-01sav":
			{
				path: 'txt!forms/orpau/admin/poll/tests/contents/01sav.json.etalon.txt'
			}
			,"group-01sav":
			{
				path: 'txt!forms/orpau/admin/group/tests/contents/01sav.json.etalon.txt'
			}
			,"admin-manager-example1":
			{
				path: 'txt!forms/orpau/admin/manager_vote/main/tests/contents/manager1.json.txt'
			}
		}
	};
	return h_spec.combine(orpau_admin_specs);
});