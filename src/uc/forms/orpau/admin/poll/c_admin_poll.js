﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/admin/poll/e_admin_poll.html'
	, 'forms/orpau/admin/questions/c_questions'
	, 'forms/base/h_validation_msg'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_questions, h_validation_msg, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var orpau_status = base_url + '?action=poll.status';

		var options = {
			field_spec:
				{
					"Из.СРО": { ajax: { url: base_url + '?action=sro.select2', dataType: 'json' } }
					,"Из.региона": { ajax: { url: base_url + '?action=region.select2', dataType: 'json' } }
					,"Из.группы": { ajax: { url: base_url + '?action=group.select2', dataType: 'json' } }
					,"Вопросы": { controller: c_questions }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:700,height:600};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' select.type').change(function () { self.OnChangeType(); }).trigger('change');

			$(sel + ' button.start').click(function () {
				if(!self.model || !self.model.id_Poll){
					self.OnAddAndStart();
				} else {
					self.OnChangeStatus('start'); 
				}
			});
			$(sel + ' button.pause').click(function () { self.OnChangeStatus('pause'); });
			$(sel + ' button.stop').click(function () { self.OnChangeStatus('stop'); });
		}

		controller.OnChangeType = function ()
		{
			var sel= this.fastening.selector;
			var type= $(sel + ' select.type').val();
			$(sel + ' div.cpw-form-orpau-admin-poll').attr('data-type',type);
			var questionsWrap = 'div.cpw-orpau-poll-questions-form div.questions';
			var статусГолосование = this.model && this.model.Состояние && (this.model.Состояние == 'Голосование' || this.model.Состояние == 'Продолжено');
			if(type == 'все') {
				$(sel + ' ' + questionsWrap).css({'max-height': статусГолосование ? '200px' : '240px'});
			} else {
				$(sel + ' ' + questionsWrap).css({'max-height': статусГолосование ? '170px' : '200px'});
			}
		}

		controller.OnChangeStatus = function(status, id) {
			var self = this;
			var id_Poll = id ? id : self.model.id_Poll;
			var url = '';
			var msgboxTitle = '';
			switch(status){
				case 'start': 
					url = orpau_status + '&cmd=start&id=' + id_Poll;
					msgboxTitle = "Рассылка уведомлений и начало голосования на сервере";
					break;
				case 'pause':
					url = orpau_status + '&cmd=pause&id=' + id_Poll;
					msgboxTitle = "Рассылка уведомлений о приостановке голосования";
					break;
				case 'stop':
					url = orpau_status + '&cmd=stop&id=' + id_Poll;
					msgboxTitle = "Рассылка уведомлений об отмене голосования";
					break;
			}
			var v_ajax = h_msgbox.ShowAjaxRequest(msgboxTitle, url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						$('#'+options_arg.modal_div).dialog("close");
						self.OnEdit(id_Poll);
					}
				}
			});
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res= base_GetFormContent.call(this);
			switch (res.Голосуют)
			{
				case 'все': 
					delete res.Из;
					break
				case 'регион':
					delete res.Из.СРО;
					delete res.Из.группы;
					break
				case 'сро':
					delete res.Из.региона;
					delete res.Из.группы;
					break
				case 'группа':
					delete res.Из.СРО;
					delete res.Из.региона;
					break
			}
			if(!res.Состояние) res.Состояние = 'Создано';
			return res;
		}

		var check_date_dd_mm_yyyyy= function(str)
		{
			var regexp = /^(\d{2})\.(\d{2})\.(\d{4})$/;
			return regexp.test(str);
		}

		var check_time_hh_mm = function (str)
		{
			var regexp = /^(\d{2})\:(\d{2})$/;
			return regexp.test(str);
		}

		controller.Validate = function ()
		{
			var res = null;
			var model = this.GetFormContent();

			if(model.Название.trim() == '')
				res = h_validation_msg.error(res, "Название голосования не может быть пустым");
			if (!check_date_dd_mm_yyyyy(model.Завершение.Дата))
				res = h_validation_msg.error(res, "Дата завершения (окончания приёма бюллетеней) должна быть указана в форме ДД.ММ.ГГГГ");
			if (!check_time_hh_mm(model.Завершение.Время))
				res = h_validation_msg.error(res, "Время завершения (окончания приёма бюллетеней) должно быть указано в форме ЧЧ:ММ");
			if(model.Вопросы.length == 0)
				res = h_validation_msg.error(res, "Список вопросов не может быть пустым");

			return res;
		}

		return controller;
	}
});