﻿define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/admin/manager_vote/vote/e_admin_manager_vote.html'
	, 'forms/orpau/cabinet/voting/questions/c_voting_questions'
	, 'forms/orpau/cabinet/voting/documents/c_voting_documents'
	, 'forms/orpau/cabinet/voting/log/c_voting_log'
],
function (c_fastened, tpl, c_voting_questions, c_voting_documents, c_voting_log)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;

		var options = {
			field_spec:
				{
					Вопросы: { controller: function () { return c_voting_questions({ base_url: base_url, disable_grid_selecting: true }); }, render_on_activate:true }
					, Документы: { controller: function () { return c_voting_documents({ base_url: base_url }); }, render_on_activate:true }
					, Журнал: { controller: function () { return c_voting_log({ base_url: base_url }); }, render_on_activate:true }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:900,height:760};

		return controller;
	}
});