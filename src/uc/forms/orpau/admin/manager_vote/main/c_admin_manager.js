﻿define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/admin/manager_vote/main/e_admin_manager.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/admin/manager_vote/vote/c_admin_manager_vote'
],
function (c_fastened, tpl, h_msgbox, c_admin_manager_vote)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_url_select2 = base_url + '?action=poll.select2';

		var options = {
			field_spec:
				{
					Голосования: { ajax: { url: base_url_select2 + '&id_Manager=' + (!options_arg ? '' : options_arg.id_Manager), dataType: 'json' } }
				}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:900,height:760};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;

			$(sel + ' [data-fc-selector="Голосования"]').on('change', function (e) { self.onLoadPoll(); });

			if (self.model && self.model.id_Manager)
				this.setLastPoolId();
		}

		controller.onLoadPoll = function()
		{
			var self = this;
			var sel = this.fastening.selector;
			var pollId = $(sel + ' [data-fc-selector="Голосования"]').select2("val");

			if (null != this.controller)
			{
				this.controller.Destroy();
				this.controller = null;
			}
			this.controller = c_admin_manager_vote({ base_url: base_url });
			this.controller.SetFormContent(
				{ 
					id_Manager: self.model.id_Manager,
					ArbitrManagerID: self.model.ArbitrManagerID,
					id_Poll: pollId
				}
			);
			this.controller.Edit(sel + ' div.cpw-orpau-poll-content');
		}

		controller.setLastPoolId = function()
		{
			var self = this;
			var sel = this.fastening.selector;
			var url = base_url + '?action=poll.last-for-manager&id_Manager=' + self.model.id_Manager;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос данных голосования на сервере", url);

			v_ajax.ajax({
				dataType: "json", type: 'GET', cache: false
				, success: function (responce, textStatus)
				{
					if (null!=responce)
						$(sel + ' [data-fc-selector="Голосования"]').select2('data', responce).trigger('change');
				}
			});
		}
		
		return controller;
	}
});