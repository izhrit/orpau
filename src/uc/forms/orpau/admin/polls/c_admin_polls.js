﻿define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/admin/polls/e_admin_polls.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/admin/poll/c_admin_poll'
	, 'forms/base/h_validation_msg'
	, 'tpl!forms/orpau/admin/polls/v_admin_poll_delete_confirm.html'
	, 'forms/orpau/admin/results/c_admin_poll_results'
],
function (c_fastened, tpl, h_msgbox, c_admin_poll, h_validation_msg, v_poll_delete_confirm, c_admin_poll_results)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_grid_url = controller.base_url + "?action=poll.jqgrid";
		controller.base_crud_url = controller.base_url + "?action=poll.crud";
		controller.base_result_url = controller.base_url + "?action=poll.result";

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;
			this.AddButtonToPagerLeft('cpw-orpau-admin-polls-grid-pager', 'Зарегистрировать голосование', function () { self.OnAdd(); });
		}

		controller.colModel =
			[
				{ name: 'id_Poll', hidden: true }
				, { label: 'Название', name: 'Name', width: 100 }
				, { label: ' ', name: 'myac', width: 20, align: 'right', search: false}
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны голосования: {1} из {2}'
				, emptyText: 'Нет голосований для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10

				, pager: '#cpw-orpau-admin-polls-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnEdit(); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка голосований", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				  { "class": "edit-admin-poll", text: "Редактировать", click: function () { self.OnEdit(); } }
				  , { "class": "result-admin-poll", text: "Посмотреть результаты", click: function () { self.OnResult(); } }
				  , { "class": "delete-admin-poll", text: "Удалить", click: function () { self.OnRemove(); } }
				];
		};

		controller.OnAdd = function ()
		{
			var self= this;
			var cc_poll= c_admin_poll({base_url:this.base_url});
			cc_poll.OnAddAndStart = function(){
				h_validation_msg.IfOkWithValidateResult(cc_poll.Validate(), function () { 
					var poll= cc_poll.GetFormContent();
					self.DoAdd(poll, true);
					$('#'+id_div).dialog("close");
				});
			}
			var id_div = 'cpw-orpau-admin-polls-do-add';
			var btnOk= "Сохранить";
			h_msgbox.ShowModal({
				title:"Регистрация голосования"
				,controller:cc_poll
				,id_div: id_div
				,buttons:[btnOk,"Отмена"]
				, onclose: function (btn, dlg_div)
				{
					if (btn == btnOk)
					{
						h_validation_msg.IfOkWithValidateResult(cc_poll.Validate(), function () { 
							var poll= cc_poll.GetFormContent();
							self.DoAdd(poll);
							$(dlg_div).dialog("close");
						});
						return false;
					}
				}
			})
		}

		controller.OnStartPoll = function(id_Poll){
			var self = this;
			var url = this.base_url + '?action=poll.status&cmd=start&id=' + id_Poll;
			var msgboxTitle = "Рассылка уведомлений и начало голосования на сервере";
			var v_ajax = h_msgbox.ShowAjaxRequest(msgboxTitle, url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					} else {
						self.OnEdit(id_Poll)
					}
				}
			});
		}

		controller.DoAdd = function (poll, startPoll)
		{
			var self= this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Сохранение данных о голосовании", this.base_crud_url + '&cmd=add');
			v_ajax.ajax({
				dataType: "json", type: 'POST', data: poll, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.ReloadGrid();
						if(startPoll){
							self.OnStartPoll(responce.id_Poll);
						}
					}
				}
			});
		}

		controller.OnEdit = function (id_Poll)
		{
			var sel= this.fastening.selector;
			if(id_Poll){
				var selected_poll_id = id_Poll;
			} else {
				var grid = $(sel + ' table.grid');
				var selrow = grid.jqGrid('getGridParam', 'selrow');
				var rowdata = grid.jqGrid('getRowData', selrow);
				var selected_poll_id = rowdata.id_Poll;
				$(sel + " ul.jquery-menu").hide();
			}

			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=get&id=' + selected_poll_id;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных голосования с сервера", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.DoEdit(selected_poll_id, data);
					}
				}
			});
		}

		controller.DoEdit = function (id_Poll, poll)
		{
			var self= this;
			var id_div = 'cpw-orpau-admin-polls-do-edit';
			var cc_poll= c_admin_poll({base_url:this.base_url, modal_div: id_div});
			cc_poll.OnEdit = function(id_Poll){
				self.OnEdit(id_Poll);
			}
			cc_poll.SetFormContent(poll);
			var btnOk= "Сохранить";

			h_msgbox.ShowModal({
				title:'Настройки голосования'
				,controller:cc_poll
				,id_div: id_div
				,buttons: poll.Состояние == 'Создано' || poll.Состояние == 'Приостановлено' ? [btnOk,"Закрыть"] :['Закрыть']
				, onclose: function (btn, dlg_div)
				{
					if (btnOk == btn)
					{
						h_validation_msg.IfOkWithValidateResult(cc_poll.Validate(), function () { 
							var edited_poll= cc_poll.GetFormContent();
							self.onConfirmChange(id_Poll, edited_poll, dlg_div);
						});
						return false;
					}
				}
			});
		}

		controller.onConfirmChange = function(id_Poll, edited_poll, parent_dlg_div) {
			var self = this;
			var btnOk = 'Да, сохранить изменения и сбросить данные по голосованию';
			h_msgbox.ShowModal
			({
				title: 'Подтверждение изменения голосования'
				, html: '<span style="font-weight: 600;">'
					+ 'При сохранении изменений голосования, все голоса АУ сбросяться!<br/>'
					+ 'Вы действительно хотите изменить голосование?</span>'
				, width: 600
				, id_div: "cpw-form-orpau-poll-change-confirm"
				, buttons: [btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.Update(id_Poll, edited_poll);
						$(parent_dlg_div).dialog("close");
					}
				}
			});
		}

		controller.Update = function (id_Poll,poll)
		{
			var self= this;
			var url= this.base_crud_url + '&cmd=update&id=' + id_Poll;
			var v_ajax= h_msgbox.ShowAjaxRequest("Сохранение данных о голосовании", url);
			v_ajax.ajax({
				dataType: "json", type: 'POST', data: poll, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						h_msgbox.ShowAjaxError('Ошибка сохранения голосования', url, poll, textStatus);
					}
					else
					{
						self.ReloadGrid();
					}
				}
			});
		}

		controller.OnRemove = function ()
		{
			var sel= this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
			({
				title: 'Подтверждение удаления' // заявлений
				, html: v_poll_delete_confirm(rowdata)
				, width: 600//, height: 250
				, id_div: "cpw-form-orpau-poll-delete-confirm"
				, buttons: [btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.Remove(rowdata.id_Poll);
					}
				}
			});
		}

		controller.Remove = function (id_Poll)
		{
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + id_Poll;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаления данных о заявлении с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
					}
				}
			});
		}

		controller.OnResult = function () {
			var self = this;
			var sel= this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			$(sel + " ul.jquery-menu").hide();

			var ajaxurl = this.base_result_url + '&cmd=get&id=' + rowdata.id_Poll;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных голосования с сервера", ajaxurl);
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.DoResult(data, rowdata.id_Poll);
					}
				}
			});
		}

		controller.DoResult = function(data, id_Poll) {
			var self= this;
			var cc_poll_res= c_admin_poll_results({base_url:this.base_url});
			cc_poll_res.SetFormContent(data);

			var btnOk= "Сохранить результаты и уведомить о них участников";
			var id_div = "cpw-form-orpau-poll-result";

			var Состояние = !data || !data.Состояние ? 'Голосование' : data.Состояние;
			var buttons = Состояние == 'Голосование' || Состояние == 'Продолжено' || Состояние == 'Приостановлено' ? [btnOk,"Отмена"] : ["Закрыть"]

			h_msgbox.ShowModal({
				title:'Результаты голосования'
				,controller:cc_poll_res
				,width: 900//, height: 500
				,id_div: id_div
				,buttons: buttons
				,onclose: function (btn, dlg_div)
				{
					if (btnOk == btn)
					{
						var результаты = cc_poll_res.Результаты();
						self.SaveResult(результаты, id_Poll);
						//return false;
					}
				}
			});
			$('#'+id_div).dialog("option", "position", {my: "center", at: "center", of: window});
		}

		controller.SaveResult = function(результаты, id_Poll)
		{
			var self = this;
			var url = this.base_result_url + '&cmd=update&id=' + id_Poll;
			var v_ajax = h_msgbox.ShowAjaxRequest("Передача подведённых итогов заседания на сервер", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: { результаты: результаты }
				, cache: false
				, success: function (responce, textStatus)
				{
					self.OnFixedResults();
				}
			});
		}

		controller.OnFixedResults= function()
		{
			h_msgbox.ShowModal({
				title: 'Сообщение о сохранении итогов'
				, id_div: "cpw-form-ama-committee-results-fixed-msg"
				, html: 'Итоги голосования подведены'
			});
		}

		return controller;
	}
});