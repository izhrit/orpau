define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/admin/group/e_admin_group.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
			
			var base_url_select2 = base_url + '?action=manager.select2';

			var options = {
				field_spec:
					{
						Состав_АУ: { 
							multiple: true, 
							ajax: { url: base_url_select2, dataType: 'json' },
							formatResult: formatState,
							formatSelection: formatSelection,
							sortResults: sortResults,
							width: '100%',
							placeholder: "Добавить АУ",
							initSelection : function (element, callback) {}
						}
					}
		};
		
		function formatState (state) {
			if (!state.id) {
			return state.text;
			}

			var markup = '<div class="member-row">';
			markup += '<div class="member-name">' + state.text + '</div>';
			if(state.inn) markup += '<div class="member-inn">ИНН: ' + state.inn + '</div>';
			if(state.sro) markup += '<div class="member-sro">СРО: ' + state.sro + '</div>';
			markup += '</div>';

			return $(markup);
		};

		function formatSelection (state) {
			var result = '';
			result = state.region ? ' (' + state.region : '';
			result += state.sro ? (result.length ? ', ' + state.sro : ' (' + state.sro) : '';
			result += result.length ? ')' : '';
			return state.text + result
		}

		function sortResults(results) {
			return results.sort(function(a, b) {
				if(a.lastName > b.lastName) return 1;
				if(a.lastName < b.lastName) return -1;
				return 0;
			});
		}


		var controller = c_fastened(tpl, options);

		controller.size = {width:500,height:600};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;

			$(sel + ' div.select-members').on('change', function(){
				var $selected = $(sel + ' div.select-members').find('.select2-search-choice').hide();
				$(sel + ' .members-count .count-field').text($selected.length);

				var $container = $(sel + ' .selected-members');
				var $list = $('<ul class="selected-members-list">');

				$selected.each(function(index, item) {
					var $li = $('<li class="tag-selected">');
					var $a = $('<a href="#" onclick="return false;" class="select2-search-choice-close">×</a>');
					$a.off('click.select2-copy').on('click.select2-copy', function(e){
						var select2Values = $(sel + ' div.select-members').select2('val');
						select2Values.splice(index, 1);
						$(sel + ' div.select-members').select2('val', select2Values);
						$($selected[index]).remove();
						$(sel + ' div.select-members').trigger('change');
					});
					$li.append($a).append($(item).text());
					$list.append($li);
				});
				
				$container.html('').append($list);
			}).trigger('change');
		}

		return controller;
	}
});