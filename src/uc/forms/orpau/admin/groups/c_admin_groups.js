﻿define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/admin/groups/e_admin_groups.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/admin/group/c_admin_group'
	, 'tpl!forms/orpau/admin/groups/v_admin_group_delete_confirm.html'
],
function (c_fastened, tpl, h_msgbox, c_admin_group, v_admin_group_delete_confirm)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_crud_url = controller.base_url + "?action=group.crud";
		controller.base_grid_url = controller.base_url + "?action=group.jqgrid";

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;
			this.AddButtonToPagerLeft('cpw-orpau-admin-groups-grid-pager', 'Зарегистрировать группу', function () { self.OnAdd(); });
		}

		controller.colModel =
			[
				{ name: 'id_Group', hidden: true }
				, { label: 'Название', name: 'Name', width: 50 }
				, { label: ' ', name: 'myac', width: 20, align: 'right', search: false}
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны группы: {1} из {2}'
				, emptyText: 'Нет групп для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10

				, pager: '#cpw-orpau-admin-groups-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.onSelectRow_if_no_menu(id, s, e, function () { self.OnEdit(id); });
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка групп", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.grid_row_buttons = function ()
		{
			var self = this;
			return [
				  { "class": "edit-admin-group", text: "Редактировать", click: function () { self.OnEdit(); } }
				, { "class": "delete-admin-group", text: "Удалить", click: function () { self.OnRemove(); } }
			];
		};

		controller.OnEdit= function (rowid)
		{
			var grid = $(this.fastening.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			var id_Group = rowdata.id_Group;

			var url= this.base_crud_url + '&cmd=get&id=' + id_Group;
			var self = this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос данных о группе", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						h_msgbox.ShowAjaxErro('Не удалось получить данные по группе', url, responce, textStatus);
					}
					else
					{
						responce.Состав_АУ.sort(function(a, b) {
							if(a.lastName > b.lastName) return 1;
							if(a.lastName < b.lastName) return -1;
							return 0;
						});
						self.GroupModal('update', 'Редактирование группы', responce, id_Group);
					}
				}
			});
		}
		
		controller.OnAdd = function ()
		{
			this.GroupModal('add', 'Регистрация группы')
		}

		controller.GroupModal = function(cmd, title, content, id_Group)
		{
			var self = this;
			var cc_admin_group = c_admin_group({base_url:this.base_url});
			if(content) cc_admin_group.SetFormContent(content);
			var saveBtn = 'Сохранить';
			var cancelBtn = 'Отмена';
			h_msgbox.ShowModal({
				title: title
				, id_div: 'id-admin-group-new'
				, controller: cc_admin_group
				, buttons: [saveBtn, cancelBtn]
				, onclose: function(btn, me)
				{
					if (btn == saveBtn)
					{
						var model = cc_admin_group.GetFormContent();

						var title = "Укажите данные для группы";
						if(!model.Name) {
							h_msgbox.ShowModal({ title: title, width: 400, html: "Введите название группы" });
							return false;
						}

						if(!model.Состав_АУ.length) {
							h_msgbox.ShowModal({ title: title, width: 400, html: "Выберите АУ, которые будет входить в состав группы" });
							return false;
						}
						var url= self.base_crud_url + '&cmd=' + cmd;
						if (id_Group)
							url+= '&id=' + id_Group;
						var v_ajax= h_msgbox.ShowAjaxRequest("Сохранение данных о группе", url);
						v_ajax.ajax({
							dataType: "json"
							, type: 'POST'
							, data: model
							, cache: false
							, success: function (responce, textStatus)
							{
								if (null == responce)
								{
									h_msgbox.ShowAjaxError('Ошибка сохранения голосования', url, responce, textStatus);
								}
								else
								{
									self.ReloadGrid();
								}
							}
						});
					}
				}
			});
		}

		controller.OnRemove = function ()
		{
			var sel= this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var self = this;
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
			({
				title: 'Подтверждение удаления' // группы
				, html: v_admin_group_delete_confirm(rowdata)
				, width: 600//, height: 250
				, id_div: "cpw-form-orpau-group-delete-confirm"
				, buttons: [btnOk, 'Отмена']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						self.Remove(rowdata.id_Group);
					}
				}
			});
		}

		controller.Remove = function (id_Poll)
		{
			var self = this;
			var ajaxurl = this.base_crud_url + '&cmd=delete&id=' + id_Poll;
			var v_ajax = h_msgbox.ShowAjaxRequest("Удаления данных о группе с сервера", ajaxurl);
			v_ajax.ajax
			({
				  dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ReloadGrid();
					}
				}
			});
		}

		return controller;
	}
});