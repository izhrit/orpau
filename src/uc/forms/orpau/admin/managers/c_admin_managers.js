﻿define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/admin/managers/e_admin_managers.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/admin/manager_vote/main/c_admin_manager'
],
function (c_fastened, tpl, h_msgbox, c_admin_manager_vote)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_url_select2 = base_url + '?action=group.select2';

		var options = {
			field_spec:
				{
					Группы: { 
						ajax: { url: base_url_select2, dataType: 'json' },
						allowClear: true,
						placeholder: 'группа не выбрана, показаны все АУ, и не учавствующие в группах в том числе'
					}
				}
		};

		var controller = c_fastened(tpl, options);
		controller.base_url = base_url;
		controller.base_crud_url = controller.base_url + "?action=manager.crud";
		controller.base_grid_url = controller.base_url + "?action=manager.jqgrid";

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;

			$(sel + ' .admin-groups-select2').on('change', function(e) { self.OnSelect2Changed(e); });
		}

		controller.colModel =
			[
				{ name: 'id_Manager', hidden: true }
				, { label: 'Фамилия', name: 'LastName', width: 50 }
				, { label: 'Имя', name: 'FirstName', width: 50 }
				, { label: 'Отчество', name: 'MiddleName', width: 50 }
				, { label: 'ИНН', name: 'INN', width: 30 }
				, { label: 'СРО', name: 'SRO', width: 50 }
				, { label: 'Регион', name: 'Region', width: 40 }
				, { name: 'id_Group', hidden: true }
				, { name: 'ArbitrManagerID', hidden: true }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны АУ: {1} из {2}'
				, emptyText: 'Нет АУ для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10

				, pager: '#cpw-orpau-admin-managers-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true
				, onSelectRow: function (id, s, e) 
				{ 
					self.OnEdit(id);
				}
				, loadComplete: function () { self.RenderRowActions(grid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка АУ", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false, odata : ['equal'] });
		}

		controller.OnSelect2Changed = function (e)
		{
			var sel= this.fastening.selector;
			$(sel + ' input#gs_id_Group').val(e.val).trigger('keydown');
		}

		controller.OnEdit= function (rowid)
		{
			var grid = $(this.fastening.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var id_Manager = rowdata.id_Manager;

			var self = this;
			var manager_data = {
				ArbitrManagerID: rowdata.ArbitrManagerID,
				id_Manager: id_Manager,
				inn: rowdata.INN
			}
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос данных голосования на сервере", self.base_crud_url + '&cmd=get&id=' + id_Manager);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: manager_data
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						var cc_admin_manager_vote = c_admin_manager_vote({base_url:self.base_url, id_Manager: id_Manager});
						cc_admin_manager_vote.SetFormContent(responce)
						h_msgbox.ShowModal({
							title: 'Голоса арбитражного управляющего'
							, id_div: 'id-admin-manager-voting'
							, controller: cc_admin_manager_vote
							, buttons:['Закрыть']
						})
					}
				}
			});
		}
		
		return controller;
	}
});