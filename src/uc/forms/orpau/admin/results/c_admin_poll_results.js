define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/admin/results/e_admin_poll_results.html'
	, 'forms/orpau/admin/results/h_admin_poll_results'
	, 'forms/orpau/admin/results/details/c_poll_result_details'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_admin_poll_results, c_poll_result_details, h_msgbox)
{
	return function (options_arg)
	{
        var controller = c_fastened(tpl, { h_admin_poll_results: h_admin_poll_results });
        controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;

        var base_Render= controller.Render;
        controller.Render = function (sel)
        {
            base_Render.call(this,sel);

            var self= this;
            $(sel + ' table.questions input[type="radio"]').change(function (e) { self.OnRadio(e); });
            $(sel + ' input[name="poll-took-place"]').change(function (e) { self.OnRadioTookPlace(e); });
			$(sel + ' table.questions a.details').click(function (e) { self.OnDetails(e); });
			
			$(sel + ' button.bulletins').click(function () { self.OnPrintBulletins(); });
			$(sel + ' button.protocol').click(function () { self.OnPrintProtocol(); });
        }

        controller.OnRadio= function(e)
		{
			var r = $(e.target);
			var tr = r.parents('tr');
			tr.removeClass('decision-made');
			tr.removeClass('decision-is-not-made');

			tr.addClass(tr.find('input:checked').attr('value'));
        }
        
        controller.OnRadioTookPlace = function (e)
		{
			var sel= this.fastening.selector;
			var d = $(sel + ' div.cpw-form-orpau-admin-polls-result');
			d.removeClass('took-place');
			d.removeClass('did-not-took-place');
			var cls = d.find('input[name="poll-took-place"]:checked').attr('value');
			d.addClass(cls);
			if ('did-not-took-place' == cls)
				$(sel + ' div.comments textarea').focus();
        }

        var найти_вопрос= function(Вопросы, номер)
		{
			for (var i= 0; i<Вопросы.length; i++)
			{
				var вопрос= Вопросы[i];
				if (номер==вопрос.Номер || !вопрос.Номер && (i+1)==номер)
					return вопрос;
			}
			return null;
        }
        
        var СобратьДеталиПоВопросу= function(номер, вопрос)
		{
			var res = {
				Вопрос: {
					Номер: номер
					, На_голосование: вопрос.На_голосование
				}
				,Голоса:[]
			};

			var Голосование = вопрос.Голосование;

			if (Голосование && null != Голосование)
			{
				for (var i = 0; i < Голосование.length; i++)
				{
					var голос_участника = Голосование[i];
					var текст_ответа = голос_участника.Ответ;

					if (текст_ответа && null != текст_ответа)
						res.Голоса.push({ Участник: голос_участника.ФИО, Ответ: текст_ответа });
				}
				return res;
			}
        }
   
        controller.OnDetails= function(e)
		{
			var a = $(e.target);
			var tr = a.parents('tr');
			var номер = tr.attr('number');

			var model = this.model;

			var вопрос= найти_вопрос(model.Вопросы, номер);
			var details = СобратьДеталиПоВопросу(номер, вопрос);

			var c_details = c_poll_result_details();
			c_details.SetFormContent(details);
			var id_div = "cpw-form-orpau-poll-result-details-msg";
			h_msgbox.ShowModal
			({
				title: 'Детали голосования по вопросу №' + номер
				, controller: c_details
				, id_div: id_div
				, buttons: ['Закрыть детали голосования']
			});
			$('#'+id_div).dialog("option", "position", {my: "center", at: "center", of: window});
		}

		//todo
		controller.OnPrintBulletins = function()
		{
			console.log('OnPrintBulletins');
			// window.open('ui-backend.php?action=meeting.bulletins&id_Meeting=' + this.model.id_Meeting,
			// 	'ama-committee-bulletins', 'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
		}

		controller.OnPrintProtocol = function()
		{
			console.log('OnPrintProtocol');
			// window.open('ui-backend.php?action=meeting.protocol&id_Meeting=' + this.model.id_Meeting,
			// 	'ama-committee-protocol', 'width=1000,height=700,resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes');
		}

		controller.Результаты = function ()
		{
			var sel = this.fastening.selector;

			var результаты = [];

			$(sel + ' tr.question').each(function (index)
			{
				var tr = $(this);
				var результат = { По_вопросу: { id: tr.attr('question-id'), Формулировка: tr.find('span.question').text() } };
				if (tr.hasClass('decision-is-not-made'))
				{
					результат.Решение_НЕ_принято = true;
				}
				else
				{
					результат.Принято_решение = tr.find('textarea').val();
				}
				результаты.push(результат);
			});

			return результаты;
		}

        return controller;
    }
})