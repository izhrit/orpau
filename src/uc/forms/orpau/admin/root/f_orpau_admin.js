define(['forms/orpau/admin/root/c_admin'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'admin'
		, Title: 'Добро пожаловать в административную часть orpau'
	};
	return form_spec;
});