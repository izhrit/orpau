﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/admin/main/e_admin_main.html'
	, 'forms/orpau/admin/polls/c_admin_polls'
	, 'forms/orpau/admin/groups/c_admin_groups'
	, 'forms/orpau/admin/managers/c_admin_managers'
],
function (c_fastened, tpl, c_admin_polls, c_admin_groups, c_admin_managers)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;

		var options = {
			field_spec:
			{
				Голосования: { controller: function () { return c_admin_polls({ base_url: base_url }); }, render_on_activate: true }
				,Группы: { controller: function () { return c_admin_groups({ base_url: base_url }); }, render_on_activate: true }
				,АУ: { controller: function () { return c_admin_managers({ base_url: base_url }); }, render_on_activate: true }
			}
		};

		var controller = c_fastened(tpl, options);

		return controller;
	}
});