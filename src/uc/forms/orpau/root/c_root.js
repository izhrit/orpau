﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/root/e_root.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/profile/h_profile'
	, 'forms/orpau/main/c_main'
	, 'forms/orpau/login/h_login_by_cert'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, h_msgbox, h_profile, c_main, h_login_by_cert, h_check_mobile)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_auth_url = base_url + "?action=manager.auth";
		var base_login_url = base_url + '?action=manager.authPassword';

		var controller = c_fastened(tpl);

		var animate_duration= 0;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.initLoginDialog(sel);
		}

		controller.OnButtonLogin = function (dialog, dialogTitle)
		{
			if (!app.start_real_orpau)
			{
				h_msgbox.ShowModal({width: 400, title:'Авторизация', html: 'Сервис будет доступен с 17.05.2021'});
			}
			else
			{
				dialog.dialog('option', 'title', dialogTitle);
				dialog.dialog('open');
			}
		}

		controller.initLoginDialog = function(sel) {
			var self = this;
			var dialogSel = '.orpau-root-login-dialog';
			var dialogTitle = 'Выбор варианта авторизации';

			var dialog = $(sel + ' ' + dialogSel).dialog({
				title: dialogTitle,
				autoOpen: false,
				width: 400,
				modal: true,
			})

			$(sel + ' button.root-login').click(function () { self.OnButtonLogin(dialog, dialogTitle); });

			function centrizeDialog()
			{ dialog.dialog("option", "position", {my: "center", at: "center", of: window}); dialog.attr('data-calmed-down','true'); }

			$(dialogSel + ' .with-certificate').click(function(){ 
				dialog.dialog('close'); 
				self.OnLoginByCert(); 
			})
			$(dialogSel + ' .with-password').click(function(){ 
				dialog.attr('data-calmed-down','false');
				dialog.dialog('option', 'title', 'Авторизация');
				slide('right', function ()
				{
					$(dialogSel + ' .orpau-root-login-form').css('display', 'inline-block').animate({height: 'show'}, animate_duration, centrizeDialog);
				});
			})
			$(dialogSel + ' .back-button').click(function(){
				dialog.dialog('option', 'title', dialogTitle);
				slide('left', function ()
				{
					$(dialogSel + ' .orpau-root-login-form').animate({height: 'hide'}, animate_duration, centrizeDialog);
				});
			});

			$(dialogSel + ' .orpau-root-login-form').submit(function(e){ 
				e.preventDefault(); 
				self.OnLoginByPass(function(){ 
					slide('left'); 
					$(dialogSel + ' .orpau-root-login-form').animate({height: 'hide'}, animate_duration, centrizeDialog);
					dialog.dialog('close');
				}); 
			})

			$(dialogSel + ' .icon-password-toggle').click(function (e) {
				var input = $(dialogSel + ' div.password-input-container input');
				if(input.attr('type') == 'password'){
					input.prop('type', 'text');
					$(this).prop('title', 'Скрыть пароль');
				} else {
					input.prop('type', 'password');
					$(this).prop('title', 'Показать пароль');
				}
			});

			$(dialogSel + ' .forgot-password').click(function(){ h_profile.ForgotPassword(base_url); });
		}

		controller.OnLoginByCert = function ()
		{
			if (h_check_mobile.isMobile())
			{
				h_msgbox.ShowModal
					({
						title: 'Вход по ЭЦП', width: 450
						, html: '<span>С мобильного устройства возможен вход только по логину.<br/>'
							+ 'Если у вас нет логина, то зайдите с ЭЦП с компьютера и задайте его.</small>'
					});
			}
			else
			{
				var self= this;
				h_login_by_cert.Login(base_url
					, function(man_cert,variant,auth_info)
					{
						self.OnLogged(man_cert,variant,auth_info);
						h_login_by_cert.SafeOfferSetupLogin(auth_info, function () { self.mainController.OnProfile(); });
					}
				);
			}
		}

		controller.AutoLogin = function(login)
		{
			var login_data = { Login: login, Password: '' };
			var self = this;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос авто авторизации на сервере", base_login_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null != responce && false != responce.ok)
					{
						self.OnLogged('', '', '', responce, 'without_datamart');
					}
				}
			});
		}

		controller.OnLoginByPass = function(closeDialogFunc)
		{
			var login_data = $('.orpau-root-login-form').serialize();
			var self = this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос авторизации на сервере", base_login_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						h_msgbox.ShowModal({
							width: 500
							, title: 'Неудачная аутентификация'
							, html: 'Вы ввели неправильное имя пользователя или пароль!'
						});
					}
					else if (false == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка аутентификации'
							, width: 450
							, html: '<span>Не удалось провести аутентификацию по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
					else
					{
						self.OnLogged('', '', '', responce, 'without_datamart');
						closeDialogFunc();
					}
				}
			});
		}

		controller.OnLogged = function (man_cert,variant,auth_info)
		{
			var self = this;
			var sel= this.fastening.selector;

			var cmain = self.mainController = c_main({ base_url: base_url });
			cmain.Edit(sel + ' div.cpw-orpau-root-page-body');
			cmain.OnLogged(man_cert,variant,auth_info);
			$(sel + ' div.cpw-orpau-root-page').hide();

			self.mainController.OnLogoutRoot = function(){
				self.mainController.Destroy();
				delete self.mainController;
				$(sel + ' div.cpw-orpau-root-page').show();
				$(sel + ' div.cpw-orpau-root-page-body').html('');
				/* disable for popover */
				$(document).off('click');
			};
		}

		// Анимация горизонтального пролистывания
		function slide (to, complete ) {
			var $wrap = $( ".orpau-login-wrap" );
			var initalLeftMargin = +$wrap.css('margin-left').replace("px", "");
			var wrapWidth = 385;
			if (to === 'right')
			{
				var newLeftMargin = (initalLeftMargin - wrapWidth);
			}
			if (to === 'left')
			{
				var newLeftMargin = (initalLeftMargin + wrapWidth);
			}
			$wrap.animate({marginLeft: newLeftMargin}, animate_duration, complete);
		}

		return controller;
	}
});