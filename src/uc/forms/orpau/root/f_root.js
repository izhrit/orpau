define(['forms/orpau/root/c_root'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'orpau2'
		, Title: 'Добро пожаловать в систему orpau'
	};
	return form_spec;
});