﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/confirmations/e_confirmations.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/confirmation/c_confirmation'
],
function (c_fastened, tpl, h_msgbox, c_confirmation)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_crud_url = controller.base_url + '?action=confirmation.crud';
		controller.base_grid_url = controller.base_url + '?action=confirmation.jqgrid';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			return url;
		}

		controller.colModel =
		[
			{ name: 'id_Confirmation', hidden: true }
			, { label: 'Номер', name: 'number', width: 100 }
			, { label: 'Дата', name: 'date', width: 100 }
			, { label: 'АУ', name: 'manager', width: 160 }
			, { label: 'Должник:', name: 'debtor', width: 300 }
			, { label: 'ИНН', name: 'inn', width: 120 }
			, { label: 'ОГРН', name: 'ogrn', width: 150 }
			, { label: 'СНИЛС', name: 'snils', width: 115 }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = this.base_grid_url;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка справок...'
				, recordtext: 'Показаны справки: {1} из {2}'
				, emptyText: 'Нет справок для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-orpau-confirmations-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { self.OnConfirmation(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка справок", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}
			

		controller.ReloadGrid = function()
		{
			var sel = controller.fastening.selector;
			$(sel + ' table.grid').trigger('reloadGrid');
		}

		controller.Update = controller.ReloadGrid;

		controller.OnConfirmation = function (rowid) 
		{
			var self = this;
			var selectedRow = $(this.fastening.selector + ' table.grid')[0].rows[rowid];
			var managerName = selectedRow.cells[3].title;
			var debtorName = selectedRow.cells[4].title;
			var id_Confirmation = selectedRow.cells[0].title;
			var confirmation_url = this.base_crud_url + '&cmd=get&id=' + id_Confirmation;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", confirmation_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cconfirmation = c_confirmation({ base_url: self.base_url });
							cconfirmation.SetFormContent(data);
							cconfirmation.Updater = controller.Updater;
							cconfirmation.OnOrder= function()
							{
								$(self.fastening.selector).trigger('orders_change');
							}
							var title = 'Справка №'+id_Confirmation+' о полномочиях ' + managerName + ' в процедуре "' + debtorName +'"';
							h_msgbox.ShowModal
								({
									title: title
									, controller: cconfirmation
									, buttons: ['Закрыть']
									, id_div: "cpw-form-orpau-confirmation"
								});
						}
					}
				});
		}

		return controller;
	}
});