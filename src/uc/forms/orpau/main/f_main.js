define(['forms/orpau/main/c_main'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'orpau'
		, Title: 'Добро пожаловать в систему orpau'
	};
	return form_spec;
});