﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/main/e_main.html'
	, 'forms/orpau/debtors/c_debtors'
	, 'forms/orpau/confirmations/c_confirmations'
	, 'forms/orpau/managers/c_managers'
	, 'forms/orpau/cabinet/main/c_manager_cabinet'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/profile/h_profile'

	, 'forms/orpau/login/h_login_by_cert'
	, 'forms/orpau/club/h_club_agreement'
	, 'forms/orpau/asp/h_asp_agreement'
	, 'forms/base/h_validate'
],
function (c_fastened, tpl, c_debtors,  c_confirmations, c_managers, c_manager_cabinet, h_msgbox, h_profile, h_login_by_cert, h_club_agreement, h_asp_agreement, h_validate)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_auth_url = base_url + "?action=manager.auth";
		var base_login_url = base_url + '?action=manager.authPassword';
		var base_asp_agreement_url = base_url + '?action=asp.agreement';

		var options = {
			field_spec:
			{
				Должники: { controller: function () { return c_debtors({ base_url: base_url }); }, render_on_activate: true }
				, Справки: { controller: function () { return c_confirmations({ base_url: base_url }); }, render_on_activate: true }
				, АУ: { controller: function () { return c_managers({ base_url: base_url }); }, render_on_activate: true }
				//, Кабинет: { controller: function () { return c_manager_cabinet({ base_url: base_url }); } }
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.BindResizeWindow();

			var self= this;
			this.initLoginDialog(sel);

			/*$(sel + " .jquery-menu").menu();
			$(sel + " .profile-dropdown").click(function(){
				$(this).find("ul.jquery-menu").toggle();
			})*/
			
			//close menu
			/*$(document).on("click", function (e) {
				if($(e.target).hasClass("open-profile")) self.OnProfile();
				if($(e.target).hasClass("logout")) { if (self.OnLogout) { self.OnLogout(); return false; } }

				if(!$(e.target).hasClass("profile-dropdown") && !$(e.target).parent().hasClass("profile-dropdown")) {
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			$(sel + ' button.вступить-в-профсоюз').click(function(){ self.ЗаявлениеВПрофсоюз(); });
			$(sel + ' button.использовать-асп').click(function(){ self.ЗаявлениеАСП(); });*/
		}

		controller.OnProfile = function() {
			var data = this.cabinet.GetFormContent();
			h_profile.ModalEditManagerProfile(base_url, data.id_Manager, null);
		}

		controller.BindResizeWindow = function ()
		{
			if (!controller.on_resize)
			{
				var sel = controller.fastening.selector;
				controller.on_resize = function ()
				{
					if ($(document).width() < 700)
					{
						return false;
					}
					else
					{
						var $grid = $(sel + ' table.grid'),
							newWidth = $grid.closest(".ui-jqgrid").parent().width();
						if (newWidth)
							$grid.setGridWidth(newWidth, true);
					}
				};
				controller.on_resize();
			}
			window.onresize = this.on_resize;
		}

		controller.initLoginDialog = function(sel) {
			var self = this;
			var dialogSel = '.orpau-login-dialog';
			var dialogTitle = 'Выбор варианта авторизации';

			var dialog = $(sel + ' .orpau-login-dialog').dialog({
				title: dialogTitle,
				autoOpen: false,
				width: 400,
				modal: true,
				// draggable: true
			})

			$(sel + ' button.login').click(function () {
				if (self.cabinet)
				{
					self.OnLogout();
				} else {
					dialog.dialog('option', 'title', dialogTitle);
					dialog.dialog('open');
				}
			});

			function centrizeDialog() { dialog.dialog("option", "position", {my: "center", at: "center", of: window}); }

			$(dialogSel + ' .with-certificate').click(function(){ 
				dialog.dialog('close'); 
				self.OnLoginByCert(); 
			})
			$(dialogSel + ' .with-password').click(function(){ 
				dialog.dialog('option', 'title', 'Авторизация');
				slide('right');
				$(dialogSel + ' .orpau-login-form').animate({height: 'show'}, 200, centrizeDialog).css('display', 'inline-block');
				
			})
			$(dialogSel + ' .back-button').click(function(){
				dialog.dialog('option', 'title', dialogTitle);
				slide('left');
				$(dialogSel + ' .orpau-login-form').animate({height: 'hide'}, 200, centrizeDialog);
			});

			$(dialogSel + ' .orpau-login-form').submit(function(e){ 
				e.preventDefault(); 
				self.OnLoginByPass(function(){ 
					slide('left'); 
					$(dialogSel + ' .orpau-login-form').animate({height: 'hide'}, 200, centrizeDialog);
					dialog.dialog('close');
				}); 
			})

			$(dialogSel + ' .icon-password-toggle').click(function (e) {
				var input = $(dialogSel + ' div.password-input-container input');
				if(input.attr('type') == 'password'){
					input.prop('type', 'text');
					$(this).prop('title', 'Скрыть пароль');
				} else {
					input.prop('type', 'password');
					$(this).prop('title', 'Показать пароль');
				}
			});

			$(dialogSel + ' .forgot-password').click(function(){ h_profile.ForgotPassword(base_url); });
		}

		controller.OnLoginByCert = function ()
		{
			if (isMobile())
			{
				h_msgbox.ShowModal
					({
						title: 'Вход по ЭЦП', width: 450
						, html: '<span>С мобильного устройства возможен вход только по логину.<br/>'
							+ 'Если у вас нет логина, то зайдите с ЭЦП с компьютера и задайте его.</small>'
					});
				return false;
			}
			else
			{
				var self = this;
				h_login_by_cert.Login(base_url
					, function (man_cert,variant,auth_info)
					{
						self.OnLogged(man_cert,variant,auth_info);
						h_login_by_cert.SafeOfferSetupLogin(auth_info, function () { self.OnProfile(); });
					}
				);
			}
		}

		controller.AutoLogin = function(login)
		{
			var login_data = { Login: login, Password: '' };
			var self = this;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос авто авторизации на сервере", base_login_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null != responce && false != responce.ok)
					{
						self.OnLogged('', '', '', responce, 'without_datamart');
					}
				}
			});
		}

		controller.OnLoginByPass = function(closeDialogFunc)
		{
			var login_data = $('.orpau-login-form').serialize();
			var self = this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос авторизации на сервере", base_login_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						h_msgbox.ShowModal({
							width: 400
							, title: 'Неудачная аутентификация'
							, html: 'Вы ввели неправильное имя пользователя или пароль!'
						});
					}
					else if (false == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка аутентификации'
							, width: 400
							, html: '<span>Не удалось провести аутентификацию по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
					else
					{
						self.OnLogged('', '', '', responce, 'without_datamart');
						closeDialogFunc();
					}
				}
			});
		}

		controller.OnLogged = function (man_cert,variant,auth_info)
		{
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-orpau-main').addClass('manager');
			$(sel + ' button.login').hide();

			auth_info.login_details = { variant: variant };

			var ccabinet= this.cabinet= c_manager_cabinet({ base_url: base_url });

			var self= this;
			ccabinet.ParentOnLogout = function ()
			{
				delete self.cabinet.ParentOnLogout;
				$(sel + ' button.login').show();
				$(sel + ' a[href="#main-tab-confirmations"]').click();
				self.cabinet.Destroy();
				$(sel + ' div#cpw-orpau-main-tabs > div#main-tab-cabinet > div.cabinet').html('');
				$(sel + ' div.cpw-orpau-main').removeClass('manager');
				delete self.cabinet;
				if (self.OnLogoutRoot)
					self.OnLogoutRoot();
			}

			ccabinet.SetFormContent(auth_info);
			ccabinet.Edit(sel + ' div#cpw-orpau-main-tabs > div#main-tab-cabinet > div.cabinet');

			$(sel + ' a[href="#main-tab-cabinet"]').click();

		}

		controller.OnLogout = function ()
		{
			var sel= this.fastening.selector;
			this.cabinet.Destroy();
			$(sel + ' div#cpw-orpau-main-tabs > div#main-tab-cabinet > div.cabinet').html('');
			$(sel + ' div.cpw-orpau-main').removeClass('manager');

			$(sel + ' .профсоюз').hide();
			$(sel + ' .асп').hide();

			$(sel + ' button.login').show();
			$(sel + ' span.profile-bar').hide();
			$(sel + ' span.profile-name').text('');
			delete this.cabinet;

			var v_ajax = h_msgbox.ShowAjaxRequest("Завершение сессии АУ на сервере", base_auth_url + '&cmd=logout');
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data || !data.ok)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						$(sel + ' a[href="#main-tab-confirmations"]').click();
					}
				}
			});

			// внешнее присваивание
			if(this.OnLogoutRoot)
				this.OnLogoutRoot();
		}

		controller.ЗаявлениеВПрофсоюз = function() {
			var self = this;
			var sel= this.fastening.selector;
			var data = this.cabinet.GetFormContent();

			if(data.ИспользуетАСП) {
				h_profile.ManagerRead(
					base_url
					, data.id_Manager
					, function(manager) {
						if(h_validate.ValidateEmail(manager.EMail) && h_validate.ValidatePhoneNumber(manager.Phone)) {
							h_club_agreement.SignClubAgreement(
								base_url
								, data.id_Manager
								, function(){ $(sel + ' .профсоюз').hide(); }
							);
						} else {
							h_profile.PleaseEditProdile(
								'Заполните контактные данные, чтобы перейти к подписанию соглашения'
								, function() { self.OnProfile() });
						}
					}
				);
			} else {
				h_club_agreement.AlertAsp();
			}
		}

		controller.ЗаявлениеАСП = function() {
			var self = this;
			var sel= this.fastening.selector;

			var data = this.cabinet.GetFormContent();
			h_profile.ManagerRead(
				base_url
				, data.id_Manager
				, function(manager) {
					if(h_validate.ValidateEmail(manager.EMail) && h_validate.ValidatePhoneNumber(manager.Phone)) {
						h_asp_agreement.SignAspAgreement(base_asp_agreement_url
							, function(){ $(sel + ' .асп').hide(); }
						);
					} else {
						h_profile.PleaseEditProdile(
							'Заполните контактные данные, чтобы перейти к подписанию соглашения'
							, function() { self.OnProfile() });
					}
				}
			);
		}

		function getNameString(val) {
			if(null == val) return 'Неустановленный АУ';
			var name = '';
			if(val.Фамилия) name += val.Фамилия;
			if(val.Имя) { 
				if(name != '') name += ' ';
				name += val.Имя.charAt(0) + '.';
			}
			if(val.Отчество) {
				if(name != '') name += ' ';
				name += val.Отчество.charAt(0) + '.'
			}
			return name;
		}

		// Анимация горизонтального пролистывания
		function slide (to) {
			var $wrap = $( ".orpau-login-wrap" );
			var initalLeftMargin = +$wrap.css('margin-left').replace("px", "");
			var wrapWidth = 385;
			if(to === 'right')
			{
				var newLeftMargin = (initalLeftMargin - wrapWidth);
			}
			if(to === 'left')
			{
				var newLeftMargin = (initalLeftMargin + wrapWidth);
			}
			$wrap.animate({marginLeft: newLeftMargin}, 200);
		}

		function isMobile() {
			var isMobile = false;
			var toMatch = [
				/Android/i,
				/webOS/i,
				/iPhone/i,
				/iPad/i,
				/iPod/i,
				/BlackBerry/i,
				/Windows Phone/i
			];

			for (var i=0; i<toMatch.length;i++){
				var toMatchItem = toMatch[i];
				if(navigator.userAgent.match(toMatchItem))
				{
					isMobile = true;
					break;
				};

			}
			return isMobile;
		}

		return controller;
	}
});