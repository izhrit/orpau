﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/managers/e_managers.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/manager/c_manager'
],
function (c_fastened, tpl, h_msgbox, c_manager)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.ReloadGrid = function()
		{
			var sel = controller.fastening.selector;
			$(sel + ' table.grid').trigger('reloadGrid');
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_grid_url = controller.base_url + '?action=manager.jqgrid';
		controller.base_crud_url = controller.base_url + '?action=manager.crud';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			return url;
		}

		controller.colModel =
			[
				{ name: 'id_Manager', hidden: true }
				, { label: 'Фамилия', name: 'LastName', width: 200 }
				, { label: 'Имя', name: 'FirstName', width: 200 }
				, { label: 'Отчество', name: 'MiddleName', width: 200 }
				, { label: 'ИНН', name: 'INN', width: 130 }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;
			var grid = $(sel + ' table.grid');
			var url = this.base_grid_url;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка ау...'
				, recordtext: 'Показаны ау: {1} из {2}'
				, emptyText: 'Нет ау для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-orpau-managers-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { self.OnManager(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка ау", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnManager = function (rowid)
		{
			var self = this;
			var selectedRow = $(this.fastening.selector + ' table.grid')[0].rows[rowid];
			var id_Manager = selectedRow.cells[0].innerHTML;
			var managerName = selectedRow.cells[1].innerHTML+ ' ' + selectedRow.cells[2].innerHTML+ ' ' + selectedRow.cells[3].innerHTML;
			var manager_url = this.base_crud_url + '&cmd=get&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных АУ с сервера", manager_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cmanager = c_manager({ base_url: self.base_url });
							cmanager.SetFormContent(data);
							h_msgbox.ShowModal
								({
									title: 'Информация об АУ "' + managerName + '"'
									, controller: cmanager
									, buttons: ['Закрыть']
									, id_div: "cpw-form-orpau-cmanager"
								});
						}
					}
				});
		}

		return controller;
	}
});