﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/superadmin/root/e_admin.html'
	, 'forms/orpau/superadmin/login/c_admin_login'
	, 'forms/orpau/superadmin/main/c_admin_main'
],
function (c_fastened, tpl, c_admin_login, c_admin_main)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;

		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			this.login_form = c_admin_login({base_url:base_url});
			this.login_form.OnOkLogin = function (logged_info) { self.OnOkLogin(logged_info); }
			this.login_form.CreateNew(sel + ' > div.cpw-orpau-admin-root > div.login');
		}

		controller.OnOkLogin = function (logged_info)
		{
			var sel= this.fastening.selector;

			this.login_form.Destroy();
			delete this.login_form;

			$(sel + ' > div.cpw-orpau-admin-root > div.login').html('');
			$(sel + ' > div.cpw-orpau-admin-root').addClass('logged');

			this.main_form = c_admin_main({base_url:base_url});
			this.main_form.SetFormContent(logged_info);
			this.main_form.Edit(sel + ' > div.cpw-orpau-admin-root > div.main');
		}

		return controller;
	}
});