define(['forms/orpau/superadmin/root/c_admin'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'superadmin'
		, Title: 'Добро пожаловать в административную часть orpau'
	};
	return form_spec;
});