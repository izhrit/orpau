define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/superadmin/manager/e_manager.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_info_url = base_url + '?action=superadmin.manager';

		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.refresh-codes').click(function(){ self.getCodes(); });
			self.getCodes();
		}

		controller.getCodes = function() {
			var self = this;
			var sel = this.fastening.selector;
			var id_Manager = self.model.id_Manager;
			
			var url = base_info_url + '&cmd=get-code&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение с сервера", url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							$(sel + ' .for-codes-table').html('');

							if(data.length > 0){
								var table = $('<table>')
								table.append(
									$('<thead>').append(
										$('<tr>').append(
											$('<td>').text('Вопрос'),
											$('<td>').text('Код')
										)
									)
								)
								var tbody = $('<tbody>');
								for(var i=0; i<data.length;i++) {
									var q = data[i];
									tbody.append(
										$('<tr>').append(
											$('<td>').text(q.Question),
											$('<td>').text(q.Code)
										)
									);
								}
								table.append(tbody);

								$(sel + ' .for-codes-table').html(table);
							} else {
								$(sel + ' .for-codes-table').html('Нет активных голосований');
							}
						}
					}
				});
		}

		return controller;
	}
});