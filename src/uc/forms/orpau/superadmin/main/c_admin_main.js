﻿define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/superadmin/main/e_admin_main.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/superadmin/manager/c_manager'
],
function (c_fastened, tpl, h_msgbox, c_manager)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_grid_url = base_url + "?action=manager.jqgrid";
		var base_crud_url = base_url + '?action=manager.crud';
		var base_info_url = base_url + '?action=superadmin.manager';

		var controller = c_fastened(tpl);

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			this.RenderGrid();
		}

		controller.colModel =
			[
				{ name: 'id_Manager', hidden: true }
				, { label: 'Фамилия', name: 'LastName', width: 200 }
				, { label: 'Имя', name: 'FirstName', width: 200 }
				, { label: 'Отчество', name: 'MiddleName', width: 200 }
				, { label: 'ИНН', name: 'INN', width: 130 }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;
			var grid = $(sel + ' table.grid');

			grid.jqGrid
			({
				datatype: 'json'
				, url: base_grid_url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка ау...'
				, recordtext: 'Показаны ау: {1} из {2}'
				, emptyText: 'Нет ау для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-orpau-admin-manager-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { self.OnManager(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка ау", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnManager = function(rowid) {
			var self = this;
			var grid = $(this.fastening.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			var id_Manager = rowdata.id_Manager;
			var LastName = rowdata.LastName;
			var FirstName = rowdata.FirstName;
			var MiddleName = rowdata.MiddleName;
			var managerName = LastName+' '+FirstName+' '+MiddleName;

			var manager_url = base_info_url + '&cmd=get-info&id=' + id_Manager;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных АУ с сервера", manager_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cmanager = c_manager({ base_url: base_url });
							cmanager.SetFormContent(data);
							h_msgbox.ShowModal
								({
									title: 'Информация об АУ "' + managerName + '"'
									, controller: cmanager
									, width: 500
									, height: 480
									, buttons: ['Закрыть']
									, id_div: "cpw-form-orpau-manager"
								});
						}
					}
				});
		}

		return controller;
	}
});