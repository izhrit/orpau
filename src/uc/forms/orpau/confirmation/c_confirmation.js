﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/confirmation/e_confirmation.html'
],
function (c_fastened, tpl)
{
	return function (options_arg) 
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 950, height: 570 };

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_crud_url = controller.base_url + '?action=confirmation.crud';
		controller.manager_crud_url = controller.base_url + '?action=manager.crud';
		controller.manager_api_url = controller.base_url + '?action=manager.API';
		controller.text_generating_url = controller.base_url + '?action=confirmation.generate&cmd=get';
		controller.printable_confirmation_url = controller.base_url + "?action=confirmation.printable&cmd=get";

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;
			$(sel + ' button.printable-btn').button({ icons: { primary: 'ui-icon-print' } }).click(function () { self.OnPrintableClick(); });
			$(sel + ' button.sign-btn').button({ icons: { primary: 'ui-icon-locked' } }).click(function () { self.OnSignBtnClick(); });

			$(sel + ' button.order-btn').click(function () { self.OnOrderBtnClick(); });
			$(sel + ' button.save-btn').button({ icons: { primary: 'ui-icon-disk' } }).click(function () { self.OnSaveClick(); })
			$(sel + ' button.cert-btn').button({ icons: { primary: 'ui-icon-help' } }).click(function () { self.OnCertBtnClick(); });

			this.ShowMarker();
		}

		controller.OnCertBtnClick = function()
		{
			var url = controller.manager_api_url + "&cmd=get_cert&id_Confirmation=" + controller.model.id_Confirmation;
			var element = document.createElement('a');
			element.setAttribute('href', url);
			element.setAttribute('download', "certificate.cer");
			element.style.display = 'none';
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		}

		controller.OnSaveClick = function ()
		{
			var url = controller.base_url + "?action=confirmation.text&id_Confirmation=" + controller.model.id_Confirmation;
			var element = document.createElement('a');
			element.setAttribute('href', url);
			element.setAttribute('download', "confirmation.txt");
			document.body.appendChild(element);
			element.click();
			document.body.removeChild(element);
		}

		controller.OnPrintableClick = function ()
		{
			var url = "";
			if (!controller.isGenerated)
				url = controller.printable_confirmation_url + "&id_Confirmation=" + controller.model.id_Confirmation;
			else url = controller.printable_confirmation_url + "&id_Manager=" + controller.model.Manager.id_Manager+"&id_Debtor="+controller.model.Debtor.id_Debtor;
			var name = 'Справка орпау №'+controller.model.id_Confirmation;
			window.open(url, name, 'toolbar=yes,location=no,status=yes,menubar=yes,resizable=yes,directories=yes,scrollbars=yes,width=' + 900 + ',height=' + 600 + '');
		}

		controller.OnSignBtnClick = function ()
		{
			if (!controller.isGenerated)
			{
				var url = controller.manager_api_url + "&cmd=get_sign&id_Confirmation=" + controller.model.id_Confirmation;
				var element = document.createElement('a');
				element.setAttribute('href', url);
				element.setAttribute('download', "signature.sig");
				element.style.display = 'none';
				document.body.appendChild(element);
				element.click();
				document.body.removeChild(element);
			}
		}

		controller.OnOrderBtnClick = function ()
		{
			var ajaxurl = controller.text_generating_url + '&id_Debtor=' + controller.model.Debtor.id_Debtor + '&id_Manager=' + controller.model.Manager.id_Manager;
			$.ajax
				({
					url: ajaxurl,
					cache: false,
					success: function (data, textStatus) 
					{
						$('.confirmation_text').html(data);
						controller.model.Text = data;
						var confirmation =
						{
							Text: controller.model.Text
							, id_Manager: controller.model.Manager.id_Manager
							, id_Debtor: controller.model.Debtor.id_Debtor
							, CaseNumber: controller.model.CaseNumber
						}
						controller.isGenerated = false;
						controller.OrderConfirmation(confirmation);
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						console.log(jqXHR);
						console.log(textStatus);
						console.log(errorThrown);
					}

				});
		}

		controller.OrderConfirmation = function (confirmation)
		{
			var ajaxurl = controller.base_crud_url + '&cmd=add';
			$.ajax({
				url: ajaxurl
				, type: 'POST'
				, data: confirmation
				, cache: false
				, success: function (data)
				{
					$('.cpw-orpau-main').trigger('orders_change');
					controller.model.id_Confirmation = data;
					$.ajax(
						{
							url: controller.base_crud_url + "&cmd=get&id=" + controller.model.id_Confirmation,
							cache: false,
							success: function (data)
							{
								data = JSON.parse(data);
								controller.model.Text = data.Text;
								$('.confirmation_text').html(controller.model.Text);
								$('.confirmation-mark').show();
								$('.sign-btn').show();
								$('.cert-btn').show();
								controller.ShowMarker();
							}
						}
					);
				}
				, error: function ()
				{
					alert("Невозможно заказать справку");
				}
			});
		}

		controller.ShowMarker = function () 
		{
			if (this.model && this.model.Number)
			{
				var Number= this.model.Number;
				var img = $(this.fastening.selector + ' img.mark-img');
				img.attr('src', this.base_url + "/" + Number + "/" + Number + "/mark.png");
				img.attr('width', 180);
				img.attr('height', 140);
			}
		}

		return controller;
	}
});