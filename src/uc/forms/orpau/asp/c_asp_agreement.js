define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/asp/e_asp_agreement.html'
],
function(c_fastened, tpl)
{
	return function()
	{
		var controller = c_fastened(tpl);

		controller.size = { width: 600, height: 660 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
		}

		return controller;
	}
})