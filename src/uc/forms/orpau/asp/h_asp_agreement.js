define([
	'forms/base/h_msgbox'
	, 'forms/orpau/base/h_cryptoapi'

	, 'tpl!forms/orpau/login/v_choosed_cert_without_inn.html'
	, 'tpl!forms/orpau/asp/v_choosed_cert_bad_for_manager.html'
	
	, 'forms/orpau/asp/c_asp_agreement'
	, 'forms/orpau/base/h_crypto_ex'
],
function (h_msgbox, h_cryptoapi
	, v_choosed_cert_without_inn, v_choosed_cert_bad_for_manager, c_asp_agreement, h_crypto_ex)
{
	var helper = {};

	helper.Присоединиться_к_соглашению_об_АСП = function(ау, base_asp_url, on_signed)
	{
		try
		{
			var self = this;
			app.cryptoapi.ChooseCertificateAnd(function (cert)
			{
				var msg_bad_cert = function (html)
				{ 
					h_msgbox.ShowModal({ width: 460, title: 'Подписание невозможно', html: html, id_div:'cpw-asp-agreement-bad-cert-choosed-msg' }); 
				}
				var dn_fields= h_cryptoapi.GetDNFields(cert.SubjectName);
				if (!dn_fields.ИНН || null == dn_fields.ИНН || '' == dn_fields.ИНН)
				{
					msg_bad_cert(v_choosed_cert_without_inn(dn_fields));
				}
				else if (dn_fields.ИНН != ау.INN)
				{
					msg_bad_cert(v_choosed_cert_bad_for_manager({ cert: dn_fields, ау: ау }));
				}
				else
				{
					self.Заполнить_и_подписать_соглашению_об_АСП_сертификатом(cert,base_asp_url,on_signed,ау);
				}
			});
		}
		catch (ex)
		{
			h_crypto_ex.handleCryptoApiEx(ex);
		}
	}

	helper.Заполнить_и_подписать_соглашению_об_АСП_сертификатом = function (cert,base_asp_url, on_signed, ау)
	{
		var self = this;
		var url = base_asp_url + '&cmd=get-application-for-accession-to-sign';
		var v_ajax = h_msgbox.ShowAjaxRequest("Подготовка заявления о присоединении к АСП на подпись", url);
		v_ajax.ajax
		({
			dataType: "json", type: 'POST', cache: false
			, data: { SerialNumber: cert.SerialNumber, Issuer: cert.IssuerName, Subject: cert.SubjectName }
			, success: function (txt, textStatus)
			{
				if (null == txt)
				{
					v_ajax.ShowAjaxError(txt, textStatus);
				}
				else
				{
					self.Подписать_соглашение_об_АСП_сертификатом(cert, txt, base_asp_url, on_signed, ау);
				}
			}
		});
	}
	
	helper.Подписать_соглашение_об_АСП_сертификатом = function (cert, txt, base_asp_url, on_signed, ау)
	{
		var self= this;

		var cc_asp_agreement= c_asp_agreement();
		cc_asp_agreement.SetFormContent({ text: txt, cert: cert });

		var btn_sign= 'Подписать заявление';
		h_msgbox.ShowModal({
			title:'Присоединение к соглашению об АСП'
			,buttons:[btn_sign,'Закрыть, не подписывая']
			,id_div:'cpw-form-orpau-asp-agreement'
			,controller:cc_asp_agreement
			,onclose: function (btn)
			{
				if (btn_sign == btn)
				{
					var base64_encoded_signature= app.cryptoapi.SignBase64(txt,/*detached=*/true,cert);
					self.Зарегистрировать_подпись_соглашения_об_АСП(base_asp_url, base64_encoded_signature, on_signed, ау);
				}
			}
		});
	}
	
	helper.Зарегистрировать_подпись_соглашения_об_АСП = function (base_asp_url, base64_encoded_signature, on_signed, ау)
	{
		var url= base_asp_url + '&cmd=sign&id_Manager=' + ау.id_Manager;
		var v_ajax= h_msgbox.ShowAjaxRequest("Регистрация подписи к заявлению о присоединении к АСП на сервере", url);
		v_ajax.ajax
		({
			dataType: "json", type: 'POST', cache: false
			, data: {base64_encoded_signature:base64_encoded_signature}
			, success: function (data, textStatus)
			{
				if (null == data)
				{
					v_ajax.ShowAjaxError(data, textStatus);
				}
				else
				{
					on_signed();
				}
			}
		});
	}
  
	return helper;
});