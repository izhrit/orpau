define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/news/e_news.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/orpau/cabinet/news/v_news_cell.html'
	, 'forms/orpau/cabinet/news/h_MessageTypes.etalon'
	, 'forms/base/codec/datetime/s_codec.mysql_txt2txt_ru_legal'
	, 'forms/orpau/cabinet/news/h_DecisionType'
],
function (c_fastened, tpl, h_msgbox, v_news_cell, h_MessageTypes, s_codec_mysql_txt2txt_ru_legal, h_DecisionType)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_grid_url = controller.base_url + '?action=news.jqgrid';

		var base_Render = controller.Render;
		controller.Render = function(sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
			var self= this;
			$(sel + ' div.refresh-btn__wrapper').on('click', function () { self.ReloadGrid(); });
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
		}

		var prepare_о_чём = function (t)
		{
			return !h_MessageTypes.Readable_short_about[t] ? 'о неизвестном' : h_MessageTypes.Readable_short_about[t];
		}

		var дата_и_время_анонсированного_события = function (t)
		{
			return s_codec_mysql_txt2txt_ru_legal.Encode(t);
		}

		var news_cell_formatter = function (cellvalue, options, row) 
		{
			return v_news_cell({ 
				row: row
				, о_чём: prepare_о_чём
				, дата_и_время_анонсированного_события: дата_и_время_анонсированного_события
				, h_DecisionType: h_DecisionType
			});
		}

		controller.colModel =
		[
			{
				formatter: news_cell_formatter
				, sortable: false
				, cellattr: function (rowId, tv, rawObject, cm, rdata) { return 'style="white-space: normal;"' }
				, title: false
			}
			, { name: 'MessageGUID', hidden: true }
			, { name: 'NewsType', hidden: true }
		];

		controller.RenderGrid = function()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = this.base_grid_url;
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, headertitles: false
				, gridview: true
				, loadtext: 'Загрузка новостей..'
				, recordtext: 'Новости за последние 2 месяца: {0} - {1} из {2}'
				, emptyText: 'Нет новостей для отображения'
				, rownumbers: false

				, rowNum: 4
				, rowList: [4, 10, 100, 500]
				, pager: '#cpw-ama-datamart-news-pager'
				, viewrecords: true
				, width: null
				, height: 'auto'
				, ignoreCase: true
				, shrinkToFit: true
				, onSelectRow: function () { self.OnClickRow(); }
				, loadError: function (jqXHR, textStatus, errorThrown) {
					h_msgbox.ShowAjaxError("Загрузка новостей..", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnClickRow = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			var url = "https://bankrot.fedresurs.ru/MessageWindow.aspx?ID=" + rowdata.MessageGUID;
			window.open(url, "Сообщение", "toolbar=no,location=no,status=no,menubar=no,resizable=yes,directories=no,scrollbars=yes,width=1000,height=600");
		}


		return controller;
	}
});
