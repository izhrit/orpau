define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/datamart/e_datamart.html'
],
function (c_fastened, tpl)
{
	return function(options_arg)
	{
		var widget= (!options_arg || !options_arg.widget) ? '' : options_arg.widget;

		var controller = c_fastened(tpl, { widget: widget });

		return controller;
	}
});
