define([
	'forms/base/fastened/c_fastened'
	,'tpl!forms/orpau/cabinet/confirmations/e_manager_confirmations.html'
	, 'forms/base/h_msgbox'
	,'forms/orpau/confirmation/c_confirmation'
],
function (c_fastened, tpl, h_msgbox, c_confirmation)
{
	return function (options_arg)
	{

		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_grid_url = controller.base_url+"?action=confirmation.jqgrid";
		controller.base_crud_url = controller.base_url + '?action=confirmation.crud';

		var base_Render = controller.Render;
		controller.Render = function(Sel)
		{
			base_Render.call(this, Sel);
			controller.RenderGrid();
		}
			
		controller.size = { width: 1050, height: "590" };

		controller.colModel =
		[
			{ name: 'id_Confirmation', hidden: true }
			, { label: 'Номер', name: 'number', width: 100 }
			, { label: 'Дата', name: 'date', width: 100 }
			, { label: 'Номер дела', name: 'CaseNumber', width: 200 }
			, { label: 'Должник', name: 'debtor', width: 350 }
			, { label: 'ИНН должника', name: 'inn', width: 120 }
			, { label: 'ОГРН должника', name: 'ogrn', width: 150}
			, { label: 'СНИЛС должника', name: 'snils', width: 115 }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;
			if (this.model)
				url+= '&id_Manager=' + this.model.id_Manager;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны справки: {1} из {2}'
				, emptyText: 'Нет процедур для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10
				, pager: '#cpw-orpau-manager-confirmations-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, onSelectRow: function (rowid) { self.OnConfirmation(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка справок", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnConfirmation = function (rowid) 
		{
			var self = this;
			var selectedRow = $(this.fastening.selector + ' table.grid')[0].rows[rowid];
			var managerName = selectedRow.cells[3].title;
			var debtorName = selectedRow.cells[4].title;
			var id_Confirmation = selectedRow.cells[0].title;
			var confirmation_url = this.base_crud_url + '&cmd=get&id=' + id_Confirmation;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", confirmation_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cconfirmation = c_confirmation({ base_url: self.base_url });
							cconfirmation.SetFormContent(data);
							cconfirmation.Updater = controller.Updater;
							cconfirmation.OnOrder= function()
							{
								$(self.fastening.selector).trigger('orders_change');
							}
							h_msgbox.ShowModal
								({
									title: 'Справка №'+id_Confirmation+' о полномочиях ' + managerName + ' в процедуре "' + debtorName +'"'
									, controller: cconfirmation
									, buttons: ['Закрыть']
									, id_div: "cpw-form-orpau-confirmation"
								});

						}
					}
						
				});
		}

		return controller;
	}
});