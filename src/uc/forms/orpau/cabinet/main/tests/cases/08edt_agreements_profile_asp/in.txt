wait_text "АУ"
wait_text_disappeared "Загрузка"

shot_check_png ..\..\shots\02edt2.png

je app.cryptoapi.mock_InternalChooseCertificateInn= '987654321';

wait_click_text "Семёнов"
wait_text "Вступить"
wait_text "Соглашение"
wait_click_text "Профиль"

wait_click_text "Присоединиться к соглашению"
wait_click_text "Подписать заявление"

wait_click_text "Посмотреть данные о подписании"
wait_text "Информация о подписании"
wait_click_text "OK"

wait_click_text "Посмотреть подписанное заявление"
wait_text "Подписанное заявление"
wait_click_text "Закрыть"

wait_click_text "Отмена"

wait_click_text "Семёнов"
wait_text_disappeared "Соглашение об АСП"
wait_text "Вступить в профсоюз"

exit