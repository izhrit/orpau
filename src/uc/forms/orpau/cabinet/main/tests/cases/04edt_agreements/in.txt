wait_text "АУ"
wait_text_disappeared "Загрузка"

wait_text "профсоюз"
shot_check_png ..\..\shots\02edt2.png

wait_click_text "В профсоюз"
wait_text "нужно подписать заявление о присоединении"
wait_click_text "Отмена"

shot_check_png ..\..\shots\02edt2.png
wait_click_text "В профсоюз"
wait_text "нужно подписать заявление о присоединении"
wait_click_text "Перейти к подписанию"
wait_text "Подписание невозможно"
wait_click_text "OK"

shot_check_png ..\..\shots\02edt2.png

je app.cryptoapi.mock_InternalChooseCertificateInn= '987654321';

shot_check_png ..\..\shots\02edt2.png
wait_click_text "В профсоюз"
wait_text "нужно подписать заявление о присоединении"
wait_click_text "Перейти к подписанию"
wait_text "Присоединение к соглашению"
wait_click_text "Закрыть, не подписывая"

shot_check_png ..\..\shots\02edt2.png

wait_click_text "Семёнов"
wait_click_text "Профиль"

wait_text "Настройка свойств АУ"
js wbt.SetModelFieldValue("EMail", "");
js wbt.SetModelFieldValue("Phone", "");
wait_click_text "Сохранить свойства АУ"

wait_click_text "В профсоюз"
wait_text "нужно подписать заявление о присоединении"
wait_click_text "Перейти к подписанию"
wait_text "укажите в профиле Ваши реквизиты"
wait_click_text "Указать контактные данные в профиле"
wait_text "Настройка свойств АУ"
wait_click_text "Отмена"

shot_check_png ..\..\shots\02edt2.png

wait_click_text "В профсоюз"
wait_text "нужно подписать заявление о присоединении"
wait_click_text "Перейти к подписанию"
wait_text "укажите в профиле Ваши реквизиты"
wait_click_text "Указать контактные данные в профиле"
wait_text "Настройка свойств АУ"

wait_click_text "Вступить в профсоюз"
wait_click_text "Перейти к подписанию соглашения"
wait_text "Укажите контактные данные"
wait_click_text "OK"

js wbt.SetModelFieldValue("EMail", "o@o.u");
js wbt.SetModelFieldValue("Phone", "89128555633");

wait_click_text "Сохранить свойства АУ"

shot_check_png ..\..\shots\02edt2.png

wait_click_text "В профсоюз"
wait_text "нужно подписать заявление о присоединении"
wait_click_text "Перейти к подписанию"
wait_click_text "Подписать заявление"

shot_check_png ..\..\shots\02edt2.png

wait_click_text "Семёнов"
wait_text_disappeared "Соглашение об АСП"
wait_click_text "Профиль"

wait_click_text "Посмотреть данные о подписании"
wait_text "Информация о подписании"
wait_click_text "OK"

wait_click_text "Посмотреть подписанное заявление"
wait_text "Подписанное заявление"
wait_click_text "Закрыть"

wait_click_text "Отмена"

shot_check_png ..\..\shots\02edt2.png

wait_click_text "В профсоюз"
wait_text "Подписание заявления"
wait_click_text "Выслать код"
wait_text "Получен код подтверждения"

je $('input.code').val(1234)
wait_click_text "Подписать заявление"

wait_text "Заявление подписано"
wait_click_text "OK"

wait_text_disappeared "профсоюз"

wait_click_text "Семёнов"
wait_text_disappeared "Соглашение об АСП"
wait_text_disappeared "профсоюз"
wait_click_text "Профиль"

wait_click_text "Посмотреть подписанное заявление"
wait_text "Подписанное заявление о вступлении в профсоюз"
wait_click_text "Закрыть"
wait_click_text "Отмена"

exit