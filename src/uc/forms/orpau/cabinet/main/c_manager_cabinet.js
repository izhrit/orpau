define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/main/e_manager_cabinet.html'
	, 'forms/orpau/cabinet/confirmations/c_manager_confirmations'
	, 'forms/orpau/cabinet/debtors/c_manager_debtors'
	, 'forms/orpau/cabinet/news/c_news'
	, 'forms/orpau/cabinet/schedule/c_schedule'
	, 'forms/orpau/cabinet/datamart/c_datamart'
	, 'forms/orpau/cabinet/voting/main/c_voting'
	, 'forms/orpau/profile/h_profile'
	, 'forms/base/h_validate'
	, 'forms/orpau/asp/h_asp_agreement'
	, 'forms/orpau/club/h_club_agreement'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/orpau/asp/v_asp_need_contacts.html'
],
function (c_fastened, tpl, c_manager_confirmations, c_manager_debtors, c_news, c_schedule, c_datamart, c_voting, h_profile, h_validate, h_asp_agreement, h_club_agreement, h_msgbox, v_asp_need_contacts)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_asp_url = base_url + '?action=asp.agreement';
		var base_auth_url = base_url + "?action=manager.auth";

		var options = {
			field_spec:
			{
				Процедуры: { controller: function () { return c_manager_debtors({ base_url: base_url }); }, render_on_activate:true }
				,Справки: { controller: function () { return c_manager_confirmations({ base_url: base_url }); }, render_on_activate:true }
				//,Новости: { controller: function () { return c_news({ base_url: base_url }); }, render_on_activate:true }
				,Новости: { controller: function () { return c_datamart({ widget: 'news' }); }, render_on_activate:true }
				//,Календарь: { controller: function () { return c_schedule({ base_url: base_url }); }, render_on_activate:true }
				,Календарь: { controller: function () { return c_datamart({ widget: 'schedule' }); }, render_on_activate:true }
				,Витрина: { controller: function () { return c_datamart({ widget: 'pdatas' }); }, render_on_activate:true }
				,Голосование: { controller: function () { return c_voting({ base_url: base_url }); }, render_on_activate:true }
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			//app.cryptoapi.mock_InternalChooseCertificateInn= '987654321';

			$(sel + " .jquery-menu").menu();
			$(sel + " .profile-dropdown").click(function(){
				$(this).find("ul.jquery-menu").toggle();
			})
			
			//close menu
			$(document).on("click", function (e) {
				if ($(e.target).is("li.menu.open-profile")) self.OnProfile();
				if ($(e.target).is("li.menu.logout")) { if (self.OnLogout) { self.OnLogout(); return false; } }
				if ($(e.target).is("li.menu.вступить-в-профсоюз")) { self.ВступитьВПрофсоюз(); }
				if ($(e.target).is("li.menu.использовать-асп")) { self.ПрисоединитьсяКСоглашениюОбАСП(); }

				if(!$(e.target).is("li.menu.profile-dropdown") && !$(e.target).parent().hasClass("profile-dropdown")) {
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});

			var self= this;
			$(sel + ' button.вступить-в-профсоюз').click(function(){ self.ВступитьВПрофсоюз(); });
			$(sel + ' button.использовать-асп').click(function(){ self.ПрисоединитьсяКСоглашениюОбАСП(); });
		}

		controller.OnLogout = function ()
		{
			var self= this;
			var ajaxurl= base_auth_url + '&cmd=logout';
			var v_ajax = h_msgbox.ShowAjaxRequest("Завершение сессии АУ на сервере", ajaxurl, 'id-div-ShowAjaxRequest-logout');
			v_ajax.ajax
			({
				dataType: "json", type: 'GET', cache: false
				, success: function (data, textStatus)
				{
					if (null == data || !data.ok)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						if (self.ParentOnLogout)
							self.ParentOnLogout();
					}
				}
			});
		}

		controller.OnProfile = function()
		{
			h_profile.ModalEditManagerProfile(base_url, this.model.id_Manager, null);
		}

		controller.ПрисоединитьсяКСоглашениюОбАСП = function()
		{
			var self = this;
			var sel= this.fastening.selector;
			h_profile.ManagerRead
			(
				base_url, this.model.id_Manager
				, function(ау)
				{
					if (h_validate.ValidateEmail(ау.EMail) && 
						h_validate.ValidatePhoneNumber(ау.Phone))
					{
						h_asp_agreement.Присоединиться_к_соглашению_об_АСП
						(
							ау, base_asp_url
							, function()
							{
								self.model.ИспользуетАСП= true;
								$(sel + ' div.cpw-orpau-manager-cabinet').addClass('ИспользуетАСП');
							}
						);
					}
					else
					{
						h_profile.PleaseEditProdile
						(
							v_asp_need_contacts()
							, function() { self.OnProfile() }
						);
					}
				}
			);
		}

		controller.ВступитьВПрофсоюз = function()
		{
			var self = this;
			var sel= this.fastening.selector;
			var id_Manager= self.model.id_Manager;

			if (!self.model.ИспользуетАСП)
			{
				h_club_agreement.AlertAsp(function () { self.ПрисоединитьсяКСоглашениюОбАСП(); });
			}
			else
			{
				h_profile.ManagerRead
				(
					base_url, id_Manager
					, function(ау)
					{
						if (h_validate.ValidateEmail(ау.EMail) && 
							h_validate.ValidatePhoneNumber(ау.Phone)) 
						{
							h_club_agreement.Вступить_в_профсоюз(
								ау, base_url,
								function(){ $(sel + ' .вступить-в-профсоюз').hide(); }
							);
						} else {
							h_profile.PleaseEditProdile(
								'Заполните контактные данные, чтобы вступить в профсоюз'
								, function() { self.OnProfile() });
						}
					}
				);
			}
		}

		return controller;
	}
});