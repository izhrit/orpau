define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/cabinet/voting/log/e_voting_log.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, h_msgbox, h_check_mobile)
{
	return function (options_arg)
	{

		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_grid_url = controller.base_url + "?action=log.jqgrid";

		var gridReloadInterval = '';
		var isMobile = h_check_mobile.isMobile;

		var base_Render = controller.Render;
		controller.Render = function (Sel)
		{
			base_Render.call(this, Sel);
			controller.RenderGrid();
		}

		controller.colModel =
			[
				{ name: 'id_Vote_log', hidden: true }
				, { label: 'Время', name: 'time', width: 50, search: false, classes: 'cpw-orpau-voting-log-time' }
				, { label: 'Событие', name: 'event', width: 200, search: false, sortable: false }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;
			if (this.model) 
			{
				url+= '&id_Manager=' + this.model.id_Manager;
				if(self.model.id_Poll) url += '&id_Poll=' + self.model.id_Poll;
			}

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: isMobile() ? '' : 'Показаны записи: {1} из {2}'
				, emptyText: 'Нет записей для отображения'
				, emptyrecords: isMobile() ? null : 'Нет записей для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10
				, pager: '#cpw-orpau-voting-log-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка записей журнала действий", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			//grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });

			gridReloadInterval = setInterval(function() {
				grid.trigger('reloadGrid');
			}, 300000); // 300 sec === 5 min 
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy= function()
		{
			base_Destroy.call(this);
			if (gridReloadInterval)
			{
				clearInterval(gridReloadInterval);
			}
		}

		return controller;
	}
});