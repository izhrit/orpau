define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/cabinet/voting/documents/e_voting_documents.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/cabinet/voting/document/c_voting_document'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, h_msgbox, c_voting_document, h_check_mobile)
{
	return function (options_arg)
	{

		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_grid_url = controller.base_url + "?action=documents.jqgrid";
		controller.base_crud_url = controller.base_url + "?action=document.crud";
		controller.base_download_url = controller.base_url + "?action=document.download";

		controller.base_document_url = controller.base_url + "?action=document.main";
		controller.base_get_url = controller.base_document_url + "&cmd=get";

		var gridReloadInterval = '';
		var isMobile = h_check_mobile.isMobile;

		var base_Render = controller.Render;
		controller.Render = function (Sel)
		{
			base_Render.call(this, Sel);
			controller.RenderGrid();
		}

		function downloadAction(data, options, rowObject) {
			var id_Vote_document = options.rowId;
			var url = controller.base_download_url
			+ '&id_Vote_document=' + id_Vote_document;

			return (
				'<div style="text-align: center">' +
					'<a href="'+ url +'" class="download-link">' +
						'<svg width="12" height="19" viewBox="0 0 20 31" fill="none" xmlns="http://www.w3.org/2000/svg" class="download-link">' +
							'<path d="M7.45098 0H12.549V16.3601H19.9827L10 26.2863L0.0173292 16.3601H7.45098V0Z" fill="#D3242C"/>' +
							'<path d="M20 31V28.4654H0V31H20Z" fill="#D3242C"/>' +
						'</svg>' +
					'</a>' +
				'</div>'
			)
		}

		function checkClickDownload(e) {
			return $(e.target).parent().hasClass('download-link') || $(e.target).parent().parent().hasClass('download-link')
		}

		var formatter= {
			md5: function (docExtra, options, rowObject)
			{
				return '<small>' + rowObject.doc_md5 + '</small>';
			}
		}

		controller.colModel = isMobile() ? 
			[
				{ name: 'id_Vote_document', hidden: true }
				, { name: 'id_Email_Message', hidden: true }
				, { label: 'Время', name: 'TimeSent', width: 70, search: false, classes: 'cpw-orpau-voting-docs-time-sent' }
				, { label: 'Документ', name: 'Document', width: 190, search: false, sortable: false }
				, { label: 'Файл', name: 'FileName', width: 35, search: false, sortable: false, formatter: downloadAction }
			] :
			[
				{ name: 'id_Vote_document', hidden: true }
				, { name: 'id_Email_Message', hidden: true }
				, { label: 'Время', name: 'TimeSent', width: 60, search: false, classes: 'cpw-orpau-voting-docs-time-sent' }
				, { label: 'Документ', name: 'Document', width: 150, search: false, sortable: false }
				, { label: 'Файл', name: 'FileName', width: 80, formatter: 'link', search: false }
				, { label: 'Размер', name: 'Size', width: 50, formatter: function(val) { return val? val+' байт': val }, search: false }
				, { label: 'Контрольная сумма (хэш) md5', name: 'doc_md5', width: 110, search: false, formatter: formatter.md5 }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;
			if (this.model) 
			{
				url+= '&id_Manager=' + this.model.id_Manager;
				if(self.model.id_Poll) url += '&id_Poll=' + self.model.id_Poll;
			}

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: isMobile() ? '' : 'Показаны документы: {1} из {2}'
				, emptyText: 'Нет документов для отображения'
				, emptyrecords: isMobile() ? null : 'Нет документов для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10
				, pager: '#cpw-orpau-voting-documents-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid, status, e) {
					if (!checkClickDownload(e))
						self.OnDocument(rowid); 
				}
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка документов", url, jqXHR.responseText, textStatus, errorThrown)
				}
				, gridComplete: function()
				{
					var rows = $(self.fastening.selector + ' table.grid')[0].rows;
					for (var i=0; i<rows.length;i++)
					{
						if(i == 0) continue;
						var item = rows[i];
		
						var id_Vote_document = item.cells[0].innerHTML;
						var id_Email_Message = item.cells[1].innerHTML;
						var url = self.base_download_url
							+ ((id_Vote_document && ''!=id_Vote_document && '&nbsp;'!=id_Vote_document)
								? '&id_Vote_document=' + id_Vote_document
								: '&id_Email_Message=' + id_Email_Message);
						var a = $(item.cells[4]).children();
						a.attr('href', url);
						a.attr('target', '_blank');
					}
				}
			});
			//grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });

			gridReloadInterval = setInterval(function() {
				grid.trigger('reloadGrid');
			}, 300000); // 300 sec === 5 min 
		}

		controller.OnDocument= function (rowid)
		{
			var grid = $(this.fastening.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			var id_Vote_document = rowdata.id_Vote_document;
			var docType = 'doc';
			if(id_Vote_document=='')
			{
				id_Vote_document = rowdata.id_Email_Message;
				docType = 'email';
			}
			var documentName = rowdata.Document;
			var time = rowdata.TimeSent;

			var url = this.base_crud_url + '&cmd=get&id=' + id_Vote_document + '&type=' + docType;

			var v_ajax = h_msgbox.ShowAjaxRequest("Получение текста документа с сервера", url);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.ShowDocument(data, documentName, time);
					}
				}
			});
		}

		controller.ShowDocument= function(text, title, date)
		{
			if(isMobile()) h_check_mobile.lockScroll();
			var cc_voting_document = c_voting_document();
			cc_voting_document.SetFormContent(text);
			h_msgbox.ShowModal({
				title: title + ' от ' + date
				, width: isMobile() ? window.innerWidth : 950
				, height: isMobile() ? window.innerHeight : 700
				, id_div: 'id-voting-document'
				, controller: cc_voting_document
				, buttons:['Закрыть текстовое отображение документа']
				, beforeClose: function() { if (isMobile()) h_check_mobile.unlockScroll(); }
			});
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy= function()
		{
			base_Destroy.call(this);
			if (gridReloadInterval)
			{
				clearInterval(gridReloadInterval);
			}
		}

		return controller;
	}
});