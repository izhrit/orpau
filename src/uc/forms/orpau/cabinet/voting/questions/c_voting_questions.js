define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/voting/questions/e_voting_questions.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/cabinet/voting/question/c_voting_question'
	, 'forms/orpau/cabinet/voting/signing/c_voting_signing'
	, 'forms/base/codec/codec.copy'
	, 'forms/orpau/base/h_questions'
	, 'forms/orpau/base/h_check_mobile'
	, 'forms/orpau/profile/h_profile'
	, 'forms/base/h_validate'
	, 'forms/base/h_times'
],
function (c_fastened, tpl, h_msgbox, c_voting_question, c_voting_signing, codec_copy, h_questions, h_check_mobile, h_profile, h_validate, h_times)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_grid_url = controller.base_url + "?action=questions.jqgrid";
		controller.base_crud_url = controller.base_url + "?action=question.crud";
		controller.vote_crud_url = controller.base_url + "?action=vote.crud";
		controller.disable_grid_selecting = options_arg && options_arg.disable_grid_selecting ? options_arg.disable_grid_selecting : false;

		var gridReloadInterval = '';
		var isMobile = h_check_mobile.isMobile;

		var base_Render = controller.Render;
		controller.Render = function (Sel)
		{
			base_Render.call(this, Sel);

			this.RenderGrid();
		}

		var formatter =   {
			sqlDate: function(sqlDate) {
				var dateAndTimeSplit = sqlDate.split(' ');
				var dateSplit = dateAndTimeSplit[0].split('-');
				var timeSplit = dateAndTimeSplit[1].split(':')

				return dateSplit[2] + '.' + dateSplit[1] + '.' + dateSplit[0] + ' ' 
				+ timeSplit[0] + ':' + timeSplit[1];
			}
			,date: function(cellvalue, options, rowObject) {
				return cellvalue && cellvalue.length > 0? '<span class="q-time">'+formatter.sqlDate(cellvalue)+'</span>': ''
			}
			,answer: function(voteExtra, options, rowObject) {
				if(typeof voteExtra == 'string') {
					voteExtra = JSON.parse(voteExtra);
				}
				return voteExtra && voteExtra.Ответ ? voteExtra.Ответ : '';
			}
			,signDate: function(voteExtra, options, rowObject) {
				if(typeof voteExtra == 'string') {
					voteExtra = JSON.parse(voteExtra);
				}
				if (!voteExtra || !voteExtra.Подписано || voteExtra.Подписано.length <= 0)
				{
					return '';
				}
				else
				{
					var txt= voteExtra.Подписано;
					var count= 0;
					for (var i = 0; i < txt.length; i++)
					{
						if (':'==txt.charAt(i))
							count++;
					}
					if (count > 1)
					{
						var pos= txt.lastIndexOf(':');
						txt= txt.substring(0,pos);
					}
					return '<span class="q-sign-time">'+txt+'</span>';
				}
			}
			,result: function(questionExtra, options, rowObject) {
				if(typeof questionExtra == 'string') {
					questionExtra = JSON.parse(questionExtra);
				}
				if(questionExtra)
				{
					if(questionExtra.Решение_НЕ_принято)
					{
						return 'решение не принято';
					}
					if(questionExtra.Принято_решение)
					{
						return 'принято решение: «' + questionExtra.Принято_решение + '»'
					}
				}
				return '';
			}
		}

		controller.colModel = isMobile() ? 
			[
				{ name: 'id_Question', hidden: true }
				, { label: 'Вопрос', name: 'Title', width: 40 }
				, { label: 'Подписано', name: 'VoteExtra', width: 23, 
					search: false, sortable: false, formatter: formatter.signDate }
				, { label: 'Результат голосования', name: 'QuestionExtra', width: 37, 
					search: false, sortable: false, formatter: formatter.result }
			] :
			[
				{ name: 'id_Question', hidden: true }
				, { label: 'Время', name: 'Time', width: 60, formatter: formatter.date }
				, { label: 'Вопрос', name: 'Title', width: 180 }
				, { label: 'Ответ АУ', name: 'VoteExtra', width: 70, 
					search: false, sortable: false, formatter: formatter.answer }
				, { label: 'Подписано', name: 'VoteExtra', width: 60, 
					search: false, sortable: false, formatter: formatter.signDate }
				, { label: 'Результат', name: 'QuestionExtra', width: 60, 
					search: false, sortable: false, formatter: formatter.result }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;
			if (this.model) 
			{
				url+= '&id_Manager=' + this.model.id_Manager;
				if (self.model.id_Poll)
					url += '&id_Poll=' + self.model.id_Poll;
			}

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: isMobile() ? null : 'Показаны вопросы: {1} из {2}'
				, emptyText: 'Нет вопросов для отображения'
				, emptyrecords: isMobile() ? null : 'Нет вопросов для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10
				, pager: '#cpw-orpau-voting-questions-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { self.CheckAuProfile(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка вопросов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });

			gridReloadInterval = setInterval(function() {
				grid.trigger('reloadGrid');
			}, 300000); // 300 sec === 5 min 
		}

		controller.CheckAuProfile = function(rowid) {
			var self = this;
			var id_Manager = this.model.id_Manager;

			h_profile.ManagerRead(
				self.base_url
				, id_Manager
				, function(manager) {
					if(h_validate.ValidateEmail(manager.EMail) && h_validate.ValidatePhoneNumber(manager.Phone)) {
						self.OnQuestion(rowid);
					} else {
						h_profile.PleaseEditProdile(
							'Заполните контактные данные, чтобы перейти к голосованию'
							, function() { h_profile.ModalEditManagerProfile(self.base_url, id_Manager, null); });
					}
				}
			);
		}

		controller.OnQuestion= function (rowid)
		{
			var self = this;

			if(self.disable_grid_selecting) return false;

			var grid = $(this.fastening.selector + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);
			var id_Question = rowdata.id_Question;

			var id_Manager = this.model.id_Manager;
			var url = self.base_crud_url + '&cmd=get' + '&id=' + id_Question + '&id_Manager=' + id_Manager;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос данных голосования на сервере", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null==data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.initModal(rowid, id_Question, data);
					}
				}
				, error: function(jqXHR, textStatus, errorThrown)
				{
					var status = jqXHR.status;
					if(status == '404') {
						h_msgbox.ShowModal({
							title: 'Внимание', width: 350
							, html: 'Данные по голосованию изменились,<br/>нужно обновить таблицу вопросов'
						});
					} else {
						v_ajax.ShowAjaxError(jqXHR, textStatus);
					}
				}
			});
		}

		controller.initModal = function(rowid, id_Question, data){
			var self = this;
			if(isMobile()) h_check_mobile.lockScroll();
			rowid = +rowid;
			var cquestion = c_voting_question({ base_url: self.base_url });
			cquestion.SetFormContent(data);

			var closeButton = 'Отменить';
			var signButton = 'Перейти к подписанию';

			var isSigned = data.VoteExtra && data.VoteExtra.Подписано && data.VoteExtra.Подписано.length > 0;

			var buttons = [signButton, closeButton];
			if(('Голосование'!=data.Status && 'Продолжено'!=data.Status) || isSigned) buttons = ['Закрыть'];
			h_msgbox.ShowModal({
				title: "Голосование"
				, controller: cquestion
				, buttons: buttons
				, id_div: "cpw-form-orpau-question"
				, beforeClose: function() { if(isMobile()) h_check_mobile.unlockScroll(); }
				, onclose: function (btn)
				{
					if(btn == signButton)
					{
						var qData = cquestion.GetFormContent();
						if (qData.Ответ) 
						{
							self.Сохранить_ответ(rowid-1, id_Question, qData, data);
						}
					}
				}
			})
		}

		controller.Сохранить_ответ = function (iquestion, id_Question, question_model, question)
		{
			var self = this;

			var question = question;
			question.id_Manager = self.model.id_Manager;

			if(!question.VoteExtra) {
				question.VoteExtra = {
					Голосование: question.QuestionExtra
					,Ответ: question_model.Ответ
					,Подписано: ''
				}
			} else {
				question.VoteExtra.Ответ = question_model.Ответ;
			}

			if(question.id_Vote){
				var url = self.vote_crud_url + '&cmd=update' + '&id=' + question.id_Vote;
			} else {
				var url = self.vote_crud_url + '&cmd=add';
			}

			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка выбранного ответа на сервер", url);
			var self = this;
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: question
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						if(!question.id_Vote){
							question.id_Vote = data.id_Vote;
						}
						self.OnSign(question);
						$(self.fastening.selector + ' table.grid').trigger('reloadGrid');
					}
				}
			});
		}

		controller.приготовить_данные_для_бюллетеня= function(data)
		{
			var self = this;
			var id_Manager = self.model.id_Manager;

			var ccopy = codec_copy();
			var бюллетень = {
				id_Question: data.id_Question
				,id_Vote: data.id_Vote
				,Участник: {
					id_Manager: id_Manager
					,Фамилия: self.model.Фамилия
					,Имя: self.model.Имя
					,Отчество: self.model.Отчество
					,ЕФРСБ: self.model.efrsbNumber
					,ИНН: self.model.ИНН
					,Телефон: self.model.Phone
					,Email: self.model.EMail
					,СостоитВПрофсоюзе: self.model.СостоитВПрофсоюзе
					,ИспользуетАСП: self.model.ИспользуетАСП
				}
				,Вопрос: {
					В_повестке: data.Title
					,Формулировка: data.QuestionExtra.Формулировка
					,Варианты: ccopy.Copy(data.QuestionExtra.Варианты ? data.QuestionExtra.Варианты : h_questions.Варианты_ответов_формы1)
					,Ответ: data.VoteExtra.Ответ ? data.VoteExtra.Ответ : ''
				}
				,Дата_голосования: h_times.safeDateTime().toLocaleDateString() //todo проверить
			};
			return бюллетень;
		}

		controller.OnSign = function (data)
		{
			var self = this;
			var бюллетень = self.приготовить_данные_для_бюллетеня(data);

			var checked = 0 != $(self.fastening.selector + ' input[type="radio"]:checked').length;
			if (data && ('Голосование'!=data.Status && 'Продолжено'!=data.Status))
			{
				h_msgbox.ShowModal({
					title: 'Подписание неуместно', width: 350
					, html: 'Голосование в настоящее время закрыто.'
				});
			}
			else if (!бюллетень.Вопрос.Ответ && !checked)
			{
				h_msgbox.ShowModal({
					title: 'Данные для подписания отсутствуют', width: 380
					, html:'Укажите ответы на вопросы, которые хотите подписать.'
				});
			}
			else
			{
				if(isMobile()) h_check_mobile.lockScroll();
				var csigning = c_voting_signing({ base_url: self.base_url });

				csigning.ау = {
					INN:self.model.ИНН
					, id_Manager:self.model.id_Manager
					, LastName:self.model.Фамилия
					, FirstName:self.model.Имя
					, MiddleName:self.model.Отчество
				};

				csigning.SetFormContent(бюллетень);

				csigning.CloseSelf = function() {
					h_msgbox.Close({id_div: "cpw-form-orpau-signing"});
					$(self.fastening.selector + ' table.grid').trigger('reloadGrid');
				}
		
				h_msgbox.ShowModal({
					title: "Подписание бюллетеня голосования"
					, controller: csigning
					, buttons: ['Отмена']
					, id_div: "cpw-form-orpau-signing"
					, beforeClose: function() { if(isMobile()) h_check_mobile.unlockScroll(); }
					, onclose: function (btn, me)
					{
						csigning.Destroy();
					}
				})
			}
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy= function()
		{
			base_Destroy.call(this);
			if (gridReloadInterval)
			{
				clearInterval(gridReloadInterval);
			}
		}

		return controller;
	}
});