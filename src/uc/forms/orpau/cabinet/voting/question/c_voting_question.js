define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/voting/question/e_voting_question.html'
	, 'forms/orpau/cabinet/voting/question/form1/c_question_form1'
	, 'forms/orpau/cabinet/voting/question/form2/c_question_form2'
	, 'forms/orpau/base/h_questions'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, c_question_form1, c_question_form2, h_questions, h_check_mobile)
{
	return function (options_arg) 
	{
		var controller = c_fastened(tpl);

		controller.size = h_check_mobile.isMobile() ? 
			{ width: window.innerWidth, height: window.innerHeight } : { width: 630, height: 620 };

		var base_render = controller.Render;
		controller.Render = function (sel)
		{
			base_render.call(this, sel);

			var question_model = {};
			var fmodel = this.fastening.model;

			var VoteExtra = fmodel.VoteExtra;
			var Форма_бюллетеня = fmodel.QuestionExtra.Форма_бюллетеня;

			if(VoteExtra && VoteExtra.Ответ) question_model.Ответ = VoteExtra.Ответ;
			if(VoteExtra && VoteExtra.Подписано) question_model.Подписано = VoteExtra.Подписано;

			var QuestionExtra = fmodel.QuestionExtra;
			if (Форма_бюллетеня == 'f2' || QuestionExtra.Варианты)
			{
				this.c_answer = c_question_form2();
				question_model.Предложенные_варианты = QuestionExtra.Варианты;
				if (VoteExtra && VoteExtra.Ответ)
				{
					var ivariant = question_model.Предложенные_варианты.indexOf(VoteExtra.Ответ);
					if (-1 != ivariant)
					{
						question_model.Выбранный_вариант = ivariant;
					}
					else
					{
						question_model.Свой_вариант = VoteExtra.Ответ;
						question_model.Выбранный_вариант = question_model.Предложенные_варианты.length;
					}
				}
			} else
			{
				var form1_ответы = h_questions.Варианты_ответов_формы1;
				if (Форма_бюллетеня == 'f1' || !VoteExtra || !VoteExtra.Ответ || -1 != form1_ответы.indexOf(VoteExtra.Ответ))
				{
					this.c_answer = c_question_form1();
				}
				else
				{
					this.c_answer = c_question_form2();
					question_model.Выбранный_вариант = 3;
					question_model.Свой_вариант = VoteExtra && VoteExtra.Ответ ? VoteExtra.Ответ : '';
					question_model.Предложенные_варианты = form1_ответы;
				}
			}
			this.c_answer.SetFormContent(question_model);
			this.c_answer.CreateNew(sel + ' div.answer');
		}

		controller.GetFormContent = function ()
		{
			var res = this.c_answer.GetFormContent();
			if (!res.Предложенные_варианты)
			{
				return res;
			}
			else
			{
				var iвариант = parseInt(res.Выбранный_вариант);
				return { Ответ: (iвариант < res.Предложенные_варианты.length) ? res.Предложенные_варианты[iвариант] : res.Свой_вариант };
			}
		}

        return controller;
    }
});