define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/voting/question/form1/e_question_form1.html'
],
function (c_fastened, tpl)
{
	return function (options_arg) 
	{
		var controller = c_fastened(tpl);
        return controller;
    }
});