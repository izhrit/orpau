define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/voting/signing/e_voting_signing.html'
	, 'forms/base/h_msgbox'
	, 'forms/base/codec/codec.copy'
	, 'tpl!forms/orpau/cabinet/voting/main/p_bulletin.txt'
	, 'forms/orpau/cabinet/voting/signing/h_voting_signing'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, h_msgbox, codec_copy, p_bulletin, h_voting_signing, h_check_mobile)
{
	return function (options_arg) 
	{
		var isMobile = h_check_mobile.isMobile;
		var options = { isMobile: isMobile };
		var controller = c_fastened(tpl, options);

		controller.size = isMobile() ? { width: window.innerWidth, height: window.innerHeight } : { width: 900, height: 630 };

		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_code_url = controller.base_url + '?action=vote.sms';

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			if (!this.model)
			{
				$(sel + ' div.ahtung').hide();
				$(sel + ' div.to-sign').hide();
				$(sel + ' div.send-key-first').hide();
				$(sel + ' div.key-sent').hide();
			}
			else
			{
				var bulletin_data = codec_copy().Copy(this.model);
				bulletin_data.Адрес_ОРПАУ = 'https://orpau.rsit.ru/ui.php';
				this.text_model = p_bulletin(bulletin_data).replace(/<br\/>/gi, "\r\n");

				$(sel + ' div.no-sign').hide();
				$(sel + ' div.key-sent').hide();
				$(sel + ' textarea').text(this.text_model);
			}

			var self = this;
			$(sel + ' button.send-code').click(function (e) { self.OnSendCode(); });
			$(sel + ' button.resend-code').click(function (e) { self.OnSendCode(); });
			$(sel + ' button.sign').click(function (e) { self.OnSign(); });
			$(sel + ' button.choose-cert').click(function(){ self.onSignByCert(); });
		}

		controller.OnSendCode= function()
		{
			$(this.fastening.selector + ' div.ahtung').hide();
			var self = this;
			var id_Vote = self.model.id_Vote;
			// todo уточнить необходимость
			var ИспользуетАСП = self.model.Участник.ИспользуетАСП;
			if(!ИспользуетАСП){
				h_msgbox.ShowModal({
					title: "Действия для подписания", width: 380
					, html: "Для подписания через код подтверждения Вам нужно подписать заявление на использование аналога собственноручной подписи!"
				});
				return false;
			}

			var url = this.base_code_url + '&cmd=throw-code&id_Vote=' + id_Vote;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса кода подтверждения", url);

			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: {data:this.model, text:this.text_model}
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						self.OnCodeSent(data);
					}
				}
			});
		}

		controller.OnSign = function ()
		{
			var sel = this.fastening.selector;
			var code = $(sel + ' input.code').val().trim();

			if ('' == code)
			{
				h_msgbox.ShowModal({
					title: "Действия при подписании", width: 380
					, html: "Для подписания Вам нужно указать полученный код подтверждения!"
				});
				$(sel + ' input.code').focus();
			}
			else
			{
				var self = this;
				var id_Vote = self.model.id_Vote;

				var url = this.base_code_url + '&cmd=catch-code&id_Vote=' + id_Vote;
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка подписи", url);
				v_ajax.ajax({
					dataType: "json"
					, type: 'POST'
					, data: { data: this.model, text: this.text_model, code: code }
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.OnSigned(data);
						}
					}
				});
			}
		}

		controller.OnSigned= function(signed)
		{
			var self = this;
			if (!signed.ok)
			{
				h_msgbox.ShowModal({
					title: "Подписание бюллетеня не произошло!", width: 380
					, html: "Подписание бюллетеня голосования не произошло по причине:<br/>"
					+ "<b>" + signed.why + "</b>"
					, onclose: function ()
					{
						self.StartSigningAgain();
					}
				});
			}
			else
			{
				h_msgbox.ShowModal({
					title: "Подписание бюллетеня", width: 380
					, html: "Подписание бюллетеня голосования и отправка его на сервер завершилась успешно!</div>"
				});
				self.CloseSelf();
			}
		}

		controller.StartSigningAgain= function()
		{
			var sel = this.fastening.selector;
			$(sel + ' div.key-sent').hide();
			$(sel + ' div.send-key-first').show();
			$(sel + ' input.code').val('');
			$(sel + ' span.test-code').hide();
		}

		controller.OnCodeSent= function(sent)
		{
			var sel = this.fastening.selector;
			$(sel + ' div.send-key-first').hide();
			$(sel + ' div.key-sent').show();
			$(sel + ' input.code').focus();

			$(sel + ' span.sms.time').text(sent.sms.time);
			$(sel + ' span.sms-number').text(sent.sms.number);
			$(sel + ' span.email.time').text(sent.email.time);
			$(sel + ' span.email-address').text(sent.email.address);

			$(sel + ' button.resend-code').hide();

			this.code_sent_now = (new Date()).getTime();
			this.OnTimer();

			$(sel + ' .resend-after').show();

			if (!sent.test_code)
			{
				$(sel + ' span.test-code').hide();
			}
			else
			{
				$(sel + ' span.test-code').show();
				$(sel + ' span.test-code span.code').text(sent.test_code);
			}

			if (!this.timer_id)
			{
				var self = this;
				var each_second = function () { self.OnTimer(); }
				this.timer_id = setInterval(each_second, 900)
			}
		}

		controller.OnTimer= function()
		{
			var sel = this.fastening.selector;
			var now = (new Date()).getTime();
			var diff = now - this.code_sent_now;
			var diff_seconds = diff / 1000;
			var to_resend = Math.floor(60 - diff_seconds);
			if (to_resend > 0)
			{
				$(sel + ' .resend-after span.seconds').text(to_resend);
			}
			else
			{
				$(sel + ' .resend-after').hide();
				$(sel + ' button.resend-code').show();
			}
		}

		controller.onSignByCert = function()
		{
			var self = this;
			var id_Vote = self.model.id_Vote;
			var postData = { data: self.model, text: self.text_model }

			var ау = {
				INN:self.ау.INN
				, id_Manager:self.ау.id_Manager
				, LastName:self.ау.LastName
				, FirstName:self.ау.FirstName
				, MiddleName:self.ау.MiddleName
			};

			var url = this.base_url + '?action=vote.cert';
			h_voting_signing.SignByCertificate(ау, url, id_Vote, postData
				, function(){
					h_msgbox.ShowModal({
						title: "Подписание бюллетеня", width: 380
						, html: "Подписание бюллетеня голосования и отправка его на сервер завершилась успешно!</div>"
					});
					self.CloseSelf();
				}
			)
		}

		var base_Destroy = controller.Destroy;
		controller.Destroy= function()
		{
			base_Destroy.call(this);
			if (this.timer_id && null!=this.timer_id)
			{
				clearInterval(this.timer_id);
				delete this.timer_id;
			}
		}

		return controller;
	}
});