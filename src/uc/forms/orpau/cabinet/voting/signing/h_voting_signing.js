define([
	'forms/base/h_msgbox'
  , 'forms/orpau/base/h_cryptoapi'

  , 'tpl!forms/orpau/login/v_choosed_cert_without_inn.html'
  , 'tpl!forms/orpau/login/v_au_not_found.html'
  , 'forms/orpau/base/h_crypto_ex'

  , 'tpl!forms/orpau/asp/v_choosed_cert_bad_for_manager.html'
],
function (h_msgbox, h_cryptoapi
    , v_choosed_cert_without_inn, v_au_not_found, h_crypto_ex, v_choosed_cert_bad_for_manager)
{
	var helper = {};

	helper.SignByCertificate = function(ау, base_sign_url, id_Vote, data, on_sign)
	{
		try
		{
			var self = this;
			app.cryptoapi.ChooseCertificateAnd(function (cert)
			{
				self.SignVote(ау, cert,base_sign_url, id_Vote, data, on_sign);
			});
		}
		catch (ex)
		{
			h_crypto_ex.handleCryptoApiEx(ex);
		}
	}

	helper.SignVote = function (ау, cert,base_sign_url, id_Vote, data, on_sign)
	{
		var dn_fields= h_cryptoapi.GetDNFields(cert.SubjectName);
		if (!dn_fields.ИНН || null == dn_fields.ИНН || '' == dn_fields.ИНН)
		{
			h_msgbox.ShowModal({width:460, title:'Аутентификация невозможна',html:v_choosed_cert_without_inn(dn_fields)});
		}
		else if (dn_fields.ИНН != ау.INN)
		{
			h_msgbox.ShowModal({width:460, title:'Аутентификация невозможна',html:v_choosed_cert_bad_for_manager({ cert: dn_fields, ау: ау })});
		}
		else
		{
			var url = base_sign_url + '&cmd=sign-by-cert&id_Vote=' + id_Vote;
			var v_ajax = h_msgbox.ShowAjaxRequest("Подписание бюллетеня", url);
			var base64_encoded_signature= app.cryptoapi.SignBase64(data.text,/*detached=*/true,cert);

			data.cert = { SerialNumber: cert.SerialNumber, Issuer: cert.IssuerName, Subject: cert.SubjectName };
			data.base64_encoded_signature = base64_encoded_signature;

			v_ajax.ajax
			({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: data
				, error: function (data, textStatus)
				{
					if (404 != data.status)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						h_msgbox.ShowModal({width:460, title:'Аутентификация неудачна',html: v_au_not_found(dn_fields)});
					}
				}
				, success: function (data, textStatus)
				{
					if (null == data || !data.ok)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						on_sign();
					}
				}
			});
		}
	}

	return helper;
});