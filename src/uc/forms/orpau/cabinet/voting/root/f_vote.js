define(['forms/orpau/cabinet/voting/root/c_vote'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'vote'
		, Title: 'Добро пожаловать в систему orpau'
	};
	return form_spec;
});