﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/voting/root/e_vote.html'
	, 'forms/orpau/cabinet/voting/main/c_voting'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/profile/h_profile'
	, 'forms/orpau/login/h_login_by_cert'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, c_voting, h_msgbox, h_profile, h_login_by_cert, h_check_mobile)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_auth_url = base_url + "?action=manager.auth";
		var base_login_url = base_url + '?action=manager.authPassword';

		var controller = c_fastened(tpl);
		var isMobile = h_check_mobile.isMobile;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.BindResizeWindow();

			var self= this;
			this.initLoginDialog(sel);

			$(sel + " .jquery-menu").menu();
			$(sel + " .profile-dropdown").click(function(){
				$(this).find("ul.jquery-menu").toggle();
			})
			
			//close menu
			$(document).on("click", function (e) {
				if($(e.target).hasClass("open-profile")) self.OnProfile();
				if($(e.target).hasClass("logout")) { if (self.OnLogout) { self.OnLogout(); return false; } }

				if(!$(e.target).hasClass("profile-dropdown") && !$(e.target).parent().hasClass("profile-dropdown")) {
					$(sel + " .profile-dropdown ul.jquery-menu").hide();
				}
			});
			
			var startsel = sel + ' .cpw-orpau-start-page';
			$(startsel + ' .forgot-password').click(function(){ h_profile.ForgotPassword(base_url); });
			$(startsel + ' .orpau-login-form').submit(function(e){
				e.preventDefault();
				var login_data = $(this).serialize();
				self.OnLoginByPass(login_data, null); 
			});
			$(startsel + ' .icon-password-toggle').click(function (e) {
				var input = $(dialogSel + ' div.password-input-container input');
				if(input.attr('type') == 'password'){
					input.prop('type', 'text');
					$(this).prop('title', 'Скрыть пароль');
				} else {
					input.prop('type', 'password');
					$(this).prop('title', 'Показать пароль');
				}
			});
		}

		controller.OnProfile = function() {
			var data = this.voting_cabinet.GetFormContent();
			h_profile.ModalEditManagerProfile(base_url, data.id_Manager, null);
		}
			
		controller.BindResizeWindow = function ()
		{
			if (!controller.on_resize)
			{
				var sel = controller.fastening.selector;
				controller.on_resize = function ()
				{
					if ($(document).width() < 300)
					{
						return false;
					}
					else
					{
						var $grid = $(sel + ' table.grid'),
							newWidth = $grid.closest(".ui-jqgrid").parent().width();
						if (newWidth)
							$grid.setGridWidth(newWidth, true);
					}
				};
				controller.on_resize();
			}
			window.onresize = this.on_resize;
		}

		controller.OnButtonLogin = function (dialog, dialogTitle)
		{
			if (this.voting_cabinet)
			{
				this.OnLogout();
			}
			else
			{
				if (!app.start_real_orpau)
				{
					h_msgbox.ShowModal({ width: 400, title: 'Авторизация', html: 'Сервис будет доступен с 17.05.2021' });
				}
				else
				{
					dialog.dialog('option', 'title', dialogTitle);
					dialog.dialog('open');
				}
			}
		}

		controller.initLoginDialog = function(sel) {
			var self = this;
			var dialogSel = '.orpau-login-dialog';
			var dialogTitle = 'Выбор варианта авторизации';

			var dialog = $(sel + ' .orpau-login-dialog').dialog({
				title: dialogTitle,
				autoOpen: false,
				width: 400,
				modal: true,
			})

			$(sel + ' button.login').click(function () { self.OnButtonLogin(dialog, dialogTitle); });

			function centrizeDialog() { dialog.dialog("option", "position", {my: "center", at: "center", of: window}); }

			$(dialogSel + ' .with-certificate').click(function(){ 
				dialog.dialog('close'); 
				self.OnLoginByCert(); 
			})
			$(dialogSel + ' .with-password').click(function(){ 
				dialog.dialog('option', 'title', 'Авторизация');
				slide('right');
				$(dialogSel + ' .orpau-login-form').animate({height: 'show'}, 200, centrizeDialog).css('display', 'inline-block');
				
			})
			$(dialogSel + ' .back-button').click(function(){
				dialog.dialog('option', 'title', dialogTitle);
				slide('left');
				$(dialogSel + ' .orpau-login-form').animate({height: 'hide'}, 200, centrizeDialog);
			});

			$(dialogSel + ' .orpau-login-form').submit(function(e){ 
				e.preventDefault();
				var login_data = $(this).serialize();
				self.OnLoginByPass(
					login_data,
					function(){ 
						slide('left'); 
						$(dialogSel + ' .orpau-login-form').animate({height: 'hide'}, 200, centrizeDialog);
						dialog.dialog('close');
					}
				);
			})

			$(dialogSel + ' .icon-password-toggle').click(function (e) {
				var input = $(dialogSel + ' div.password-input-container input');
				if(input.attr('type') == 'password'){
					input.prop('type', 'text');
					$(this).prop('title', 'Скрыть пароль');
				} else {
					input.prop('type', 'password');
					$(this).prop('title', 'Показать пароль');
				}
			});

			$(dialogSel + ' .forgot-password').click(function(){ h_profile.ForgotPassword(base_url); });
		}

		controller.OnLoginByCert = function ()
		{
			if(isMobile())
			{
				h_msgbox.ShowModal
				({
					title: 'Вход по ЭЦП'
					, width: window.innerWidth-20
					, html: '<span>С мобильного устройства возможен вход только по логину.<br/>Если у вас нет логина, то зайдите с ЭЦП с компьютера и задайте его.</small>'
				});
				return false;
			}

			var self= this;
			h_login_by_cert.Login(base_url, base_auth_url
				, function(cert, token, base64_encoded_signature, auth_info, variant)
				{
					self.OnLogged(cert, token, base64_encoded_signature, auth_info, variant);

					if(!auth_info.Login) {
						var btnOk = 'Да';
						h_msgbox.ShowModal
						({
							title: ''
							, width: 450
							, html: '<div style="line-height:1em;font-size:22px;font-weight:600">Хотите задать логин?</div>'
							, buttons: [btnOk, 'Нет']
							, onclose: function (btn)
							{
								if (btn == btnOk)
									self.OnProfile();
							}
						});
					}
				}
			);
		}

		controller.AutoLogin = function(login)
		{
			var login_data = { Login: login, Password: '' };
			var self = this;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос авто авторизации на сервере", base_login_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					if (null != responce && false != responce.ok)
					{
						self.OnLogged('', '', '', responce, 'without_datamart');
					}
				}
			});
		}

		controller.OnLoginByPass = function(login_data, closeDialogFunc)
		{
			var self = this;
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос авторизации на сервере", base_login_url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: login_data
				, success: function (responce, textStatus)
				{
					var mobileMsgBoxWidth = window.innerWidth-20;
					if (null==responce)
					{
						h_msgbox.ShowModal({
							width: isMobile() ? mobileMsgBoxWidth : 500
							, title: 'Неудачная аутентификация'
							, html: 'Вы ввели неправильное имя пользователя или пароль!'
						});
					}
					else if (false == responce.ok)
					{
						h_msgbox.ShowModal
						({
							title: 'Ошибка аутентификации'
							, width: isMobile() ? mobileMsgBoxWidth : 450
							, html: '<span>Не удалось провести аутентификацию по причине:</span><br/> <center><b>\"'
								+ responce.reason + '\"</b></center>'
								+ '<small>C дополнительными вопросами обращайтесь в службу поддержки.</small>'
						});
					}
					else
					{
						self.OnLogged('', '', '', responce, 'without_datamart');
						if(closeDialogFunc) closeDialogFunc();
					}
				}
			});
		}

		controller.OnLogged = function (cert, token_variant, base64_encoded_signature, auth_info, variant)
		{
			var sel= this.fastening.selector;
			$(sel + ' div.cpw-orpau-start-page').hide();
			$(sel + ' div.cpw-orpau-main').show();
			if(!isMobile()) $(sel + ' button.login').hide();
			$(sel + ' span.profile-bar').show();
			$(sel + ' span.profile-name').text(getNameString(auth_info));

			auth_info.login_details = { cert: cert, token:token_variant, base64_encoded_signature:base64_encoded_signature, variant: variant };

			$(sel + ' a[href="#main-tab-cabinet"]').click();

			var voting_cabinet= this.voting_cabinet= c_voting({ base_url: base_url });
			voting_cabinet.SetFormContent(auth_info);
			voting_cabinet.Edit(sel + ' div.cabinet');

		}

		controller.OnLogout = function ()
		{
			var sel= this.fastening.selector;
			this.voting_cabinet.Destroy();
			$(sel + ' div#cpw-orpau-main-tabs > div#main-tab-cabinet > div.cabinet').html('');
			$(sel + ' div.cpw-orpau-main').hide();
			$(sel + ' div.cpw-orpau-start-page').show();

			if(!isMobile()) $(sel + ' button.login').show();
			$(sel + ' span.profile-bar').hide();
			$(sel + ' span.profile-name').text('');
			delete this.voting_cabinet;

			var v_ajax = h_msgbox.ShowAjaxRequest("Завершение сессии АУ на сервере", base_auth_url + '&cmd=logout');
			v_ajax.ajax
			({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data || !data.ok)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						$(sel + ' a[href="#main-tab-confirmations"]').click();
					}
				}
			});
		}

		function getNameString(val) {
			if(null == val) return 'Неустановленный АУ';
			var name = '';
			if(val.Фамилия) name += val.Фамилия;
			if(val.Имя) { 
				if(name != '') name += ' ';
				name += val.Имя.charAt(0) + '.';
			}
			if(val.Отчество) {
				if(name != '') name += ' ';
				name += val.Отчество.charAt(0) + '.'
			}
			return name;
		}

		// Анимация горизонтального пролистывания
		function slide (to) {
			var $wrap = $( ".orpau-login-wrap" );
			var initalLeftMargin = +$wrap.css('margin-left').replace("px", "");
			var wrapWidth = 385;
			if(to === 'right')
			{
				var newLeftMargin = (initalLeftMargin - wrapWidth);
			}
			if(to === 'left')
			{
				var newLeftMargin = (initalLeftMargin + wrapWidth);
			}
			$wrap.animate({marginLeft: newLeftMargin}, 200);
		}

		return controller;
	}
});