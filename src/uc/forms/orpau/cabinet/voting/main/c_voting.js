define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/orpau/cabinet/voting/main/e_voting.html'
	, 'forms/orpau/cabinet/voting/questions/c_voting_questions'
	, 'forms/orpau/cabinet/voting/documents/c_voting_documents'
	, 'forms/orpau/cabinet/voting/log/c_voting_log'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/base/h_check_mobile'
],
function (c_fastened, tpl, c_voting_questions, c_voting_documents, c_voting_log, h_msgbox, h_check_mobile)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'orpau' : options_arg.base_url;
		var base_voting_url = base_url + '?action=vote.crud';

		var options = {
			field_spec:
			{
				  Вопросы: { controller: function () { return c_voting_questions({ base_url: base_url }); }, render_on_activate:true }
				, Документы: { controller: function () { return c_voting_documents({ base_url: base_url }); }, render_on_activate:true }
				, Журнал: { controller: function () { return c_voting_log({ base_url: base_url }); }, render_on_activate:true }
			}
			, isMobile: h_check_mobile.isMobile
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			$(sel + ' .refresh-btn').click(function(){
				$(sel + ' .grid').trigger('reloadGrid');
			});
		}

		// controller.Edit = function(sel) {
		// 	this.getVoting(sel);
		// }

		controller.getVoting = function(sel) 
		{
			var self = this;
			var manager_data = {
				ArbitrManagerID: self.model.ArbitrManagerID,
				id_Manager: self.model.id_Manager,
				inn: self.model.ИНН
			}
			var v_ajax= h_msgbox.ShowAjaxRequest("Запрос данных голосования на сервере", base_voting_url + '&cmd=get');
			v_ajax.ajax({
				dataType: "json", type: 'GET', cache: false
				, success: function (responce, textStatus)
				{
					if (null==responce)
					{
						h_msgbox.ShowModal({
							width: 500
							, title: 'Ошибка получения данных'
							, html: 'Не удалось получить данные по голосованиям'
						});
					}
					else
					{
						self.SetFormContent(responce);
						//self.model.voting = setFormContentPrepare(responce);
						self.Render(sel);
					}
				}
			});
		}

		function setFormContentPrepare (data) {
			return ("string" != typeof data) ? data :
					('[' != data.charAt(0) && '{' != data.charAt(0)) ? null :
					JSON.parse(data);
		}

		return controller;
	}
});