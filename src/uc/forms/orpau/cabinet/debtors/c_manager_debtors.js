define([
	'forms/orpau/base/c_adapted_grid'
	, 'tpl!forms/orpau/cabinet/debtors/e_manager_debtors.html'
	, 'forms/base/h_msgbox'
	, 'forms/orpau/debtor/c_debtor'
],
function (c_fastened, tpl, h_msgbox, c_debtor)
{
	return function (options_arg)
	{

		var controller = c_fastened(tpl);
		controller.base_url = !options_arg ? 'orpau' : options_arg.base_url;
		controller.base_grid_url = controller.base_url + "?action=debtor.jqgrid";
		controller.base_crud_url = controller.base_url + '?action=debtor.crud';

		var base_Render = controller.Render;
		controller.Render = function (Sel)
		{
			base_Render.call(this, Sel);
			controller.RenderGrid();
			this.on_resize();
		}

		controller.size = { width: 1050, height: "590" };

		controller.colModel =
			[
				{ name: 'id_Debtor', hidden: true }
				, { label: 'Должник', name: 'title', width: 595 }
				, { label: 'ИНН', name: 'inn', width: 180 }
				, { label: 'ОГРН', name: 'ogrn', width: 185 }
				, { label: 'СНИЛС', name: 'snils', width: 180 }
			];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = this.base_grid_url;
			if (this.model)
				url+= '&ArbitrManagerID=' + this.model.ArbitrManagerID;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: controller.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показаны процедуры: {1} из {2}'
				, emptyText: 'Нет процедур для отображения'
				, pgtext: "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 10

				, pager: '#cpw-orpau-manager-debtors-grid-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true
				, onSelectRow: function (rowid) { self.OnDebtor(rowid); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка процедур", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnDebtor = function (rowid)
		{
			var self = this;
			var selectedRow = $(this.fastening.selector + ' table.grid')[0].rows[rowid];
			var debtorName = selectedRow.cells[1].innerHTML;
			var id_Debtor = selectedRow.cells[0].innerHTML;
			var debtor_url = controller.base_crud_url + '&cmd=get&id=' + id_Debtor;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных наблюдателя с сервера", debtor_url);
			v_ajax.ajax
				({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (data, textStatus)
					{
						if (null == data)
						{
							v_ajax.ShowAjaxError(data, textStatus);
						}
						else
						{
							var cdebtor = c_debtor({ base_url: self.base_url });
							cdebtor.SetFormContent(data);
							h_msgbox.ShowModal
								({
									title: 'Информация о должнике ' + debtorName
									, controller: cdebtor
									, buttons: ['Закрыть']
									, id_div: "cpw-form-orpau-debtor"
								});

						}
					}
				});
		}

		return controller;
	}
});