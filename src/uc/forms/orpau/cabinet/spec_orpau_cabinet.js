define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var orpau_cabinet_specs = {

		controller: {
			"manager_cabinet": 
			{
				path: 'forms/orpau/cabinet/main/c_manager_cabinet'
				, title: 'Кабинет АУ'
				, keywords: 'Процедуры'
			}
			,"news":
			{
				path: 'forms/orpau/cabinet/news/c_news'
				, title: 'Новости'
			}
			,"schedule":
			{
				path: 'forms/orpau/cabinet/schedule/c_schedule'
				, title: 'Календарь'
			}
			,"datamart":
			{
				path: 'forms/orpau/cabinet/datamart/c_datamart'
				, title: 'Витрина данных'
			}
			,"voting":
			{
				path: 'forms/orpau/cabinet/voting/main/c_voting'
				, title: 'Раздел голосования АУ'
			}
			,"voting_questions":
			{
				path: 'forms/orpau/cabinet/voting/questions/c_voting_questions'
				, title: 'Вопросы на голосование АУ'
			}
			,"voting_documents":
			{
				path: 'forms/orpau/cabinet/voting/documents/c_voting_documents'
				, title: 'Документы к голосованию АУ'
			}
			,"voting_log":
			{
				path: 'forms/orpau/cabinet/voting/log/c_voting_log'
				, title: 'Журнал голосования АУ'
			}
		}

		, content: {
			 "logged-manager-1": { 
				path: 'txt!forms/orpau/cabinet/main/tests/contents/manager1.json.txt'
				, title: 'Автоирзованный АУ 1 (Иванов Иван Иванович)'
			}
			,"logged-manager-2": { 
				path: 'txt!forms/orpau/cabinet/main/tests/contents/manager2.json.txt'
				, title: 'Автоирзованный АУ 2 (Семёнов Семён Семёнович)'
			}
			,"logged-manager-3": { 
				path: 'txt!forms/orpau/cabinet/main/tests/contents/manager3.json.txt'
				, title: 'Автоирзованный АУ 3 (Афанасьев Григорий Григорьевич)'
			}
		}
	};
	return h_spec.combine(orpau_cabinet_specs);
});