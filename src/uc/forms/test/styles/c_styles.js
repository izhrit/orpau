﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/styles/v_styles.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			$(sel + ' .jquery-btn').button();
		}

		return controller;
	}
});