﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/collection/simple/e_test_collection.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});