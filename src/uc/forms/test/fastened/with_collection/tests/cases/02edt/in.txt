include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\fastened\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "+"

shot_check_png ..\..\shots\01sav.png

js wbt.SetModelFieldValue("Наименование", "ОАО Ага");

je $('[array-index="0"] .delete').click();
shot_check_png ..\..\shots\02edt-deleted.png

je wbt.Model_selector_prefix= '*[array-index="0"] ';
js wbt.SetModelFieldValue("", "3");

shot_check_png ..\..\shots\02edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01edt-1.json.result.txt
exit