start_store_lines_as fastened_fields_1

  js wbt.SetModelFieldValue("Стандартные_элементы.Текст", "начальный текст");
  js wbt.SetModelFieldValue("Стандартные_элементы.Значение_из_выпадающего_списка", "варианта2");
  js wbt.SetModelFieldValue("Стандартные_элементы.Значение_открыжено", 'checked');
  js wbt.SetModelFieldValue("Стандартные_элементы.Выбранный_вариант", "вариант1");
  js wbt.SetModelFieldValue("Стандартные_элементы.Многострочный_текст", "начальный\nмногострочный\nтекст");

  js wbt.SetModelFieldValue("Элементы_jquery_ui.Дата", "12.04.1961");

stop_store_lines

start_store_lines_as fastened_fields_2

  js wbt.SetModelFieldValue("Стандартные_элементы.Текст", "отредактированный текст");
  js wbt.SetModelFieldValue("Стандартные_элементы.Значение_из_выпадающего_списка", "варианта3");
  js wbt.SetModelFieldValue("Стандартные_элементы.Значение_открыжено", null);
  js wbt.SetModelFieldValue("Стандартные_элементы.Выбранный_вариант", "вариант2");
  js wbt.SetModelFieldValue("Стандартные_элементы.Многострочный_текст", "отредактированный\nмногострочный\nтекст");

  js wbt.SetModelFieldValue("Элементы_jquery_ui.Дата", "08.08.2008");

stop_store_lines
