require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/txt'
		}
	}
}),

require
(
	[
		  'forms/orpau/main/f_main'
		  ,'forms/orpau/cabinet/voting/root/f_vote'
		  ,'forms/orpau/root/f_root'
	],
	function ()
	{
		var extension =
		{
			Title: 'Единая система ОРПАУ'
			, key: 'orpau'
			, forms: {}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 0; i < forms.length; i++)
		{
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);