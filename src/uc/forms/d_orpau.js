define([
	'txt!forms/orpau/tests/orpau.json.txt'
],
function (content_ini_txt)
{
	return JSON.parse(content_ini_txt);
});