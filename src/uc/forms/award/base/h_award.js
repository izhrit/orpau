﻿define([
	  'forms/base/h_msgbox'
], function (h_msgbox)
{
	var helper = {};

	helper.ЗаКогоПроголосовал = function (Голосующий)
	{
		if (!Голосующий.ПроголосовалЗа)
		{
			return '<a href="#">Кликните, чтобы проголосовать..</a>';
		}
		else
		{
			var кандидат = Голосующий.ПроголосовалЗа;
			var text = кандидат.Фамилия;
			text += ' ' + кандидат.Имя;
			text += ' ' + кандидат.Отчество;

			var result = '<div class="am-info">';
			result += '<span class="am-name">' + text + '</span>';
			result += '<br>';
			result += '<span class="gray-text font-14">' + кандидат.СРО + '</span>';
			result += '</div>';
			result += '<i class="next"></i>';
			return result;
		}
	};

	helper.RequestNominees = function (base_url, on_nominees)
	{
		var url = base_url + '?action=award.nominees';
		var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о кандидатах на премию", url);
		v_ajax.ajax({
			dataType: "json"
			, type: 'GET'
			, cache: false
			, success: function (data, textStatus)
			{
				if (null == data)
				{
					v_ajax.ShowAjaxError(data, textStatus);
				}
				else
				{
					on_nominees(data);
				}
			}
		});
	}

	return helper;
});