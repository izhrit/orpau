﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/signing/e_award_signing.html'
	, 'forms/award/signed/c_award_signed'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_award_signed, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' button.back').click(function () { self.OnBack(); });
			$(sel + ' button.sign').click(function () { self.OnSign(); });
			self.RememberCurrentPage();
		}

		controller.OnBack = function ()
		{
			if (this.OnBackToVoting) {
				this.ForgetCurrentPage();
				this.OnBackToVoting();
			}
		}

		controller.Sign= function(На_подписание)
		{
			var use_activex = window.use_activex;
			var check_cert_by_name = window.use_validate_certificate;
			if (!use_activex)
			{
				return { cms: "test-cms" };
			}
			else if (!('ActiveXObject' in window))
			{
				h_msgbox.ShowModal({
					title: "Неудача подписи", width: 400
					, html: "Ваш браузер не поддерживает возможность работы с электронной подписью."
						+ " Воспользуйтесь браузером IE версии 10 и выше."
				});
				return null;
			}
			else
			{
				var cert = null;
				try
				{
					cert = $.capicom.getDialogBoxCertificates();

					try {
						if (check_cert_by_name) {
							var cert_name = getNameFromCert(cert.SubjectName);
							var au_name = controller.model.АУ.Фамилия + " " + controller.model.АУ.Имя + " " + controller.model.АУ.Отчество;

							if (cert_name != au_name) {
								h_msgbox.ShowModal({
									title: "Неудача подписи", width: 400
									, html: "Указанный сертификат выдан на имя, которое отличается от имени выбранного арбитражного управляющего"
								});
								return null;
							}
						}
					} catch (ex) {}
				}
				catch (ex) { }
				var res = {}
				if (null == cert)
				{
					return null;
				}
				else
				{
					var cms = $.capicom.signBase64(На_подписание, false, cert.thumbprint);
                    if (cms===false) return null;
					return { cms: cms };
				}
			}

			function getNameFromCert(subjectNameFromCert) {
				var sn = subjectNameFromCert;
				var sn_split = sn.split(',');

				for (var i = 0; i < sn_split.length; i++) {
					var item_split = sn_split[i].toString().split('=');
					if (item_split[0].trim() == "CN") {
						return item_split[1];
					}
				}
			}
		}

		controller.OnSign= function()
		{
			var signed = controller.Sign(this.model.На_подписание);
			if (null != signed)
				this.OnSigned(signed);
		}

		controller.OnSigned = function (signed)
		{
			var self = this;
			var url = this.base_url + '?action=award.vote';
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение информации об отданном голосе", url);
			var data = {
				VoterINN: this.model.АУ.inn
				, id_Nominee: this.model.ПроголосовалЗа.id_Nominee
				, cms: signed.cms
				, bulletin: this.model.На_подписание
				, poll: controller.model.poll
				, vote_data: JSON.stringify(controller.model.ПроголосовалЗа)
			};
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: data
				, cache: false
				, success: function (data, textStatus)
				{
					if (self.GotoSigned) {
						self.ForgetCurrentPage();
						self.GotoSigned(controller.model.АУ, controller.model.На_подписание);
					}
				}
			});
		}
		
		controller.SafeDestroyPlacedController= function()
		{
			if (null != this.placed_controller)
			{
				var sel = this.fastening.selector;
				this.placed_controller.Destroy();
				$('.placer').html('');
				this.placed_controller = null;
			}
		}

		controller.ForgetCurrentPage = function () {
			$.cookie("current_page", null);
		}

		controller.RememberCurrentPage = function () {
			$.cookie("current_page", "award_singning");
		}

		return controller;
	}
});