define(['forms/award/main/c_award'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'orpau'
		, Title: 'Добро пожаловать в систему голосования orpau'
	};
	return form_spec;
});