﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/signed/e_award_signed.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		controller.base_Render = controller.Render;

		controller.Render = function (sel)
		{
			controller.base_Render.call(this, sel);
			$('.back-to-polls').click(controller.OnBack);
		}

		controller.OnBack = function()
		{
			controller.OnBackToPolls()
		}

		return controller;
	}
});