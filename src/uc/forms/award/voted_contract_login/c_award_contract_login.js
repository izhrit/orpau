﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/voted_contract_login/e_award_contract_login.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

		var options = {
			field_spec: {
				manager: {
					ajax: {
						dataType: 'json'
						, url: base_url + '?action=award.managers'
					}
				}
			}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.contract_auth_button').click(function () { self.ContractLogin(sel, self.model.manager.data.efrsbNumber, self.model.inn); });
			$(sel + ' button.back').click(function () { self.OnBack(); });
		}

		controller.ContractLogin = function(sel, efrsbNumber, inn)
		{
			var login = $(sel + ' #contract_login').val();
			var password = $(sel + ' #contract_password').val();

			if (login.trim() == '' || password.trim() == '') {
				$(sel+' .contract_auth_error').show();
				return;
			}

			var self= this;
			var sel = this.fastening.selector;
			var resAjax = false;
			var url = base_url + '?action=award.managers_server&operation=contractAuth&login=' + login+ '&password='+ btoa(password) + "&efrsbNumber="+efrsbNumber+"&inn="+inn;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о голосовании АУ", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (result, textStatus)
				{
					if (result != 0) {
						var license_id = result.license_id;
						var manager_regnum = result.manager_regnum;

						document.location.search = "?license_id="+license_id+"&manager_regnum="+manager_regnum;
					} else {
						$(sel+' .contract_auth_error').show();
					}
				}
			});

		}

		controller.OnBack = function ()
		{
			if (this.OnBackToLogin)
				this.OnBackToLogin();
		}


		return controller;
	}
});