define([
  'forms/base/fastened/c_fastened'
  , 'tpl!forms/award/poll/e_poll.html'
  , 'tpl!forms/award/base/p_bulletin.txt'
  , 'forms/award/signing/c_award_signing'
],
	function (c_fastened, tpl, p_bulletin, c_signing)
  {
    return function (option_args)
    {
      var controller = c_fastened(tpl);
      
      controller.base_url = option_args?option_args.base_url:'orpau';

      controller.base_render = controller.Render;

      controller.Render = function (sel)
      {
        controller.base_render.call(this, sel);
        $('.to-signing')[0].onclick = controller.OnSign;
        $('.back-to-polls')[0].onclick = controller.OnBackToPolls;
        $(sel + ' li').click(function(e) {
          var targetNode = e.target.nodeName;
          if(targetNode == "I") return false;
          
          var radioButton = $(this).find("input");
  
          $(sel + ' li').removeClass("selected-candidate");
  
          if(radioButton.attr('checked')) {
            if(targetNode != "INPUT") $(radioButton).attr('checked', false)
          } else {
            $(this).addClass("selected-candidate");
            if(targetNode != "INPUT") $(radioButton).attr('checked', 'checked');
          }
  
          if(targetNode == "INPUT" && radioButton.attr('checked')) $(this).addClass("selected-candidate");
        })
  
      }

      controller.OnSign = function()
      {
        var email = controller.GetEmail();
        if (email&&email!="")
        {
          controller.OnSigning(controller.model.manager, email,
            controller.GetChoosenNominee(), controller.model.poll.ExtraColumns, controller.model.poll);
        }
        else alert('Введите email!')
      }

      controller.GetBulletin = function()
      {
        var manager = controller.model.manager;
        var nominees = controller.model.poll.ExtraColumns;
        var choosenNominee = controller.GetChoosenNominee();
        var barg = {
          Голосующий: manager
          , ПроголосовалЗа: choosenNominee
          , Кандидаты: nominees
          , Время: "02.02.2020"//dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()))
          , poll: controller.model.poll
        };
        return bulletin = p_bulletin(barg);
      }

      controller.GetEmail = function()
      {
        return $('.email')[0].value;
      }

      controller.GetChoosenNominee = function()
      {
        var inputs = $('.nominee-input');
				var choosenNomineeID = null;
				for( var i = 0; i < inputs.length; i++)
				{
					if(inputs[i].checked)
					{
						choosenNomineeID = inputs[i].value;
						break;
					}
        }
        var choosenNominee = null;
        for(var i = 0; i<controller.model.poll.ExtraColumns.length; i++)
        {
          if (controller.model.poll.ExtraColumns[i].id_Nominee==choosenNomineeID)
          {
            choosenNominee = controller.model.poll.ExtraColumns[i];
          }
        }
        return choosenNominee;
      }

      return controller;
    }
  });