﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/admin/votes/e_award_votes.html'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, h_msgbox)
	{
		return function (options_arg)
		{
			var controller = c_fastened(tpl);

			controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
			controller.base_grid_url = controller.base_url + '?action=award.jqgrid';

			var base_Render = controller.Render;
			controller.Render = function (sel)
			{
				base_Render.call(this, sel);
				this.RenderGrid();
				controller.BindResizeWindow();
				window.onresize();
				$('.back-to-polls').click(controller.OnBackToPolls);
			}

			controller.colModel =
				[
					{ name: 'id_Nominee', hidden: true }
					, { label: 'Номинант', name: 'Name', width: 80, sortable: false }
					, { label: 'Кол-во голосов', name: 'votes_count', width: 20, sortable: false }
				];

			controller.RenderGrid = function ()
			{
				var sel = this.fastening.selector;
				var self = this;
				var grid = $(sel + ' table.grid');
				var url = this.base_grid_url + '&id_Poll=' + controller.model.id_Poll;
				grid.jqGrid
					({
						datatype: 'json'
						, url: url
						, colModel: self.colModel
						, gridview: true
						, loadtext: 'Загрузка зарегистрированных голосов..'
						, recordtext: 'Общее кол-во голосов: {2}'
						, emptyText: 'Нет зарегистрированных голосов для отображения'
						, rownumbers: false
						, rowNum: 8
						, rowList: [5, 10, 15]
						, pager: '#cpw-award-votes-pager'
						, viewrecords: true
						, autowidth: true
						, multiselect: false
						, multiboxonly: false
						, height: '500'
						, ignoreCase: true
						, shrinkToFit: true
						, loadError: function (jqXHR, textStatus, errorThrown)
						{
							h_msgbox.ShowAjaxError("Загрузка списка зарегистрированных голосов", url, jqXHR.responseText, textStatus, errorThrown)
						}
						, loadComplete: function()
						{
							if (window.onresize)
								window.onresize();
						}
					});
				grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
				grid.jqGrid("navGrid", "#cpw-cpw-ama-dm-award-votes-pager",
					{ edit: false, add: false, del: false, search: false, refresh: true, refreshtext: 'обновить', refreshstate: 'current' }
				);
			}

			controller.BindResizeWindow = function ()
			{
				if (!controller.on_resize)
				{
					var sel = controller.fastening.selector;
					controller.on_resize = function ()
					{
						if ($(document).width() < 700)
						{
							return false;
						}
						else
						{
							var $grid = $('table.grid'),
								newWidth = $grid.closest(".ui-jqgrid").parent().width();
							if (newWidth)
								$grid.setGridWidth(newWidth, true);
						}
					};
					controller.on_resize();
				}
				window.onresize = this.on_resize;
			}

			return controller;
		}
	});