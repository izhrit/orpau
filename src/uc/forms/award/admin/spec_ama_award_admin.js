define(function ()
{

	return {

		controller: {
		  "award_results":     { path: 'forms/award/admin/results/c_award_results' }
		, "award_admin":       { path: 'forms/award/admin/main/c_award_admin' }
		, "award_votes":       { path: 'forms/award/admin/votes/c_award_votes' }
		, "award_admin_login": { path: 'forms/award/admin/login/c_award_admin_login' }
		}

		, content: {
			"award_results_example1": { path: 'txt!forms/award/admin/results/tests/contents/example1.json.txt' }
		}

	};

});