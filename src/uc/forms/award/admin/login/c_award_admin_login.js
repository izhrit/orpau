﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/admin/login/e_award_admin_login.html'
	, 'forms/base/h_msgbox'
	, 'forms/award/admin/main/c_award_admin'
	, 'forms/award/admin/admin_polls/c_admin_polls'
	, 'forms/award/admin/votes/c_award_votes'
],
function (c_fastened, tpl, h_msgbox, c_award_admin, c_polls, c_award_votes)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_polls_url = controller.base_url + "?action=award.polls"

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			var self = this;
			$(sel + ' button.login').click(function () { self.OnLogin(); });
			$(sel + ' button.logout').click(function () { self.OnLogout(); });
		}

		controller.OnLogin= function()
		{
			var self = this;
			var sel= this.fastening.selector;
			var login = $(sel + ' select').val();
			var password = $(sel + ' input[type="password"]').val();
			var url = this.base_url + '?action=award.login';
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос авторизации", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: {login:login,password:password}
				, cache: false
				, success: function (data, textStatus)
				{
					if (null == data)
					{
						v_ajax.ShowAjaxError(data, textStatus);
					}
					else
					{
						if (true == data.ok)
						{
							self.OnLogged(login);
						}
						else
						{
							h_msgbox.ShowModal({title:"Неудачная авторизация",html:"Вы ввели неправильные данные для входа"});
						}
					}
				}
			});
		}

		controller.OnLogged = function (login)
		{
			controller.SafeDestroyPlacedController();
			var sel = this.fastening.selector;
			$(sel + ' div.auth').show();
			$(sel + ' div.login').hide();
			$(sel + ' div.auth span.name').text(login);
			controller.OnPolls();
		}

		controller.OnPolls = function()
		{
			var sel = controller.fastening.selector;
			$.ajax({
				url: controller.base_polls_url
				, cache: false
				, success: function (data)
				{
					controller.placed_controller = c_polls({ base_url: controller.base_url });
					controller.placed_controller.OnLogin = controller.OnLogout; 
					controller.placed_controller.OnVotes = controller.OnVotes;
					controller.placed_controller.SetFormContent({ manager: {}, polls: JSON.parse(data) });
					controller.placed_controller.CreateNew(sel + ' div.main');
				}
			})

		}

		controller.OnVotes = function(choosenID)
		{
			controller.SafeDestroyPlacedController();
			controller.placed_controller = c_award_votes({ base_url: controller.base_url })
			controller.placed_controller.OnBackToPolls = controller.OnPolls;
			controller.placed_controller.SetFormContent({id_Poll:choosenID});
			controller.placed_controller.CreateNew('div.main');
		}

		controller.OnLogout= function ()
		{
			var sel = this.fastening.selector;
			$('div.auth').hide();
			$('div.login').show();

			controller.placed_controller.Destroy();
			delete this.placed_controller;
			this.main = null;
			$('div.main').html('');

			$('select').val('');
			$('input[type="password"]').val('');
		}

		
		controller.SafeDestroyPlacedController = function ()
			{
				if (null != this.placed_controller)
				{
					var sel = this.fastening.selector;
					this.placed_controller.Destroy();
					$(sel + ' div.main').html('');
					this.placed_controller = null;
				}
			}

		return controller;
	}
});