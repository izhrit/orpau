﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/admin/main/e_award_admin.html'
	, 'forms/award/admin/results/c_award_results'
	, 'forms/award/admin/votes/c_award_votes'
],
function (c_fastened, tpl, c_award_results, c_award_votes)
{
	return function (options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');

		var options = {
			field_spec:
				{
					Номинанты: { controller: function () { return c_award_results({ base_url: base_url }); } }
					, Голоса: { controller: function () { return c_award_votes({ base_url: base_url }); } }
				}
		};

		var controller = c_fastened(tpl, options);
		return controller;
	}
});