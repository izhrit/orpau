define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/admin/admin_polls/e_polls.html'
	, 'forms/base/h_msgbox'
	, 'forms/award/admin/votes/c_award_votes'
],
	function (c_fastened, tpl, h_msgbox, c_award_votes)
	{
		function Module_function(option_args)
		{
			var controller = c_fastened(tpl);

			controller.base_url = option_args?option_args.base_url:'orpau';
			controller.base_poll_url = controller.base_url+'?action=award.poll&id_Poll=';
			controller.base_votes_url = controller.base_url+"?action=award.votes"
			controller.base_polls_url = controller.base_url + "?action=award.polls";

			controller.base_render = controller.Render;

			controller.Render = function (sel)
			{
				controller.base_render.call(this,sel);
				$('.back-to-login').click(controller.OnBackToLogin);
				$('.poll').click(controller.OnPollClick);
			}

			controller.OnPolls = function()
			{
				$.ajax({
					url: controller.base_polls_url
					, cache: false
					, success: function (data)
					{
						var sel = controller.fastening.selector;
						controller.placed_controller = Module_function({ base_url: controller.base_url });
						controller.placed_controller.SetFormContent({ manager: {}, polls: JSON.parse(data) });
						controller.placed_controller.CreateNew(sel);
					}
				});
			}

			controller.OnPollClick = function(sender)
			{
				if(controller.OnVotes)
				{
					choosenID = this.value;
					controller.OnVotes(choosenID);
				}
			}

			controller.OnBackToLogin = function()
			{
				controller.OnLogin();
			}

			return controller;
		}
		return Module_function;
	});