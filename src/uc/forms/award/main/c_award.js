﻿define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/main/e_award.html'
	, 'forms/award/manager/c_award_manager'
	, 'forms/award/contract/c_award_contract'
	, 'forms/base/ql'
	, 'forms/award/login/c_award_login'
	, 'forms/base/h_msgbox'
	, 'forms/award/voted_signed/c_award_voted_signed'
	, 'forms/award/voted_ama/c_award_voted_ama'
	, 'forms/award/voting/c_award_voting'
	, 'forms/award/base/h_award'
	, 'forms/award/signing/c_award_signing'
	, 'tpl!forms/award/base/p_bulletin.txt'
	, 'forms/base/h_times'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/award/signed/c_award_signed'
	, 'forms/award/voted_source/c_award_voted_source'
	, 'forms/award/voted_contract_login/c_award_contract_login'
	, 'forms/award/polls/c_polls'
	, 'forms/award/poll/c_poll'
	, 'forms/award/vote_exist/c_vote_exist'
],
	function (c_fastened,
		tpl,
		c_award_manager,
		c_award_contract,
		ql,
		c_award_login,
		h_msgbox,
		c_award_voted_signed,
		c_award_voted_ama,
		c_award_voting,
		h_award,
		c_award_signing,
		p_bulletin,
		h_times,
		h_codec_datetime,
		c_award_signed,
		c_award_voted_source,
		c_award_contract_login,
		c_polls,
		c_poll,
		c_vote_exist)
	{
		return function (options_arg)
		{
			var controller = c_fastened(tpl);

			controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
			controller.base_polls_url = controller.base_url + "?action=award.polls"
			controller.base_poll_url = controller.base_url + "?action=award.poll"
			controller.base_votes_url = controller.base_url + "?action=award.votes"


			var Голосующий = function (m)
			{
				var voting = {
					Фамилия: m.last_name
					, Имя: m.first_name
					, Отчество: m.middle_name
					, ИНН: m.inn
				};

				if (typeof m.voted_for != "undefined")
				{
					voting.ПроголосовалЗа = {
						Фамилия: m.voted_for.last_name,
						Имя: m.voted_for.first_name,
						Отчество: m.voted_for.middle_name,
						СРО: m.voted_for.sro
					}
				}

				return voting;
			}

			var base_Render = controller.Render;
			controller.Render = function (sel)
			{
				base_Render.call(this, sel);
				this.RenderLoginPage(sel);
			}

			controller.RenderLoginPage = function (sel)
			{
				var self = this;

				try
				{
					var current_page = $.cookie("current_page");
					if (current_page == "award_singning")
					{
						h_award.RequestNominees(self.base_url, GetNominees);
						function GetNominees(data)
						{
							var singning_manager = JSON.parse($.cookie("singning_manager"));
							var singning_email = $.cookie("singning_email");
							var singning_votedfor = JSON.parse($.cookie("singning_votedfor"));
							var singning_candidates = data;
							self.OnSigning(singning_manager, singning_email, singning_votedfor, singning_candidates);
							return;
						}
					}
				} catch (ex) { }

				if (!this.model || null == this.model || 0 == this.model.length)
				{
					this.placed_controller = c_award_login({ base_url: this.base_url });
					this.placed_controller.CreateNew(sel + ' div.placer');
					this.placed_controller.OnOkLogin = function (inn, existManagerInServer) { self.OnOkLogin(inn, existManagerInServer); }
					this.placed_controller.OnVotedSource = function (inn) { self.OnVotedSource(inn); }
				}
				else
				{
					if (1 == this.model.length)
					{
						controller.OnPolls(Голосующий(this.model[0]));
						//this.placed_controller.SetFormContent(Голосующий(this.model[0]));
					}
					else
					{
						this.placed_controller = c_award_contract({ base_url: this.base_url });
						this.placed_controller.OnPolls = controller.OnPolls;
						this.placed_controller.SetFormContent({ Голосующие: ql.map(this.model, Голосующий), Договор: this.model[0].contract_number });
					}
					this.placed_controller.Edit(sel + ' div.placer');
				}
			}

			controller.OnVotedSource = function (manager)
			{
				this.SafeDestroyPlacedController();
				var self = this;
				this.placed_controller = c_award_voted_source({ base_url: this.base_url });
				this.placed_controller.SetFormContent(manager);
				this.placed_controller.OnOkLogin = function (inn) { self.OnOkLogin(inn); }
				this.placed_controller.OnBackToLogin = function () { self.OnLogin(manager); }
				this.placed_controller.OnContractLogin = function () { self.OnContractLogin(manager); }
				this.placed_controller.Edit(this.fastening.selector + ' div.placer');
			}

			controller.OnContractLogin = function (manager)
			{
				this.SafeDestroyPlacedController();
				var self = this;
				this.placed_controller = c_award_polls({ base_url: this.base_url });
				this.placed_controller.SetFormContent(manager);
				this.placed_controller.OnBackToLogin = function () { self.OnLogin(manager); }
				this.placed_controller.Edit(this.fastening.selector + ' div.placer');
			}

			controller.OnOkLogin = function (manager, existManagerInServer)
			{
				var self = this;
				self.OnLogged(manager, existManagerInServer);
			}

			controller.SafeDestroyPlacedController = function ()
			{
				if (null != this.placed_controller)
				{
					var sel = this.fastening.selector;
					this.placed_controller.Destroy();
					$(sel + ' div.placer').html('');
					this.placed_controller = null;
				}
			}

			controller.OnLogged = function (manager, existManagerInServer)
			{
				this.SafeDestroyPlacedController();
				var self = this;
				if (!existManagerInServer)
				{
					this.OnPolls(manager);
				} else
				{
					this.OnVotedSource(manager);
				}
			}

			controller.OnLogin = function (manager)
			{
				this.SafeDestroyPlacedController();
				this.RenderLoginPage(this.fastening.selector);
				if (null != manager)
					this.placed_controller.SelectManager(manager);
			}

			controller.OnPolls = function (manager)
			{
				$.ajax({
					url: controller.base_polls_url
					, success: function (data)
					{
						controller.placed_controller = c_polls({ base_url: controller.base_url });
						controller.placed_controller.OnPoll = function (data) { controller.OnPoll(data, manager) }
						controller.placed_controller.OnBackToLogin = function (manager) { controller.OnLogin(manager) }
						controller.placed_controller.SetFormContent({ manager: manager, polls: JSON.parse(data) });
						controller.placed_controller.Edit('.placer');
					}
				})
			}

			controller.OnPoll = function (pollID, manager)
			{
				var poll_url = controller.base_poll_url + "&id_Poll=" + pollID;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о голосовании", poll_url)
				v_ajax.ajax({
					url: poll_url
					, success: function (data)
					{
						controller.SafeDestroyPlacedController();
						controller.placed_controller = c_poll({ base_url: controller.base_url });
						controller.placed_controller.SetFormContent({ manager: manager, poll: JSON.parse(data) });
						controller.placed_controller.OnBackToPolls = function () { controller.OnPolls(manager) };
						controller.placed_controller.OnSigning = function (manager, email, ПроголосовалЗа, Кандидаты)
						{
							controller.OnSigning(manager, email, ПроголосовалЗа, Кандидаты, data)
						};
						controller.placed_controller.Edit(".placer");
					}
				})
			}

			controller.OnVoting = function (manager, email, ПроголосовалЗа)
			{
				var self = this;
				h_award.RequestNominees(this.base_url, function (Кандидаты)
				{
					self.SafeDestroyPlacedController();
					//self.placed_controller = c_polls({ base_url: self.base_url });
					self.placed_controller = c_award_voting({ base_url: self.base_url });
					self.placed_controller.OnBackToLogin = function () { self.OnLogin(manager); }
					self.placed_controller.OnGotoSigning = function (email, ПроголосовалЗа)
					{
						self.OnSigning(manager, email, ПроголосовалЗа, Кандидаты, poll);
					}
					manager.ГолосоватьЭлектроннойПодписью = true;
					self.placed_controller.SetFormContent({ Голосующий: manager, Кандидаты: Кандидаты });
					var sel = self.fastening.selector;
					self.placed_controller.Edit(sel + ' div.placer');
					if (email)
						$(sel + ' input[type="text"].email').val(email);
					if (ПроголосовалЗа)
					{
						for (var i = 0; i < Кандидаты.length; i++)
						{
							var кандидат = Кандидаты[i];
							if (кандидат.ИНН == ПроголосовалЗа.ИНН)
							{
								$($(sel + ' div[model-selector="Кандидаты"] input')[i + 1]).attr('checked', 'checked');
								break;
							}
						}
					}

				});
			}

			controller.OnSigning = function (manager, email, ПроголосовалЗа, Кандидаты, poll)
			{
				var self = this;
				self.SafeDestroyPlacedController();
				self.placed_controller = c_award_signing({ base_url: self.base_url });
				self.placed_controller.OnBackToVoting = function () { self.OnPoll(JSON.parse(poll).id_Poll, manager); }
				var dt_codec = h_codec_datetime.ru_legal_txt2dt();
				var barg = {
					Голосующий: manager
					, ПроголосовалЗа: ПроголосовалЗа
					, Кандидаты: Кандидаты
					, Время: dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()))
					, poll: JSON.parse(poll)
				};
				var bulletin = p_bulletin(barg);
				bulletin = bulletin.replace(/<br\/>/gi, "\r\n");
				manager.email = email;
				self.placed_controller.SetFormContent({ АУ: manager, На_подписание: bulletin, ПроголосовалЗа: ПроголосовалЗа, poll: poll });
				self.placed_controller.GotoSigned = function () { self.OnSigned(manager, bulletin); }
				self.placed_controller.Edit(self.fastening.selector + ' div.placer');

				$.cookie("singning_manager", JSON.stringify(manager));
				$.cookie("singning_email", email);
				$.cookie("singning_votedfor", JSON.stringify(ПроголосовалЗа));
			}

			controller.OnSigned = function (manager, bulletin)
			{
				var self = this;
				self.SafeDestroyPlacedController();
				self.placed_controller = c_award_signed({ base_url: self.base_url });
				self.placed_controller.SetFormContent({ АУ: manager, Подписано: bulletin });
				self.placed_controller.OnBackToPolls = function () { controller.OnPolls(manager) }
				self.placed_controller.Edit(self.fastening.selector + ' div.placer');
			}

			return controller;
		}
	});