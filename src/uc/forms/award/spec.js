define([
	  'forms/base/h_spec'
	, 'forms/award/admin/spec_ama_award_admin'
]
, function (h_spec, admin_spec)
{
	var award_specs = {

		controller: {
		  "award_voting":       { path: 'forms/award/voting/c_award_voting' }
		, "award_contract":     { path: 'forms/award/contract/c_award_contract' }
		, "award_manager":      { path: 'forms/award/manager/c_award_manager' }
		, "award":              { path: 'forms/award/main/c_award' }
		, "award_login":        { path: 'forms/award/login/c_award_login' }
		, "award_voted_ama":    { path: 'forms/award/voted_ama/c_award_voted_ama' }
		, "award_voted_signed": { path: 'forms/award/voted_signed/c_award_voted_signed' }
		, "award_signing":      { path: 'forms/award/signing/c_award_signing' }
		, "award_signed":       { path: 'forms/award/signed/c_award_signed' }
		, "award_poll":      	{ path: 'forms/award/poll/c_poll' }
		, "award_polls":       	{ path: 'forms/award/polls/c_polls' }

		}

		, content: {
		  "award_voting_example1":       { path: 'txt!forms/award/voting/tests/contents/example1.json.txt' }
		, "award_voting_example2":       { path: 'txt!forms/award/voting/tests/contents/example2.json.txt' }
		, "award_contract_example1":     { path: 'txt!forms/award/contract/tests/contents/example1.json.txt' }
		, "award_manager_example1":      { path: 'txt!forms/award/manager/tests/contents/example1.json.txt' }
		, "award_example1":              { path: 'txt!forms/award/main/tests/contents/example1.json.txt' }
		, "award_example2":              { path: 'txt!forms/award/main/tests/contents/example2.json.txt' }
		, "award_voted_ama_example1":    { path: 'txt!forms/award/voted_ama/tests/contents/example1.json.txt' }
		, "award_voted_ama_example2":    { path: 'txt!forms/award/voted_ama/tests/contents/example2.json.txt' }
		, "award_voted_signed_example1": { path: 'txt!forms/award/voted_signed/tests/contents/example1.json.txt' }
		, "award_voted_signed_example2": { path: 'txt!forms/award/voted_signed/tests/contents/example2.json.txt' }
		, "award_signing_example1":      { path: 'txt!forms/award/signing/tests/contents/example1.json.txt' }
		, "award_signed_example1":       { path: 'txt!forms/award/signed/tests/contents/example1.json.txt' }
		, "award_poll_example1":       	 { path: 'txt!forms/award/poll/tests/contents/example1.json.txt' }
		, "award_polls_example1":        { path: 'txt!forms/award/polls/tests/contents/example1.json.txt' }
		}

	};

	return h_spec.combine(admin_spec, award_specs);
});