﻿define([
		'forms/base/fastened/c_fastened'
		, 'tpl!forms/award/voted_source/e_award_voted_source.html'
		, 'forms/base/h_msgbox'
	],
	function (c_fastened, tpl, h_msgbox)
	{
		return function (options_arg)
		{
			var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/datamart');

			var options = {
				field_spec: {
					manager: {
						ajax: {
							dataType: 'json'
							, url: base_url + '?action=award.managers'
						}
					}
				}
			};

			var controller = c_fastened(tpl, options);

			var base_Render = controller.Render;
			controller.Render = function (sel)
			{
				base_Render.call(this, sel);

				var self = this;

				$(sel + ' button.certificate').click(function () { self.OnLogin(); });
				$(sel + ' button.contract').click(function () { self.ContractLogin(sel, self.model.efrsbNumber); });
				$(sel + ' button.back').click(function () { self.OnBack(); });
			}

			controller.ContractLogin = function(sel, efrsbNumber)
			{
				var sel = this.fastening.selector;
				if (this.OnContractLogin && null != this.OnContractLogin)
					this.OnContractLogin(this.GetFormContent());
			}

			controller.OnLogin = function ()
			{
				if (!this.model || null==this.model)
				{
					h_msgbox.ShowModal({
						title: 'Нельзя проголосовать анонимно'
						, width: 410
						, html: 'Выберите пожалуйста Арбитражного управляющего из списка, от лица которого вы хотите проголосовать, прежде чем перейти к голосованию!'
					});
				}
				else
				{
					var sel = this.fastening.selector;
					if (this.OnOkLogin && null != this.OnOkLogin)
						this.OnOkLogin(this.GetFormContent());
				}
			}

			controller.OnBack = function ()
			{
				if (this.OnBackToLogin)
					this.OnBackToLogin();
			}


			return controller;
		}
	});