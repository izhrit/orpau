﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/voted_signed/e_award_voted_signed.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.back').click(function () { self.OnBack(); });
		}

		controller.OnBack = function ()
		{
			if (this.OnBackToLogin)
				this.OnBackToLogin();
		}

		return controller;
	}
});