define([
	'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/polls/e_polls.html'
	, 'forms/award/poll/c_poll'
	, 'forms/award/login/c_award_login'
	, 'forms/base/h_msgbox'
],
	function (c_fastened, tpl, c_poll, c_login, h_msgbox)
	{
		return function (option_args)
		{
			var controller = c_fastened(tpl);

			controller.base_url = option_args?option_args.base_url:'orpau';
			controller.base_poll_url = controller.base_url+'?action=award.poll&id_Poll=';
			controller.base_votes_url = controller.base_url+"?action=award.votes"

			var base_render = controller.Render;
			controller.Render = function (sel)
			{
				base_render.call(this,sel);
				$('.back-to-login').click(controller.OnBackToLogin);
				this.RenderPolls();
			}

			controller.RenderPolls = function()
			{
				var votes_url = controller.base_votes_url+'&operations=GetVotes&inn='+this.model.manager.inn;
				v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о голосах", votes_url)
				v_ajax.ajax({
					cache:false,
					url:votes_url,
					success: function(data)
					{
						data = JSON.parse(data);
						controller.lock_voted_polls(data);
						polls = $('.poll');
						for(var i = 0; i < polls.length; i++)
						{
							var voted = polls[i].getAttribute('voted');
							if (voted!='signatured')
								polls[i].onclick = controller.OnPollClick;
							if (!voted)
							{
								polls[i].innerHTML+='<span style="color:grey">(Нажмите, чтобы проголосовать)</span>';
							}
						}
					}
				})
			}

			controller.lock_voted_polls = function(votes)
			{
				polls = $('.poll');
				for(var i = 0; i < votes.length; i++)
				{
					for(var j = 0; j < polls.length; j++)
					{
						if (votes[i].id_Poll==polls[j].value)
						{
							if(votes[i].BulletinVoterSignature)
							{
								polls[j].setAttribute('voted', 'signatured');
								polls[j].innerHTML+='<span style="color:grey">(Ваш голос уже учтён)</span>';
							}
							else
							{
								polls[j].setAttribute('voted', 'non-signatured');
								polls[j].innerHTML+='<span style="color:grey">(Ваш голос уже учтён, нажмите, чтобы подтвердить его подписью)</span>'
							}
						}
					}
				}
			}

			controller.OnPollClick = function(sender)
			{
				choosenID = this.value;
				controller.OnPoll(choosenID, controller.model.manager)
			}

			controller.OnBackToLogin = function()
			{
				this.OnLogin();
			}
			
			controller.DestroyPlacedController = function ()
			{
				$('.polls').html('');
				this.cvoting = null;
			}

			return controller;
		}
	});