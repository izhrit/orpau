define([
	'forms/base/fastened/c_fastened'
  , 'tpl!forms/award/vote_exist/e_vote_exist.html'
],
function (c_fastened, tpl)
{
  return function ()
  {
	  var controller = c_fastened(tpl);

	  var base_Render = controller.Render;
	  controller.Render= function(sel)
	  {
		  base_Render.call(this, sel);
		  var self = this;
		  $(sel + ' button.back-to-polls').click(function () { self.OnBack(); });
	  }

	  controller.OnBack= function()
	  {
		  if (this.OnBackToPolls)
			  this.OnBackToPolls(controller.model.manager);
	  }

	  return controller;
  }
});