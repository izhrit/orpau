﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/award/contract/e_award_contract.html'
	, 'forms/award/base/h_award'
	, 'forms/base/h_msgbox'
	, 'forms/award/voting/c_award_voting'
	, 'forms/base/ql'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/base/codec/url/codec.url'
	, 'forms/award/polls/c_polls'
	, 'forms/award/poll/c_poll'
],
function (c_fastened, tpl, h_award, h_msgbox, c_award_voting, ql, codec_url_args, codec_url, c_polls, c_poll)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl, { h_award: h_award });

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'orpau');
		controller.base_polls_url = controller.base_url + "?action=award.polls";
		controller.base_poll_url = controller.base_url + "?action=award.poll";

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div.am-card').click(function (e) { e.preventDefault(); self.OnVoting(e); });
			$(sel + ' button.vote').click(function (e) { e.preventDefault(); self.OnVote(); });
			$(sel + ' button.back').click(function (e) { e.preventDefault(); self.OnBack(); });
		}

		controller.OnVoting = function (e)
		{
			var ia = $(e.target);
			var iarrayitem = ia.parents('[data-fc-type="array-item"]');
			var index = iarrayitem.attr('data-array-index');
			this.ShowVotingByIndex(index);
		}

		controller.ShowVotingByIndex = function (index)
		{
			var Голосующий = this.model.Голосующие[index];
			var self = this;

			try {
				if (Голосующий.ПроголосовалЗа) {
					var url = self.base_url + '?action=award.votes&operations=IsSignedVoice&inn=' + Голосующий.ИНН;
					var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных об отданном голосе", url);
					v_ajax.ajax({
						dataType: "json"
						, type: 'GET'
						, cache: false
						, success: function (data, textStatus) {
							debugger;
							if (data == '1') {
								self.WarningMsg();
							} else {
								/*h_award.RequestNominees(self.base_url, function (Кандидаты) {
									self.ShowVoting(Кандидаты, Голосующий, index);
								});*/
								controller.OnPolls(Голосующий);
							}
						}
					});
				} else {
					/*h_award.RequestNominees(self.base_url, function (Кандидаты) {
						self.ShowVoting(Кандидаты, Голосующий, index);
					});*/
					controller.OnPolls(Голосующий);
				}
			} catch (ex) {
				/*h_award.RequestNominees(self.base_url, function (Кандидаты) {
					self.ShowVoting(Кандидаты, Голосующий, index);
				});*/
				controller.OnPolls(Голосующий);
			}

			/*h_award.RequestNominees(this.base_url, function (Кандидаты)
			{
				self.ShowVoting(Кандидаты, Голосующий, index);
			});*/
			controller.OnPolls(Голосующий);
		}

        controller.WarningMsg = function()
        {
            alert("Ваш голос уже был учтён голосованием при помощи электронной подписи");
        }

		controller.ShowVoting = function (Кандидаты, Голосующий, index)
		{
			var sel = this.fastening.selector;
			var self = this;
			var cvoting = c_award_voting({ base_url: this.base_url });
			this.Кандидаты = Кандидаты;
			this.index = index;
			this.Голосующий = Голосующий;
			cvoting.SetFormContent({ Кандидаты: Кандидаты, Голосующий: Голосующий });
			cvoting.Edit(sel + ' div.voting div.placer');
			this.cvoting = cvoting;

			$(sel + ' div.main').hide();
			$(sel + ' div.voting').show();
		}

		controller.SafeDestroyPlacedController = function ()
		{
			if (null != this.cvoting)
			{
				var sel = this.fastening.selector;
				this.cvoting.Destroy();
				$(sel + ' div.voting div.placer').html('');
				this.cvoting = null;
				$(sel + ' div.main').show();
				$(sel + ' div.voting').hide();
			}
		}

		controller.OnBack = function ()
		{
			if (null != this.cvoting)
				this.SafeDestroyPlacedController();
		}

		controller.OnVote = function ()
		{
			if (null != this.cvoting)
			{
				var ПроголосовалЗа = this.cvoting.ПроголосовалЗа();
				if (null != ПроголосовалЗа)
				{
					this.СохранитьРезультат(this.Голосующий, ПроголосовалЗа, this.index, this.Кандидаты);
					this.SafeDestroyPlacedController();
				}
				else
				{
					var title = "Проверка перед сохранением";
					h_msgbox.ShowModal({ title: title, width: 410, html: "Выберите кандидата, за которого вы хотите отдать свой голос" });
				}
			}
		}

		controller.СохранитьРезультат = function (Голосующий, ПроголосовалЗа, index, Кандидаты)
		{
			var decoded_url = codec_url().Decode(window.location);
			var args = codec_url_args().Decode(decoded_url);
			var license_id = args.license_id;

			var sel = this.fastening.selector;
			var self = this;
			var url = this.base_url + '?action=award.vote';
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение информации об отданном голосе", url);
			var data = { Голосующий: Голосующий, ПроголосовалЗа: ПроголосовалЗа, license_id: license_id };
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, data: data
				, cache: false
				, success: function (data, textStatus)
				{
					Голосующий.ПроголосовалЗа = ql.find_first_or_null(Кандидаты, function (k)
					{
						return k.ИНН == ПроголосовалЗа.ИНН;
					});
					var html = h_award.ЗаКогоПроголосовал(Голосующий);

					$(sel + ' [array-index="' + index + '"] div.am-card').html(html);
				}
			});
		}
		
		controller.OnPolls = function (manager)
		{
			$.ajax({
				url: controller.base_polls_url
				, success: function (data)
				{
					controller.placed_controller = c_polls({ base_url: controller.base_url });
					controller.placed_controller.OnPoll = function (data) { controller.OnPoll(data, manager) }
					controller.placed_controller.OnBackToLogin = function (manager) { controller.OnLogin(manager) }
					controller.placed_controller.SetFormContent({ manager: manager, polls: JSON.parse(data) });
					controller.placed_controller.Edit('.main');
				}
			})
		}

		controller.OnPoll = function (pollID, manager)
		{
			var poll_url = controller.base_poll_url + "&id_Poll=" + pollID;
			var v_ajax = h_msgbox.ShowAjaxRequest("Получение данных о голосовании", poll_url)
			v_ajax.ajax({
				url: poll_url
				, success: function (data)
				{
					controller.SafeDestroyPlacedController();
					controller.placed_controller = c_poll({ base_url: controller.base_url });
					controller.placed_controller.SetFormContent({ manager: manager, poll: JSON.parse(data) });
					controller.placed_controller.OnBackToPolls = function () { controller.OnPolls(manager) };
					controller.placed_controller.OnSigning = function (manager, email, ПроголосовалЗа, Кандидаты)
					{
						controller.OnSigning(manager, email, ПроголосовалЗа, Кандидаты, data)
					};
					controller.placed_controller.Edit(".placer");
				}
			})
		}

		return controller;
	}
});