define([
	  'forms/base/log'
	, 'forms/base/h_constraints'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_times'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/fastened/fastening/h_fastening_clips'
	, 'forms/base/codec/codec.copy'
],
function (GetLogger, h_constraints, h_msgbox, h_times, h_fastening_clip, h_fastening_clips, codec_copy)
{
	return function ()
	{
		var log = GetLogger('b_object');

		var fastened =
		{
			field_attrs: function(field_name,render_as)
			{
				var res = ' data-fc-by="cpw" ';
				if (field_name && null != field_name)
				{
					res += 'data-fc-selector="' + field_name + '" ';
					this.CurrentFieldName = field_name;
				}
				if (render_as && null != render_as)
					res += 'data-fc-type="' + render_as + '" ';
				return res;
			},

			div: function(model_selector,render_as)
			{
				var res = '<div' + this.field_attrs(model_selector, render_as) + '>';

				res += 'There should be \"' + render_as + '\" for \"' + model_selector + '\"!';

				return res + '</div>';
			},

			value: function ()
			{
				return this.get_model_field_value(this.CurrentFieldName);
			},

			get_model_field_value: function (field_selector)
			{
				if (!this.current_level_model || -1==this.CurrentCollectionIndex)
				{
					return '';
				}
				else
				{
					var root_to_select= (!this.CurrentCollectionIndex && 0 != this.CurrentCollectionIndex) 
						? this.current_level_model : this.current_level_model[this.CurrentCollectionIndex];
					return !field_selector ? root_to_select : h_fastening_clip.get_model_field_value(root_to_select, field_selector);
				}
			},

			get_Marker_class: function (p)
			{
				if (!p)
					p = ' ';
				return (!this.options || null == this.options || !this.options.Marker_class || null == this.options.Marker_class || '' == this.options.Marker_class)
					? '' : (p + this.options.Marker_class);
			},

			get_fc_options_for_model_selector: function (model_selector)
			{
				return (!this.options || !this.options.field_spec || !this.options.field_spec[model_selector]) ? null : this.options.field_spec[model_selector];
			},

			update_dom: function()
			{

			},

			find_fastened_to_the_dom_item: function (dom_item)
			{
				var i_item = dom_item.attr('data-fc-index');
				while (dom_item && (!i_item || null == i_item))
				{
					dom_item = dom_item.parent();
					i_item = dom_item.attr('data-fc-index');
				}
				var res = { dom_item: dom_item };
				if (i_item && null != i_item)
					res.i_item = i_item;
				return res;
			},

			build_model_selector: function (fc_dom_item)
			{
				var root_dom_item = $(this.selector);
				var res = '';

				var count = 0;
				for (var ditem = fc_dom_item; null != ditem && 0 != ditem.length && ditem[0] != root_dom_item[0] && count < 100; ditem = ditem.parent(), count++)
				{
					var ditem_model_selector = ditem.attr('data-fc-selector');
					if (ditem_model_selector)
						res = ditem_model_selector + res;
				}

				return res;
			},

			get_fc_controller: function(model_selector)
			{
				var icontroller= $(this.selector + ' [data-fc-selector="' + model_selector + '"]');
				var id = icontroller.attr('data-fc-id');
				return this.fc_data[id].controller;
			},

			get_fc_model_value: function (fc_dom_item)
			{
				var model_selector = this.build_model_selector(fc_dom_item);
				var self = this;
				var sel = this.selector;
				var model = null;
				h_fastening_clip.each_first_level_fastening($(sel), function (dom_item)
				{
					var item_model_selector = dom_item.attr('data-fc-selector');
					var fc_id = dom_item.attr('data-fc-id');
					var fc_methods = h_fastening_clips.find_methods_for(dom_item);
					var fc_data = self.fc_data[fc_id];
					if (item_model_selector || '' == item_model_selector)
					{
						if (0 == model_selector.indexOf(item_model_selector))
						{
							model = fc_methods.save_to_model(model, item_model_selector, dom_item, fc_data);
						}
					}
				});
				var res = h_fastening_clip.get_model_field_value(model, model_selector);
				return res;
			},

			set_fc_model_value: function (fc_dom_item, model_value)
			{
				var model_selector = this.build_model_selector(fc_dom_item);
				var self = this;
				var sel = this.selector;
				h_fastening_clip.each_first_level_fastening($(sel), function (dom_item)
				{
					var item_model_selector = dom_item.attr('data-fc-selector');
					if (item_model_selector || '' == item_model_selector)
					{
						var fc_id = dom_item.attr('data-fc-id');
						var fc_methods = h_fastening_clips.find_methods_for(dom_item);
						var fc_data = self.fc_data[fc_id];
						if (0==model_selector.indexOf(item_model_selector))
						{
							var new_fc_data = fc_methods.load_from_model(model_value, model_selector, dom_item, fc_data);
							if (new_fc_data)
								new_fc_data.id = fc_id;
							self.fc_data[fc_id] = new_fc_data;
						}
					}
				});
				if (this.on_after_update_dom)
					this.on_after_update_dom();
			},

			render_fastened: function ()
			{
				var self = this;
				var sel = this.selector;
				h_fastening_clip.each_first_level_fastening($(sel), function (dom_item)
				{
					var fc_id = dom_item.attr('data-fc-id');
					var fc_methods = h_fastening_clips.find_methods_for(dom_item);
					var model_selector = dom_item.attr('data-fc-selector');
					var fc_options = self.get_fc_options_for_model_selector(model_selector);
					var fc_data = !self.fc_data ? null : self.fc_data[fc_id];
					fc_data = fc_methods.render(fc_options, self.model, model_selector, dom_item, fc_data);
					if (fc_data && null != fc_data)
					{
						if (!self.fc_data)
							self.fc_data = {};
						self.fc_data[fc_id] = fc_data;
					}
				});
			},

			acyclic_copy_fc_data_data: function(data)
			{
				var res = {};

				for (var name in data)
				{
					var field = data[name];
					res[name] = ('function' == typeof field) ? 'function' : codec_copy().Copy(field);
				}

				return res;
			},

			acyclic_copy_fc_data: function(fc_data)
			{
				var fc_data_copy = {};
				for (var id in fc_data)
				{
					var fc_data_item = fc_data[id];
					if (!fc_data_item)
					{
						fc_data_copy[id] = 'undefined';
					}
					else
					{
						var fc_data_item_copy = {};
						if (fc_data_item.afc_type && null != fc_data_item.afc_type && '' != fc_data_item.afc_type)
							fc_data_item_copy.afc_type = fc_data_item.afc_type;
						if (fc_data_item.amodel_selector && null != fc_data_item.amodel_selector && '' != fc_data_item.amodel_selector)
							fc_data_item_copy.amodel_selector = fc_data_item.amodel_selector;
						if (fc_data_item.cdata && null != fc_data_item.cdata)
							fc_data_item_copy.cdata = this.acyclic_copy_fc_data_data(fc_data_item.cdata);
						if (fc_data_item.children)
							fc_data_item_copy.children = this.acyclic_copy_fc_data(fc_data_item.children);
						fc_data_copy[id] = fc_data_item_copy;
					}
				}
				return fc_data_copy;
			},

			acyclic_copy_fastening: function()
			{
				var res = {};
				if (this.controller && null != this.controller)
					res.controller = '!null';
				if (this.model && null != this.model)
					res.model = '!null';
				res.selector = this.selector;
				if (this.fc_data)
					res.fc_data = this.acyclic_copy_fc_data(this.fc_data);
				return res;
			},

			SaveFieldsToModel: function (selector, model)
			{
				var self = this;
				h_fastening_clip.each_first_level_fastening($(selector), function (dom_item)
				{
					var fc_id = dom_item.attr('data-fc-id');
					var fc_methods = h_fastening_clips.find_methods_for(dom_item);
					var model_selector = dom_item.attr('data-fc-selector');
					var item_fc_data = !self.fc_data ? null : self.fc_data[fc_id];
					var new_model = fc_methods.save_to_model(model, model_selector, dom_item, item_fc_data);
					if ((!model || null == model) && new_model && null != new_model)
						model = new_model;
				});
				return model;
			},

			Destroy: function (selector)
			{
				var self = this;
				h_fastening_clip.each_first_level_fastening($(selector), function (dom_item)
				{
					var fc_id = dom_item.attr('data-fc-id');
					var fc_methods = h_fastening_clips.find_methods_for(dom_item);
					var item_fc_data = !self.fc_data ? null : self.fc_data[fc_id];
					fc_methods.destroy(dom_item,item_fc_data);
				});
			},

			Validate: function()
			{
				return null;
			}
		};
		h_fastening_clips.add_template_argument_methods(fastened);
		return fastened;
	}
});
