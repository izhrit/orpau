﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_fields = fc_abstract();

	fc_fields.match = function (adom_item, tag_name, fc_type)
	{
		return 'DIV' == tag_name && 'fields' == fc_type;
	}

	fc_fields.do_render = function (model,controller,selector)
	{
		if (!model)
		{
			controller.CreateNew(selector);
		}
		else
		{
			controller.SetFormContent(model);
			controller.Edit(selector);
		}
	}

	fc_fields.do_safe_render = function (options, model, controller, selector)
	{
		if (!options.render_on_activate)
		{
			this.do_render(model, controller, selector);
		}
		else
		{
			var tab_page_item= $(selector).parents('div[role="tabpanel"]');
			var tab_page_item_id= tab_page_item.attr('id');
			if ('false' == tab_page_item.attr('aria-hidden'))
			{
				this.do_render(model, controller, selector);
			}
			else
			{
				var tab_control_item= tab_page_item.parents('div.ui-tabs');
				var self= this;
				tab_control_item.on('tabsactivate', function (event, ui)
				{
					if (tab_page_item_id == ui.newPanel.attr('id'))
					{
						self.do_render(model, controller, selector);
						tab_page_item_id= null;
					}
				});
			}
		}
	}

	fc_fields.render = function (options, model, model_selector, adom_item, fc_data)
	{
		if (!options)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent options for "' + model_selector + '"!');
		}
		else if (!options.controller)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent controller for "' + model_selector + '"!');
		}
		else
		{
			var controller = options.controller();

			var selector= $(adom_item).selector;
			this.do_safe_render(options, model, controller, selector);

			if (!fc_data)
				fc_data = {};

			fc_data.afc_type = 'fields';
			fc_data.amodel_selector = model_selector;
			fc_data.controller = controller;
		}
		return fc_data;
	}

	fc_fields.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var controller = fc_data.controller;
		return fc_data;
	}

	fc_fields.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		var controller = fc_data.controller;
		var fields = controller.GetFormContent();

		for (var field_name in fields)
			model = h_fastening_clip.set_model_field_value(model, field_name, fields[field_name]);

		return model;
	}

	fc_fields.destroy = function (adom_item, fc_data)
	{
		if (fc_data && null != fc_data)
		{
			var controller = fc_data.controller;
			controller.Destroy();
		}
	}

	fc_fields.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.fields_control = function (model_selector)
		{
			return this.fastened_div(model_selector, 'fields');
		}
	}

	return fc_fields;
});