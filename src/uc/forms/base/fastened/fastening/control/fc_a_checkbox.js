﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip, h_msgbox)
{
	var fc_link_with_modal_dialog = fc_abstract();

	fc_link_with_modal_dialog.match = function (adom_item, tag_name, fc_type)
	{
		return 'A' == tag_name && 'a-checkbox' == fc_type;
	}

	fc_link_with_modal_dialog.render = function (options, model, model_selector, adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		if (!options)
		{
			dom_item.html(dom_item.html() + '<br/>absent options for "' + model_selector + '"!');
		}
		else
		{
			var value = !model ? null : h_fastening_clip.get_model_field_value(model, model_selector);
			if ((value || false===value) && options.text)
				dom_item.text(options.text(!!value));

			if (!fc_data)
				fc_data = {};
			fc_data.value = value;

			dom_item.on(
				{
					click: function (e)
					{
						e.preventDefault();
						var value = fc_data.value;
						fc_data.value = !value;
						if (null != options.text)
							dom_item.text(options.text(fc_data.value));
					}
				});
		}
		return fc_data;
	}

	fc_link_with_modal_dialog.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		return fc_data;
	}

	fc_link_with_modal_dialog.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		return h_fastening_clip.set_model_field_value(model, model_selector, fc_data.value);
	}

	fc_link_with_modal_dialog.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.a_checkbox_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector, 'a-checkbox');
		}
	}

	return fc_link_with_modal_dialog;
});