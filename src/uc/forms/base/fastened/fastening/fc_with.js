﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_with = fc_abstract();

	var fc_type_with = 'with';

	fc_with.match = function (adom_item, tag_name, fc_type)
	{
		return fc_type_with == fc_type;
	}

	fc_with.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var self = fc_with;
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_methods = self.h_fastening_clips.find_methods_for(child_dom_item);
			var child_fc_id = child_dom_item.attr('data-fc-id');
			var child_model_selector = child_dom_item.attr('data-fc-selector');
			var child_fc_data = !fc_data || !fc_data.children  ? null : fc_data.children[child_fc_id];
			child_fc_data = child_fc_methods.load_from_model(model, child_model_selector, child_dom_item, child_fc_data);
			if (child_fc_data)
				child_fc_data.id = child_fc_id;
			if (!fc_data)
				fc_data = { children: {} };
			if (!fc_data.children)
				fc_data.children = {};
			fc_data.children[child_fc_id] = child_fc_data;
		});
		return fc_data;
	}

	fc_with.save_to_model = function (model, model_selector, adom_item, fc_data)
	{
		var self = fc_with;
		var dom_item = $(adom_item);
		var fc_model = h_fastening_clip.get_model_field_value(model, model_selector);
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_methods = self.h_fastening_clips.find_methods_for(child_dom_item);
			var child_fc_id = child_dom_item.attr('data-fc-id');
			var child_model_selector = child_dom_item.attr('data-fc-selector');
			var child_fc_data = (!fc_data || !fc_data.children) ? null : fc_data.children[child_fc_id];
			fc_model = child_fc_methods.save_to_model(fc_model, child_model_selector, child_dom_item, child_fc_data);
		});
		model = h_fastening_clip.set_model_field_value(model, model_selector, fc_model);
		return model;
	}

	fc_with.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.start_with = function ()
		{
			this.push_fc_context();
		}

		_template_argument.with_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector, fc_type_with);
		}

		_template_argument.end_with = function ()
		{
			var children = this.pop_fc_context();
			this.store_fc_data({ children: children, fc_type: fc_type_with });
		}
	}

	return fc_with;
});