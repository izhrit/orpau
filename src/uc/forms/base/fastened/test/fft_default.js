﻿define([
	  'forms/base/fastened/test/fft_abstract'
]
, function (fft_abstract)
{
	var default_fastened_field_tester = fft_abstract();

	default_fastened_field_tester.match = function (dom_item, tag_name, fc_type)
	{
		return true;
	}

	default_fastened_field_tester.check_value = function (dom_item, value)
	{
		var evalue= dom_item.val();
		if (value==evalue)
		{
			return null; // 'ok, value is "' + value + '"';
		}
		else
		{
			return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
		}
	}

	default_fastened_field_tester.set_value = function (dom_item, value)
	{
		dom_item.val(value);
		dom_item.change();
		dom_item.trigger('focusout');
		var fixed_value= (!value || !value.replace) ? value : value.replace(/\r\n/g,'\n').replace(/\n/g,'\r\n');
		return 'set value "' + fixed_value + '"';
	}

	return default_fastened_field_tester;
});