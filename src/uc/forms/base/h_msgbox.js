define([
	'tpl!forms/base/ajax/v_ajax_request.html'
	,'forms/base/h_numbering'
	,'forms/orpau/base/h_check_mobile'
],
function (v_ajax_request, h_numbering, h_check_mobile)
{
	var helper_res =
	{
		Create_OnOpen_ForMsgBoxWithController: function (options)
		{
			return function ()
			{
				var id_div = options.id_div;
				if (!id_div)
					id_div = 'cpw-msgbox';
				var box_sel = '#' + id_div;

				options.controller.Edit(box_sel);
			}
		}

		, Create_OnClose_ForMsgBoxWithController: function (options)
		{
			return function ()
			{
				var id_div = options.id_div;
				if (!id_div)
					id_div = 'cpw-msgbox';
				var box_sel = '#' + id_div;

				if (options.controller)
				{
					options.controller.Destroy();
					options.controller = null;
				}
				var box = $(box_sel);
				box.dialog('destroy');
				box.html('');
			}
		}

		, PrepareDiv: function (options)
		{
			var id_div = options.id_div;
			if (!id_div)
				id_div = 'cpw-msgbox';
			var box_sel = '#' + id_div;

			var box = $(box_sel);
			if (0 == box.length)
			{
				$('body').prepend('<div id="' + id_div + '" class="cpw-msgbox" style="display:none">Здесь будет всплывающая форма</div>');
				box = $(box_sel);
			}
			return box;
		}

		, Close: function (options)
		{
			var id_div = 'cpw-msgbox';
			if (options && options.id_div)
				id_div = options.id_div;
			var box_sel = '#' + id_div;
			$(box_sel).dialog("close");
		}

		, ShowModal: function (options)
		{
			if (!options)
				options= {};
			if (!options.modal)
				options.modal = true;
			this.Show(options);
		}

		, Show: function (options)
		{
			var box = this.PrepareDiv(options);

			if (options.html)
			{
				box.html(options.html);
				options.close = this.Create_OnClose_ForMsgBoxWithController(options);
				delete options.html;
			}
			else if (options.controller)
			{
				box.html('');
				options.open = this.Create_OnOpen_ForMsgBoxWithController(options);
				options.close = this.Create_OnClose_ForMsgBoxWithController(options);
				if (options.controller.size)
				{
					options.width = options.controller.size.width;
					options.height = options.controller.size.height;
				}
			}
			else if (options.title)
			{
				box.html(options.title);
			}

			if (!options.modal)
				options.modal = true;
			if (!options.modal)
				options.resizable = false;

			var buttons = (!options.buttons) ? ['OK'] : options.buttons;

			var on_close = options.onclose ? options.onclose : function (bname) { };
			var on_after_close = options.onafterclose ? options.onafterclose : function (bname) { };
			delete options.onclose;

			options.buttons = {};
			for (var i = 0; i < buttons.length; i++)
			{
				var button_name = buttons[i];
				options.buttons[button_name] =
					(function (bname, onclose, onafterclose)
					{
						return function ()
						{
							var res = onclose(bname,this);
							if (false != res)
							{
								$(this).dialog("close");
								onafterclose(bname);
							}
						}
					})
				(button_name, on_close, on_after_close);
			}

			options.position = { my: 'center', at: 'center', of: window };

			box.dialog(options);
		}

		, ShowAjaxRequest: function (title, ajaxurl, aid_div)
		{
			var h_msgbox = this;
			var id_div = aid_div ? aid_div : "id-div-ShowAjaxRequest";

			var timestart = new Date();
			var timerid = setInterval(function ()
			{
				var t = new Date();
				var milliseconds = 0;
				$('div.cpw-form-ajax-request span.time').text(t.toLocaleString());
				var seconds = Math.floor((t - timestart) / 1000);
				seconds = seconds + ' ' + h_numbering(seconds, 'секунда', 'секунды', 'секунд');
				$('div.cpw-form-ajax-request span.seconds').text(seconds);
			}
			, 900);

			var safeClearInterval= function()
			{
				if (null != timerid)
				{
					var timerid_to_close = timerid;
					timerid = null;
					clearInterval(timerid_to_close);
				}
			}

			var res = {
				title: title
				, ajaxurl: ajaxurl
				, shown: true
				, ShowAjaxError: function (data, textStatus)
				{
					this.stop();
					h_msgbox.ShowAjaxError(this.title, this.ajaxurl, data, textStatus);
				}
				,stop: function()
				{
					if (this.shown)
					{
						safeClearInterval();
						h_msgbox.Close({ id_div: id_div });
						this.shown = false;
					}
				}
				,on_error: function(old_error)
				{
					var self = this;
					return function(data, textStatus)
					{
						if (!old_error)
						{
							self.ShowAjaxError(data, textStatus);
						}
						else
						{
							self.stop();
							old_error.call(null, data, textStatus);
						}
					}
				}
				,prepare: function(args)
				{
					args.url= this.ajaxurl;
					args.error= this.on_error(args.error);
					var self = this;
					var old_success = args.success;
					args.success= function(data, textStatus)
					{
						self.stop();
						if (old_success && null!=old_success)
							return old_success.call(null, data, textStatus);
					}
					return args;
				}
				,ajax: function(args)
				{
					$.ajax(this.prepare(args));
				}
			}

			var btnClose = 'Закрыть отображение вызова ajax запроса';
			h_msgbox.Show({
				width: h_check_mobile.isMobile() ? window.innerWidth : 640, id_div: id_div
				, title: 'Вызов ajax запроса (обращение к серверу)'
				, html: v_ajax_request({ request: title, timestart: timestart })
				, buttons: [btnClose]
				, onclose: function ()
				{
					safeClearInterval();
					res.shown = false;
				}
			});

			return res;
		}

		, ShowAjaxError: function (title, url, data, textStatus, errorThrown)
		{
			var error_html = title + '<br/>';
			error_html += 'url: <b><a href="' + url + '">' + url.replace(/&/g,'&amp;') + '</a></b>!<br/>\r\n';
			error_html += 'textStatus: <b>' + textStatus + '</b><br/>\r\n';
			if (errorThrown)
				error_html += 'errorThrown: <b>' + errorThrown + '</b><br/>\r\n';
			if (data && null != data && data.responseText)
			{
				var txt = data.responseText;
				error_html += 'responseText: <pre style="font-size:0.8em;white-space: pre-wrap;">' + txt + '</pre>\r\n';
			}
			error_html += 'data: <small>' + JSON.stringify(data, null, '\t') + '</small><br/>\r\n';
			var title = 'Отчёт об ошибке ajax запроса';
			var buttons = ['Закрыть отчёт об ошибке и продолжить работу'];
			this.ShowModal({ html: error_html, width: h_check_mobile.isMobile() ? window.innerWidth : 900, height: 400, title: 'Ошибка ajax запроса', buttons: buttons });
		}

		, ShowDeffered: function (options)
		{
			var self = this;
			setTimeout(function () { self.Show(options); }, 50);
		}
	};
	return helper_res;
});
