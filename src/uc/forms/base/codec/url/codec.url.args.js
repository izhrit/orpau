define([
	  'forms/base/codec/codec'
],
function (BaseCodec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.isInt= function(value)
		{
			return !isNaN(value) &&
					parseInt(Number(value)) == value &&
					!isNaN(parseInt(value, 10));
		}

		codec.SetArgValue= function(res,name,value)
		{
			if (null == res)
				res = {};
			var pos_brace = name.indexOf('[');
			if (-1 == pos_brace)
			{
				res[name] = value;
			}
			else
			{
				var braced_substring = name.substring(pos_brace+1, name.length - 1);
				var braced_parts = braced_substring.split('][');

				var parent = res;
				var fname = name.substring(0, pos_brace);
				for (var i= 0; i<braced_parts.length; i++)
				{
					var ename = braced_parts[i];
					if (''==ename)
					{
						if (!parent[fname])
							parent[fname] = [];
						parent = parent[fname];
						fname = parent.length+'';
					}
					else if (this.isInt(ename))
					{
						if (!parent[fname])
							parent[fname] = [];
						parent = parent[fname];
						fname = parseInt(ename);
					}
					else
					{
						if (!parent[fname])
							parent[fname] = {};
						parent = parent[fname];
						fname= ename;
					}
				}
				parent[fname] = value;
			}
			return res;
		}

		codec.Decode= function(txt)
		{
			var pos = txt.indexOf('?');
			if (-1 != pos)
				txt = txt.substring(pos + 1);
			var parts = txt.split('&');
			var res = null;
			for (var i = 0; i < parts.length; i++)
			{
				var part = parts[i];
				var pair = part.split('=');
				if (2 == pair.length)
				{
					var name = pair[0];
					var value = pair[1];
					res= this.SetArgValue(res, name, value);
				}
			}
			return res;
		}

		codec.Encode= function(data)
		{
			var res = '';
			for (var name in data)
			{
				if ('' != res)
					res += '&';
				res += name + '=' + data[name];
			}
			return res;
		}

		return codec;
	}
});
