define([
	  'forms/base/codec/codec'
],
function (BaseCodec)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Encode = function (str)
		{
			return encodeURIComponent(str + '');
		}

		codec.Decode= function(str)
		{
			return decodeURIComponent((str + '').replace(/\+/g, '%20'));
		}

		return codec;
	}
});
