define([
	  'forms/base/codec/url/codec.url.url-args'
]
, function (codec_url_urlargs)
{
	return function(url_prefix)
	{
		var service_base = {}

		var ccodec_url_urlargs= codec_url_urlargs();

		service_base.options =
		{
			ParseUrl: function(url)
			{
				return ccodec_url_urlargs.Decode(url);
			}
		}

		if (url_prefix)
			service_base.options.url_prefix= url_prefix;

		service_base.action = function (cx)
		{
			cx.completeCallback(400, 'bad request', { text: 'unimplemented service!' });
		}

		service_base.send = function (options, originalOptions, jqXHR, headers, completeCallback)
		{
			var self= this;
			this.action({
				options: options
				, originalOptions: originalOptions
				, jqXHR: jqXHR
				, url_args: function () { return self.options.ParseUrl(options.url); }
				, data_args: function () { return ccodec_url_urlargs.Decode(options.data); }
				, headers: headers
				, completeCallback: completeCallback
			});
			completeCallback(400, 'bad request', { text: 'unimplemented service!' });
		}

		service_base.prepare_send_abort = function (options, originalOptions, jqXHR)
		{
			var self= this;
			var send_abort=
			{
				send: function (headers, completeCallback)
				{
					self.send(options, originalOptions, jqXHR, headers, completeCallback);
				}
				, abort: function ()
				{
				}
			}
			return send_abort;
		};

		service_base.prepare_try_to_prepare_send_abort = function ()
		{
			var self = this;
			return function (options, originalOptions, jqXHR)
			{
				if (0 == options.url.indexOf(self.options.url_prefix))
				{
					return self.prepare_send_abort(options, originalOptions, jqXHR);
				}
			}
		}

		service_base.reg_ajaxTransport = function ()
		{
			$.ajaxTransport('+*', this.prepare_try_to_prepare_send_abort());
		}

		return service_base;
	}
});
