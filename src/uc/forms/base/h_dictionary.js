﻿define(function(){
	var h_dictionary=
	{
		CreateDictRowWrapper: function(values)
		{
			var res_row_wrapper= {};
			for (var i= 0; i<values.header.length; i++)
			{
				var field_name= values.header[i];
				res_row_wrapper[field_name]= (function(ifield,field_name)
				{
					return function() {return this.row[ifield];}
				})(i,field_name);
			}
			res_row_wrapper.wrap= function()
			{
				var res= {};
				for (var i= 0; i<values.header.length; i++)
				{
					var field_name= values.header[i];
					res[field_name]= this[field_name]();
				}
				return res;
			}
			return res_row_wrapper;
		}

		,FindByNameOrCode: function(values,term)
		{
			var res = [];
			var fixed_term = term.toUpperCase();
			var row_wrapper= this.CreateDictRowWrapper(values);
			for (var i = 0; i < values.rows.length; i++)
			{
				row_wrapper.row = values.rows[i];
				if (-1!=row_wrapper.NAME().toUpperCase().indexOf(fixed_term) ||
					-1!=row_wrapper.CODE().toUpperCase().indexOf(fixed_term))
				{
					var id= row_wrapper.ID();
					res.push({id:id, text:id, row: row_wrapper.wrap() });
				}
				if (res.length > 20)
					break;
			}
			return res;
		}

		, FindById: function (values, id)
		{
			var row_wrapper = this.CreateDictRowWrapper(values);
			for (var i = 0; i < values.rows.length; i++)
			{
				row_wrapper.row = values.rows[i];
				var row_id = row_wrapper.ID();
				if (id == row_id)
				{
					var res = { id: id, text: id, row: row_wrapper.wrap() };
					return res;
				}
			}
			return null;
		}

		,FindFirstByField:function(values,field_name,field_value)
		{
			var row_wrapper= this.CreateDictRowWrapper(values);
			for (var i = 0; i < values.rows.length; i++)
			{
				row_wrapper.row = values.rows[i];
				if (field_value==row_wrapper[field_name]())
				{
					var id= row_wrapper.ID();
					return {id:id, text:id, row: row_wrapper.wrap() };
				}
			}
			return null;
		}

	};
	return h_dictionary;
});