﻿define([
	'forms/base/h_times'
],
function (helper_Times)
{
	var helper =
	{
		ValidateSymbolsINN: function (e)
		{
			var key = e.charCode || e.keyCode || 0;
			return key == 8
				|| key == 9
				|| key == 35
				|| key == 36
				|| key == 45
				|| key == 46
				|| (key == 65 && e.ctrlKey)
				|| (key == 67 && e.ctrlKey)
				|| (key == 86 && e.ctrlKey)
				|| (key == 88 && e.ctrlKey)
				|| (key == 90 && e.ctrlKey)
				|| (key >= 37 && key <= 40)
				|| (key >= 48 && key <= 57 && !e.shiftKey)
				|| (key >= 96 && key <= 105);
		}

		, IsValidINN: function (inn)
		{
			if (!inn || inn == "")
			{
				return false;
			}
			else
			{
				if (inn.length == 10)
				{
					return inn[9] == String(((
							2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
							3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
							4 * inn[6] + 6 * inn[7] + 8 * inn[8]
						) % 11) % 10);
				}
				else if (inn.length == 12)
				{
					return inn[10] == String(((
							7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
							10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
							9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
							8 * inn[9]
						) % 11) % 10) && inn[11] == String(((
							3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
							4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
							5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
							6 * inn[9] + 8 * inn[10]
						) % 11) % 10);
				}
				else if (inn.length == 14)
				{
					return true;
				}
				return false;
			}
		}

		, ValidateINN: function (elem)
		{
			var inn = elem.val();
			var res = helper.IsValidINN(inn);
			if (res)
				helper.RemoveErrorClass(elem);
			else
				helper.AddErrorClass(elem, 'Невалидный ИНН');
			return res;
		}

		, OnValidateINN: function (e)
		{
			if (e.keyCode != 9)
			{
				helper.ValidateINN($(e.target));
			}
		}

		, OnConvertToDateFormat: function (e)
		{
			if ($(e.target).val() != "")
			{
				helper.ConvertToDateFormat($(e.target));
				helper.IsValidDate($(e.target));
			}
		}

		, ConvertToDateFormat: function (elem)
		{
			var year, month, day;
			var txtDate = elem.val();
			if (txtDate != "")
			{
				if (txtDate.indexOf('.') != -1)
				{
					var parts = txtDate.split('.');
					year = parts[2];
					month = parts[1];
					day = parts[0];
				}
				else
				{
					var parts = txtDate.split('-');
					year = parts[0];
					month = parts[1];
					day = parts[2];
				}
				if (year != undefined && month != undefined && day != undefined)
				{
					var newDate = $.datepicker.formatDate("dd.mm.yy", new Date(year, month - 1, day));
					elem.val(newDate);
				}
			}
		}

		, IsValidDate: function (elem)
		{
			var res = true;
			if (elem.val() && elem.val() != "")
			{
				var parts = elem.val().split('.');
				var year = parts[2];
				var month = parts[1];
				var day = parts[0];
				res = year != undefined && year.length == 4
				   && month != undefined && month.length == 2
				   && day != undefined && day.length == 2
				if (res)
					helper.RemoveErrorClass(elem);
				else
					helper.AddErrorClass(elem, 'Неверный формат даты');
			}
			return res;
		}

		, IsValidPeriodDate: function (elemStartDate, elemEndDate, error)
		{
			var res = true;
			var startDate = elemStartDate ? elemStartDate.val() : helper_Times.nowDateUTC('r');
			var endDate = elemEndDate ? elemEndDate.val() : helper_Times.nowDateUTC('r');
			var start_date = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			var end_date = new Date(endDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			res = start_date <= end_date;
			var title = error ? error : '';
			if (!res && elemStartDate)
				helper.AddErrorClass(elemStartDate, title);
			else if (res && elemStartDate)
				helper.RemoveErrorClass(elemStartDate);
			if (!res && elemEndDate)
				helper.AddErrorClass(elemEndDate, title);
			else if (res && elemEndDate)
				helper.RemoveErrorClass(elemEndDate);
			return res;
		}

		, IsValidPeriodDateParams: function (attrs)
		{
			var res = true;
			var elemStartDate = attrs[0];
			var elemEndDate = attrs[1];
			var error = attrs[2];
			var startDate = elemStartDate ? elemStartDate.val() : helper_Times.nowDateUTC('r');
			var endDate = elemEndDate ? elemEndDate.val() : helper_Times.nowDateUTC('r');
			var start_date = new Date(startDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			var end_date = new Date(endDate.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			res = start_date <= end_date;
			var title = error ? error : '';
			if (!res && elemStartDate)
				helper.AddErrorClass(elemStartDate, title);
			else if (res && elemStartDate)
				helper.RemoveErrorClass(elemStartDate);
			if (!res && elemEndDate)
				helper.AddErrorClass(elemEndDate, title);
			else if (res && elemEndDate)
				helper.RemoveErrorClass(elemEndDate);
			return res;
		}

		, OnValidateDateIsNotEmpty: function (e)
		{
			var elem = $(e.target);
			helper.ValidateIsNotEmpty(elem);
			helper.IsValidDate(elem);
		}

		, OnValidateDateBeforeNow: function (elem)
		{
			helper.IsValidDateBeforeNow(elem);
		}

		, IsValidDateBeforeNow: function (elem)
		{
			return elem.val() == "" ||
				   (helper.IsValidDate(elem) &&
				   helper.IsValidPeriodDate(elem, null, 'Значение должно быть не позднее текущей даты'));
		}

		, OnValidateIsNotEmpty: function (e)
		{
			if (e.keyCode != 9)
			{
				var el = $(e.target);
				helper.ValidateIsNotEmpty(el);
			}
		}

		, ValidateIsNotEmpty: function (elem)
		{
			var res = true;
			if (res = ($.trim(elem.val()).length != 0))
			{
				helper.RemoveErrorClass(elem);
			}
			else
			{
				helper.AddErrorClass(elem, 'Не может быть пустым');
			}
			return res;
		}

		, OnValidateSelect2IsNotEmpty: function (e)
		{
			if (e.keyCode != 9)
			{
				var el = $(e.target);
				helper.ValidateIsNotEmpty(el);
			}
		}

		, ValidateSelect2IsNotEmpty: function (elem)
		{
			var res = true;
			if (res = ($.trim(elem.val()).length != 0))
				helper.RemoveErrorClass(elem);
			else
				helper.AddErrorClass(elem, 'Не может быть пустым');
			return res;
		}

		, OnValidateAddressIsNotEmpty: function (attrs)
		{
			var address = attrs[0] ? attrs[0] : null;
			var idControl = attrs[1] ? attrs[1] : null;
			if (!address || !idControl)
			{
				return false;
			}
			var res = helper.ValidateAddressIsNotEmpty(address, idControl);
			return res;
		}

		, ValidateAddressIsNotEmpty: function (address, idControl)
		{
			var res = address != undefined && (address.text && '' != address.text && null != address.text);
			if (res)
			{
				$('#' + idControl + '_errors').hide();
			}
			else
			{
				$('#' + idControl + '_errors').show();
			}
			return res;
		}

		, IsValidControlsOnTabPage: function (resultValidation, tabPage)
		{
			if (resultValidation)
				helper.RemoveErrorClass(tabPage);
			else
				helper.AddErrorClass(tabPage);

		}

		, IsValidName: function (elem)
		{
			var regexp = /[^А-Я-Ёа-я-ё0-9IVXLCDM—\s\.,\'\-]/
			var value = elem.val();
			var res = !regexp.test(value);
			if (res)
				helper.RemoveErrorClass(elem);
			else
				helper.AddErrorClass(elem, 'Может содержать только буквы (кириллица), цифры, символы: «.», «,», «\'», «-», «I», «V», «X», «L», «C», «D», «M», пробел');
			return res;
		}

		, AddErrorClass: function (elem, title)
		{
			elem.addClass('error');
			elem.prop('title', title);
		}

		, RemoveErrorClass: function (elem)
		{
			elem.removeClass('error');
			elem.prop('title', '');
		}

		, ValidateProfileCommonData: function (content)
		{
			var messageError = '';
			if (!content.supplierInfo || content.supplierInfo == "")
				messageError += '\r\n   "' + 'Уникальный идентификатор системы, передающей данные, получаемый в ППО "Территория"' + '"';
			if (!content.subdivision || content.subdivision.id == -1)
				messageError += '\r\n   "' + 'Территориальный орган / структурное подразделение ФМС' + '"';
			if (!content.employee || content.employee.name == "")
				messageError += '\r\n   "' + 'Строка, идентифицирующая пользователя (ФИО / логин)' + '"';
			if (!content.employee || content.employee.ummsId == "")
				messageError += '\r\n   "' + 'Идентификатор пользователя в ППО "Территория"' + '"';
			return messageError;
		}

		, ValidatePhoneNumber: function(phone)
		{
			if(!phone) return false;
			var res = phone.replace(/\D+/g, '');
			return res.length == 11;
		}

		, ValidateEmail: function(email)
		{
			if(!email) return false;
			return /[a-z0-9]+@[a-z0-9]+\.[a-z]+/.test(email.toLowerCase());
		}
	}
	return helper;
});