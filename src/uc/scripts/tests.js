
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

window= {};
app= { UseConsoleLog: true, cpw_Now: function(){return new Date(2015, 6, 26, 17, 36, 0, 0 );} };

function ProcessException(e)
{
  WScript.Echo(e);
  WScript.Echo(e.description);
  for (var prop in e)
  {
    WScript.Echo(prop + ":" + e[prop]);
  }
  WScript.Echo("}");
}

function WSHInclude(path)
{
  try
  {
    if (!fso)
      WScript.Echo('fso is null');
    var objStream = new ActiveXObject("ADODB.Stream");
    if (!objStream)
      WScript.Echo('can not create objStream');
    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.LoadFromFile(path);
    try
    {
      var txt= objStream.ReadText();
      if (!txt)
        WScript.Echo('ReadText for file returns null');
      try
      {
        var res= eval(txt);
      }
      catch (e)
      {
        WScript.Echo('eval throw exception!');
        throw e;
      }
      return res;
    }
    finally
    {
      objStream.Close();
      delete objStream;
    }
  }
  catch (e)
  {
    WScript.Echo("can not include path \"" + path + "\"");
    WScript.Echo("shell.CurrentDirectory=\"" + shell.CurrentDirectory + "\"");
    ProcessException(e);
    throw e;
  }
}

var varg= WScript.Arguments;

function ReadAllText(fpath,funicode)
{
  try
  {
    var objStream = new ActiveXObject("ADODB.Stream");
    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.LoadFromFile(fpath);
    try
    {
      return objStream.ReadText();
    }
    finally
    {
      objStream.Close();
    }
  }
  catch (ex)
  {
    WScript.Echo("can not load file \"" + fpath + "\"");
    throw ex;
  }
}

function WriteAllText(fpath,text)
{
  var file= fs.CreateTextFile(fpath, true)
  try
  {
    file.Write(text);
  }
  finally
  {
    file.Close();
  }
}

function define()
{
  var arguments_count= arguments.length;
  if (1>arguments_count)
  {
    WScript.Echo("ERROR: define without arguments!");
  }
  else
  {
    var arg0= arguments[0];
    if( Object.prototype.toString.call( arg0 ) === '[object Function]' )
    {
      var res= arg0();
      return res;
    }
    else if( Object.prototype.toString.call( arg0 ) === '[object Array]' )
    {
      var resolved= [];
      for (var i=0; i<arg0.length; i++)
      {
        var dep= arg0[i];
        var include_res= null;
        if (0===dep.indexOf('txt!'))
        {
          include_res= ReadAllText(baseUrl + dep.replace("/","\\").replace('txt!',''),1);
        }
        else if (0===dep.indexOf('tpl!'))
        {
          var tpl_txt= ReadAllText(baseUrl + dep.replace("/","\\").replace('tpl!',''),1);
          include_res= cpw_forms_tests_underscore.template(tpl_txt);
        }
        else
        {
          include_res= WSHInclude(baseUrl + dep.replace("/","\\") + ".js");
        }
        resolved.push(include_res);
      }
      return arguments[1].apply(null,resolved);
    }
  }
}

var baseUrl= "..\\..\\..\\";

function require(deps,func)
{
  var resolved= [];
  for (var i=0; i<deps.length; i++)
  {
    var dep= deps[i];
    var include_res= WSHInclude(baseUrl + dep.replace("/","\\") + ".js");
    resolved.push(include_res);
  }
  return func.apply(null,resolved);
}

function WriteAllTextUtf8(txt,fpath_out)
{
    var objStream = new ActiveXObject("ADODB.Stream");

    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.WriteText(txt);

    var binStream = new ActiveXObject("ADODB.Stream");
    binStream.Type = 1;
    binStream.Mode = 3;
    binStream.Open();

    objStream.Position= 3;
    objStream.CopyTo(binStream);
    objStream.Flush();
    objStream.Close();
    
    binStream.SaveToFile(fpath_out,2);
    binStream.Close();
}

function Decode(base_url,codec_path,fpath_in,fpath_out)
{
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  WSHInclude(base_url + '\\js\\vendors\\underscore.js');
  baseUrl= base_url;
  require([codec_path],function(create_codec)
  {
    var xml_string= ReadAllText(fpath_in,1);
    var codec= create_codec();
    var data, xml_string2;
    codec.formatted= true;
    codec.skipComments= true;
    try
    {
      data= codec.Decode(xml_string);
    }
    catch (e)
    {
      WScript.Echo("can not decode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    try
    {
      xml_string2= JSON.stringify(data, null, "\t").replace(/\n/g, '\r\n');
    }
    catch (e)
    {
      WScript.Echo("can not encode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    WriteAllTextUtf8(xml_string2,fpath_out);
  });
}

function Decode_html(base_url,codec_path,fpath_in,fpath_out)
{
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  WSHInclude(base_url + '\\js\\vendors\\underscore.js');
  baseUrl= base_url;
  require([codec_path],function(create_codec)
  {
    var xml_string= ReadAllText(fpath_in,1);
    var codec= create_codec();
    var data, xml_string2;
    codec.formatted= true;
    codec.skipComments= true;
    try
    {
      data= codec.Decode(xml_string);
    }
    catch (e)
    {
      WScript.Echo("can not decode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
	var text_out= '<html><body><head><title>Lenta.ru</title><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /></head>'
		+ data
		+ "</body></html>";
    WriteAllTextUtf8(text_out,fpath_out);
  });
}

function Encode(base_url,codec_path,fpath_in,fpath_out)
{
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  baseUrl= base_url;
  require([codec_path],function(create_codec)
  {
    var txt_string= ReadAllText(fpath_in,1);
    var codec= create_codec();
    var data, xml_string2;
    codec.formatted= true;
    codec.skipComments= true;
    try
    {
      data= JSON.parse(txt_string);
    }
    catch (e)
    {
      WScript.Echo("can not JSON.parse..");
      ProcessException(e);
      throw e;
    }

    try
    {
      xml_string2= codec.Encode(data);
	  if ('string' != typeof xml_string2)
		xml_string2= JSON.stringify(xml_string2, null, '\t').replace(/\n/g, '\r\n');;
    }
    catch (e)
    {
      WScript.Echo("can not encode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    WriteAllTextUtf8(xml_string2,fpath_out);
  });
}

function Encode_module(base_url,codec_path,module_path,fpath_out)
{
      WScript.Echo("Encode_module {");
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  baseUrl= base_url;
  require([codec_path,module_path],function(create_codec,module)
  {
    var codec= create_codec();
    var res_text;

    try
    {
      res_text= codec.Encode(module);
    }
    catch (e)
    {
      WScript.Echo("can not encode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    WriteAllTextUtf8(res_text,fpath_out);
  });
      WScript.Echo("Encode_module }");
}

function ReadWrite(base_url,codec_path,fpath_in,fpath_out)
{
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  baseUrl= base_url;
  require([codec_path],function(create_codec)
  {
    var xml_string= ReadAllText(fpath_in,1);
    var codec= create_codec();
    var data, xml_string2;
    codec.formatted= true;
    codec.skipComments= true;
    try
    {
      data= codec.Decode(xml_string);
    }
    catch (e)
    {
      WScript.Echo("can not decode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    try
    {
      xml_string2= codec.Encode(data);
    }
    catch (e)
    {
      WScript.Echo("can not encode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    WriteAllTextUtf8(xml_string2,fpath_out);
  });
}



function ReadToXaml(base_url,controller_path,fpath_in,fpath_out)
{  
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  WSHInclude(base_url + '\\js\\vendors\\underscore.js');
  baseUrl= base_url;
  require([controller_path],function(create_controller)
  {
    var text_in= ReadAllText(fpath_in,1);
    var controller= create_controller();
    var text_out;
    try
    {
      controller.SetFormContent(text_in);
    }
    catch (e)
    {
      WScript.Echo("can not SetFormContent..");
      WScript.Echo("controller_path=" + controller_path);
      ProcessException(e);
      throw e;
    }
    try
    {
      text_out= controller.BuildXamlView();
    }
    catch (e)
    {
      WScript.Echo("can not BuildXamlView..");
      WScript.Echo("controller_path=" + controller_path);
      ProcessException(e);
      throw e;
    }
    WriteAllTextUtf8(text_out,fpath_out);
  });
}

function ReadToHtml(base_url,controller_path,fpath_in,fpath_out,extra_arg)
{
  WSHInclude(base_url + '\\js\\vendors\\underscore.js');
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  baseUrl= base_url;
  require([controller_path],function(create_controller)
  {
    var text_in= ReadAllText(fpath_in,1);
    var controller= create_controller();
    var text_out;
    try
    {
      controller.SetFormContent(text_in);
    }
    catch (e)
    {
      WScript.Echo("can not SetFormContent..");
      WScript.Echo("controller_path=" + controller_path);
      ProcessException(e);
      throw e;
    }
    try
    {
      text_out= controller.BuildHtmlReport(extra_arg);
    }
    catch (e)
    {
      WScript.Echo("can not BuildHtmlReport..");
      WScript.Echo("controller_path=" + controller_path);
      ProcessException(e);
      throw e;
    }
	text_out= '<html><body><head><title>Lenta.ru</title><meta content="text/html; charset=utf-8" http-equiv="Content-Type" /></head>'
		+ text_out
		+ "</body></html>";
    WriteAllTextUtf8(text_out,fpath_out);
  });
}

function ReadToTxt(base_url,controller_path,fpath_in,fpath_out,extra_arg)
{
  WSHInclude(base_url + '\\js\\vendors\\underscore.js');
  WSHInclude(base_url + '\\js\\vendors\\JSON2.js');
  baseUrl= base_url;
  require([controller_path],function(create_controller)
  {
    var text_in= ReadAllText(fpath_in,1);
    var controller= create_controller();
    var text_out;
    try
    {
      controller.SetFormContent(text_in);
    }
    catch (e)
    {
      WScript.Echo("can not SetFormContent..");
      WScript.Echo("controller_path=" + controller_path);
      ProcessException(e);
      throw e;
    }
    try
    {
      text_out= controller.BuildTxtReport(extra_arg);
    }
    catch (e)
    {
      WScript.Echo("can not BuildTxtReport..");
      WScript.Echo("controller_path=" + controller_path);
      ProcessException(e);
      throw e;
    }
    WriteAllTextUtf8(text_out,fpath_out);
  });
}

function NormalizeXml(fpath_in,fpath_out)
{
  var xd = new ActiveXObject("MSXML2.DOMDocument.6.0");
  xd.load(fpath_in);
  xd.save(fpath_out);
  WScript.Echo('fpath_out=' + fpath_out);
}

function CSVToArray( strData, strDelimiter ){
        // Check to see if the delimiter is defined. If not,
        // then default to comma.
        strDelimiter = (strDelimiter || ",");

        // Create a regular expression to parse the CSV values.
        var objPattern = new RegExp(
            (
                // Delimiters.
                "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

                // Quoted fields.
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

                // Standard fields.
                "([^\"\\" + strDelimiter + "\\r\\n]*))"
            ),
            "gi"
            );


        // Create an array to hold our data. Give the array
        // a default empty first row.
        var arrData = [[]];

        // Create an array to hold our individual pattern
        // matching groups.
        var arrMatches = null;


        // Keep looping over the regular expression matches
        // until we can no longer find a match.
        var i= 0;
        while (arrMatches = objPattern.exec( strData )){

            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[ 1 ];

            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (
                strMatchedDelimiter.length &&
                strMatchedDelimiter !== strDelimiter
                ){
                if (0==(i%500))
                  WScript.Echo('line ' + i + ' :' + JSON.stringify(arrData[ arrData.length - 1 ]));
                i++
                // Since we have reached a new row of data,
                // add an empty row to our data array.
                arrData.push( [] );
            }

            var strMatchedValue;

            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[ 2 ]){

                // We found a quoted value. When we capture
                // this value, unescape any double quotes.
                strMatchedValue = arrMatches[ 2 ].replace(
                    new RegExp( "\"\"", "g" ),
                    "\""
                    );

            } else {

                // We found a non-quoted value.
                strMatchedValue = arrMatches[ 3 ];

            }


            // Now that we have our value string, let's add
            // it to the data array.
            arrData[ arrData.length - 1 ].push( strMatchedValue );
        }

        // Return the parsed data.
        return( arrData );
    }

function PrepareDictionaryInternal(fpath_in,skip_id_without_menu)
{
  WSHInclude('..\\js\\vendors\\JSON2.js');
  WScript.Echo('read file');
  var text_csv= ReadAllText(fpath_in,1);
  WScript.Echo('csv to array');
  var csv_arr= CSVToArray(text_csv,';');
  WScript.Echo('write to json');
  var header= csv_arr[0].slice(0,3);
  //var header= csv_arr[0];

  var res_array= [];
  for (var i= 1; i< csv_arr.length; i++)
  {
    var row= csv_arr[i];
    if (row.length>=3)
    {
      if (!skip_id_without_menu || ''==row[2] || -1!=row[2].indexOf('-'))
        res_array.push(row.slice(0,3));
        //res_array.push(row);
    }
    if (0==(i%500))
      WScript.Echo('line ' + i);
  }

  var res_json= JSON.stringify({header:header,rows:res_array},null,'\t');

  var res_txt= 'define(function(){var res=' + res_json + '; return res;});'

  WriteAllTextUtf8(res_txt,fpath_in + '.js');
  WScript.Echo('ok');
}

function PrepareDictionary(fpath_in)
{
  PrepareDictionaryInternal(fpath_in,false);
}

function PrepareDictionaryIdWithMinus(fpath_in)
{
  PrepareDictionaryInternal(fpath_in,true);
}

if (varg.Count() > 0)
{
  var cmd= varg.Item(0);
  switch (cmd)
  {
    case 'read_write':    ReadWrite (varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4)); break;
    case 'read_to_xaml':  ReadToXaml(varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4)); break;
    case 'read_to_html':  ReadToHtml(varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4),varg.Item(5)); break;
    case 'read_to_txt':   ReadToTxt(varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4),varg.length <= 5 ? null : varg.Item(5)); break;
    case 'normalize_xml': NormalizeXml(varg.Item(1),varg.Item(2)); break;
    case 'prepare_dict':  PrepareDictionary(varg.Item(1)); break;
    case 'prepare_dict_id_with_minus':  PrepareDictionaryIdWithMinus(varg.Item(1)); break;
    case 'decode':        Decode (varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4)); break;
    case 'decode_html':   Decode_html (varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4)); break;
    case 'encode':        Encode (varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4)); break;
	case 'encode_module': Encode_module (varg.Item(1),varg.Item(2),varg.Item(3),varg.Item(4)); break;
    default: WScript.Echo("unknown cmd=" + cmd);
  }
}
