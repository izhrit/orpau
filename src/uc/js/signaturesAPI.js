define([
],
function () {
	var API = {};

	API.getCertificatesList = $.capicom.getCertificatesList;
	API.getDialogBoxCertificates = $.capicom.getDialogBoxCertificates;
	API.findCertificateByHash = $.capicom.findCertificateByHash;
	API.signBin = $.capicom.signBin;
	API.signBase64 = $.capicom.signBase64;
	API.verifyBin = $.capicom.verifyBin;
	API.verifyBase64 = $.capicom.verifyBase64;
	API.verifyBase64Detached = $.capicom.verifyBase64Detached;

	return API;
});