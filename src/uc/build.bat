call :build-orpau
copy built\orpau.js ..\srv\web\js\

call :build-orpau-admin
copy built\orpau-admin.js ..\srv\web\js\

call :build-orpau-superadmin
copy built\orpau-superadmin.js ..\srv\web\js

exit /B

rem -----------------------------------------------------------
:build-orpau
node optimizers\r.js -o conf\build-orpau.js
exit /B

:build-orpau-admin
node optimizers\r.js -o conf\build-orpau-admin.js
exit /B

:build-orpau-superadmin
node optimizers\r.js -o conf\build-orpau-superadmin.js

exit /B