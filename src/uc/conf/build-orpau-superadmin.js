({
    baseUrl: "..",
    include: ['forms/e_orpau_superadmin'],
    name: "optimizers/almond",
    out: "..\\built\\orpau-superadmin.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})