({
    baseUrl: "..",
    include: ['forms/e_orpau_admin'],
    name: "optimizers/almond",
    out: "..\\built\\orpau-admin.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})