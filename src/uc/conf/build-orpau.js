({
    baseUrl: "..",
    include: ['forms/e_orpau'],
    name: "optimizers/almond",
    out: "..\\built\\orpau.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})