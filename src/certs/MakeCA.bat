set UTILS=C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x64

call :prep_cert CA3 IIP CA3
exit

:prep_cert
del %1.pfx
del %1.pvk
del %1.cer
call "%UTILS%\makecert" -pe -sky Exchange -n "CN=%2 %3 %4" %1.cer -sv %1.pvk -r
call "%UTILS%\pvk2pfx" -pvk %1.pvk -spc %1.cer -pfx %1.pfx
exit /B

