{
	"verification_successful": false,
	"verification_time": "01.01.2001 0:00:00",
	"signature_checked": true,
	"signature_checking_message": null,
	"signers": [
		{
			"verification_successful": false,
			"verification_time": "01.01.2001 0:00:01",
			"certificate": {
				"Algorithm": "sha1RSA",
				"Issuer": "CN=Тестовый центр сертифкации",
				"NotAfter": "01.01.2040 3:59:59",
				"NotBefore": "01.04.2012 19:47:30",
				"SerialNumber": "8FE9989E7A0DB0AA424E6046260DAA13",
				"Subject": "CN=Зеев",
				"Thumbprint": "D64678AC86BB6F68C030D949D024EA71F6F4410F"
			},
			"certificate_not_before_and_not_after": false,
			"chain": [
				{
					"certificate": {
						"Algorithm": "sha1RSA",
						"Issuer": "CN=Тестовый центр сертифкации",
						"NotAfter": "01.01.2040 3:59:59",
						"NotBefore": "01.04.2012 19:47:30",
						"SerialNumber": "8FE9989E7A0DB0AA424E6046260DAA13",
						"Subject": "CN=Зеев",
						"Thumbprint": "D64678AC86BB6F68C030D949D024EA71F6F4410F"
					},
					"info": "",
					"statuses": null
				},
				{
					"certificate": {
						"Algorithm": "sha1RSA",
						"Issuer": "CN=Тестовый центр сертифкации",
						"NotAfter": "01.01.2040 3:59:59",
						"NotBefore": "01.12.2011 11:32:11",
						"SerialNumber": "A8093DF3AF8BD090455EE3D6C4213C69",
						"Subject": "CN=Тестовый центр сертифкации",
						"Thumbprint": "B161707CF7389E307A9342517323BC1255E1658B"
					},
					"info": "",
					"statuses": null
				}
			],
			"chain_valid": true,
			"chain_verification_policy": {
				"RevocationFlag": "EntireChain",
				"RevocationMode": "NoCheck",
				"UrlRetrievalTimeout": "00:00:30",
				"VerificationFlags": "NoFlag"
			},
			"signing_time": "18.09.2020 0:00:00"
		}
	]
}