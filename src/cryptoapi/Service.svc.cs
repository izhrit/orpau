﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

using log4net;

namespace rit.cryptoapi
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service : IService
    {
        static ILog log = LogManager.GetLogger(typeof(Service));

        ServiceImplementation implementation = new ServiceImplementation();

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/VerifySignature", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public SignatureVerificationResult VerifySignature
            (
            string auth_token
            , string base64_encoded_document
            , string base64_encoded_signature
            , CertificateVerificationPolicy policy
            )
        {
            log.Debug("VerifySignature {");
            try
            {
                return implementation.VerifySignature(auth_token, base64_encoded_document, base64_encoded_signature, policy);
            }
            catch (Exception ex)
            {
                log.Error("VerifySignature throw exception:",ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("base64_encoded_document=" + base64_encoded_document);
                log.Error("base64_encoded_signature=" + base64_encoded_signature);
                throw;
            }
            finally
            {
                log.Debug("VerifySignature }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/VerifySignatureFromCapicom", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public SignatureVerificationResult VerifySignatureFromCapicom
            (
            string auth_token
            , string document
            , string base64_encoded_signature
            , CertificateVerificationPolicy policy
            )
        {
            log.Debug("VerifySignatureFromCapicom {");
            try
            {
                return implementation.VerifySignatureFromCapicom(auth_token, document, base64_encoded_signature, policy);
            }
            catch (Exception ex)
            {
                log.Error("VerifySignatureFromCapicom throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("document=" + document);
                log.Error("base64_encoded_signature=" + base64_encoded_signature);
                throw;
            }
            finally
            {
                log.Debug("VerifySignatureFromCapicom }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/VerifySignedFromCapicom", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public SignedVerificationResult VerifySignedFromCapicom
            (
            string auth_token
            , string base64_encoded_signed
            , CertificateVerificationPolicy policy
            )
        {
            log.Debug("VerifySignedFromCapicom {");
            try
            {
                return implementation.VerifySignedFromCapicom(auth_token, base64_encoded_signed, policy);
            }
            catch (Exception ex)
            {
                log.Error("VerifySignedFromCapicom throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("base64_encoded_signed=" + base64_encoded_signed);
                throw;
            }
            finally
            {
                log.Debug("VerifySignedFromCapicom }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/GetBase64EncodedCertificateFromCapicomSignature", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string GetBase64EncodedCertificateFromCapicomSignature
            (
            string auth_token
            , string base64_encoded_signature
            , int zero_based_position_of_signer = 0
            )
        {
            log.Debug("GetBase64EncodedCertificateFromCapicomSignature {");
            try
            {
                return implementation.GetBase64EncodedCertificateFromCapicomSignature(auth_token, base64_encoded_signature, zero_based_position_of_signer);
            }
            catch (Exception ex)
            {
                log.Error("GetBase64EncodedCertificateFromCapicomSignature throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("base64_encoded_signature=" + base64_encoded_signature);
                log.ErrorFormat("zero_based_position_of_signer={0}", zero_based_position_of_signer);
                throw;
            }
            finally
            {
                log.Debug("GetBase64EncodedCertificateFromCapicomSignature }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/Sign", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string Sign(string auth_token, string base64_encoded_document, string base64_encoded_certificate)
        {
            log.Debug("Sign {");
            try
            {
                return implementation.Sign(auth_token,base64_encoded_document,base64_encoded_certificate);
            }
            catch (Exception ex)
            {
                log.Error("Sign throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("base64_encoded_document=" + base64_encoded_document);
                log.Error("base64_encoded_certificate=" + base64_encoded_certificate);
                throw;
            }
            finally
            {
                log.Debug("Sign }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/SignAsCapicom", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string SignAsCapicom(string auth_token, string document, string base64_encoded_certificate)
        {
            log.Debug("SignAsCapicom {");
            try
            {
                return implementation.SignAsCapicom(auth_token, document, base64_encoded_certificate);
            }
            catch (Exception ex)
            {
                log.Error("SignAsCapicom throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("document=" + document);
                log.Error("base64_encoded_certificate=" + base64_encoded_certificate);
                throw;
            }
            finally
            {
                log.Debug("SignAsCapicom }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/FindOutDetailsOfTheCases", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public Case_Details[] FindOutDetailsOfTheCases(string auth_token, Case_SearchConditions[] cases, int current_unix_time= 0)
        {
            log.DebugFormat("FindOutDetailsOfTheCases {{ current_unix_time={0}", current_unix_time);
            try
            {
                return implementation.FindOutDetailsOfTheCases(auth_token, cases, current_unix_time);
            }
            catch (Exception ex)
            {
                log.Error("FindOutDetailsOfTheCases throw exception:", ex);
                throw;
            }
            finally
            {
                log.Debug("FindOutDetailsOfTheCases }");
            }
        }

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
         UriTemplate = "/VerifyCertificate", ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public CertificateVerificationResult VerifyCertificate(string auth_token, string base64_encoded_certificate)
        {
            log.Debug("VerifyCertificate {");
            try
            {
                return implementation.VerifyCertificate(auth_token, base64_encoded_certificate);
            }
            catch (Exception ex)
            {
                log.Error("VerifyCertificate throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("base64_encoded_certificate=" + base64_encoded_certificate);
                throw;
            }
            finally
            {
                log.Debug("VerifyCertificate }");
            }
        }

        private static bool AlwaysGoodCertificate(object sender
            , System.Security.Cryptography.X509Certificates.X509Certificate certificate
            , System.Security.Cryptography.X509Certificates.X509Chain chain,
            System.Net.Security.SslPolicyErrors policyErrors)
        {
            return true;
        }

        [WebInvoke(Method = "GET", UriTemplate = "/DownLoadManagerCertificate?auth={auth_token}&ID={MessageGUID}")]
        public byte[] DownLoadManagerCertificate(string auth_token, string MessageGUID)
        {
            log.Debug("DownLoadManagerCertificate {");
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 
                    | System.Net.SecurityProtocolType.Tls 
                    | System.Net.SecurityProtocolType.Tls11 
                    | System.Net.SecurityProtocolType.Tls12;
                System.Net.ServicePointManager.Expect100Continue = true;
                System.Net.ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(AlwaysGoodCertificate);
                return implementation.DownLoadManagerCertificate(auth_token,MessageGUID);
            }
            catch (Exception ex)
            {
                log.Error("DownLoadManagerCertificate throw exception:", ex);
                log.Error("used arguments:");
                log.Error("auth_token=" + auth_token);
                log.Error("MessageGUID=" + MessageGUID);
                throw;
            }
            finally
            {
                log.Debug("DownLoadManagerCertificate }");
            }
        }
    }
}
