﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

using log4net;

namespace rit.cryptoapi
{
    public class Certificate
    {
        public string Issuer;
        public string SerialNumber;
        public string Thumbprint;
        public string Subject;
        public string NotBefore;
        public string NotAfter;
        public string Algorithm;

        public Certificate() { }
        internal Certificate(X509Certificate2 c)
        {
            Issuer = c.Issuer;
            SerialNumber = c.SerialNumber;
            Thumbprint = c.Thumbprint;
            Subject = c.Subject;
            NotAfter = c.NotAfter.ToString();
            NotBefore = c.NotBefore.ToString();
            Algorithm = c.SignatureAlgorithm.FriendlyName;
        }
    }

    public class VerificationResult
    {
        public string verification_time;
        public bool verification_successful;

        internal DateTime StartVerification(Auth.Settings mode)
        {
            DateTime current_time= mode.GetCurrentTime();
            verification_time = current_time.ToString();
            verification_successful = false;
            return current_time;
        }
    }

    public class CertificateVerificationPolicy
    {
        public string RevocationFlag;
        public string RevocationMode;
        public string UrlRetrievalTimeout;
        public string VerificationFlags;

        public CertificateVerificationPolicy() { }
        internal CertificateVerificationPolicy(X509ChainPolicy policy)
        {
            RevocationFlag= policy.RevocationFlag.ToString();
            RevocationMode= policy.RevocationMode.ToString();
            UrlRetrievalTimeout= policy.UrlRetrievalTimeout.ToString();
            VerificationFlags= policy.VerificationFlags.ToString();
        }

        internal X509ChainPolicy CreateX509ChainPolicy()
        {
            X509ChainPolicy res = new X509ChainPolicy();

            X509RevocationMode xRevocationMode;
            if (Enum.TryParse(RevocationMode, out xRevocationMode))
                res.RevocationMode = xRevocationMode;

            X509RevocationFlag xRevocationFlag;
            if (Enum.TryParse(RevocationFlag, out xRevocationFlag))
                res.RevocationFlag = xRevocationFlag;

            X509VerificationFlags xVerificationFlags;
            if (Enum.TryParse(VerificationFlags, out xVerificationFlags))
                res.VerificationFlags = xVerificationFlags;

            TimeSpan xUrlRetrievalTimeout;
            if (TimeSpan.TryParse(UrlRetrievalTimeout, out xUrlRetrievalTimeout))
                res.UrlRetrievalTimeout = xUrlRetrievalTimeout;

            return res;
        }

        static internal X509ChainPolicy DefaultX509ChainPolicy()
        {
            X509ChainPolicy res = new X509ChainPolicy();

            res.RevocationFlag = X509RevocationFlag.EntireChain;
            res.RevocationMode = X509RevocationMode.Online;
            res.UrlRetrievalTimeout = new TimeSpan(0, 0, 30);
            res.VerificationFlags = X509VerificationFlags.NoFlag;

            return res;
        }
    }

    public class ChainStatus
    {
        public string status;
        public string info;

        public ChainStatus() { }
        internal ChainStatus(X509ChainStatus s)
        {
            status = s.Status.ToString();
            info = s.StatusInformation;
        }
    }

    public class ChainElement
    {
        public Certificate certificate;
        public string info;
        public ChainStatus [] statuses;

        public ChainElement() { }
        internal ChainElement(X509ChainElement e)
        {
            certificate = new Certificate(e.Certificate);
            info = e.Information;
            if (0!= e.ChainElementStatus.Length)
            {
                statuses = new ChainStatus[e.ChainElementStatus.Length];
                int i = 0;
                foreach (X509ChainStatus s in e.ChainElementStatus)
                    statuses[i++] = new ChainStatus(s);
            }
        }
    }

    public class CertificateVerificationResult : VerificationResult
    {
        public Certificate certificate;
        public bool certificate_not_before_and_not_after;
        public CertificateVerificationPolicy chain_verification_policy;
        public bool chain_valid;
        public ChainElement[] chain;

        public CertificateVerificationResult() { }
        internal CertificateVerificationResult(X509Certificate2 c, Auth.Settings mode, CertificateVerificationPolicy apolicy)
        {
            DateTime current_time= StartVerification(mode);
            certificate = new Certificate(c);

            certificate_not_before_and_not_after = c.NotBefore <= current_time && current_time <= c.NotAfter;

            X509Chain xchain = new X509Chain();
            xchain.ChainPolicy = null != apolicy ? apolicy.CreateX509ChainPolicy() : CertificateVerificationPolicy.DefaultX509ChainPolicy();
            chain_verification_policy = new CertificateVerificationPolicy(xchain.ChainPolicy);

            chain_valid = xchain.Build(c);

            chain = new ChainElement[xchain.ChainElements.Count];
            int i= 0;
            foreach (X509ChainElement e in xchain.ChainElements)
                chain[i++] = new ChainElement(e);

            verification_successful = certificate_not_before_and_not_after && chain_valid;
        }
    }

    public class SignerVerificationResult : CertificateVerificationResult
    {
        public string signing_time;

        public SignerVerificationResult() { }
        internal SignerVerificationResult(SignerInfo signer, Auth.Settings mode, CertificateVerificationPolicy apolicy)
            : base(signer.Certificate, mode,apolicy)
        {
            DateTime? st = FindSiginingTime(signer.SignedAttributes);
            if (null!=st)
                signing_time = st.ToString();
        }

        static DateTime? FindSiginingTime(CryptographicAttributeObjectCollection attributes)
        {
            if (null != attributes)
            {
                foreach (CryptographicAttributeObject a in attributes)
                {
                    foreach (AsnEncodedData ad in a.Values)
                    {
                        Pkcs9SigningTime sta = ad as Pkcs9SigningTime;
                        if (null != sta && null != sta.SigningTime)
                            return sta.SigningTime;
                    }
                }
            }
            return null;
        }
    }

    public class SignatureVerificationResult : VerificationResult
    {
        public bool signature_checked= false;
        public string signature_checking_message;
        public SignerVerificationResult[] signers= null;

        static public SignatureVerificationResult Start(Auth.Settings mode)
        {
            SignatureVerificationResult res = new SignatureVerificationResult();
            res.StartVerification(mode);
            return res;
        }
    }

    public class SignedVerificationResult : SignatureVerificationResult
    {
        public string signed_text;

        static new public SignedVerificationResult Start(Auth.Settings mode)
        {
            SignedVerificationResult res = new SignedVerificationResult();
            res.StartVerification(mode);
            return res;
        }
    }

    public class Debtor
    {
        public bool isCitizen = true;
        public string Name;
        public string INN;
        public string OGRN;
        public string SNILS;

        public bool isSearchableSide()
        {
            return !string.IsNullOrEmpty(Name);
        }

        public CaseBook.CaseSide CaseSide()
        {
            CaseBook.CaseSide side = new CaseBook.CaseSide();
            side.Name = Name;
            side.Inn = INN;
            side.Ogrn = OGRN;
            side.ShortName = Name;
            side.Type = 1;
            return side;
        }
    }

    public class Case_SearchConditions
    {
        static ILog log = LogManager.GetLogger(typeof(Case_SearchConditions));

        public string id;
        public Debtor debtor;
        public string CourtTag;

        public Case_SearchConditions() { }

        DateTime GetCurrentTime(int current_unix_time)
        {
            if (0 == current_unix_time)
            {
                return DateTime.Now;
            }
            else
            {
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(current_unix_time).ToLocalTime();
                return dtDateTime;
            }
        }

        public CaseBook.SearchCaseArbitrRequest Request(int current_unix_time, bool test_mode)
        {
            CaseBook.SearchCaseArbitrRequest req = new CaseBook.SearchCaseArbitrRequest();
            //req.Count = 20; // too large
            //req.Count = 5; // ok
            //req.Count = 12; // ok
            //req.Count = 16; // ok
            //req.Count = 18; // too large
            //req.Count = 17; // ok
            req.Count = 15;
            req.Sides = new CaseBook.CaseSide[] { debtor.CaseSide() };
            req.Accuracy = CaseBook.AccuracyMode.All;
            // does not work:req.CaseTypeCodes = new CaseBook.CaseType? [] { !cc.debtor.isCitizen ? CaseBook.CaseType.Bankrupt : CaseBook.CaseType.BankruptCitizen };

            if (!test_mode)
                req.IsActive= true;
            DateTime now = GetCurrentTime(current_unix_time);

            req.StartDateFrom = now.AddDays(-30);
            req.StartDateTo = now;

            /*req.StartDateFrom = now.AddDays(-90);
            req.StartDateTo = now.AddDays(90);*/

            return req;
        }

        string fix_snils(string snils)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (char c in snils)
            {
                if (Char.IsDigit(c))
                    sb.Append(c);
            }
            return sb.ToString();
        }

        public bool match(CaseBook.ShortCaseInfo cinfo)
        {
            if (CourtTag != cinfo.CourtTag)
            {
                log.ErrorFormat("       (CourtTag != cinfo.CourtTag) - {0} != {1}", CourtTag, cinfo.CourtTag);
                return false;
            }
            if (debtor.isCitizen)
            {
                if (CaseBook.CaseType.BankruptCitizen != cinfo.CaseTypeCode)
                {
                    log.ErrorFormat("       debtor.isCitizen but cinfo.CaseTypeCode={0}", cinfo.CaseTypeCode);
                    return false;
                }
            }
            else
            {
                if (CaseBook.CaseType.Bankrupt != cinfo.CaseTypeCode)
                {
                    log.ErrorFormat("       !debtor.isCitizen but cinfo.CaseTypeCode={0}", cinfo.CaseTypeCode);
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(debtor.SNILS))
            {
                string snils = fix_snils(cinfo.Bancruptcy.CitizenSnils);
                if (debtor.SNILS != snils)
                {
                    log.ErrorFormat("       (debtor.SNILS != cinfo.Bancruptcy.CitizenSnils) - {0} != {1}", debtor.SNILS, snils);
                    return false;
                }
                
            }
            return true;
        }
    }

    public class Case_Details
    {
        public string id;
        public string CaseNumber;
        public string NextSessionDate;
        public Case_Details(){}
        public Case_Details(string id)
        {
            this.id = id;
        }
    }
}
