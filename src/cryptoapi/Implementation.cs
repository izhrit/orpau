﻿using System;
using System.Collections.Generic;

using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

using log4net;

namespace rit.cryptoapi
{
    public class ServiceImplementation : IService
    {
        static ILog log = LogManager.GetLogger(typeof(ServiceImplementation));

        void TraceAttributes(System.Security.Cryptography.CryptographicAttributeObjectCollection attributes)
        {
            if (null == attributes)
            {
                log.Debug("(null == attributes)");
            }
            else if (0 == attributes.Count)
            {
                log.Debug("(0==attributes.Count)");
            }
            else
            {
                foreach (System.Security.Cryptography.CryptographicAttributeObject a in attributes)
                {
                    foreach (System.Security.Cryptography.AsnEncodedData ad in a.Values)
                    {
                        string class_name = ad.GetType().FullName;

                        switch (class_name)
                        {
                            case "System.Security.Cryptography.Pkcs.Pkcs9ContentType":
                                System.Security.Cryptography.Oid ct = ((Pkcs9ContentType)ad).ContentType;
                                log.DebugFormat("      {0}({1})={2}({3})", ad.Oid.FriendlyName, ad.Oid.Value, ct.FriendlyName, ct.Value);
                                break;
                            case "System.Security.Cryptography.Pkcs.Pkcs9SigningTime":
                                log.DebugFormat("      {0}({1})={2}", ad.Oid.FriendlyName, ad.Oid.Value, ((Pkcs9SigningTime)ad).SigningTime);
                                break;
                            case "System.Security.Cryptography.Pkcs.Pkcs9MessageDigest":
                                log.DebugFormat("      {0}({1})={2}", ad.Oid.FriendlyName, ad.Oid.Value, Convert.ToBase64String(((Pkcs9MessageDigest)ad).MessageDigest));
                                break;
                            default:
                                log.DebugFormat("      {0}({1})={2}", ad.Oid.FriendlyName, ad.Oid.Value, ad.Format(multiLine: true));
                                log.DebugFormat("          attribute type:{0}", class_name);
                                break;
                        }
                    }
                }
            }
        }

        void TraceSigner(SignerInfo signer)
        {
            TraceAttributes(signer.SignedAttributes);
        }

        void TraceSigners(SignerInfoCollection signers)
        {
            foreach (SignerInfo signer in signers)
                TraceSigner(signer);
        }

        void VerifySignedCms(Auth.Settings mode, CertificateVerificationPolicy policy, SignedCms signed_cms, SignatureVerificationResult result)
        {
            log.DebugFormat("signed_cms.Detached={0}", signed_cms.Detached);
            try
            {
                signed_cms.CheckSignature(verifySignatureOnly: true);
                result.signature_checked = true;
            }
            catch (System.Security.Cryptography.CryptographicException cex)
            {
                log.Error("catched exception on CheckSignature :", cex);
                result.signature_checking_message = cex.Message;
                result.signature_checked = false;
            }

            result.verification_successful = result.signature_checked;

            result.signers = new SignerVerificationResult[signed_cms.SignerInfos.Count];
            int i = 0;
            foreach (SignerInfo signer in signed_cms.SignerInfos)
            {
                SignerVerificationResult sign_verification = new SignerVerificationResult(signer, mode, policy);
                result.signers[i] = sign_verification;
                if (!sign_verification.verification_successful)
                    result.verification_successful = false;
                i++;
            }
            TraceSigners(signed_cms.SignerInfos);
        }

        public SignatureVerificationResult VerifySignature
            (
            string auth_token
            , string base64_encoded_document
            , string base64_encoded_signature
            , CertificateVerificationPolicy policy
            )
        {
            Auth.Settings mode = Auth.Check(auth_token);

            byte[] document_bytes = Convert.FromBase64String(base64_encoded_document);
            byte[] signature_bytes = Convert.FromBase64String(base64_encoded_signature);

            ContentInfo document_content = new ContentInfo(document_bytes);
            SignedCms signed_cms = new SignedCms(document_content, detached: true);

            SignatureVerificationResult result = SignatureVerificationResult.Start(mode);

            signed_cms.Decode(signature_bytes);
            VerifySignedCms(mode,policy,signed_cms,result);

            return result;
        }

        public SignatureVerificationResult VerifySignatureFromCapicom
            (
            string auth_token
            , string document
            , string base64_encoded_signature
            , CertificateVerificationPolicy policy
            )
        {
            Auth.Settings mode = Auth.Check(auth_token);

            byte[] document_bytes = System.Text.Encoding.Unicode.GetBytes(document);
            byte[] signature_bytes = Convert.FromBase64String(base64_encoded_signature);

            ContentInfo document_content = new ContentInfo(document_bytes);
            SignedCms signed_cms = new SignedCms(document_content, detached: true);

            SignatureVerificationResult result = SignatureVerificationResult.Start(mode);

            signed_cms.Decode(signature_bytes);
            VerifySignedCms(mode,policy,signed_cms,result);

            return result;
        }

        public SignedVerificationResult VerifySignedFromCapicom
            (
            string auth_token
            , string base64_encoded_signed
            , CertificateVerificationPolicy policy
            )
        {
            Auth.Settings mode = Auth.Check(auth_token);
            byte[] signed_bytes = Convert.FromBase64String(base64_encoded_signed);

            SignedVerificationResult result = SignedVerificationResult.Start(mode);

            SignedCms signed_cms = new SignedCms();

            signed_cms.Decode(signed_bytes);
            result.signed_text = System.Text.Encoding.Unicode.GetString(signed_cms.ContentInfo.Content);
            VerifySignedCms(mode, policy, signed_cms, result);

            return result;
        }

        public string GetBase64EncodedCertificateFromCapicomSignature
            (
            string auth_token
            , string base64_encoded_signature
            , int zero_based_position_of_signer = 0
            )
        {
            Auth.Settings mode = Auth.Check(auth_token);
            byte[] signature_bytes = Convert.FromBase64String(base64_encoded_signature);

            SignedCms signed_cms = new SignedCms();
            signed_cms.Decode(signature_bytes);

            SignerInfo signer = signed_cms.SignerInfos[zero_based_position_of_signer];

            X509Certificate2 cert = signer.Certificate;
            byte[] cert_bytes = cert.GetRawCertData();

            return Convert.ToBase64String(cert_bytes);
        }

        public string Sign
            (
            string auth_token
            , string base64_encoded_document
            , string base64_encoded_certificate
            )
        {
            Auth.Settings mode = Auth.Check(auth_token);

            log.DebugFormat("base64_encoded_document={0}", base64_encoded_document);

            byte[] document_bytes = Convert.FromBase64String(base64_encoded_document);
            byte[] certificate_bytes = Convert.FromBase64String(base64_encoded_certificate);

            X509Certificate2 certificate = new X509Certificate2(certificate_bytes);

            ContentInfo document_content = new ContentInfo(document_bytes);

            SignedCms signed_cms = new SignedCms(document_content, detached: true);
            CmsSigner signer = new CmsSigner(certificate);
            signer.SignedAttributes.Add(new Pkcs9SigningTime(mode.GetCurrentTime()));
            signed_cms.ComputeSignature(signer);
            TraceSigners(signed_cms.SignerInfos);

            byte[] signature_bytes = signed_cms.Encode();

            string res = Convert.ToBase64String(signature_bytes);

            return res;
        }

        public string SignAsCapicom
            (
            string auth_token
            , string document
            , string base64_encoded_certificate
            )
        {
            Auth.Settings mode= Auth.Check(auth_token);

            byte[] document_bytes = System.Text.Encoding.UTF8.GetBytes(document);
            byte[] certificate_bytes = Convert.FromBase64String(base64_encoded_certificate);

            X509Certificate2 certificate = new X509Certificate2(certificate_bytes);

            ContentInfo document_content = new ContentInfo(document_bytes);

            SignedCms signed_cms = new SignedCms(document_content, detached: true);
            CmsSigner signer = new CmsSigner(certificate);
            signer.SignedAttributes.Add(new Pkcs9SigningTime(mode.GetCurrentTime()));
            signed_cms.ComputeSignature(signer);
            TraceSigners(signed_cms.SignerInfos);

            byte[] signature_bytes = signed_cms.Encode();

            string res = Convert.ToBase64String(signature_bytes);

            return res;
        }

        CaseBook.WebServiceClient PrepareCaseBookService(out bool test_mode)
        {
            System.Collections.Specialized.NameValueCollection app_settings = System.Configuration.ConfigurationManager.AppSettings;

            CaseBook.WebServiceClient client = new CaseBook.WebServiceClient();

            System.ServiceModel.Security.UserNamePasswordClientCredential credentials = client.ClientCredentials.UserName;
            credentials.UserName = app_settings["casebook_UserName"];// "clubau";
            credentials.Password = app_settings["casebook_Passord"];// "RxVTQJlmQO";

            test_mode = ("true" == app_settings["test_mode"]); // false;

            log.ErrorFormat("test_mode={0}", test_mode);
            log.Error("credentials.UserName=" + credentials.UserName);
            log.Error("credentials.Password=" + new String('*', credentials.Password.Length));

            return client;
        }

        void Trace(int i, CaseBook.ShortCaseInfo cinfo)
        {
            log.ErrorFormat("    {0}) CaseTypeCode={1}, CourtTag={2}, IsActive={3}", i, cinfo.CaseTypeCode, cinfo.CourtTag, cinfo.IsActive);
            if (CaseBook.CaseType.Bankrupt==cinfo.CaseTypeCode || CaseBook.CaseType.BankruptCitizen == cinfo.CaseTypeCode)
            {
                log.ErrorFormat("         RegistrationDate={0}", cinfo.RegistrationDate);
                foreach (CaseBook.CaseSide side in cinfo.Sides)
                    log.ErrorFormat("         side:{0}", side.Name);
                if (null != cinfo.Bancruptcy)
                    log.ErrorFormat("      cinfo.Bancruptcy.CitizenSnils={0}", cinfo.Bancruptcy.CitizenSnils);
            }
        }

        public Case_Details FindOutDetailsOfTheCases(CaseBook.WebServiceClient client, Case_SearchConditions cc, int current_unix_time, bool test_mode)
        {
            if (null != cc.debtor && cc.debtor.isSearchableSide())
            {
                CaseBook.SearchCaseArbitrRequest req = cc.Request(current_unix_time,test_mode);
                CaseBook.PagedResultOfShortCaseInfoNcCATIYq responce = null;
                try
                {
                    log.ErrorFormat("current_unix_time={0}", current_unix_time);
                    log.ErrorFormat("    request casebook SearchCaseArbitr ({0}-{1}, {2} from {3} to {4}).."
                        ,req.Page* req.Count, (req.Page+1) * req.Count+1 - 1
                        ,((null==req.IsActive? false : req.IsActive.Value) ? "active" : "any")
                        , null==req.StartDateFrom ? "?" : req.StartDateFrom.Value.ToShortDateString()
                        , null==req.StartDateTo ? "?" : req.StartDateTo.Value.ToShortDateString());
                    responce = client.SearchCaseArbitr(req);
                    log.Error("    .. got responce from casebook SearchCaseArbitr");
                }
                catch (Exception ex)
                {
                    log.Error("    .. can not SearchCaseArbitr!", ex);
                }
                if (null == responce)
                {
                    log.Error("  responce of SearchCaseArbitr is null");
                }
                else
                {
                    log.ErrorFormat("  responce.TotalCount={0}", responce.TotalCount);
                    int j = 0;
                    foreach (CaseBook.ShortCaseInfo cinfo in responce.Items)
                    {
                        Trace(j++, cinfo);
                        if (cc.match(cinfo))
                        {
                            Case_Details cd= new Case_Details(cc.id);
                            cd.CaseNumber = cinfo.Number;
                            if (null != cinfo.NextSession)
                                cd.NextSessionDate = cinfo.NextSession.Date.ToString("dd.MM.yyyy HH:mm");
                            return cd;
                        }
                    }
                }
            }
            return null;
        }

        public Case_Details[] FindOutDetailsOfTheCases(string auth_token, Case_SearchConditions[] cases, int current_unix_time= 0)
        {
            Auth.Check(auth_token);
            List<Case_Details> details = new List<Case_Details>();
            bool test_mode;
            CaseBook.WebServiceClient client = PrepareCaseBookService(out test_mode);
            for (int i= 0; i < cases.Length; i++)
            {
                Case_SearchConditions cc= cases[i];
                log.ErrorFormat("{0}. id={1}, debtor.Name={2}, CourtTag={3}", i,cc.id,cc.debtor.Name,cc.CourtTag);
                if (null != cc.debtor && cc.debtor.isSearchableSide())
                {
                    Case_Details cd= FindOutDetailsOfTheCases(client, cc, current_unix_time, test_mode);
                    if (null != cd)
                        details.Add(cd);
                }
            }
            return details.ToArray();
        }

        public CertificateVerificationResult VerifyCertificate(string auth_token, string base64_encoded_certificate)
        {
            Auth.Settings mode = Auth.Check(auth_token);
            byte[] certificate_bytes = Convert.FromBase64String(base64_encoded_certificate);
            X509Certificate2 certificate = new X509Certificate2(certificate_bytes);
            CertificateVerificationResult result = new CertificateVerificationResult(certificate,mode,apolicy:null);
            return result;
        }

        public byte[] DownLoadManagerCertificate(string auth_token, string MessageGUID)
        {
            Auth.Check(auth_token);
            string url = "https://bankrot.fedresurs.ru/MessageCertificate.aspx?ID=" + MessageGUID + "&attempt=1";
            log.ErrorFormat("url={0}", url);
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            System.Net.HttpStatusCode status;
            byte[] buffer = new byte[10000];
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
            using (System.IO.Stream receiveStream = response.GetResponseStream())
            using (System.IO.MemoryStream responce_stream= new System.IO.MemoryStream())
            {
                status = response.StatusCode;
                if (System.Net.HttpStatusCode.OK != status)
                    throw new Exception(string.Format("got HTTPCODE:{0}", status));
                for (int read_bytes = receiveStream.Read(buffer,0,buffer.Length);
                     0!= read_bytes;
                     read_bytes = receiveStream.Read(buffer, 0, buffer.Length))
                {
                    responce_stream.Write(buffer,0,read_bytes);
                }
                return responce_stream.GetBuffer();
            }
        }
    }
}
