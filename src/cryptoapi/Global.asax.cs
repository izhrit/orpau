﻿using System;

using log4net;

namespace rit.cryptoapi
{
    public class Global : System.Web.HttpApplication
    {
        static ILog log = LogManager.GetLogger(typeof(Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            log.Error("Application_Start");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            log.Debug("Session_Start");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            log.Debug("Application_BeginRequest");
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            log.Error("Application_AuthenticateRequest");
        }

        void Trace_sender_EventArgs(object sender, EventArgs e)
        {
            log.ErrorFormat("sender.GetType().FullName=\"{0}\"", sender.GetType().FullName);
            log.ErrorFormat("e.GetType().FullName=\"{0}\"", e.GetType().FullName);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            log.Error("Application_Error (object sender, EventArgs e) {");
            Trace_sender_EventArgs(sender,e);
            Exception ex = Server.GetLastError();
            log.Error("Server.GetLastError():");
            log.Error(ex.ToString());
            log.Error("Application_Error }");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            log.Debug("Session_End");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            log.Error("Application_End (object sender, EventArgs e) {");
            Trace_sender_EventArgs(sender, e);
            log.Error("Application_End }");
        }
    }
}