﻿namespace rit.cryptoapi
{
    public interface IService
    {
        SignatureVerificationResult VerifySignature
            (
            string auth_token
            , string base64_encoded_document
            , string base64_encoded_signature
            , CertificateVerificationPolicy policy
            );
        SignatureVerificationResult VerifySignatureFromCapicom
            (
            string auth_token
            , string document
            , string base64_encoded_signature
            , CertificateVerificationPolicy policy
            );
        string GetBase64EncodedCertificateFromCapicomSignature
            (
            string auth_token
            , string base64_encoded_signature
            , int zero_based_position_of_signer= 0
            );
        SignedVerificationResult VerifySignedFromCapicom
            (
            string auth_token
            , string base64_encoded_signed
            , CertificateVerificationPolicy policy
            );
        string Sign
            (
            string auth_token
            , string base64_encoded_document
            , string base64_encoded_certificate
            );
        string SignAsCapicom
            (
            string auth_token
            , string document
            , string base64_encoded_certificate
            );
        Case_Details[] FindOutDetailsOfTheCases
            (
            string auth_token
            , Case_SearchConditions[] cases
            , int current_unix_time= 0
            );
        CertificateVerificationResult VerifyCertificate
            (
            string auth_token
            , string base64_encoded_certificate
            );
        byte[] DownLoadManagerCertificate
            (
            string auth_token
            , string MessageGUID
            );
    }
}
