<? 

require_once '../assets/libs/alib_email_send_from_db.php';
require_once '../assets/libs/alib_email_send.php';
require_once '../assets/helpers/trace.php';

function test_email_send_dir($argv)
{
	$argv_count= count($argv);
	if ($argv_count<7)
	{
		echo "use arguments: email.send.dir server user password subject path\r\n";
		return;
	}

	global $email_settings;
	$email_settings->smtp_server= $argv[2];
	$email_settings->smtp_user= $argv[3];
	$email_settings->smtp_password= $argv[4];
	$email_settings->enabled= true;

	$subj= $argv[5];
	$dir= $argv[6];

	$letter_body= file_get_contents("$dir/letter.txt");
	$attachments= array();
	$files = new DirectoryIterator($dir);
	foreach ($files as $fileinfo)
	{
		if (!$fileinfo->isDot())
		{
			$filename= $fileinfo->getFilename();
			if ('letter.txt'!=$filename)
			{
				$attachments[]= (object)array(
					'FileName'=>$filename
					,'Content'=>file_get_contents("$dir/$filename")
				);
			}
		}
	}

	echo "server={$email_settings->smtp_server}\r\n";
	echo "user={$email_settings->smtp_user}\r\n";
	//echo "password={$email_settings->smtp_password}\r\n";
	echo "port={$email_settings->smtp_port}\r\n";
	echo "subj=$subj\r\n";
	echo "dir=$dir\r\n";
	echo "--------------------------------\r\n";
	require_once '../assets/libs/alib_email_send.php';
	$res= SendEmail((object)array(
		 'RecipientEmail'=> 'vva@russianit.ru'
		,'ExtraParams'=>(object)array(
			'Receiver'=>'viktor'
			,'Subject'=>$subj
			,'Body'=>$letter_body
		)
	), $attachments);
	print_r($res);
}

function test_emails($fixed_test_name,$argv)
{
	switch ($fixed_test_name)
	{
		case 'send.dir': test_email_send_dir($argv); break;
		default: trace("unknown test smtp command $fixed_test_name!!!");
	}
}