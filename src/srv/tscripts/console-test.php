<? 

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';

require_once '../assets/libs/poll_state/notify_status_change.php';

function test_notify()
{
	require_once '../assets/helpers/trace.php';

	global $test_trace;
	$test_trace= true;

	global $_SESSION;
	$_SESSION= array('current-test-time'=>'2020-01-01T20:20:20');

	check_poll_change_date();
}

function load_and_test_job_parts($fixed_cmd, $prefix, $argv)
{
	require_once 'test_job_parts.php';
	test_job_parts_group($fixed_cmd, $prefix, $argv);
}

function load_and_test_jobs($fixed_cmd, $prefix, $argv)
{
	require_once 'test_jobs.php';
	test_jobs_group($fixed_cmd, $prefix, $argv);
}

function load_and_test_emails($fixed_test_name,$prefix,$argv)
{
	require_once 'test_sendemails.php';
	test_emails($fixed_test_name,$argv);
}

function load_and_test_smtp($fixed_test_name,$prefix,$argv)
{
	require_once 'real_sendemails.php';
	test_emails($fixed_test_name,$argv);
}

$prefixed_test_groups= array
(
	  (object)array('prefix'=>'job_parts.', 'tests_func'=>'load_and_test_job_parts')
	, (object)array('prefix'=>'jobs.',      'tests_func'=>'load_and_test_jobs')
	, (object)array('prefix'=>'email.',     'tests_func'=>'load_and_test_emails')
	, (object)array('prefix'=>'smtp.',      'tests_func'=>'load_and_test_smtp')
);

function try_to_use_test_groups($cmd,$argv,$prefixed_test_groups)
{
	foreach ($prefixed_test_groups as $prefixed_test_group)
	{
		$prefix= $prefixed_test_group->prefix;
		if (0===strpos($cmd,$prefix))
		{
			$fixed_test_name= substr($cmd,strlen($prefix));
			$tests_func= $prefixed_test_group->tests_func;
			$tests_func($fixed_test_name,$prefix,$argv);
			return true;
		}
	}
	return false;
}

function Process_command_line_arguments($prefixed_test_groups)
{
	global $argv;
	$cmd= $argv[1];
	if (!try_to_use_test_groups($cmd,$argv,$prefixed_test_groups))
	{
		switch ($cmd)
		{
			case 'notify': test_notify(); break;
			default: echo "unknown test command |$cmd|\r\n";
		}
	}
}

global $current_date_time;
$current_date_time= date_create('2020-05-08');

try
{
	Process_command_line_arguments($prefixed_test_groups);
}
catch (Exception $ex)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	echo 'catched Exception:';
	print_r($ex);
}
