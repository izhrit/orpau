<? 

require_once '../assets/libs/alib_email_send_from_db.php';

function trace_txt($txt)
{
	//echo date('m/d/Y h:i:s a: ', time());
	echo $txt;
	echo "\r\n";
	return $txt;
}

function trace($data)
{
	return trace_txt(is_string($data) ? $data : print_r($data,true));
}

global $test_SendEmail_ok;
$test_SendEmail_ok= true;

function SendEmail($row_email_to_send, $rows_attachment)
{
	global $test_SendEmail_ok;
	if ($test_SendEmail_ok)
	{
		trace("send email to {$row_email_to_send->RecipientEmail}");
		return 'ok';
	}
	else
	{
		trace("fail email to {$row_email_to_send->RecipientEmail}");
		throw new Exception('test send email exception!');
	}
}

function test_email_send($argv)
{
	global $email_settings;
	$email_settings->smtp_server= $argv[2];
	$email_settings->smtp_user= $argv[3];

	trace('email.send {');
	SendPortionOfEmailMessages(100);
	trace('email.send }');
}

function test_email_fail($argv)
{
	global $email_settings;
	$email_settings->smtp_server= $argv[2];
	$email_settings->smtp_user= $argv[3];

	global $test_SendEmail_ok;
	$test_SendEmail_ok= false;

	trace('email.send {');
	SendPortionOfEmailMessages(100);
	trace('email.send }');
}

function test_emails($fixed_test_name,$argv)
{
	switch ($fixed_test_name)
	{
		case 'send': test_email_send($argv); break;
		case 'fail': test_email_fail($argv); break;
		default: trace("unknown test email command $fixed_test_name!!!");
	}
}