<? 

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';

function test_job_parts($job_parts)
{
	foreach ($job_parts as $p)
	{
		echo $p['title']."\r\n";
		$exec= $p['exec'];
		if (!is_callable($exec))
			echo "exec is not callable!!!!!!! \r\n";
	}
}

function test_minutely_job_parts()
{
	require_once '../assets/libs/job_parts/minutely_job_parts.php';
	test_job_parts($minutely_job_parts);
}

function test_hourly_job_parts()
{
	require_once '../assets/libs/job_parts/hourly_job_parts.php';
	test_job_parts($hourly_job_parts);
}

function test_nightly_job_parts()
{
	require_once '../assets/libs/job_parts/nightly_job_parts.php';
	test_job_parts($nightly_job_parts);
}

function test_job_parts_group($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'minutely': test_minutely_job_parts(); break;
		case 'hourly': test_hourly_job_parts(); break;
		case 'nightly': test_nightly_job_parts(); break;
		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}