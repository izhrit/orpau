<? 

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

require_once '../assets/libs/cryptoapi.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

global $base_url, $method, $document_path, $certificate_path, $result_path, $auth, $base64_signature_path, $signature_path, $current_time;
$current_time= null;
$signature_path= null;
$base64_signature_path= null;

function get_name_value($arg)
{
	$pos= strpos($arg,'=');
	return (object)array('name_part'=>substr($arg,0,$pos),'value'=>substr($arg,$pos+1));
}

function ProcessArguments()
{
	global $argv, $base_url, $method, $document_path, $certificate_path, $result_path, $auth, $signature_path, $base64_signature_path, $current_time;
	$count= count($argv);
	for ($i= 1; $i<$count; $i++)
	{
		$nv= get_name_value($argv[$i]);
		switch ($nv->name_part)
		{
			case '--base-url': $base_url= str_replace(' ','%20',$nv->value); break;
			case '--method': $method= $nv->value; break;
			case '--auth': $auth= $nv->value; break;

			case '--document-path': $document_path= $nv->value; break;
			case '--certificate-path': $certificate_path= $nv->value; break;
			case '--signature-path': $signature_path= $nv->value; break;
			case '--base64-signature-path': $base64_signature_path= $nv->value; break;
			case '--result-path': $result_path= $nv->value; break;
			case '--current-time': $current_time= $nv->value; break;
		}
	}
}

function prepare_CryptoAPI()
{
	global $test_cryptoapi_url, $auth;
	return new CryptoAPI((object)array('base_url'=>$test_cryptoapi_url,'auth'=>$auth));
}

function test_SignAsCapicom()
{
	global $base_url, $document_path, $certificate_path, $result_path, $auth;

	$document_bytes= file_get_contents($document_path);

	$certificate_bytes= file_get_contents($certificate_path);

	$cryptoapi= prepare_CryptoAPI();

	$signature_bytes= $cryptoapi->SignAsCapicom((object)array(
		'document'=>$document_bytes
		,'certificate_bytes'=>$certificate_bytes
	));

	file_put_contents($result_path,$signature_bytes);
}

function test_VerifyCertificate()
{
	global $base_url, $document_path, $auth;

	if (!file_exists($document_path))
	{
		echo "can not find \"$document_path\"!\r\n";
		exit();
	}

	$cryptoapi= prepare_CryptoAPI();
	$certificate_bytes= file_get_contents($document_path);

	$result= $cryptoapi->VerifyCertificate((object)array(
		'certificate_bytes'=>$certificate_bytes
	));

	echo nice_json_encode($result);
}

function test_VerifySignature()
{
	echo "test_VerifySignature!\r\n";
	global $base_url, $document_path, $signature_path, $result_path, $auth, $base64_signature_path;

	if (!file_exists($document_path))
	{
		echo "can not find \"$document_path\"!\r\n";
		exit();
	}
	$cryptoapi= prepare_CryptoAPI();
	$document_bytes= file_get_contents($document_path);
	if (null!=$signature_path)
	{
		if (!file_exists($signature_path))
		{
			echo "can not find \"$signature_path\"!\r\n";
			exit();
		}
	
		$signature_bytes= file_get_contents($signature_path);

		$result= $cryptoapi->VerifySignature((object)array(
			'document_bytes'=>$document_bytes
			,'signature_bytes'=>$signature_bytes
		));
	}
	echo "0\r\n";
	if (null!=$base64_signature_path)
	{
		echo "1\r\n";
		if (!file_exists($base64_signature_path))
		{
			echo "can not find \"$base64_signature_path\"!\r\n";
			exit();
		}
	
		$signature_bytes= file_get_contents($base64_signature_path);

		$result= $cryptoapi->VerifySignature((object)array(
			'document_bytes'=>$document_bytes
			,'base64_encoded_signature'=>$signature_bytes
		));
	}

	file_put_contents($result_path,nice_json_encode($result));
}

function test_GetCertificate()
{
	global $base_url, $document_path, $auth, $result_path;

	if (!file_exists($document_path))
	{
		echo "can not find \"$document_path\"!\r\n";
		exit();
	}
	$signed_document= file_get_contents($document_path);

	$cryptoapi= prepare_CryptoAPI();

	$result= $cryptoapi->GetBase64EncodedCertificateFromCapicomSignature((object)array(
		'signed'=>$signed_document
	));

	file_put_contents($result_path,base64_decode($result));
}

function test_VerifySignedFromCapicom()
{
	global $base_url, $document_path, $auth;

	if (!file_exists($document_path))
	{
		echo "can not find \"$document_path\"!\r\n";
		exit();
	}
	$signed_document= file_get_contents($document_path);
	$signed_bytes= base64_decode($signed_document);

	$cryptoapi= prepare_CryptoAPI();

	$result= $cryptoapi->VerifySignedFromCapicom((object)array(
		'signed'=>$signed_bytes
	));

	echo nice_json_encode($result);
}

function test_FindOutDetailsOfTheCases()
{
	global $base_url, $document_path, $signature_path, $result_path, $auth, $current_time;
	if (!file_exists($document_path))
		throw new Exception("can not find \"$document_path\"!");
	$cases_txt= file_get_contents($document_path);
	$cases= json_decode($cases_txt);
	if (null==$cases)
		throw new Exception("can not parse file from \"$document_path\" :\r\n$cases_txt");
	$cryptoapi= prepare_CryptoAPI();
	$current_unix_time= 0;
	if (null!=$current_time)
	{
		$dt= date_create_from_format('Y-m-d\TH:i:s',$current_time);
		if (false!==$dt)
			$current_unix_time= $dt->getTimestamp();// 1594379673;
	}
	$result= $cryptoapi->FindOutDetailsOfTheCases($cases, $current_unix_time);
	echo nice_json_encode($result);
}

function DoRequest()
{
	global $method;
	switch ($method)
	{
		case 'SignAsCapicom': test_SignAsCapicom(); break;
		case 'VerifySignature': test_VerifySignature(); break;
		case 'VerifySignedFromCapicom': test_VerifySignedFromCapicom(); break;
		case 'GetCertificate': test_GetCertificate(); break;
		case 'FindOutDetailsOfTheCases': test_FindOutDetailsOfTheCases(); break;
		case 'VerifyCertificate': test_VerifyCertificate(); break;
	}
}

ProcessArguments();

try
{
	DoRequest();
}
catch (Rest_exception $rex)
{
	echo 'Unhandled rest exception: ' . get_class($rex) . ' - ' . $rex->getMessage();
	echo "\r\nresponce:----\r\n".$rex->responce."\r\n-------------\r\n";
	throw $rex;
}
