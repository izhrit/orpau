<? 

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once '../assets/helpers/log.php';

function test_sync_efrsb_debtors($argv)
{
	require_once '../assets/libs/efrsb/sync_efrsb_debtor.php';
	$portion_size= $argv[2];
	$max_to_insert= $argv[3];
	sync_debtors($portion_size,$max_to_insert);
}

function test_sync_efrsb_managers($argv)
{
	require_once '../assets/libs/efrsb/sync_efrsb_manager.php';
	$portion_size= $argv[2];
	$max_to_insert= $argv[3];
	sync_managers($portion_size,$max_to_insert);
}

function test_sync_efrsb_debtor_managers($argv)
{
	require_once '../assets/libs/efrsb/sync_efrsb_debtor_manager.php';
	$portion_size= $argv[2];
	$max_to_insert= $argv[3];
	sync_debtor_managers($portion_size,$max_to_insert);
}

function test_sync_efrsb_sros($argv)
{
	require_once '../assets/libs/efrsb/sync_efrsb_sro.php';
	$portion_size= $argv[2];
	$max_to_insert= $argv[3];
	sync_sros($portion_size,$max_to_insert);
}

function load_and_test_sync_efrsb($fixed_cmd, $prefix, $argv)
{
	switch ($fixed_cmd)
	{
		case 'sros': test_sync_efrsb_sros($argv); break;
		case 'managers': test_sync_efrsb_managers($argv); break;
		case 'debtors': test_sync_efrsb_debtors($argv); break;
		case 'debtor_managers': test_sync_efrsb_debtor_managers($argv); break;
		default: echo "unknown test command $prefix$fixed_cmd\r\n";
	}
}

global $prefixed_jobs_test_groups;
$prefixed_jobs_test_groups= array
(
	  (object)array('prefix'=>'sync.efrsb.', 'tests_func'=>'load_and_test_sync_efrsb')
);

function test_jobs_group($fixed_cmd, $prefix, $argv)
{
	global $prefixed_jobs_test_groups;
	if (!try_to_use_test_groups($fixed_cmd,$argv,$prefixed_jobs_test_groups))
	{
		switch ($fixed_cmd)
		{
			default: echo "unknown test command $prefix$fixed_cmd\r\n";
		}
	}
}