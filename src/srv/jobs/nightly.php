<?php

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once '../assets/helpers/job.php';

global $job_params;

if (!isset($job_params))
{
	echo '$job_params is undefined!';
	exit();
}

if (!isset($job_params->pid_file_path_for_nightly_job))
{
	echo '$job_params->pid_file_path_for_nightly_job is undefined!';
	exit();
}

require_once '../assets/libs/job_parts/nightly_job_parts.php';

$nightly_job_pid_file_path= $job_params->pid_file_path_for_nightly_job;

safe_execute_job_parts_locked_by_pid_file($nightly_job_parts,$nightly_job_pid_file_path);