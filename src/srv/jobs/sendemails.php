<?

require_once '../assets/config.php';
require_once '../assets/helpers/trace.php';
require_once '../assets/libs/alib_email_send.php';
require_once '../assets/libs/alib_email_send_from_db.php';

function Job_send_emails()
{
	global $job_params;

	if (file_exists($job_params->pid_file_path_for_send_email_job))
	{
		write_to_log('job send emails pid file exists!');
		exit();
	}

	file_put_contents($job_params->pid_file_path_for_send_email_job,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		SendPortionOfEmailMessages(100);
	}
	catch (Exception $ex) 
	{
		unlink($job_params->pid_file_path_for_send_email_job);
		throw $ex;
	}
	unlink($job_params->pid_file_path_for_send_email_job);
}

try
{
	Job_send_emails();
}
catch (Exception $ex)
{
	$ex_class= get_class($ex);
	$ex_Message= $ex->getMessage();
	write_to_log("Unhandled exception in job send emails occurred: $ex_class - $ex_Message");
	throw $ex;
}


