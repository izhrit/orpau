<?php

require_once '../assets/config.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$possible_file_actions= array
(
	 'confirmation.crud' => 'confirmation/confirmation_crud'
	,'confirmation.generate' => 'confirmation/confirmation_generator'
	,'confirmation.jqgrid' => 'confirmation/confirmation_jqgrid'
	,'confirmation.printable' => 'confirmation/confirmation_printable'

	,'debtor.crud' => 'debtor/debtor_crud'
	,'debtor.jqgrid' => 'debtor/debtor_jqgrid'

	,'manager.auth' => 'manager/manager_auth'
	,'manager.authPassword' => 'manager/manager_auth_password'
	,'manager.crud' => 'manager/manager_crud'
	,'manager.jqgrid' => 'manager/manager_jqgrid'
	,'manager.password' => 'manager/manager_password'
	,'manager.select2' => 'manager/manager_select2'
	,'manager.for-inn' => 'manager/manager_for_inn'

	,'news.jqgrid'=> 'events_news/news_jqgrid'
	,'schedule.events'=> 'events_news/schedule_info'

	,'procedure.crud' => 'procedure_crud'

	,'mark.create' => 'mark_creator'

	,'award.managers' => 'award/award_managers'
	,'award.managers_server' => 'award/award_managers'
	,'award.manager' => 'award/award_manager'
	,'award.polls' => 'award/award_polls'
	,'award.poll' => 'award/award_poll'
	,'award.vote' => 'award/award_vote'
	,'award.votes' => 'award/award_votes'
	,'award.jqgrid' => 'award/award_votes_jqgrid'
	,'award.login' => 'award/award_login'

	,'admin.auth' => 'admin/admin_auth'
	,'region.select2' => 'admin/region_select2'
	,'sro.select2' => 'admin/sro_select2'

	,'poll.jqgrid' => 'poll/poll_jqgrid'
	,'poll.crud' => 'poll/poll_crud'
	,'poll.select2' => 'poll/poll_select2'
	,'poll.result' => 'poll/poll_results'
	,'poll.last-for-manager' => 'poll/poll_last_for_manager'
	,'poll.status' => 'poll/poll_status'

	,'group.jqgrid' => 'group/group_jqgrid'
	,'group.select2' => 'group/group_select2'
	,'group.crud' => 'group/group_crud'

	,'vote.crud' => 'voting/vote_crud'
	,'vote.sms' => 'voting/vote_sms'
	,'vote.cert' => 'voting/vote_cert'

	,'questions.jqgrid' => 'voting/question_jqgrid'
	,'question.crud' => 'voting/question_crud'

	,'documents.jqgrid' => 'voting/document_jqgrid'
	,'document.crud' => 'voting/document_crud'
	,'document.download' => 'voting/document_download'

	,'log.jqgrid' => 'voting/log_jqgrid'
	,'test-time'=> 'test_time'

	,'club.agreement' => 'voting/club_agreement'
	,'asp.agreement' => 'voting/asp_accession'
	,'superadmin.manager' => 'superadmin/manager_info'

	,'monitoring.jqgrid' => 'monitoring/manager_monitoring_jqgrid'
);

$action= null;
if (isset($_GET['action']))
{
	$action= $_GET['action'];
}
else
{
	$path= null;
	if (isset($_SERVER['ORIG_PATH_INFO']))
	{
		$path= $_SERVER['ORIG_PATH_INFO'];
	}
	else if (isset($_SERVER['PHP_SELF']))
	{
		$path= $_SERVER['PHP_SELF'];
	}
	// example: "/ui-backend.php/9999999999/9999999999/mark.png""
	if (null!=$path)
	{
		$parts= array_reverse(explode('/',$path));
		if (count($parts)>=3 && 'mark.png'==$parts[0])
		{
			global $_GET;
			$action= 'mark.create';
			$_GET= array('confirmation_number'=>$parts[1],'qr_reference'=>$parts[2]);
		}
	}
}

if (null==$action)
{
	echo(0);
}
else
{
	global $trace_methods;
	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		require_once '../assets/helpers/log.php';
		require_once '../assets/helpers/validate.php';
		exit_bad_request('unknown action!');
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require '../assets/actions/backend/'.$subpath.'.php';
		}
		catch (Exception $exception)
		{
			require_once '../assets/helpers/validate.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			exit_internal_server_error('Unhandled exception!');
		}
	}
}
