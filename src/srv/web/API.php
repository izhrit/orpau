<?php

require_once '../assets/config.php';
require_once '../assets/helpers/log.php';

write_to_log('in API.php');
write_to_log($_GET);

$possible_file_actions= array
(
	  'confirmation.API' => 'confirmation_API'
);

if (!isset($_GET['action']))
{
	echo(0);
}
else
{
	$action= $_GET['action'];

	global $trace_methods;
	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		require_once '../assets/helpers/log.php';
		require_once '../assets/helpers/validate.php';
		exit_bad_request('unknown action!');
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require '../assets/actions/API/'.$subpath.'.php';
		}
		catch (Exception $exception)
		{
			require_once '../assets/helpers/validate.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			exit_internal_server_error('Unhandled exception!');
		}
	}
}