<?php 
require_once '../assets/config.php';
global $test_settings;
if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time && isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	session_start();
	safe_store_test_time();
}
if (isset($_COOKIE[session_name()]) && !isset($_GET['start']))
{
	global $_SESSION, $auth_info;
	if(!isset($_SESSION))
		session_start();
	if (isset($_SESSION['auth_info']))
		$auth_info= $_SESSION['auth_info'];
}
global $auth_info;
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="language" content="ru" />

		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<script type="text/javascript" src="js/vendors/capicom.js"></script>
		<script type="text/javascript" src="js/vendors/qrcode.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>

		<script type="text/javascript" src="js/vendors/polyfill.js"></script>
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/main.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/tooltip.css" />
		<script type="text/javascript" src="js/vendors/fullcalendar/popper.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/tooltip.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/main.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/locales/ru.js"></script>
		<!-- вот это надо в extension ы! } -->

		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/orpau.css" />

		<script type="text/javascript">
		/*<![CDATA[*/

<? global $ama_datamart_settings, $start_real_orpau, $url_offer_asp; ?>
app= {
	datamart_url:'<?= $ama_datamart_settings->use_base_url ?>for-orpau.php'
	,url_offer_asp: '<?= $url_offer_asp ?>'
	,start_real_orpau: <?= (isset($start_real_orpau) && $start_real_orpau) ? 'true' : 'false' ?>
};

window.onerror = function (message, source, lineno, columnNo, error)
{
	var msg = "Ошибка: <b>" + message + "</b><br/><br/>\n" +
		"файл: <b>" + source + "</b><br/>\n" +
		"строка: <b>" + lineno + '</b>';
	msg += "<br/>\nпозиция: <b>" + columnNo + '</b>';
	msg += "<br/>\nstack:<br/>\n<small>" + error.stack.replace(/\n/ig,'\n<br/>') + '</small>';
	var div = $("#cpw-unhandled-error-message-form");
	div.html(msg);
	div.dialog({
		modal: true,
		width:800,height:"auto",
		buttons: { Ok: function () { $(this).dialog("close"); } }
	});
};
function cpw_log_time()
{
	var t= new Date();
	var m= t.getMonth()+1;
	var d= t.getDate();
	if (d<10)
		d= '0' + d;
	if (m<10)
		m= '0' + m;
	return t.getFullYear()+'-'+m+'-'+d+' '+t.toLocaleTimeString();
}
function cpw_js_log(txt)
{
	var ilog_sel= 'body.cpw-orpau > div.cpw-orpau-ui-content small.log';
	$(ilog_sel).append(cpw_log_time() + '<div style="text-align:right">' + txt + '</div><br/>');
}
function RegisterCpwFormsExtension(extension)
{
	cpw_js_log('вызван RegisterCpwFormsExtension для extension.key="' + extension.key + '"');
	if ('orpau'==extension.key)
	{
		var base_url= '<?= isset($orpau_bck_url) ? $orpau_bck_url : 'orpau_bck_url.undefined.in.config.php' ?>';
		var sel= 'body.cpw-orpau > div.cpw-orpau-ui-content';
		var form_spec = extension.forms.vote.CreateController({base_url:base_url});
		cpw_js_log('создан controller');
		form_spec.CreateNew(sel);
		<? if (isset($auth_info->Login)) : ?>
			<? $login = $auth_info->Login; ?>
			<? if (null!=$login) : ?>
				form_spec.AutoLogin('<?= $login ?>');
			<? endif; ?>
		<? endif; ?>
	}
}
/*]]>*/
		</script>

		<script type="text/javascript" src="js/cryptoapi.js"></script>
		<script type="text/javascript" src="js/orpau.js?20210515"></script>

		<title>Единая информационная система арбитражных управляющих</title>
		<!--[if lt IE 7]><script type="text/javascript">window.IEVersion=6;</script><![endif]-->
		<!--[if IE 7]><script type="text/javascript">window.IEVersion=7;</script><![endif]-->
		<!--[if IE 8]><script type="text/javascript">window.IEVersion=8;</script><![endif]-->
		<!--[if IE 9]><script type="text/javascript">window.IEVersion=9;</script><![endif]-->
	</head>

	<body class="cpw-orpau cpw-ama" style="margin: 0px;">
		<div id="cpw-unhandled-error-message-form" title="Необработанная ошибка!"></div>
		<div class="cpw-orpau-ui-content">

			<div class="ui-placeholder">
				<div>
					
					<small>
						
						<?= date_format(date_create(),'Y-m-d H:i:s') ?><br/>
						<div style="text-align:right">формируется html-страница на сервере.</div><br/>
						Здесь на html-странице должна отобразиться<br/>
					</small>
					<p style="text-align:center;">

						Форма сайта ОРПАУ

					</p>
					<small class="log">
						После загрузки и выполнения javascript.
						<br/><br/>
						<script>document.write(cpw_log_time());</script>
						<div style="text-align:right">выполняется javascript в браузере.</div><br/>
					</small>
					<center><img src="img/loading-spinner.gif" /></center>

				</div>
			</div>

		</div>

		<div class="orpau-footer">
			При возникновении вопросов, замечаний или проблем в работе с Единой ИС Арбитражных управляющих, 
			обращайтесь в <a target="_blank" href="https://russianit.ru/support/">службу поддержки</a>.
		</div>
	</body>

</html>
