app.cryptoapi = {

	ChooseCertificateAnd: function (do_with_certificate)
	{
		var cert = $.capicom.getDialogBoxCertificates();
		do_with_certificate(cert);
	}

	, SignBase64: function (rawData, detached, cert)
	{
		return $.capicom.signBase64(rawData, detached, cert.thumbprint);
	}

	, CertificateByThumbprint: function (tmb)
	{
		var store = new ActiveXObject("CAPICOM.Store");
		// ��������� ��������� ������������ ������������
		store.Open($.capicom.CAPICOM_CURRENT_USER_STORE, "My", $.capicom.CAPICOM_STORE_OPEN_READ_ONLY);
		// ����� ������������, ��� ������� ������������� ��������� ��������� (thumbprint), � ��������� ������������
		var filteredCertificates = store.Certificates.Find($.capicom.CAPICOM_CERTIFICATE_FIND_SHA1_HASH, tmb);
		// ������������� ������� CAPICOM.Signer: ��� �������� ����� �������
		return filteredCertificates.Item(1);
	}
};
