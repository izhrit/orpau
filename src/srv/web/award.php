<?php
require_once '../Assets/actions/Backend/award/managers.php';
require_once '../Assets/helpers/json.php';

$manager_regnum = !isset($_GET["manager_regnum"]) ? null : $_GET["manager_regnum"];
$managers = !isset($_GET["license_id"]) ? array() : GetManagers($_GET["license_id"], $manager_regnum);

?>

<html>
	<head>
		<title>Премия арбитражный управляющий 2020</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/award.png">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/award.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>
		<script type="text/javascript" src="js/vendors/capicom.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>
		<!-- вот это надо в extension ы! } -->

		<script type="text/javascript">
		app= { };
		function RegisterCpwFormsExtension(extension)
		{
			if ('orpau'==extension.key)
			{
				var base_url= '<?= $orpau_bck_url ?>';
				var sel= '.cpw-orpau ';
				var form_spec = extension.forms.orpau.CreateController({base_url:base_url});
				var managers=<?= nice_json_encode($managers) ?>;
				form_spec.SetFormContent(managers);
				form_spec.CreateNew(sel);
			}
		}
		window.onerror = function (message, source, lineno)
		{
			alert("Ошибка:" + message + "\n" +
					"файл:" + source + "\n" +
					"строка:" + lineno);
		}

        window.use_activex = true;//'<= $use_activex ?>';
        window.use_validate_certificate = false;//'<= $use_validate_certificate ?>';
		</script>

		<script type="text/javascript" src="built/award.js?<?= time() ?>"></script>
	</head>
	<body class="cpw-orpau">
		Здесь должна быть форма голосования
	</body>
</html>