<?php

require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/config.php';

class Debtor_crud extends Base_crud
{
	function read($id_Debtor)
	{
		$txt_query = 'select 
				d.id_Debtor, d.BankruptId, d.ArbitrManagerID
				,d.Name, d.INN, d.OGRN, d.SNILS
				,d.LegalAdress, d.LegalCaseList

				,m.id_Manager
				,m.FirstName, m.LastName, m.MiddleName
				,m.RegNum, m.INN m_INN, m.SRORegNum
				,m.CorrespondenceAddress

				,c.id_Confirmation, c.TimeOfCreation, c.Text
			from debtor d
			left join manager m on d.ArbitrManagerID=m.ArbitrManagerID
			left join confirmation c on c.id_Debtor=d.id_Debtor and c.id_Manager=m.id_Manager
			where ? = d.id_Debtor
			order by c.TimeOfCreation desc
			limit 1
			;';
		$rows= execute_query($txt_query, array('s', $id_Debtor));
		$count_rows= count($rows);
		if (1!=$count_rows)
			exit_not_found("got $count_rows debtors for id_Debtor=$id_Debtor");

		$row= $rows[0];
		$debtor= array(
			'id_Debtor'=>$row->id_Debtor ,'BankruptId'=>$row->BankruptId ,'ArbitrManagerID'=>$row->ArbitrManagerID
			,'Name'=>$row->Name, 'INN'=>$row->INN, 'OGRN'=>$row->OGRN, 'SNILS'=>$row->SNILS
			,'LegalAdress'=>$row->LegalAdress, 'LegalCaseList'=>(null==$row->LegalCaseList ? null : json_decode($row->LegalCaseList))
			,'Manager'=> array(
				'id_Manager'=>$row->id_Manager
				,'FirstName'=>$row->FirstName, 'LastName'=>$row->LastName, 'MiddleName'=>$row->MiddleName
				,'RegNum'=>$row->RegNum, 'INN'=>$row->m_INN, 'SRORegNum'=>$row->SRORegNum
				,'CorrespondenceAddress'=>$row->CorrespondenceAddress
			)
			,'Confirmation'=>array(
				'id_Confirmation'=>$row->id_Confirmation
				,'TimeOfCreation'=>$row->TimeOfCreation
				,'Text'=>$row->Text
			)
		);

		echo nice_json_encode($debtor);
	}
}

$crud = new Debtor_crud();
$crud->process_cmd();