<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

$id = $_GET['id'];
$status = null;
$txt_query="select Status ,uncompress(p.ExtraColumns) ExtraColumns from poll p where id_Poll=?";
$rows = execute_query($txt_query, array('s', $id));
if (1==count($rows))
{
	$status = $rows[0]->Status;
}
else
{
	write_to_log("wrong id_Poll \"$id\"");
	echo 'null';
}

function UpdatePollRow($id, $status)
{
	$current_time= date_format(safe_date_create(),'Y-m-d\TH:i:s');
	$txt_query="update poll set Status=?, TimeChange=? where id_Poll=?";
	$affected_rows = execute_query_get_affected_rows($txt_query, array('sss', $status, $current_time, $id));
	if (1==$affected_rows)
	{
		echo '{ "ok": true }';
	}
	else if (0==$affected_rows)
	{
		echo '{ "ok": true, "affected": 0 }';
	}
	else
	{
		exit_bad_request("affected $affected_rows when update id_Poll=$id");
	}
}

$cmd = $_GET['cmd'];

switch ($cmd)
{
	case 'start': UpdatePollRow($id, 'p'==$status ? 'u' : 'v'); break;
	case 'pause': UpdatePollRow($id, 'p'); break;
	case 'stop': UpdatePollRow($id, 'c'); break;
};