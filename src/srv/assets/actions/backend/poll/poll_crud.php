<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

class Fill_poll_row_query_builder extends Fill_row_query_builder
{
	public function comma_separated_params($poll)
	{
		$date= date_create_from_format('d.m.Y H:i',$poll->Завершение['Дата'].' '.$poll->Завершение['Время']);
		$date_sql= date_format($date,'Y-m-d\TH:i:s');

		$this->comma_par('Name',$poll->Название);
		$this->comma_par('DateFinish',$date_sql);

		$encodePoll = json_encode($poll);
		$this->parametrized("\r\n, ExtraColumns=compress(?)",$encodePoll);

		$from_type = $poll->Голосуют;
		if ('все'==$from_type)
		{
			$this->comma_par('id_SRO', null);
			$this->comma_par('id_Region', null);
			$this->comma_par('id_Manager_group', null);
		}
		else
		{
			$from = $poll->Из;
			switch($from_type)
			{
				case 'сро':    $this->comma_par('id_SRO', $from['СРО']['id']); break;
				case 'регион': $this->comma_par('id_Region', $from['региона']['id']); break;
				case 'группа': $this->comma_par('id_Manager_group', $from['группы']['id']); break;
			}
		}
	}
}

class Poll_crud extends Base_crud
{
	function create($poll)
	{
		$qb= new Fill_poll_row_query_builder('insert into poll set ');
		$qb->comma_separated_params((object)$poll);
		$qb->parametrized("\r\n, Status=?","n");
		$id_Poll= $qb->execute_query_get_last_insert_id();

		$this->createQuestions($poll, $id_Poll);

		echo nice_json_encode(array("ok"=>true ,"id_Poll"=>$id_Poll));
	}

	function createQuestions($poll, $id_Poll)
	{
		foreach ($poll['Вопросы'] as $question_row)
		{
			$qb = new Fill_row_query_builder('insert into question set ');
			$qb->comma_par('id_Poll', $id_Poll);
			$qb->comma_par('Title', $question_row['В_повестке']);
			$encodeExtra = json_encode($question_row['На_голосование']);
			$qb->parametrized("\r\n, Extra=compress(?)",$encodeExtra);
			$id_Question= $qb->execute_query_get_last_insert_id();
		}
	}

	function read($id_Poll)
	{
		global $poll_status_description;

		$txt_query= 'select 
			p.id_Poll
			,p.Name Название
			,uncompress(p.ExtraColumns) ExtraColumns
			,p.DateFinish DateFinish
			,p.id_SRO
			,p.id_Region
			,p.id_Manager_group
			,p.Status
		from poll p
		where p.id_Poll=?;';
		$rows = execute_query($txt_query, array('s', $id_Poll));

		if (0==count($rows))
		{
			exit_not_found("can not find poll id_Poll=$id_Poll");
		}
		else
		{
			$row = $rows[0];
			$extra_columns= json_decode($row->ExtraColumns);
			$date_finish= date_create_from_format('Y-m-d H:i:s',$row->DateFinish);
			$questions = property_exists($extra_columns, 'Вопросы') ? $extra_columns->Вопросы : null;
			$d = FindDescription($poll_status_description, 'code', empty($row->Status) ? 'n' : $row->Status);

			$poll= array(
				'id_Poll'=>$row->id_Poll
				,'Название'=>$row->Название
				,'Вопросы'=>$questions
				,'Завершение'=>array(
					'Дата'=>date_format($date_finish,'d.m.Y')
					,'Время'=>date_format($date_finish,'H:i')
				)
				,'Голосуют'=>$extra_columns->Голосуют
				,'Состояние'=>$d['descr']
			);

			if ('все'!=$extra_columns->Голосуют)
				$poll['Из']= $extra_columns->Из;

			echo nice_json_encode($poll);
		}
	}

	function update($id_Poll,$poll)
	{
		$qb= new Fill_poll_row_query_builder('update poll set ');
		$qb->comma_separated_params((object)$poll);
		if(empty($poll['Состояние'])){
			$qb->parametrized("\r\n, Status=?","n");
		}
		$qb->parametrized("\r\nwhere id_Poll=?",$id_Poll);
		$affected_rows= $qb->execute_query_get_affected_rows();

		$qb= new Fill_row_query_builder('delete from question where ');
		$qb->parametrized(' id_Poll=?',$id_Poll,'i');
		$qb->execute_query_no_result();

		$this->createQuestions($poll, $id_Poll);

		if (1==$affected_rows)
		{
			echo '{ "ok": true }';
		}
		else if (0==$affected_rows)
		{
			echo '{ "ok": true, "affected": 0 }';
		}
		else
		{
			exit_bad_request("affected $affected_rows when update id_Poll=$id_Poll");
		}
	}

	function delete($id_Polls)
	{
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		foreach ($id_Polls as $id_Poll)
		{
			$qb= new Fill_row_query_builder('delete from poll where');
			$qb->parametrized(' id_Poll=?',$id_Poll,'i');
			$affected= $qb->execute_query_get_affected_rows($connection);
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not delete poll where id_Poll=$id_Poll (affected $affected rows)");
			}
		}
		$connection->commit();
		echo '{ "ok": true }';
	}
}

$crud= new Poll_crud();
$crud->process_cmd();