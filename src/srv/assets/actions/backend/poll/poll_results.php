<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

function Read_results($id) {
	global $poll_status_description;

	$txt_query= 'select 
		p.id_Poll
		,p.Name Название
		,uncompress(p.ExtraColumns) ExtraColumns
		,p.DateFinish DateFinish
		,p.Status
	from poll p
	where id_Poll=?';
	$poll_rows = execute_query($txt_query,array('s', $id));

	if (0==count($poll_rows))
	{
		exit_not_found("can not find poll id_Poll=$id");
	}
	else
	{
		$poll_row = $poll_rows[0];
		$extra_columns = json_decode($poll_row->ExtraColumns);
		$date_finish= date_create_from_format('Y-m-d H:i:s',$poll_row->DateFinish);
		$d = FindDescription($poll_status_description, 'code', $poll_row->Status);

		$poll= array(
			'id_Poll'=>$poll_row->id_Poll
			,'Название'=>$poll_row->Название
			,'Завершение'=>array(
				'Дата'=>date_format($date_finish,'d.m.Y')
				,'Время'=>date_format($date_finish,'H:i')
			)
			,'Голосуют'=>$extra_columns->Голосуют
			,'Состояние'=>$d['descr']
			,'Результаты'=>isset($extra_columns->Результаты) ? $extra_columns->Результаты : null
		);

		if ('все'!=$extra_columns->Голосуют)
			$poll['Из']= $extra_columns->Из;

		$txt_query= 'select
		    id_Question Id
			,Title
			,uncompress(Extra) QuestionExtra
		from question
		where id_Poll=?';
		$question_rows = execute_query($txt_query,array('s', $id));

		if(0!=count($question_rows))
		{
			foreach ($question_rows as $question)
			{
				$txt_query= "select
					v.id_Vote
					,v.VoteData
					,v.VoteTime
					, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) FullName
				from vote v
				left join manager m on v.id_Manager=m.id_Manager
				where v.id_Question=?";
				$vote_rows = execute_query($txt_query,array('s', $question->Id));

				$question_extra = json_decode($question->QuestionExtra);
				$vote_data = array();

				if (0!=count($vote_rows))
				{
					foreach ($vote_rows as $vote)
					{
						$vote_extra = json_decode($vote->VoteData);
						if(empty($vote_extra->Подписано))
						{
							continue;
						}
						$vote_extra->ФИО=$vote->FullName;
						$vote_extra->id=$vote->id_Vote;
						$vote_data[] = $vote_extra;
					}
				}
				$poll['Вопросы'][] = array(
					'id'=>$question->Id
					,'В_повестке'=>$question->Title
					,'На_голосование'=>$question_extra
					,'Голосование'=>$vote_data
				);
			}
		}
		echo nice_json_encode($poll);
	}
}

function Update_results($id) {
	$result= $_POST['результаты'];

	$txt_query= 'select 
		uncompress(p.ExtraColumns) ExtraColumns
	from poll p
	where id_Poll=?';
	$poll_rows = execute_query($txt_query,array('s', $id));

	if (0==count($poll_rows))
	{
		exit_not_found("can not find poll id_Poll=$id");
	}
	else
	{
		$poll_row = $poll_rows[0];
		$extra_columns = json_decode($poll_row->ExtraColumns);
		$extra_columns->Результаты=$result;
		$current_time= date_format(safe_date_create(),'Y-m-d\TH:i:s');

		$txt_query = "update poll set 
	        ExtraColumns=compress(?)
			,Status=?
			,TimeChange=?
		where id_Poll=?";
		execute_query_no_result($txt_query,array('ssss', json_encode($extra_columns), 'e', $current_time, $id));

		foreach ($result as $item)
		{
			$id_Question = $item['По_вопросу']['id'];
			$txt_query="select
				uncompress(Extra) QuestionExtra
			from question
			where id_Question=?";
			$rows = execute_query($txt_query, array('s', $id_Question));
			if (1==count($rows))
			{
				$question_extra = json_decode($rows[0]->QuestionExtra);
				if(isset($item['Решение_НЕ_принято']))
				{
					$question_extra->Решение_НЕ_принято=$item['Решение_НЕ_принято'];
				}
				if(isset($item['Принято_решение']))
				{
					$question_extra->Принято_решение=$item['Принято_решение'];
				}
				$txt_query="update question set
                    Extra=compress(?)
				where id_Question=?";
				execute_query_no_result($txt_query,array('ss', json_encode($question_extra), $id_Question));
			}
			else
			{
				exit_bad_request("some problems with id_Question=$id_Question");
			}
		}
	}
	echo '{ "ok": true }';
}

$cmd= $_GET['cmd'];
$id = $_GET['id'];
switch ($cmd)
{
	case 'get':
		Read_results($id);
		break;
	case 'update':
		Update_results($id);
		break;
	default:
		exit_bad_request("unknown cmd=$cmd");
}