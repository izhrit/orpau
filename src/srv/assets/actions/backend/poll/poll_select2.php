<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$id_Manager = null;
if (isset($_GET['id_Manager']))
{
	$id_Manager= $_GET['id_Manager'];
	if (!is_numeric($id_Manager))
		exit_bad_request("bad numeric _GET['id_Manager']");
}
if (!empty($id_Manager))
{
	$txt_query = "
		select 
			  p.id_Poll id
			, p.Name text
		from poll p 
		left join manager_group_manager mgm on mgm.id_Manager_group=p.id_Manager_group
		left join sro on p.id_SRO = sro.id_SRO
		left join manager m on
             p.id_Region = m.id_Region or
             sro.RegNum=m.SRORegNum or
             mgm.id_Manager=m.id_Manager or
             (
             	m.id_Manager=m.id_Manager and 
             	p.id_Manager_group is Null and 
             	p.id_SRO is NULL and 
             	p.id_Region is NULL
			)
		where p.Name like ?
		and m.id_Manager=$id_Manager
		order by id_Poll desc
		limit 20
	;";
} else {
	$txt_query = "
		select 
			  p.id_Poll id
			, p.Name text
		from poll p 
		where p.Name like ?
		order by id_Poll desc
		limit 20
	;";
}

$rows= execute_query($txt_query,array('s',$_GET['q'].'%'));

echo nice_json_encode(array('results'=>$rows));