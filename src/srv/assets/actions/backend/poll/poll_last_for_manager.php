<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

$id_Manager= CheckMandatoryGET_id('id_Manager');

$txt_query= 'select p.id_Poll id, p.Name text
from poll p
left join manager_group_manager mgm on mgm.id_Manager_group=p.id_Manager_group
left join sro on p.id_SRO=sro.id_SRO
left join manager m on
	((p.id_Region is not null && m.id_Region=p.id_Region) or
	 (p.id_SRO is not null && m.SRORegNum=sro.RegNum) or
	 (p.id_Manager_group is not null && m.id_Manager=mgm.id_Manager) or
	 (p.id_Manager_group is null && p.id_SRO is null && p.id_Region is null)
	)
where m.id_Manager=?
order by p.id_Poll desc
limit 1
';
$rows= execute_query($txt_query,array('s',$id_Manager));

$rows_count= count($rows);
if (1!=$rows_count)
{
	echo nice_json_encode(null);
}
else
{
	echo nice_json_encode($rows[0]);
}
