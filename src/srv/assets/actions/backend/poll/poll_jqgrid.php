<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	id_Poll
	,Name
";
$from_where= "from poll WHERE id_Poll=id_Poll ";

$filter_rule_builders= array(
	'Name'=> 'std_filter_rule_builder'
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);