<?php

require_once "../assets/helpers/phpqrcode/qrlib.php";
require_once "../assets/config.php";
header("Content-Type:Image/png");

$id_Confirmation = $_GET['qr_reference'];
$confirmation_number = $_GET['confirmation_number'];
global $base_apps_url;
$qr_url = $base_apps_url."ui-backend.php?action=confirmation.printable&id_Confirmation=".$id_Confirmation;

$qr_file_name = uniqid().".png";
$qrCode = QRcode::png($qr_url, $qr_file_name, $level = QR_ECLEVEL_L, $size = 150, $margin = 1, $saveandprint=false, $back_color = 0xFFFFFF, $fore_color = 0x007500);

$template_img = imagecreatefrompng('../web/img/MarkTemplate.png');
$template_size = getimagesize('../web/img/MarkTemplate.png');
$qrcode_img = imagecreatefrompng($qr_file_name);
$qrcode_size = getimagesize($qr_file_name);
imagecopyresized($template_img, $qrcode_img, 120, 10, 0, 0, 150, 150, $qrcode_size[0], $qrcode_size[1]);
imagestring($template_img, 2, 180, 188, $confirmation_number, 0x007500);


unlink($qr_file_name);
imagepng($template_img);
imagedestroy($template_img);
imagedestroy($qrcode_img);