<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';

function GetAgreementsState($id_Manager)
{
	$txt_query= 'select AgreementText, AgreementSignature, ClubAgreementText from manager where id_Manager=?';
	$rows= execute_query($txt_query,array('s', $id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
	}
	else
	{
		$row=$rows[0];
		return (object)array
		(
			'AspAgreementState'=>(null!=$row->AgreementText && null!=$row->AgreementSignature)
			,'ClubAgreementState'=>(null!=$row->ClubAgreementText)
		);
	}
}