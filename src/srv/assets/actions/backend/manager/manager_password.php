<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/alib_email_dispatch_to_db.php';
require_once '../assets/libs/alib_manager.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

$manager = null;
if(isset($_GET['id_Manager'])) {
	$manager = GetManager();
}

function GetManager()
{
	CheckMandatoryGET_id('id_Manager');
	CheckMandatoryGET('email');
	$id_Manager= intval($_GET['id_Manager']);
	$txt_query= "select
	    EMail
		,Password
		,FirstName
		,LastName
		,MiddleName
		,id_Manager
	from manager
	where id_Manager=?";
	$rows= execute_query($txt_query,array('i',$id_Manager));

	if (null==$rows || 1!=count($rows))
		exit_not_found("can not find manager id_Manager=$id_Manager");

	$manager = $rows[0];
	$manager->EMail= $_GET['email'];
	return $manager;
}

function StorePassword($connection,$manager,$password)
{
	$txt_query= "update manager set Password= ?, EMail= ?, Salt=null where id_Manager=?";
	execute_query_no_result($txt_query,
		array('ssi',$password,$manager->EMail,$manager->id_Manager));
}

function PrepareNewPasswordLetter($manager,$password)
{
	$letter= (object)array('subject'=>'Учётные данные на Единой ИС Арбитражных управляющих');
	ob_start();
	include "../assets/actions/backend/manager/letters/password_new.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}

function PrepareBlockPasswordLetter($manager)
{
	$letter= (object)array('subject'=>'Учётные данные на Единой ИС Арбитражных управляющих');
	ob_start();
	include "../assets/actions/backend/manager/letters/password_block.php";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	return $letter;
}

function PostLetterToManager($connection, $letter_data, $manager, $EmailType_descr)
{
	 $manager_name= $manager->LastName.' '.$manager->FirstName.' '.$manager->MiddleName;
	 return Dispatch_Email_Message($connection, $letter_data,$manager->EMail,$manager_name,
	 				  $EmailType_descr,$manager->id_Manager);
}


function ChangePassword($manager)
{
	global $email_settings;
	$new_password= (true!=$email_settings->enabled) ? 'new_test_pass' : generate_password(8);

	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		StorePassword($connection, $manager, $new_password);
		$letter= PrepareNewPasswordLetter($manager, $new_password);
		if (false==PostLetterToManager($connection, $letter,$manager,'смена пароля АУ'))
		{
			$connection->rollback();
			exit_internal_server_error("Can not send email to $manager->EMail");
		}
		else
		{
			$connection->commit();
			echo '{ "ok": true }';
			exit;
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $manager->EMail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function BlockPassword($manager)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		StorePassword($connection,$manager,null);
		$letter= PrepareBlockPasswordLetter($manager);
		if (false==PostLetterToManager($connection, $letter,$manager,'блокирование пароля АУ'))
		{
			$connection->rollback();
			exit_internal_server_error("Can not send email to $manager->EMail");
		}
		else
		{
			$connection->commit();
			echo '{ "ok": true }';
			exit;
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $manager->EMail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function ResetByLogin() {
	CheckMandatoryGET('login');
	$login = $_GET['login'];
	$phone = preg_replace('/\D+/', '', $login);
	if(strripos($login, '@') != false && strlen($phone) != 11){
		$phone = null;
	}
	CheckManagerAccess($login, $phone);
	$txt_query = "select
		EMail
		,Password
		,FirstName
		,LastName
		,MiddleName
		,id_Manager
	from manager where EMail=? or Login=? or Phone=?";
	$rows = execute_query($txt_query, array('sss', $login, $login, $phone));
	if (1==count($rows))
	{
		ChangePassword($rows[0]);
	}
	else
	{
		echo 'null';
	}
}

function CheckManagerAccess($login, $phone) {
	$txt_query= "select
          id_Manager
        , Password
	from manager
	where (EMail=? or Login=? or Phone=?)";
	$rows= execute_query($txt_query,array('sss',$login,$login, $phone));

	if(1==count($rows)){
		if(is_null($rows[0]->Password)) {
			echo '{ "ok": false, "reason": "Нет разрешения на доступ через логин и пароль" }';
			exit;
		}
	} else {
		echo 'null';
		exit;
	}
}

CheckMandatoryGET('cmd');
$cmd= $_GET['cmd'];
switch ($cmd)
{
	case 'change': ChangePassword($manager); break;
	case 'block': BlockPassword($manager); break;
	case 'reset-by-login': ResetByLogin(); break;
	default: exit_bad_request("bad _GET['cmd']=$cmd:");
}