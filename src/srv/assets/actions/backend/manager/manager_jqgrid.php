<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
		m.id_Manager id_Manager
		, m.LastName LastName
		, m.FirstName FirstName
		, m.MiddleName MiddleName
		, m.INN INN
		, sro.Name SRO
		, r.Name Region
		, GROUP_CONCAT(mgm.id_Manager_group) id_Group
";

$from_where= '
		from manager m
		left join sro on sro.RegNum=m.SRORegNum
		left join region r on r.id_Region=m.id_Region
		left join manager_group_manager mgm on mgm.id_Manager=m.id_Manager
		WHERE m.id_Manager=m.id_Manager
		and m.LastName<>"kjh" and m.LastName<>"test (временно для тестирования)"
';
$filter_rule_builders= array(
	'id_Manager'=> 'query_field'
	,'FirstName'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND m.FirstName like '$data%'";
	}
	,'LastName'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND m.LastName like '$data%'";
	}
	,'MiddleName'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND m.MiddleName like '$data%'";
	}
	,'INN'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND m.INN like '$data%'";
	}
	,'SRO'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND sro.Name like '%$data%'";
	}
	,'Region'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND r.Name like '$data%'";
	}
	,'id_Group'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND mgm.id_Manager_group like '$data%'";
	}
);
$groupBy = "GROUP BY m.id_Manager ";
$orderBy = $groupBy."ORDER BY LastName, FirstName, MiddleName";
if ($_GET['sidx']!='')
{
	$orderfield = $_GET['sidx'];
	$orderBy = $groupBy."ORDER BY $orderfield ";
	if ($_GET['sord']=='desc')
	{
		$orderBy.= ' DESC ';
	}
}

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders, $orderBy);