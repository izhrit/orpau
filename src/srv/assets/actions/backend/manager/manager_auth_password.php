<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

$login= $_POST['Login'];
$password= $_POST['Password'];
$phone = getPhoneFromLogin($login);

function OkLogin($ainfo, $login)
{
	global $_SESSION, $auth_info;
	if(!isset($_SESSION))
		session_start();

	$agreementsState = GetAgreementsState($ainfo->id_Manager);
	$result = (object)array(
		'id_Manager'=> $ainfo->id_Manager
		,'ArbitrManagerID'=> $ainfo->ArbitrManagerID
		,'Фамилия'=> $ainfo->LastName, 'Имя'=> $ainfo->FirstName, 'Отчество'=> $ainfo->MiddleName
		,'ИНН'=> $ainfo->INN, 'efrsbNumber'=> $ainfo->RegNum
		,'EMail'=> $ainfo->EMail, 'Phone'=> $ainfo->Phone
		,'Login'=> $login, 'ИспользуетАСП'=>$agreementsState->AspAgreementState
		,'СостоитВПрофсоюзе'=>$agreementsState->ClubAgreementState
	);

	$auth_info= $result;
	$_SESSION['auth_info']= $auth_info;

	echo nice_json_encode($result);
}

function CheckManagerAccess($login, $phone) {
    $txt_query= "select
          id_Manager
        , Password
	from manager
	where (EMail=? or Login=? or Phone=?)";
    $rows= execute_query($txt_query,array('sss',$login,$login, $phone));

    if(1==count($rows)){
        if(is_null($rows[0]->Password)) {
            echo '{ "ok": false, "reason": "Нет разрешения на доступ через логин и пароль" }';
            exit;
        }
    } else {
        echo 'null';
        exit;
    }
}

function hasPassword($password, $salt){
	return md5(md5($password).$salt);
}

function GetSalt($login, $phone) {
	$txt_query="select Salt from manager where (EMail=? or Login=? or Phone=?)";
	$rows=execute_query($txt_query, array('sss', $login, $login, $phone));
	if(1==count($rows)){
		return $rows[0]->Salt;
	} else {
		echo 'null';
		exit;
	}
}

function getPhoneFromLogin($login)
{
	$phone = preg_replace('/\D+/', '', $login);
	if(strripos($login, '@') != false && strlen($phone) != 11){
		$phone = null;
	}
	return $phone;
}

function LoginManager($login,$password, $phone) {
    CheckManagerAccess($login, $phone);
    $salt = GetSalt($login, $phone);

    if(!empty($salt)){
    	$prep_password = hasPassword($password, $salt);
    } else {
	    $prep_password = $password;
    }

    $txt_query= "select
		 id_Manager
		, ArbitrManagerID
		, LastName
		, FirstName
		, MiddleName
		, INN
		, RegNum
		, EMail
		, Phone
		, Login
	from manager
	where (EMail=? or Login=? or Phone=?) && Password=?;";
	$rows= execute_query($txt_query,array('ssss',$login,$login,$phone,$prep_password));
	$count= count($rows);
	if (1==$count)
	{
		OkLogin($rows[0], $login);
	}
	else
	{
		write_to_log("wrong login \"$login\" password \"$password\" as manager - found $count rows");
		echo 'null';
	}
}

function TryAutoLogin($login, $phone)
{
	global $_SESSION, $auth_info, $_POST;

	CheckManagerAccess($login, $phone);

	if (!isset($_SESSION))
		session_start();

	if (!isset($_SESSION['auth_info']))
		return false;

	$auth_info= $_SESSION['auth_info'];

	if (!isset($auth_info->Login) || $login != $auth_info->Login)
		return false;

	$agreementsState = GetAgreementsState($auth_info->id_Manager);
	$auth_info->ИспользуетАСП=$agreementsState->AspAgreementState;
	$auth_info->СостоитВПрофсоюзе=$agreementsState->ClubAgreementState;

	echo nice_json_encode($auth_info);
	return true;
}

if (TryAutoLogin($login, $phone))
	exit;

LoginManager($login,$password, $phone);