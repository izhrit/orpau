<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$txt_query= "
	select 
		  m.id_Manager id
		, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) text
		, m.LastName lastName
		, m.FirstName firstName
		, m.MiddleName middleName
		, m.INN inn
		, sro.Name sro
		, r.Name region
	from manager m
	left join sro on sro.RegNum=m.SRORegNum
	left join region r on r.id_Region=m.id_Region
	where LastName like ?
	limit 20
;";

$rows= execute_query($txt_query,array('s',$_GET['q'].'%'));

echo nice_json_encode(array('results'=>$rows));