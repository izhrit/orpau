<?php

require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/config.php';
require_once '../assets/libs/alib_manager.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

class Manager_crud extends Base_crud
{
	function read($id_Manager)
	{
		$txt_query= "select
			m.id_Manager
			, m.FirstName
			, m.LastName
			, m.MiddleName
			, m.RegNum
			, m.INN
			, m.CorrespondenceAddress
			, m.Phone
			, m.EMail
			, m.Password
			, m.Login
			, m.ArbitrManagerID
			, sro.ShortTitle SROName
			, sro.RegNum SRORegNum
		from manager m
		left join sro on m.SRORegNum = sro.RegNum
		where m.id_Manager= ?";

		$rows= execute_query($txt_query, array('s',$id_Manager));
		$count_rows= count($rows);
		if(1!=$count_rows){
			exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
		} else {
			$row = $rows[0];
			$agreementsState = GetAgreementsState($row->id_Manager);

			$has_password = strlen($row->Password) > 0;
			$row->Password=$has_password;
			$row->ИспользуетАСП=$agreementsState->AspAgreementState;
			$row->СостоитВПрофсоюзе=$agreementsState->ClubAgreementState;
			echo nice_json_encode($row);
		}
	}

	function update($id_Manager, $manager)
	{
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			$txt_query = "update manager set EMail=?, Login=?, Phone=? where id_Manager=?";
			execute_query_no_result(
				$txt_query, array('sssi',$manager["EMail"],$manager["Login"],$manager['Phone'],$manager["id_Manager"]));
		}
		catch (mysqli_sql_exception $ex)
		{
			$connection->rollback();
			ProcessMySqlException_check_duplicate_login($ex, $manager->EMail);
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}

		$connection->commit();
		echo '{ "ok": true }';
		exit;
	}
}

$crud = new Manager_crud();
$crud->process_cmd();