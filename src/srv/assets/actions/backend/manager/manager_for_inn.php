<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$inn= $_POST['inn'];

if ($inn!=preg_replace("/[^0-9,]/", "", $inn))
	exit_bad_request('inn argument can contain only digits and commas!');

$txt_query= "
	select 
		  m.id_Manager id
		, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) text
		, m.LastName lastName
		, m.FirstName firstName
		, m.MiddleName middleName
		, m.INN inn
		, sro.Name sro
	from manager m
	left join sro on sro.RegNum=m.SRORegNum
	where m.INN in ($inn)
;";

$rows= execute_query($txt_query,array());

echo nice_json_encode($rows);