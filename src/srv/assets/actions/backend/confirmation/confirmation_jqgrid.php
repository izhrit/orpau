<?php
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	id_confirmation as number
	,DATE_FORMAT(TimeOfCreation, '%d/%m/%Y') as date 
	,concat(lastName,' ',LEFT(firstName,1),'.',LEFT(middleName,1),'.') as manager
	,debtor.Name as debtor
	,debtor.SNILS as snils
	,debtor.OGRN as ogrn
	,debtor.INN as inn
	,c.CaseNumber
	,id_Confirmation
";

$from_where= "from confirmation as c, manager, debtor 
WHERE c.id_Manager = manager.id_Manager && c.id_Debtor=debtor.id_Debtor";
$filter_rule_builders= array(
	'date' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND TimeOfCreation like '$data%'";
	}
	,'manager'=>'query_field'
	,'debtor'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.Name like '$data%'";
	}
	,'snils'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.SNILS like '$data%'";
	}
	,'ogrn'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.OGRN like '$data%'";
	}
	,'inn'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.INN like '$data%'";
	},'number'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND c.id_Confirmation like '$data'";
	},'CaseNumber'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND c.CaseNumber like '$data'";
	}
	,'id_Confirmation'=>'query_field'
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);