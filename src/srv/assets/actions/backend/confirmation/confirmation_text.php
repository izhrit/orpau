<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/config.php';

if (!isset($_GET['id_Confirmation']))
	exit_bad_request('skipped $_GET["id_Confirmation"]');

$id_Confirmation = $_GET['id_Confirmation'];
$txt_query = "SELECT Text FROM confirmation WHERE id_Confirmation = ?";
$rows= execute_query($txt_query, array('s', $id_Confirmation));
$Text = $rows[0]->Text;
$conf_file_name = "orpau_confirm_".$id_Confirmation.".txt";
file_put_contents($conf_file_name, $Text);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . basename($conf_file_name));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($conf_file_name));

readfile($conf_file_name);
unlink($conf_file_name);
