<?php
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/config.php';


class confirmation_generator
{
	function generate()
	{
		$id_Manager = $_GET['id_Manager'];
		$id_Debtor = $_GET['id_Debtor'];
		$today = date('d/m/Y');

		$txt_query = "select 
			LastName, FirstName, MiddleName, m.INN, DATE_FORMAT(SRORegDate, '%d/%m/%Y') as SRORegDate,
			sro.Name as SROName, CorrespondenceAddress, EMail, Phone
		 from manager m
		 left join sro ON m.SRORegNum = sro.RegNum 
		 where ? = id_Manager";
		$rows= execute_query($txt_query, array('s',$id_Manager));
		$manager = $rows[0];

		$txt_query = "SELECT Name, INN, OGRN, SNILS, BankruptID, LegalCaseList FROM debtor WHERE ? = id_Debtor";
		$rows= execute_query($txt_query, array('s',$id_Debtor));
		$debtor = $rows[0];

		$messages = $this->get_sources_for_debtor($debtor->BankruptID);
		if(count($messages)>0)
			$messageNumber = array_pop($messages)->Number;

		ob_start();
		include "../assets/actions/Backend/templates/confirmation_template.php";
		$result = ob_get_contents();
		ob_end_clean();

		echo $result;
	}

	function get_sources_for_debtor($bankruptID)
	{
		$sources_url = "https://probili.ru/efrsb-tests/dm-api.php?action=get-source&BankruptId=";
		$source_url = "https://bankrot.fedresurs.ru/MessageWindow.aspx?ID=";
		$responce = file_get_contents($sources_url.$bankruptID);
		$sources =  json_decode($responce);
		$normalized_sources = array();
		if (isset($sources->manager) && $sources->manager->status=="true")
		{
			$date = new DateTime($sources->manager->PublishDate);
			$manager_data = (object)array(
				"Description"=>$sources->manager->MessageName,
				"Date"=>$date->format('d.m.y'),
				"url"=>$source_url.$sources->manager->MessageGUID,
				"Number"=>$sources->manager->Number);
			array_push($normalized_sources, $manager_data);
		}
		if (isset($sources->debtor) && $sources->debtor->status=="true")
		{
			$date = new DateTime($sources->debtor->PublishDate);
			$debtor_data = (object)array(
					"Description"=>$sources->debtor->MessageName,
					"Date"=>$date->format('d.m.y'),
					"url"=>$source_url.$sources->debtor->DebtorGUID,
					"Number"=>$sources->debtor->Number
				);
			array_push($normalized_sources, $debtor_data);
		}
		return $normalized_sources;
	}
}

$generator = new confirmation_generator();
$generator->generate();
