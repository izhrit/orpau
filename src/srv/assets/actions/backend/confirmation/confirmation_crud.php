<?php

require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/config.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/validate.php';

class Confirmation_crud extends Base_crud
{
	function read($id_Confirmation)
	{
		$txt_query = "SELECT 
			id_Confirmation,
			id_Debtor,
			id_Manager,
			Number,
			DATE_FORMAT(TimeOfCreation, '%d/%m/%Y'),
			Text,
			Signature,
			EfrsbMessageNumber,
			JudicalActId
		FROM confirmation WHERE ? = id_Confirmation";
		$rows = execute_query($txt_query, array('s',$id_Confirmation));

		$count_rows= count($rows);
		if (1!=$count_rows)
			exit_not_found("got $count_rows confirmations for id_Confirmation=$id_Confirmation");

		echo nice_json_encode($rows[0]);
	}
	
	function CreateConfirmation($id_Manager, $id_Debtor, $CaseNumber, $Text)
	{
		session_start();
		
		$txt_query = "SELECT INN FROM manager WHERE ?=id_Manager";
		$result = execute_query($txt_query, array('s', $id_Manager));
		$token = $_SESSION['token'];
		$verified = $result[0]->INN == $token['INN'];
		if ($verified)
		{
			$date = new DateTime();
			$id_Confirmation = $this->CreateEmptyConfirmation($id_Manager, $id_Debtor, $date);
			try 
			{
				$Text = str_replace('{Номер справки}', $id_Confirmation, $Text);
				$signature = $this->SignConfirmationText($Text);
				$this->UpdateConfirmationSignatureAndText($id_Confirmation, $Text, $CaseNumber, $signature);
				echo $id_Confirmation;
			}
			catch (Exception $ex)
			{
				$txt_query = "DELETE FROM confirmation WHERE id_Confirmation = ?";
				execute_query($txt_query, array('s', $id_Confirmation));
				throw $ex;
			}
		}
		else 
		{
			exit_bad_request('инн токена и не совпадает с инн АУ');
		}
	}

	function UpdateConfirmationSignatureAndText($id_Confirmation, $Text, $CaseNumber, $signature)
	{
		$txt_query = "UPDATE confirmation 
			SET Text = ?, signature = ?, CaseNumber = ?
			WHERE id_Confirmation = ?";
		execute_query($txt_query, array('ssss', $Text, $signature, $CaseNumber, $id_Confirmation));
	}

	function CreateEmptyConfirmation($id_Manager, $id_Debtor, $date)
	{
		$txt_query = "INSERT INTO confirmation 
		(id_Manager, id_Debtor, Text, TimeOfCreation, signature) 
		VALUES(?,?,?,?,?)";

		execute_query($txt_query,  
			array('sssss',
				$id_Manager,$id_Debtor,"",$date->format('y-m-d'),""));

		$txt_query = "SELECT id_Confirmation FROM confirmation
			WHERE id_Manager = ? AND id_Debtor = ? AND TimeOfCreation = ?";

		$query_result = execute_query($txt_query, 
			array('sss', $id_Manager, $id_Debtor, $date->format('y-m-d')));

		$id_Confirmation = array_pop($query_result)->id_Confirmation;
		return $id_Confirmation;
	}

	function SignConfirmationText($Text)
	{
		global $crypto_api_url;
		$Curl = curl_init();
		curl_setopt_array($Curl, array(
			CURLOPT_URL => $crypto_api_url.'Sign',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $Text
		));
		$signature = curl_exec($Curl);
		$httpcode = curl_getinfo($Curl, CURLINFO_HTTP_CODE);
		if (200!=$httpcode)
			exit_internal_server_error("can not request sign code $httpcode for url \"$url\")");
		return $signature;
	}

	function CreateProcedure($id_Manager, $id_Debtor)
	{
		$txt_query = "INSERT INTO audit_procedure
			(id_Manager, id_Debtor, procedure_Type)
			VALUES (?,?,?)";
		
		execute_query($txt_query, array('sss',  $id_Manager, $id_Debtor, 'Н'));
	}

	function create($conf)
	{
		$id_Debtor = $_POST['id_Debtor'];
		$id_Manager = $_POST['id_Manager'];
		$CaseNumber = $_POST['CaseNumber'];
		$Text = str_replace('_',' ',$_POST['Text']);
		$txt_query = "SELECT id_Manager, id_Confirmation, id_Debtor, TimeOfCreation, Text 
		FROM confirmation WHERE ? = id_Manager AND ? = id_Debtor";
		$is_already_exist = count(
			execute_query(
				$txt_query, 
				array(
					'ss',
					$id_Manager,
					$id_Debtor)
					)
				) > 0;

		/*if (!$is_already_exist)
		{*/
			$this->CreateConfirmation($id_Manager, $id_Debtor, $CaseNumber, $Text);
		//}
	}

}

$crud = new Confirmation_crud();
$crud->process_cmd();