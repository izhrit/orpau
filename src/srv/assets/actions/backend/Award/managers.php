<?php
require_once __DIR__ . '/../../../helpers/log.php';
require_once __DIR__ . '/../../../helpers/db.php';
require_once __DIR__ . '/../../../config.php';
require_once __DIR__ . '/../../../helpers/json.php';

if (isset($_GET["operation"]))
{
	switch ($_GET["operation"])
	{
		case "checkmanagerinserver": {
			ExistManagerInServerLicense($_GET["managerName"],$_GET["efrsbNumber"],$_GET["inn"]);
		} break;
		case "contractAuth": {
			ContractAuth($_GET["login"],$_GET["password"],$_GET["efrsbNumber"],$_GET["inn"]);
		} break;
	}
}


function GetManagers($license_id, $reg_num)
{
	if (is_null($license_id))
	{
		return null;
	}
	else
	{
		if (is_null($reg_num))
		{
			return GetManagersByLicenseId($license_id);
		}
		else
		{
			return GetManagerByLicenseIdAndRegnum($license_id, $reg_num);
		}
	}
}

function getRealIPAddr()
{
	//check ip from share internet
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} //to check ip is pass from proxy

	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return !empty($ip) ? $ip : "unknow";
}

function GetManagersByLicenseId($license_id)
{
	global $server_license_url, $use_mock_server_license;

	if ($use_mock_server_license) {
		$managers_response = MockManagers();
	} else {
		$managers_response = file_get_contents($server_license_url . "?action=getManagersByLicenseId&license_id=" . $license_id . "&ip=".getRealIPAddr());
	}

	$managers = FillAlreadyVoted(json_decode($managers_response, true));

	return $managers;
}

function FillAlreadyVoted($managers)
{
	$txt_query= "
		SELECT 
			AN.* 
		FROM AwardVote AV 
		INNER JOIN AwardNominee AN on AV.id_AwardNominee = AN.id_AwardNominee 
		WHERE AV.inn =?
	;";
	for ($i = 0, $j = count($managers); $i < $j; $i++)
	{
		$awardvote = execute_query($txt_query, array('s', $managers[$i]["inn"]));
		if (!empty($awardvote))
		{
			$managers[$i]["voted_for"] = array(
				"last_name"   => $awardvote[0]->lastName,
				"first_name"  => $awardvote[0]->firstName,
				"middle_name" => $awardvote[0]->middleName,
				"inn" => $awardvote[0]->inn,
				"sro"         => $awardvote[0]->SRO
			);
		}
	}
	return $managers;
}

function GetManagerByLicenseIdAndRegnum($license_id, $regnum)
{
	global $server_license_url, $use_mock_server_license;

	if ($use_mock_server_license)
	{
		$managers_response = MockManagers(true);
	}
	else
	{
		$managers_response = file_get_contents($server_license_url 
			. "?action=getManagerByLicenseIdAndRegnum&license_id=" 
			. $license_id . "&regnum=" . $regnum . "&ip=".getRealIPAddr());
	}

	$managers = FillAlreadyVoted(json_decode($managers_response, true));

	return $managers;
}

function GetEmailByLecenseId($license_id)
{
	global $server_license_url, $use_mock_server_license;

	if ($use_mock_server_license) {
		return 'ee@aa.uu';
	}
	else
	{
		$email_response = file_get_contents($server_license_url
											. "?action=getEmailByLicenseId&license_id=" . $license_id);
		return $email_response;
	}
}

function MockManagers($single = false)
{
	$mock_managers = array(
		array(
			"last_name"       => "Иванов",
			"first_name"      => "Иван",
			"middle_name"     => "Иванович",
			"inn"     => "0001",
			"contract_number" => "666666",
			"voted_for"       => array(
				"last_name"   => "Серёгин",
				"first_name"  => "Серёга",
				"middle_name" => "Сергеевич",
				"EFRSBNumber" => "322",
				"sro"         => "СРО задорных"
			)
		),
		array(
			"last_name"       => "Петров",
			"first_name"      => "Петр",
			"middle_name"     => "Петрович",
			"inn"     => "0002",
			"contract_number" => "666666"
		),
		array(
			"last_name"       => "Петров",
			"first_name"      => "Иван",
			"middle_name"     => "Петрович",
			"inn"     => "0003",
			"contract_number" => "666666",
		)
	);

	if ($single)
	{
		$mock_managers = array( $mock_managers[0] );
	}

	return nice_json_encode($mock_managers);
}

function ExistManagerInServerLicense($managerName, $efrsbNumber, $inn)
{
	global $server_license_url;
	$url = $server_license_url."?action=checkManagerInServer";
	$data = array('managerName' => $managerName, 'efrsbNumber' => $efrsbNumber);

	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
//	if ($result === FALSE) {}
	echo $result;
}

function ContractAuth($login, $password, $efrsbNumber, $inn)
{
	global $server_license_url;
	$url = $server_license_url."?action=contractAuth";
	$data = array('login' => $login, 'password' => $password, 'efrsbNumber' => $efrsbNumber);

	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
//	if ($result === FALSE) {}
	echo $result;
}