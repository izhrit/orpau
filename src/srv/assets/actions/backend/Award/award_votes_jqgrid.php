<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$id_Poll = $_GET['id_Poll'];

$fields= "
	1 id_Nominee
	, '' Name
	, count(id_Manager) votes_count
";

$from_where= "FROM vote where id_Poll=$id_Poll";

$group_order= " 
group by id_Nominee, Name
order by votes_count desc
";

$filter_rule_builders= array(
	 'Name'=> function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and JSON_EXTRACT(VoteData, '$.Name') like '%$data%' ";
	}
	,'count_votes'=> function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and count_votes like '$data%' ";
	}
);

execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,$group_order);
