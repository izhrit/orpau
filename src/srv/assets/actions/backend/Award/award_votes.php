<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

switch ($_GET["operations"]) {
	case "VotesForNominants": { VotesForNominants(); } break;
	case "AllVotes": { AllVotes(); } break;
	case "GetSignedVoices": { GetSignedVoices(); } break;
	case "IsSignedVoice": { IsSignedVoice($_GET['inn']); } break;
	case "GetVotes": { GetVotes($_GET['inn']); } break;
}

function GetVotes($inn)
{
	$txt_query = 
	"SELECT id_Vote, id_Poll, VoteTime, BulletinVoterSignature
	FROM vote
	join manager on manager.id_Manager=vote.id_Manager WHERE inn=?";
	$votes = execute_query($txt_query, array('s', $inn));
	if (empty($votes))
	{
		echo '{"voted":false}';
	}
	else
	{
		echo json_encode($votes);
	}

}

function IsSignedVoice($voting_inn)
{
	$vote = execute_query("SELECT BulletinText, BulletinSignature, id_AwardVote FROM AwardVote WHERE inn=?", array('s', $voting_inn));
	if (!empty($vote)) {
		if (!empty($vote[0]->BulletinText) && !empty($vote[0]->BulletinSignature)) {
			echo 1;
			return;
		}
	}

	echo 0;
}

function VotesForNominants()
{
	$txt_query = "
		SELECT v.id_AwardVote
			 , v.efrsbNumber       efrsbNumber
			 , v.VoteTime          VoteTime
			 , v.email             email
			 , v.BulletinText      BulletinText
			 , v.BulletinSignature BulletinSignature
			 , count(n.id_AwardNominee) voicesCount
			 , n.lastName nominee_lastName
			 , n.firstName nominee_firstName
			 , n.middleName nominee_middleName
			 , n.efrsbNumber nominee_efrsbNumber
		FROM AwardVote v
		INNER JOIN AwardNominee n ON v.id_AwardNominee = n.id_AwardNominee
		GROUP BY n.id_AwardNominee ORDER BY voicesCount DESC;
;";
	$rows      = execute_query($txt_query, array());

	$csvTitle = "Номинант;Количество голосов";
	$csvRows  = array();

	foreach ($rows as $row) {
		$nominee   = $row->nominee_lastName . " " . $row->nominee_firstName . " " . $row->nominee_middleName . " (ЕФРСБ:" . $row->nominee_efrsbNumber . ")";
		$line      = $nominee . ";" . $row->voicesCount;
		$csvRows[] = $line;
	}
	GenerateCsvInMemory($csvTitle, $csvRows);
}

function AllVotes()
{
	$txt_query = "
		SELECT v.id_AwardVote
			 , v.efrsbNumber       efrsbNumber
			 , v.VoteTime          VoteTime
			 , v.email             email
			 , v.BulletinText      BulletinText
			 , v.BulletinSignature BulletinSignature
			 , n.lastName nominee_lastName
			 , n.firstName nominee_firstName
			 , n.middleName nominee_middleName
			 , n.efrsbNumber nominee_efrsbNumber
		FROM AwardVote v
		INNER JOIN AwardNominee n ON v.id_AwardNominee = n.id_AwardNominee;
;";
	$rows      = execute_query($txt_query, array());

	$csvTitle = "ЕФРСБ голосующего;Время голосования;Емейл голосующего;Бюллетень текс (base64);Бюллетень сигнатура (base64);Проголосовал за";
	$csvRows  = array();

	foreach ($rows as $row) {
		$nominee            = $row->nominee_lastName . " " . $row->nominee_firstName . " " . $row->nominee_middleName . " (ЕФРСБ:" . $row->nominee_efrsbNumber . ")";
		$efrsb              = $row->efrsbNumber;
		$vote_time          = $row->VoteTime;
		$emeil              = $row->email;
		$bulletin_text_base64      = base64_encode($row->BulletinText);
		$bulletin_signature_base64 = base64_encode($row->BulletinSignature);

		$line = $efrsb . ";" . $vote_time . ";" . $emeil . ";" . $bulletin_text_base64 . ";" . $bulletin_signature_base64 . ";" . $nominee;
		$csvRows[] = $line;
	}

	GenerateCsvInMemory($csvTitle, $csvRows);
}

function GetSignedVoices()
{
	// Prepare File
	$file = tempnam("SignedVoices", "zip");
	$zip = new ZipArchive();
	$zip->open($file, ZipArchive::OVERWRITE);

	// Stuff with content
	$voices = execute_query("SELECT * FROM AwardVote WHERE BulletinText IS NOT NULL", array());
	foreach ($voices as $row) {
		$zip->addFromString($row->id_AwardVote.".txt", $row->BulletinText);
		$zip->addFromString($row->id_AwardVote.".sig", $row->BulletinSignature);
	}

	// Close and send to users
	$zip->close();
	header('Content-Type: application/zip');
	header('Content-Length: ' . filesize($file));
	header('Content-Disposition: attachment; filename="SignedVoices.zip"');
	readfile($file);
	unlink($file);
}

function GenerateCsvInMemory($title, $rows)
{
	$rf = fopen('php://memory', 'w');
	if (is_resource($rf)) {
		fwrite($rf, $title . "\r\n");

		foreach ($rows as $row) {
			fwrite($rf, $row . "\r\n");
		}

		fseek($rf, 0);
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename="file.csv";');
		fpassthru($rf);
		fclose($rf);
	}
}

