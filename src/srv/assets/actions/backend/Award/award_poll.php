<?php

require_once __DIR__.'/../../../helpers/db.php';

$id_Poll = $_GET['id_Poll'];

$txt_query = 
"select id_Poll, Name, ExtraColumns
from poll
where id_Poll=?";

$rows= execute_query($txt_query, array('s', $id_Poll));
$result = $rows[0];

$result->ExtraColumns = json_decode($result->ExtraColumns);

echo json_encode($result);