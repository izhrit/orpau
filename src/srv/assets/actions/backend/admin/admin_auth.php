<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

function findAdminUser($login, $password)
{
	global $admin_users;
	foreach ($admin_users as $u)
	{
		if ($login==$u->login && $password==$u->password)
			return $u;
	}
	return null;
}

function DoLogin()
{
	$login= $_POST['login'];
	$password= $_POST['password'];

	$logged_user= findAdminUser($login, $password);

	if (null==$logged_user)
	{
		echo nice_json_encode(null);
	}
	else
	{
		global $_SESSION, $auth_info;
		if(!isset($_SESSION))
			session_start();

		$ainfo= (object)array(
			  'id_Admin'=>$logged_user->id_Admin
			, 'login'=> $login
		);
		if (isset($logged_user->rights))
			$ainfo->rights= $logged_user->rights;

		$auth_info= $ainfo;
		$_SESSION['auth_info']= $ainfo;

		echo nice_json_encode($auth_info);
	}
}

CheckMandatoryGET_execute_action_variant('cmd',array(
	'login'=>    function(){DoLogin();}
));
