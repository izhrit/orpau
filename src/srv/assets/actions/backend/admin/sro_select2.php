<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$txt_query= "
	select 
		  id_SRO id
		, Name text
	from sro 
	where Name like ?
	limit 20
;";

$rows= execute_query($txt_query,array('s',$_GET['q'].'%'));

echo nice_json_encode(array('results'=>$rows));