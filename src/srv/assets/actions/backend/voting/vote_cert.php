<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

function Read_vote($id_vote)
{
	$txt_query = "select
			v.id_Vote
			,v.Confirmation_code
			,v.Confirmation_code_time
			,v.VoteData 
			,m.id_Manager
			,m.FirstName
			,m.LastName
			,m.MiddleName
			,concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) FullName
			,m.Phone
			,m.EMail
			,q.Title QuestionTitle
		from vote v
		left join manager m on m.id_Manager=v.id_Manager
		left join question q on v.id_Question = q.id_Question
		left join poll p on q.id_Poll = p.id_Poll
		where v.id_Vote=?";
	$rows= execute_query($txt_query,array('s', $id_vote));

	$question=$rows[0];
	$question->ФИО=$question->LastName.' '.$question->FirstName.' '.$question->MiddleName;
	return $question;
}

function prepareSignature($vote, $log_time, $cert)
{
	global $sms_settings, $email_settings;
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');
	$подписано_сертификатом = true;
	$сертификат = $cert;
	$запись_об_уведомление_о_заседании=findVote_log($vote, 'n');
	if(empty($запись_об_уведомление_о_заседании))
	{
		$запись_об_уведомление_о_заседании=findVote_log($vote, 'u');
	}
	$запись_о_гололосовании=findVote_log($vote, 'b');
	$документ_бюллетень= findVote_document($vote,'b');
	$запись_об_отправке_кода= findVote_log($vote,'c');
	$вход_с_устройства = $_SERVER['REMOTE_ADDR'];

	ob_start();
	include "../assets/actions/backend/voting/views/documents/signature.php";
	$signature= fix_end_of_lines(ob_get_contents());
	ob_end_clean();

	return $signature;
}

function findVote_document($vote,$Vote_document_type)
{
	$count= count($vote->Документы);
	for ($i= $count-1; $i>=0; $i--)
	{
		$doc= $vote->Документы[$i];
		if (isset($doc->Vote_document_type) && $doc->Vote_document_type==$Vote_document_type)
			return $doc;
	}
	return null;
}

function findVote_log($vote,$Vote_log_type)
{
	global $Vote_log_type_description, $Параметры_для_писем_по_голосованию;
	$count= count($vote->Журнал);
	for ($i= $count-1; $i>=0; $i--)
	{
		$log= $vote->Журнал[$i];
		if ($log->Vote_log_type==$Vote_log_type)
		{
			if($Vote_log_type=='n' || $Vote_log_type=='u'){
				$d= FindDescription($Vote_log_type_description,'code',$Vote_log_type);
				$p= $Параметры_для_писем_по_голосованию[$d['descr']];
				$log->Subject=$p->Начало_темы_письма;
			}
			$date_format = date_create_from_format('d.m.Y H:i:s',$log->Vote_log_time);
			$log->Vote_log_time = date_format($date_format, 'd.m.Y');
			return $log;
		}
	}
	return null;
}

function SignByCert ()
{
	$id_Vote=$_GET['id_Vote'];
	$vote= Read_vote($id_Vote);
	$post_data= safe_POST($_POST);
	$bulletin_text = $post_data['text'];
	$cert = $post_data['cert'];
	$base64_encoded_signature = $post_data['base64_encoded_signature'];

	$row_Vote_doc_bulletin= PrepareDocument($id_Vote,safe_date_create(),'b',$bulletin_text);
	$row_Vote_doc_signature= PrepareDocument($id_Vote,safe_date_create(),'k',base64_decode($base64_encoded_signature));

	$дополнительно = array(
		'вложения'=>array($row_Vote_doc_bulletin)
		,'параметры'=>(object)array(
			'бюллетень'=> (object)array(
				'md5'=>$row_Vote_doc_bulletin->doc_md5
				,'длина'=>$row_Vote_doc_bulletin->Размер
				,'FileName'=>$row_Vote_doc_bulletin->FileName
			)
		)
	);
	send_voting_email_by_vote($id_Vote, 'Письмо о подписи', $дополнительно);

	$ЖурналДокументы= array('id_Vote'=>$id_Vote);
	Load_ЖурналДокументы($ЖурналДокументы);
	$vote->Журнал= $ЖурналДокументы['Журнал'];
	$vote->Документы= $ЖурналДокументы['Документы'];

	$log_time= safe_date_create();

	/*$txt_signature= prepareSignature($vote, $log_time, (object)$cert);
	$row_Vote_document_sig= PrepareDocument($id_Vote,$log_time,'s',$txt_signature);*/

	//$operator_signature= prepareSignatureOfOperator($txt_signature);
	//$row_Vote_document_rit_sig= PrepareDocument($id_Vote,safe_date_create(),'r',$operator_signature);

	/*$дополнительно = array(
		'вложения'=>array($row_Vote_document_sig,$row_Vote_document_rit_sig)
		,'параметры'=>(object)array(
			'row_Vote_document_sig'=> $row_Vote_document_sig
			,'row_Vote_document_rit_sig'=> $row_Vote_document_rit_sig
		)
	);*/
//	send_voting_email_by_vote($id_Vote, 'Подписан бюллетень', $дополнительно);

	$log_time_json= date_format(safe_date_create(),'d.m.Y H:i:s');

	$vote_data= json_decode($vote->VoteData);
	$vote_data->Подписано=$log_time_json;

	execute_query_no_result(
		"update vote set 
			  VoteData=?
			, BulletinText=?
			, BulletinVoterSignature=?
			, Confirmation_code=null
			, Confirmation_code_time=null 
		where id_Vote=?;"
		, array('ssss',nice_json_encode($vote_data), $bulletin_text, $base64_encoded_signature, $id_Vote)
	);

	$res= array('ok'=>true,'Подписано'=>$log_time_json);
	echo nice_json_encode($res);
}

$cmd= $_GET['cmd'];
switch ($cmd)
{
	case 'sign-by-cert':
		SignByCert();
		break;
	default:
		exit_bad_request("unknown cmd=$cmd");
}