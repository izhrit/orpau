<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';
require_once '../assets/libs/alib_email_dispatch_to_db.php';

$id_Manager= $_GET['id_Manager'];
if (!is_numeric($id_Manager))
	exit_bad_request("bad numeric _GET['id_Manager']");

$fields= "id_Vote_document
	, id_Email_Message
	, id_Vote
	, Type
	, date_format(dTimeSent,'%d.%m.%Y %H:%i:%s') TimeSent
	, FileName
	, doc_md5
	, Size
	, QuestionTitle
	, EmailType
	, TimeDispatch ";

$from_where= " from ((select 
	  null id_Vote_document
	, em.id_Email_Message
	, em.RecipientId id_Vote
	, em.EmailType Type
	, es.TimeSent dTimeSent
	, null FileName
	, '' doc_md5
	, '' Size
	, null QuestionTitle
	, em.EmailType
	, em.TimeDispatch 
	from email_message em
	inner join email_sent es on em.id_Email_Message=es.id_Email_Message
	where em.RecipientId=$id_Manager and RecipientType='c' )
	union
	(select 
	  d.id_Vote_document
	, null id_Email_Message
	, d.id_Vote
	, d.Vote_document_type Type
	, d.Vote_document_time dTimeSent
	, d.FileName FileName
	, if(d.Body is null,null,md5(uncompress(d.Body))) doc_md5
	, if(d.Body is null,null,length(uncompress(d.Body))) Size
	, q.Title QuestionTitle
	, null EmailType
	, null TimeDispatch
	from vote_document d
	left join vote v on v.id_Vote=d.id_Vote
	left join question q on q.id_Question=v.id_Question
	WHERE d.id_Vote=d.id_Vote 
	";
/*
$union_fields="
	  null id_Vote_document
	, em.id_Email_Message
	, em.RecipientId id_Vote
	, em.EmailType Type
	, date_format(es.TimeSent,'%d.%m.%Y %H:%i') TimeSent
	, null FileName
	, if(es.Message is null,null,md5(uncompress(es.Message))) doc_md5
	, if(es.Message is null,null,length(uncompress(es.Message))) Size
	, null QuestionTitle
	, em.EmailType
	, em.TimeDispatch ";

$union_from="
	from email_message em
	inner join email_sent es on em.id_Email_Message=es.id_Email_Message
	where em.RecipientId=$id_Manager and RecipientType='c' ";

$fields= $union_fields.$union_from." union "." select 
	  d.id_Vote_document
	, null id_Email_Message
	, d.id_Vote
	, d.Vote_document_type Type
	, date_format(d.Vote_document_time,'%d.%m.%Y %H:%i') TimeSent
	, d.FileName FileName
	, if(d.Body is null,null,md5(uncompress(d.Body))) doc_md5
	, if(d.Body is null,null,length(uncompress(d.Body))) Size
	, q.Title QuestionTitle
	, null EmailType
	, null TimeDispatch
";
$from_where= "
	from vote_document d
	left join vote v on v.id_Vote=d.id_Vote
	left join question q on q.id_Question=v.id_Question
	WHERE d.id_Vote=d.id_Vote
";
*/

if (isset($_GET['id_Manager']))
{
	$from_where.= " and v.id_Manager=$id_Manager ";
}

if (isset($_GET['id_Poll']))
{
	$id_Poll= $_GET['id_Poll'];
	if (!is_numeric($id_Poll))
		exit_bad_request("bad numeric _GET['id_Poll']");
	$from_where.= " and q.id_Poll=$id_Poll ";
}

$from_where.= ")) u ";

$filter_rule_builders= array();

$orderBy = 'order by dTimeSent desc, id_Vote_document desc, id_Email_Message desc';
if ($_GET['sidx']!='')
{
	$orderfield = $_GET['sidx'];
	$orderBy = "ORDER BY $orderfield ";
	if ($_GET['sord']=='desc')
	{
		$orderBy.= ' DESC ';
	}
}

$result = execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders, $orderBy);

global $Vote_document_type_description, $email_Template_specs;
foreach ($result['rows'] as $row)
{
	if (!isset($row->FileName))
	{
		$row->FileName = build_FileName_for_SentEmail($row);
		$row->Document=$row->FileName;

		$d= FindDescription($email_Template_specs,'code',$row->Type);
		$document = $d != null ? $d['Наименование_документа'] : "неизвестный тип документа $row->Type";
		$row->Document=$document;
	}
	else
	{
		$d= FindDescription($Vote_document_type_description,'code',$row->Type);
		$document = $d != null ? $d['Наименование_документа'] : "неизвестный тип документа $row->Type";
		$document.=' ('.$row->QuestionTitle.')';
		$row->Document=$document;
	}
}

/*$txt_query= "select count(*) count $union_from";
$rows_count= execute_query($txt_query,array());
$union_rows_count= $rows_count[0]->count;

$limit_size= $_GET['rows'];
$records = $result['records'] + $union_rows_count;

$result['total'] = ceil($records/$limit_size);
$result['records'] = $records;*/

echo nice_json_encode($result);