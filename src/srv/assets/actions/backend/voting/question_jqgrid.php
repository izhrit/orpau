<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	q.id_Question
	,v.VoteData VoteExtra
	,v.VoteTime Time
	,q.Title Title
	,uncompress(q.Extra) QuestionExtra
";
$from_where= "
	from question q
         left join poll p on p.id_Poll = q.id_Poll
         left join manager_group_manager mgm on p.id_Manager_group = mgm.id_Manager_group
         left join sro on p.id_SRO = sro.id_SRO
         left join manager m on
             (p.id_Region = m.id_Region or
             sro.RegNum=m.SRORegNum or
             mgm.id_Manager=m.id_Manager or
             (
             	m.id_Manager=m.id_Manager and 
             	p.id_Manager_group is Null and 
             	p.id_SRO is NULL and 
             	p.id_Region is NULL
			)) and
			p.Status != 'n'
		left join vote v on q.id_Question = v.id_Question and v.id_Manager = m.id_Manager
";

if (isset($_GET['id_Manager']))
{
	$id_Manager= $_GET['id_Manager'];
	if (!is_numeric($id_Manager))
		exit_bad_request("bad numeric _GET['id_Manager']");
	$from_where.= " where m.id_Manager=$id_Manager ";
}

if (isset($_GET['id_Poll']))
{
	$id_Poll= $_GET['id_Poll'];
	if (!is_numeric($id_Poll))
		exit_bad_request("bad numeric _GET['id_Poll']");
	$from_where.= " and p.id_Poll=$id_Poll ";
}

$filter_rule_builders= array(
	'id_Question'=> 'query_field'
	,'Time' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND v.VoteTime like '$data%'";
	}
	,'Title'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND q.Title like '$data%'";
	}
);

$orderBy = '';
if ($_GET['sidx']!='')
{
	$orderfield = $_GET['sidx'];
	$orderBy = "ORDER BY $orderfield ";
	if ($_GET['sord']=='desc')
	{
		$orderBy.= ' DESC ';
	}
}

$result = execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders, $orderBy);
foreach ($result['rows'] as $row)
{
	if (isset($row->QuestionExtra) && null!=$row->QuestionExtra)
		$row->QuestionExtra = json_decode($row->QuestionExtra);
	if (isset($row->VoteExtra) && null!=$row->VoteExtra)
		$row->VoteExtra = json_decode($row->VoteExtra);

}

echo nice_json_encode($result);