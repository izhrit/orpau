<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';
require_once '../assets/helpers/SMSTransport.php';

require_once '../assets/libs/cryptoapi.php';
require_once '../assets/actions/backend/manager/manager_auth.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

require_once '../assets/libs/asp/asp_letters.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

function Prep_Заявление_о_вступлении_в_профсоюз_на_подпись($ау)
{
	ob_start();
	include 'club_agreement.html';
	$заявление= ob_get_contents();
	ob_end_clean();
	return $заявление;
}

function Prep_Письмо_подтверждение_вступления_в_профсоюз($ау,$асп,$подпись_оператора)
{
	ob_start();
	include '../assets/libs/asp/tpl/terminate.html';
	$подтверждение= ob_get_contents();
	ob_end_clean();
	return $подтверждение;
}

function Prep_АСП($ау,$документ,$current_time,$имя_файла_с_подписью_РИТ)
{
	ob_start();
	include '../assets/libs/asp/tpl/asp.html';
	$асп= ob_get_contents();
	ob_end_clean();
	return $асп;
}

function Выполнить_для_ау($id_Manager,$действия_для_ау)
{
	$rows= execute_query(
		'select 
			  m.id_Manager
			, m.LastName Фамилия, m.FirstName Имя, m.MiddleName Отчество
			, m.INN ИНН, m.RegNum
			, m.Phone Телефон, m.EMail EMail
			, m.CorrespondenceAddress Почтовый_адрес
			, r.Name Регион
			, ConfirmationCode
			, ConfirmationCodeTime
		from manager m
		left join region r on m.id_Region = r.id_Region
		where id_Manager=?',
		array('s',$id_Manager));
	$count_rows= count($rows);

	if (1!=$count_rows)
	{
		exit_internal_server_error("found $count_rows managers with id_Manager={$id_Manager}");
	}
	else
	{
		$ау= $rows[0];
		if (null==$ау->Телефон)
		{
			exit_internal_server_error("Phone is null for manager with id={$auth_info->id_Manager}");
		}
		else if (null==$ау->EMail)
		{
			exit_internal_server_error("EMail is null for manager with id={$auth_info->id_Manager}");
		}
		else
		{
			$действия_для_ау($ау,safe_date_create());
		}
	}
}

function Get_Заявление_о_вступлении_в_профсоюз()
{
	global $auth_info;
	Выполнить_для_ау
	(
		$auth_info->id_Manager
		,function($ау, $current_time)
		{
			$заявление= Prep_Заявление_о_вступлении_в_профсоюз_на_подпись($ау);
			write_to_log($заявление);

			$_SESSION['заявление_о_вступлении_в_профсоюз']= (object)array(
				'параметры'=>(object)array('ау'=>$ау)
				,'текст'=>$заявление
			);

			echo nice_json_encode($заявление);
		}
	);
}

function safeSendSmsConfirmationCode($Confirmation_code,$phone)
{
	global $sms_settings;
	if (false==$sms_settings->enabled)
	{
		return false;
	}
	else
	{
		$txt= "{$Confirmation_code} – код подтверждения для подписания заявления о вступлении в ОРПАУ";
		$sms = new SMSTransport($sms_settings);
		write_to_log("send sms to {$phone}: $txt");
		$res= $sms->send($phone,$txt,$sms_settings->FROM);
		write_to_log($res);
		return $res;
	}
}

function Имя_сгенерированного_для_ау_файла($ау,$prefix,$extension)
{
	$name= $ау->Фамилия;
	if (isset($ау->Имя))
		$name.= mb_substr($ау->Имя,0,1);
	if (isset($ау->Отчество))
		$name.= mb_substr($ау->Отчество,0,1);
	return $prefix.$name.$extension;
}

function Вложение_для_ау_filename($filename,$content,$title= null)
{
	$вложение= array(
		'filename'=>$filename
		,'content'=>$content
		,'длина'=>strlen($content)
		,'md5'=>md5($content)
	);
	if (null!=$title)
		$вложение['title']= $title;
	return $вложение;
}

function Вложение_для_ау($ау,$prefix,$extension,$content,$title= null)
{
	$filename= Имя_сгенерированного_для_ау_файла($ау,$prefix,$extension);
	return Вложение_для_ау_filename($filename,$content,$title);
}

function Заявление_в_ОРПАУ($ау,$content)
{
	return Вложение_для_ау($ау,'вОРПАУ.','.txt',$content,'Заявление о вступлении в ОРПАУ');
}

function Отправить_уведомление_о_подписании_заявления_в_ОРПАУ($ау)
{
	global $auth_info;
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		$заявление= Заявление_в_ОРПАУ($ау,$_SESSION['заявление_о_вступлении_в_профсоюз']->текст);
		$response= Отправить_уведомление_о_подписании_документа($connection,$ау,$заявление,'вступление в профсоюз');

		if (false==$response)
		{
			$connection->rollback();
			exit_internal_server_error("Can not send email to {$ау->EMail}");
		}
		else
		{
			$connection->commit();
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $manager->EMail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function Выслать_проверочный_код()
{
	global $auth_info;

	Выполнить_для_ау
	(
		$auth_info->id_Manager
		,function($ау, $current_time)
		{
			global $sms_settings, $email_settings;

			Отправить_уведомление_о_подписании_заявления_в_ОРПАУ($ау);

			$confirmation_code=  (true!=$email_settings->enabled)
				? '1234' : generate_password(4,array('0','1','2','3','4','5','6','7','8','9'));

			safeSendSmsConfirmationCode($confirmation_code,$ау->Телефон);

			$txt_query= "update manager set ConfirmationCode=?, ConfirmationCodeTime=? where id_Manager=?";
			execute_query_no_result($txt_query
				,array('sss',$confirmation_code,date_format($current_time,'Y-m-d\TH:i:s'),$ау->id_Manager));

			$res= array('sms'=>array('number'=>$ау->Телефон, 'time'=>date_format($current_time,'d.m.Y H:i:s')));
			if (false==$sms_settings->enabled)
				$res['test_code']= $confirmation_code;
			echo nice_json_encode($res);
		}
	);
}

function total_minutes($date_diff)
{
	$minutes = $date_diff->days * 24 * 60;
	$minutes += $date_diff->h * 60;
	$minutes += $date_diff->i;
	return $minutes;
}

function prepareSignatureOfOperator($txt)
{
	global $operator_pem_file_path;

	if (null==$operator_pem_file_path)
	{
		return 'Заменитель подписи в тестовом режиме (настройка пути к сертификату НЕ задана)';
	}
	else
	{
		$temp_file = tmpfile();
		try
		{
			$meta_data= stream_get_meta_data($temp_file);
			$temp_file_path = $meta_data['uri'];
			$temp_file_path_sig = $temp_file_path.'.sig';
			fwrite($temp_file, $txt);
			fflush($temp_file);

			try
			{
				openssl_pkcs7_sign($temp_file_path, $temp_file_path_sig, $operator_pem_file_path,
					array($operator_pem_file_path, ""),
					array(),PKCS7_DETACHED | PKCS7_BINARY
				);
				$smime_p7s= file_get_contents($temp_file_path_sig);
				unlink($temp_file_path_sig);
			}
			catch (Exception $ex)
			{
				unlink($temp_file_path_sig);
				throw $ex;
			}
			fclose($temp_file);
		}
		catch (Exception $ex)
		{
			fclose($temp_file);
			throw $ex;
		}
		$signature= smime_p7s_to_sig($smime_p7s);
		return $signature;
	}
}

function Отправить_подтверждение_подписи_заявления_в_ОРПАУ($ау,$current_time)
{
	global $auth_info;
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		$prefix= 'вОРПАУ.';
		$content= $_SESSION['заявление_о_вступлении_в_профсоюз']->текст;
		$документ= Заявление_в_ОРПАУ($ау,$content);
		$имя_файла_с_подписью_РИТ= Имя_сгенерированного_для_ау_файла($ау,$prefix,'.asp.txt.sig');
		$текст_асп= Prep_АСП($ау,$документ,$current_time,$имя_файла_с_подписью_РИТ);
		$асп= Вложение_для_ау($ау,$prefix,'.asp.txt',$текст_асп);
		$подпись_оператора= Вложение_для_ау_filename($имя_файла_с_подписью_РИТ,prepareSignatureOfOperator($текст_асп));

		$letter= (object)array('subject'=>'Подписание документа "Заявление о вступлении в ОРПАУ"');
		$letter->body_txt= Prep_Письмо_подтверждение_вступления_в_профсоюз($ау,$асп,$подпись_оператора);
		$letter->attachments= array($асп,$подпись_оператора);

		$manager_name= "{$auth_info->Фамилия} {$auth_info->Имя} {$auth_info->Отчество}";
		$response = Dispatch_Email_Message($connection, $letter, $ау->EMail, $manager_name,
			'вступление в профсоюз', $ау->id_Manager);
		if (false==$response)
		{
			$connection->rollback();
			exit_internal_server_error("Can not send email to {$ау->EMail}");
		}
		else
		{
			$connection->commit();
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $ау->EMail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function Проверочный_код_отвергнут($id_Manager,$msg_why)
{
	$txt_query= "update manager set ConfirmationCode=null, ConfirmationCodeTime=null where id_Manager=?";
	execute_query_no_result($txt_query,array('s',$id_Manager));
	echo nice_json_encode(array('ok'=>false,'why'=>$msg_why));
}

function Обработать_проверочный_код($received_code)
{
	global $auth_info;
	Выполнить_для_ау
	(
		$auth_info->id_Manager
		,function($ау, $current_time) use ($received_code)
		{
			global $email_settings;
			$Confirmation_code_time= date_create_from_format('Y-m-d H:i:s',$ау->ConfirmationCodeTime);
			$diff= date_diff($current_time,$Confirmation_code_time);

			$max_minutes_to_confirm_sms_code= 30;
			if (total_minutes($diff)>$max_minutes_to_confirm_sms_code)
			{
				Проверочный_код_отвергнут($ау->id_Manager,"Код подтверждения предъявлен слишком поздно (спустя более $max_minutes_to_confirm_sms_code минут )!");
			}
			else if ($received_code!=$ау->ConfirmationCode)
			{
				Проверочный_код_отвергнут($ау->id_Manager,'Указанный код подтверждения не соответствует высланному!');
			}
			else
			{
				execute_query_no_result(
					"update manager set ClubAgreementText=?, ConfirmationCode=null, ConfirmationCodeTime=null 
					 where id_Manager=?;"
					,array('ss', $_SESSION['заявление_о_вступлении_в_профсоюз']->текст, $ау->id_Manager)
				);

				Отправить_подтверждение_подписи_заявления_в_ОРПАУ($ау,$current_time);

				$res= array('ok'=>true,'Подписано'=>date_format($current_time,'d.m.Y H:i:s'));
				echo nice_json_encode($res);
			}
		}
	);
}

function GetSignedAgreement ($id_Manager)
{
	$rows = execute_query("select ClubAgreementText from manager where id_Manager=?"
		, array('s', $id_Manager));
	$count_rows= count($rows);
	if(1!=count($rows)){
		exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
	} else {
		echo nice_json_encode($rows[0]->ClubAgreementText);
	}
}

CheckMandatoryGET('cmd');

switch ($_GET['cmd'])
{
	case 'get-union-application-to-sign':
		write_to_log('cmd:get-union-application-to-sign');
		Get_Заявление_о_вступлении_в_профсоюз();
		break;
	case 'throw-code':
		write_to_log('cmd:throw-code');
		Выслать_проверочный_код();
		break;
	case 'sign':
		write_to_log('cmd:sign');
		$post_data= safe_POST($_POST);
		Обработать_проверочный_код($post_data['code']);
		break;
	case 'get-signed-agreement':
		CheckMandatoryGET('id');
		GetSignedAgreement($_GET['id']);
		break;
}