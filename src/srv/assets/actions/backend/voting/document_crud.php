<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

class Fill_poll_row_query_builder extends Fill_row_query_builder
{
	public function comma_separated_params($poll)
	{

	}
}
class Document_crud extends Base_crud
{
	function create($doc)
	{
		echo '{ "ok": true }';
	}

	function read($id_doc)
	{
		if (!isset($_GET['type']) || 'doc'==$_GET['type'])
		{
			$txt_query = "select
				id_Vote_document
				,Vote_document_type
				,Vote_document_time
				,FileName
				,uncompress(Body) Body
			from vote_document
			where id_Vote_document= ?";
			$rows = execute_query($txt_query, array('s', $id_doc));
			echo nice_json_encode($rows[0]);
		}
		else
		{
			$txt_query="select
				em.id_Email_Message
				,uncompress(em.Details) Details
			from email_message em
			inner join email_sent es on em.id_Email_Message=es.id_Email_Message
			where em.id_Email_Message=?";
			$rows = execute_query($txt_query, array('s', $id_doc));
			$row= $rows[0];
			$details= json_decode($row->Details);
			$row->Body= $details->Body;
			$row->Subject= $details->Subject;
			echo nice_json_encode($row);
		}
	}

	function update($id_doc, $doc)
	{
		echo '{ "ok": true }';
	}

	function delete($id_docs)
	{
		echo '{ "ok": true }';
	}
}

$crud= new Document_crud();
$crud->process_cmd();