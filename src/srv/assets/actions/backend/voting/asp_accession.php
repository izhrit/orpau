<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';

require_once '../assets/libs/cryptoapi.php';
require_once '../assets/actions/backend/manager/manager_auth.php';
require_once '../assets/libs/alib_email_dispatch_to_db.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

function соглашение_to_fix()
{
	global $url_offer_asp;
	$url = $url_offer_asp;
	$path_parts = pathinfo($url);

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_NOBODY, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_FILETIME, true);

	curl_setopt($curl, CURLOPT_HEADER, true);

	$result = curl_exec($curl);
	if ($result === false) {
		die (curl_error($curl));
	}

	$timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
	$file_create_date = date("Y-m-d H:i:s", $timestamp);
	$file_size = curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
	$md5 = md5_file($url);

	return (object)array(
		'filename'=>$path_parts['basename']
		,'file_create_date'=>$file_create_date
		,'file_size'=>$file_size
		,'md5'=>$md5
		,'url'=>$url
	);
}

function Prep_Заявление_о_присоединении_на_подпись($ау,$сертификат,$соглашение)
{
	ob_start();
	include 'asp_accession.html';
	$заявление= ob_get_contents();
	ob_end_clean();
	return $заявление;
}

function Get_Заявление_о_присоединении_на_подпись($сертификат)
{
	$сертификат =(object)$сертификат;
	$subject= GetDnFields($сертификат->Subject);
	if (!isset($subject->ИНН))
		exit_bad_request("skipped ИНН in Subject!",$сертификат);

	$rows= execute_query(
		'select 
			  m.LastName   Фамилия
			, m.FirstName  Имя
			, m.MiddleName Отчество
			, m.INN        ИНН
			, m.Phone      Телефон
			, m.EMail      EMail
			, r.Name       Регион
		from manager m
		left join region r on m.id_Region = r.id_Region
		where INN=?',
		array('s',$subject->ИНН));
	$count_rows= count($rows);

	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with INN={$subject->ИНН}");
	}
	else
	{
		$ау= $rows[0];
		$соглашение= соглашение_to_fix();

		$заявление= Prep_Заявление_о_присоединении_на_подпись($ау,$сертификат,$соглашение);

		$_SESSION['заявление_о_присоединении_к_АСП']= (object)array(
			'параметры'=>(object)array('ау'=>$ау,'сертификат'=>$сертификат,'соглашение'=>$соглашение)
			,'текст'=>$заявление
		);

		echo nice_json_encode($заявление);
	}
}

function Зарегистрировать_подпись_заявления_о_присоединении_к_АСП($base64_encoded_signature)
{
	if (!isset($_SESSION['заявление_о_присоединении_к_АСП']))
		exit_bad_request("undefined session заявление_о_присоединении_к_АСП");

	$заявление_о_присоединении_к_АСП= $_SESSION['заявление_о_присоединении_к_АСП'];
	$_SESSION['заявление_о_присоединении_к_АСП']= null;
	unset($_SESSION['заявление_о_присоединении_к_АСП']);

	if (!CheckTokenSignature($заявление_о_присоединении_к_АСП->текст,$base64_encoded_signature))
	{
		exit_unauthorized("can not check signature!");
	}
	else
	{
		write_to_log('signature is checked!');

		global $auth_info;

		$txt_query = "update manager set AgreementText=?, AgreementSignature=?, PhoneInAgreement=? where id_Manager=?";
		execute_query_no_result($txt_query, array('sssi'
			,$заявление_о_присоединении_к_АСП->текст
			,$base64_encoded_signature
			,$заявление_о_присоединении_к_АСП->параметры->ау->Телефон
			,$auth_info->id_Manager
		));

		//Send_Agreement_letter($id_Manager, $manager, $token);

		echo '{ "ok": true }';
	}
}

function Send_Agreement_letter($id_Manager, $manager, $token) {
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		$email = $manager->EMail;
		$letter= (object)array('subject'=>'Заявление о присоединении к соглашению об использовании аналога собственноручной подписи');
		$letter->body_txt= $token;

		$manager_name= $manager->Фамилия.' '.$manager->Имя.' '.$manager->Отчество;
		$response = Dispatch_Email_Message($connection, $letter, $email, $manager_name,
			'подписание соглашения АСП', $id_Manager);
		if (false==$response)
		{
			$connection->rollback();
			exit_internal_server_error("Can not send email to $email");
		}
		else
		{
			$connection->commit();
			echo '{ "ok": true }';
			exit;
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
		ProcessMySqlException_check_duplicate_login($ex, $manager->EMail);
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function VerifyAgreementSignature ($id_Manager)
{
	global $base_cryptoapi_url, $base_cryptoapi_auth;

	$txt_query= 'select AgreementText, AgreementSignature from manager where id_Manager=?';
	$rows = execute_query($txt_query, array('s', $id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
	}
	else
	{
		$row = $rows[0];
		$cryptoapi= new CryptoAPI((object)array(
			'base_url'=>$base_cryptoapi_url
			,'auth'=>$base_cryptoapi_auth
		));
		$result= $cryptoapi->VerifySignatureFromCapicom((object)array(
			'document'=>$row->AgreementText
			,'base64_encoded_signature'=>$row->AgreementSignature
		));
		echo nice_json_encode($result);
	}
}

function GetSignedAgreement ($id_Manager)
{
	$txt_query= 'select AgreementText from manager where id_Manager=?';
	$rows = execute_query($txt_query, array('s', $id_Manager));
	$count_rows= count($rows);
	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
	}
	else
	{
		echo nice_json_encode($rows[0]->AgreementText);
	}
}

CheckMandatoryGET('cmd');

switch ($_GET['cmd'])
{
	case 'get-application-for-accession-to-sign':
		write_to_log('cmd:get-application-for-accession-to-sign');
		$data_to_auth= safe_POST($_POST);
		CheckMandatoryPOST('Subject',$data_to_auth);
		CheckMandatoryPOST('Issuer',$data_to_auth);
		CheckMandatoryPOST('SerialNumber',$data_to_auth);
		Get_Заявление_о_присоединении_на_подпись($data_to_auth);
		break;
	case 'sign':
		write_to_log('cmd:sign');
		$d= safe_POST($_POST);
		CheckMandatoryPOST('base64_encoded_signature',$d);
		Зарегистрировать_подпись_заявления_о_присоединении_к_АСП($d['base64_encoded_signature']);
		break;
	case 'verify-signature':
		CheckMandatoryGET('id');
		VerifyAgreementSignature($_GET['id']);
		break;
	case 'get-signed-application-for-accession':
		CheckMandatoryGET('id');
		GetSignedAgreement($_GET['id']);
		break;
}