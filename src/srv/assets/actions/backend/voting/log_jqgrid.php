<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

$fields= "
	l.id_Vote_log
	,l.id_Vote
	,l.Vote_log_type type
	,l.Vote_log_time time
	,l.body body
";
$from_where= "
	from vote_log l
	left join vote v on v.id_Vote=l.id_Vote
	left join question q on q.id_Question=v.id_Question
	WHERE l.id_Vote=l.id_Vote 
";

if (isset($_GET['id_Manager']))
{
	$id_Manager= $_GET['id_Manager'];
	if (!is_numeric($id_Manager))
		exit_bad_request("bad numeric _GET['id_Manager']");
	$from_where.= " and v.id_Manager=$id_Manager ";
}

if (isset($_GET['id_Poll']))
{
	$id_Poll= $_GET['id_Poll'];
	if (!is_numeric($id_Poll))
		exit_bad_request("bad numeric _GET['id_Poll']");
	$from_where.= " and q.id_Poll=$id_Poll ";
}

$filter_rule_builders= array(
	'BulletinText'=> 'std_filter_rule_builder'
);

$orderBy = 'ORDER BY time desc ';
if ($_GET['sidx']!='')
{
	$orderfield = $_GET['sidx'];
	$orderBy = "ORDER BY $orderfield ";
	if ($_GET['sord']=='desc')
	{
		$orderBy.= ' DESC ';
	}
}

$result = execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders, $orderBy);

global $Vote_log_type_description;
foreach ($result['rows'] as $row)
{
	$d= FindDescription($Vote_log_type_description,'code',$row->type);
	$event = $d != null ? $d['text'] : "случилось событие закодированное как $row->type";

	if($row->body != null){
		$event.= ' '.$row->body;
	}
	$row->event=$event;
}
echo nice_json_encode($result);