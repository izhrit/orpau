<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/time.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

class Fill_vote_row_query_builder extends Fill_row_query_builder
{
	public function comma_separated_params($vote)
	{
		$this->comma_par('id_Manager',$vote->id_Manager);
		$this->comma_par('id_Question',$vote->id_Question);
		$vote_data = nice_json_encode($vote->VoteExtra);
		$this->comma_par('VoteData',$vote_data);

		$vote_time = date_format(safe_date_create(),'Y-m-d\TH:i:s');
		$this->comma_par('VoteTime',$vote_time);
	}
}
class Vote_crud extends Base_crud
{
	function create($vote)
	{
		$qb= new Fill_vote_row_query_builder('insert into vote set ');
		$qb->comma_separated_params((object)$vote);
		$id_Vote= $qb->execute_query_get_last_insert_id();

		PrepareLog($id_Vote,'Выбран ответ',$vote['Title']);
		echo nice_json_encode(array(
			'ok'=>true
			,'id_Vote'=>$id_Vote
		));
	}

	function read($id_vote)
	{
		echo '{ "ok": true }';
	}

	function update($id_Vote, $vote)
	{
		$qb= new Fill_vote_row_query_builder('update vote set ');
		$qb->comma_separated_params((object)$vote);
		$qb->parametrized("\r\nwhere id_Vote=?",$id_Vote);
		$affected= $qb->execute_query_get_affected_rows();

		PrepareLog($id_Vote,'Выбран ответ',$vote['Title']);

		echo '{ "ok": true }';
	}

	function delete($id_votes)
	{
		echo '{ "ok": true }';
	}
}

$crud= new Vote_crud();
$crud->process_cmd();