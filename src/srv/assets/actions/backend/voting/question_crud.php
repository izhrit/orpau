<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

class Question_crud extends Base_crud
{
	function create($question)
	{
		echo '{ "ok": true }';
	}

	function read($id_question)
	{
		write_to_log('read');
		global $poll_status_description;

		$txt_query= 'select 
			q.id_Question
			,v.VoteData VoteExtra
			,v.VoteTime Time
			,q.Title Title
			,uncompress(q.Extra) QuestionExtra
			,p.DateFinish
			,v.id_Vote
			,uncompress(p.ExtraColumns) PollExtra
			,p.Status
		from question q
		left join poll p on p.id_Poll = q.id_Poll
		left join vote v on q.id_Question = v.id_Question and v.id_Manager=?
		where q.id_Question=?';

		$id_Manager = null;
		if (isset($_GET['id_Manager']))
		{
			$id_Manager= $_GET['id_Manager'];
			if (!is_numeric($id_Manager))
				exit_bad_request("bad numeric _GET['id_Manager']");
		}

		$rows = execute_query($txt_query, array('ss', $id_Manager, $id_question));

		if (0==count($rows))
		{
			exit_not_found("can not find question id_Question=$id_question");
		}
		else
		{
			$row = $rows[0];
			write_to_log_named('Status',$row->Status);

			$vote_extra = json_decode($row->VoteExtra);
			$date_finish= date_create_from_format('Y-m-d H:i:s',$row->DateFinish);
			write_to_log_named('date_finish',date_format($date_finish, 'd.m.Y H:i'));
			$date_current = safe_date_create();
			write_to_log_named('date_current',date_format($date_current, 'd.m.Y H:i'));
			$question_extra = json_decode($row->QuestionExtra);

			$row_status = $row->Status;
			$d = FindDescription($poll_status_description, 'code', $row->Status);
			$poll_status=$d['descr'];

			if($row_status!='n' && $row_status!='p' && $row_status!='c' && $row_status!='e' && $date_current > $date_finish) {
				$poll_status = 'Время для голосования прошло'; //todo подумай
			}

			$questionRow = array(
				'id_Question'=>$row->id_Question
				,'VoteExtra'=>$vote_extra
				,'Time'=>$row->Time
				,'Title'=>$row->Title
				,'QuestionExtra'=>$question_extra
				,'Status'=>$poll_status
				,'id_Vote'=>$row->id_Vote
				,'DateFinish'=>date_format($date_finish, 'd.m.Y H:i')
			);

			write_to_log($questionRow);

			echo nice_json_encode($questionRow);
		}
	}

	function update($id_question, $question)
	{
		echo '{ "ok": true }';
	}

	function delete($id_questions)
	{
		echo '{ "ok": true }';
	}
}

$crud= new Question_crud();
$crud->process_cmd();