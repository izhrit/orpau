<?

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	q.id_Question
	, v.VoteData VoteExtra
	, v.VoteTime Time
	, q.Title Title
	, uncompress(q.Extra) QuestionExtra
	
	, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) FullName
	, m.LastName LastName
	, m.FirstName FirstName
	, m.MiddleName MiddleName
	, m.INN INN
	, sro2.Name SRO
	, r.Name Region
	, GROUP_CONCAT(mg.Name separator ', ') 'Group'
	, p.id_Poll
";
$from_where= "
	from question q
	left join poll p on p.id_Poll = q.id_Poll and p.Status != 'n'
	left join manager_group_manager mgm on p.id_Manager_group = mgm.id_Manager_group
	left join sro on p.id_SRO = sro.id_SRO
	left join manager m on
	(p.id_Region = m.id_Region or
	sro.RegNum=m.SRORegNum or
	mgm.id_Manager=m.id_Manager or
	(
		m.id_Manager=m.id_Manager and
		p.id_Manager_group is Null and
		p.id_SRO is NULL and
		p.id_Region is NULL
	))
	 
	left join region r on r.id_Region=m.id_Region
	left join manager_group_manager mgm2 on m.id_Manager = mgm2.id_Manager
	left join manager_group mg on mgm2.id_Manager_group = mg.id_Manager_group
	left join sro sro2 on m.SRORegNum = sro2.RegNum

	inner join vote v on q.id_Question = v.id_Question and v.id_Manager = m.id_Manager and v.VoteData like '%Да%' 
	where q.id_Question=q.id_Question
";

$filter_rule_builders= array(
	'id_Question'=> 'query_field'
	,'FullName' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND m.LastName like '$data%' OR m.FirstName like '$data%' OR m.MiddleName like '$data%' ";
	}
	,'INN' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND m.INN like '$data%' ";
	}
	,'SRO' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND sro2.Name like '$data%' ";
	}
	,'Region' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND r.Name like '$data%' ";
	}
	,'Group' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND mg.Name like '$data%' ";
	}
	,'Time' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND v.VoteTime like '$data%'";
	}
	,'Title'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND q.Title like '$data%'";
	}
	, 'id_Poll' => function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND p.id_Poll like '$data%'";
	}
);

$groupBy = "group by q.id_Question, m.id_Manager, v.VoteData, v.VoteTime ";
$orderBy = $groupBy.' order by v.VoteTime ';
if ($_GET['sidx']!='')
{
	$orderfield = $_GET['sidx'];
	$orderBy = $groupBy."ORDER BY $orderfield ";
	if ($_GET['sord']=='desc')
	{
		$orderBy.= ' DESC ';
	}
}

$result = execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders, $orderBy);
foreach ($result['rows'] as $row)
{
	if (isset($row->QuestionExtra) && null!=$row->QuestionExtra)
		$row->QuestionExtra = json_decode($row->QuestionExtra);
	if (isset($row->VoteExtra) && null!=$row->VoteExtra)
		$row->VoteExtra = json_decode($row->VoteExtra);
}

echo nice_json_encode($result);