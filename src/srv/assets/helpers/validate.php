<?php

function exit_internal_server_error($error_text)
{
	header("HTTP/1.1 500 Internal Server Error");
	echo 'Internal Server Error';
	exit_with_error($error_text);
}

function exit_not_found($error_text)
{
	header("HTTP/1.1 404 Not Found");
	echo 'Not Found';
	exit_with_error($error_text);
}

function exit_bad_request($error_text,$post_data= null)
{
	header("HTTP/1.1 400 Bad Request");
	echo 'bad request';
	exit_with_error($error_text,$post_data);
}

function exit_unauthorized($error_text)
{
	header("HTTP/1.1 401 Unauthorized");
	echo 'Unauthorized';
	exit_with_error($error_text);
}

function exit_with_error($error_text,$post_data= null)
{
	write_to_log($error_text);
	if (function_exists('write_to_log_auth_info'))
		write_to_log_auth_info();
	write_to_log('$_GET:');
	write_to_log($_GET);
	if (null!=$post_data)
	{
		write_to_log('post data:');
		write_to_log($post_data);
	}
	exit;
}

function CheckMandatory($name,$fields,$fields_name)
{
	if (!isset($fields[$name]))
		exit_bad_request('skipped '.$fields_name.'['.$name.']');
}

function CheckMandatoryGET($name)
{
	CheckMandatory($name,$_GET,'_GET');
}

function CheckMandatoryPOST($name,$post_data)
{
	CheckMandatory($name,$post_data,'_POST');
}

function CheckMandatoryGET_variants($name,$variants)
{
	CheckMandatoryGET($name);
	$variant= $_GET[$name];
	if (!in_array($variant,$variants))
		exit_bad_request("unexpected _GET['$name']=$variant");
	return $variant;
}

function CheckMandatoryGET_get_variant_by_name($name,$variants)
{
	CheckMandatoryGET($name);
	$variant= $_GET[$name];
	if (!isset($variants[$variant]))
		exit_bad_request("unexpected _GET['$name']=$variant");
	return $variants[$variant];
}

function CheckMandatoryGET_execute_action_variant($name,$action_variants)
{
	$action= CheckMandatoryGET_get_variant_by_name($name,$action_variants);
	$action();
}

function CheckMandatoryGET_id($name)
{
	CheckMandatoryGET($name);
	$id= $_GET[$name];
	if (!is_numeric($id))
		exit_bad_request("bad numeric _GET['$name']");
	return $id;
}
