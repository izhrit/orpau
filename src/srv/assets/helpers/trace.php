<?


function trace_txt($txt)
{
	global $test_trace;
	if (!isset($test_trace) || !$test_trace)
		echo date('m/d/Y h:i:s a: ', time());
	echo $txt;
	echo "\r\n";
	return $txt;
}

function trace($data)
{
	return trace_txt(is_string($data) ? $data : print_r($data,true));
}
