<?php

class SMSTransport
{
	private $response;
	private $config;

	function __construct($config)
	{
		$this->config= $config;
	}

	public function send($phone,$text,$from)
	{
		$params['from'] = $from;
		$params['to'] = $phone;
		$params['text'] = $text;
		$response = $this->request('send', $params);
		return $response;
	}

	protected function request($action, $params = array())
	{
		$params['user'] = $this->config->HTTPS_LOGIN;
		$params['password'] = md5($this->config->HTTPS_PASSWORD);
		$params['answer'] = 'json';

		$url = $this->config->HTTPS_ADDRESS . $action . '/?' . http_build_query($params);
		
		$request = curl_init($url);
		curl_setopt($request,CURLOPT_CONNECTTIMEOUT,10);
		curl_setopt($request,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($request,CURLOPT_TIMEOUT,60);
		curl_setopt($request,CURLOPT_USERAGENT,'Generic Client');
		
		$response = curl_exec($request);
		
		curl_close($request);
		
		return $this->response = json_decode($response, true);
	}
}

