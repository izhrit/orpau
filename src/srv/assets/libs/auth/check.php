<?php

function CheckAuth()
{
	session_start();
	if (!isset($_SESSION['auth_info']))
	{
		header("HTTP/1.1 401 Unauthorized");
		write_to_log("wrong access without auth_info");
		write_to_log($_SESSION);
		exit;
	}
	return $_SESSION['auth_info'];
}