<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/time.php';

global $email_Recipient_specs, $email_Template_specs;
//todo вынести
$email_Recipient_specs= array
(
	 array('table'=>'manager', 'code'=>'m')
	,array('table'=>'vote', 'code'=>'c')
);

$email_Template_specs= array
(
	 array('table'=>'manager', 'code'=>'p', 'descr'=>'смена пароля АУ')
	,array('table'=>'manager', 'code'=>'b', 'descr'=>'блокирование пароля АУ')
	,array('table'=>'manager', 'code'=>'e', 'descr'=>'подписание соглашения АСП')

	,array('table'=>'manager', 'code'=>'x', 'descr'=>'уведомление о вступлении в профсоюз')
	,array('table'=>'manager', 'code'=>'d', 'descr'=>'вступление в профсоюз')

	,array('table'=>'vote', 'code'=>'n', 'descr'=>'о новом голосовании', 'Наименование_документа'=>'Уведомление о голосовании', 'FileName_prefix'=>'N')
	,array('table'=>'vote', 'code'=>'t', 'descr'=>'голосование обновлено', 'Наименование_документа'=>'Уведомление о продолжении', 'FileName_prefix'=>'T')
	,array('table'=>'vote', 'code'=>'r', 'descr'=>'об итогах голосования', 'Наименование_документа'=>'Уведомление об итогах', 'FileName_prefix'=>'R')
	,array('table'=>'vote', 'code'=>'l', 'descr'=>'голосование отменено', 'Наименование_документа'=>'Уведомление об отмене', 'FileName_prefix'=>'L')
	,array('table'=>'vote', 'code'=>'a', 'descr'=>'о начале подписания', 'Наименование_документа'=>'Уведомление о голосовании', 'FileName_prefix'=>'I')
	,array('table'=>'vote', 'code'=>'s', 'descr'=>'о подписании', 'Наименование_документа'=>'Уведомление о подписи', 'FileName_prefix'=>'G')
	,array('table'=>'vote', 'code'=>'o', 'descr'=>'голосование приостановлено', 'Наименование_документа'=>'Уведомление о приостановке', 'FileName_prefix'=>'P')
	,array('table'=>'vote', 'code'=>'u', 'descr'=>'голосование продолжено', 'Наименование_документа'=>'Уведомление о продолжении', 'FileName_prefix'=>'T')
);

function FindByField($description_array,$key_name,$key_value)
{
    foreach ($description_array as $d)
    {
        if ($key_value==$d[$key_name])
            return $d;
    }
    return null;
}

function Find_Email_spec($email_desc)
{
    global $email_Template_specs;
    global $email_Recipient_specs;

    $Email_spec= FindByField($email_Template_specs,'descr',$email_desc);
    if (null!=$Email_spec)
    {
        if (!isset($Email_spec['Recipient']))
        {
            $Email_spec['Recipient']= FindByField($email_Recipient_specs,'table',$Email_spec['table']);
        }
    }
    return $Email_spec;
}

function Dispatch_Email_Message($connection, $letter, $to_email, $to_name, $tpl_descr, $RecipientId)
{
	$email_spec= Find_Email_spec($tpl_descr);
	if (null==$email_spec)
	{
		write_to_log("wrong email template descriptor: \"{$tpl_descr}\"");
		return false;
	}
	else
	{
		$EmailType= $email_spec['code'];
		$RecipientType= $email_spec['Recipient']['code'];
		$Details= array('Subject'=>$letter->subject, 'Receiver'=>$to_name, 'Body'=>$letter->body_txt);
		$dispatch_time= safe_date_create();
		$dispatch_time_sql= date_format($dispatch_time,'Y-m-d\TH:i:s');
		$json_Details= nice_json_encode($Details);
		$txt_query= "insert into email_message set
			  RecipientEmail=?, RecipientId=?, RecipientType=?
			, EmailType=?, TimeDispatch=?, Details=compress(?);";
		$id_Email_Message= $connection->execute_query_get_last_insert_id($txt_query,
			array('ssssss'
			, $to_email,        $RecipientId,  $RecipientType
			, $EmailType, $dispatch_time_sql
			, $json_Details));
		if (isset($letter->attachments))
		{
			$txt_query= "insert into email_attachment set id_Email_Message=?, FileName=?, Content=?;";
			foreach ($letter->attachments as $a)
				$connection->execute_query($txt_query,array('sss', $id_Email_Message, $a['filename'], $a['content']));
		}
		$txt_query= "insert into email_tosend set id_Email_Message=?, TimeDispatch=?;";
		$connection->execute_query($txt_query,array('ss', $id_Email_Message, $dispatch_time_sql));

		return (object)array('EmailType'=>$EmailType
		, 'TimeDispatch'=> date_format($dispatch_time,'d.m.Y H:i:s')
		, 'id_Email_Message'=>$id_Email_Message
		);
	}
}