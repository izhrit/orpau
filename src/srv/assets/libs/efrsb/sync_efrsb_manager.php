<?php

require_once '../assets/helpers/time.php';
require_once "../assets/helpers/db.php";

global $regions_by_name;
$regions_by_name= array();

function get_id_Region($region_name)
{
	if (null==$region_name)
	{
		return null;
	}
	else
	{
		global $regions_by_name;
		if (isset($regions_by_name[$region_name]))
		{
			return $regions_by_name[$region_name];
		}
		else
		{
			$txt_query= 'select id_Region from region where Name=?';
			$rows= execute_query($txt_query,array('s',$region_name));
			$count_rows= count($rows);
			if (1==$count_rows)
			{
				$id_Region= $rows[0]->id_Region;
				$regions_by_name[$region_name]= $id_Region;
				return $id_Region;
			}
			else if (0==$count_rows)
			{
				$txt_query= 'insert into region set Name=?;';
				$id_Region= execute_query_get_last_insert_id($txt_query,array('s',$region_name));
				$regions_by_name[$region_name]= $id_Region;
				return $id_Region;
			}
			else
			{
				throw new Exception("got $count_rows regions with Name=$region_name");
			}
		}
	}
}

function insert_managers($managers)
{
	$res= -1;
	foreach ($managers as $m)
	{
		if ($res<$m->Revision)
			$res= $m->Revision;
		$id_Region= get_id_Region($m->Region);

		$txt_query ="INSERT INTO manager
			(FirstName, LastName, MiddleName, INN, ArbitrManagerID, 
			CorrespondenceAddress, Revision, SRORegNum, SRORegDate, id_Region)
			VALUES(?,?,?,?,?,?,?,?,?,?)
			ON DUPLICATE KEY UPDATE 
			FirstName=?, LastName=?, MiddleName=?, INN=?, ArbitrManagerID=?, 
			CorrespondenceAddress=?, Revision=?, SRORegNum=?, SRORegDate=?, id_Region=?";
		execute_query($txt_query, array("ssssssssssssssssssss",
			$m->FirstName,$m->LastName,$m->MiddleName,
			$m->INN,$m->ArbitrManagerID,$m->CorrespondenceAddress,$m->Revision,$m->SRORegNum,$m->SRORegDate,$id_Region,
			$m->FirstName,$m->LastName,$m->MiddleName,
			$m->INN,$m->ArbitrManagerID,$m->CorrespondenceAddress,$m->Revision,$m->SRORegNum,$m->SRORegDate,$id_Region
		));
	}
	$count_inserted = count($managers);
	echo job_time() . " inserted $count_inserted managers\r\n";
	return $res;
}

function request_efrsb_managers($max_revision, $portion_size)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/manager?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt_array($curl, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request managers (code $httpcode for url \"$url\")");

	$managers= json_decode($curl_response);

	if (null===$managers)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_managers= count($managers);
	echo job_time() . " .. got $count_managers managers.\r\n";

	return $managers;
}

function sync_managers($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from manager ..\r\n";
	$rows= execute_query('SELECT MAX(m.Revision) AS max_revision from manager m;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . " .. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('10 minutes'));

	$inserted= 0;
	for ($managers= request_efrsb_managers($max_revision, $portion_size), $managers_count= count($managers);
		 0!=$managers_count;
		 $managers= request_efrsb_managers($max_revision, $portion_size), $managers_count= count($managers))
	{
		$max_revision= insert_managers($managers);
		$inserted+= $managers_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==$managers_count)
		echo job_time() . " no managers to insert!\r\n";
}