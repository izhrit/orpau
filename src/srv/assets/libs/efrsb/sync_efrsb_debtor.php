<?php

require_once '../assets/helpers/time.php';
require_once "../assets/helpers/db.php";
require_once "../assets/helpers/json.php";

function insert_debtors($debtors)
{
	$res= -1;
	foreach ($debtors as $d)
	{
		if ($res<$d->Revision)
			$res= $d->Revision;

		$LegalAdress= !isset($d->LegalAdress)?'':$d->LegalAdress;
		$LegalCaseList= !isset($d->LegalCaseList)?'':nice_json_encode($d->LegalCaseList);

		$txt_query =	"INSERT INTO debtor".
			" (BankruptId, Revision, Name, INN, OGRN, SNILS, ArbitrManagerID, LegalAdress, LegalCaseList)".
			"VALUES(?,?,?,?,?,?,?,?,?)".
			"ON DUPLICATE KEY UPDATE BankruptId=?, Revision=?, Name=?, INN=?,
				OGRN=?, SNILS=?, ArbitrManagerID=?, LegalAdress=?, LegalCaseList=?";
		execute_query($txt_query, array
			(
				"ssssssssssssssssss",
				$d->BankruptId, $d->Revision, $d->Name, $d->INN, $d->OGRN, $d->SNILS, $d->ArbitrManagerID, $LegalAdress, $LegalCaseList,
				$d->BankruptId, $d->Revision, $d->Name, $d->INN, $d->OGRN, $d->SNILS, $d->ArbitrManagerID, $LegalAdress, $LegalCaseList
			));
	}
	$count_inserted = count($debtors);
	echo job_time() . " inserted $count_inserted debtors\r\n";
	return $res;
}

function request_efrsb_debtors($max_revision, $portion_size)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/debtor?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt_array($curl, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request debtors (code $httpcode for url \"$url\")");

	$debtors= json_decode($curl_response);

	if (null===$debtors)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_debtors= count($debtors);
	echo job_time() . " .. got $count_debtors debtors.\r\n";

	return $debtors;
}

function sync_debtors($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from debtor ..\r\n";
	$rows= execute_query('SELECT MAX(d.Revision) AS max_revision from debtor d;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . " .. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('10 minutes'));

	$inserted= 0;
	for ($debtors= request_efrsb_debtors($max_revision, $portion_size), $debtors_count= count($debtors);
		 0!=$debtors_count;
		 $debtors= request_efrsb_debtors($max_revision, $portion_size), $debtors_count= count($debtors))
	{
		$max_revision= insert_debtors($debtors);
		$inserted+= $debtors_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==$debtors_count)
		echo job_time() . " no debtors to insert!\r\n";
}