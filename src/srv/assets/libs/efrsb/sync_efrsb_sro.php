<?php

require_once '../assets/helpers/time.php';
require_once "../assets/helpers/db.php";

function insert_sros($sros)
{
	$res= -1;
	foreach ($sros as $s)
	{
		if ($res<$s->Revision)
			$res= $s->Revision;

		$txt_query = 'INSERT INTO sro
				(Name,Revision,RegNum,INN,ShortTitle,Title)
				VALUES(?,?,?,?,?,?)
				ON DUPLICATE KEY UPDATE 
				Name=?, Revision=?, RegNum=?, INN=?, ShortTitle=?, Title=?';
		execute_query($txt_query, array("ssssssssssss",
				$s->Name,$s->Revision,$s->RegNum,$s->INN,$s->ShortTitle,$s->Title
				,$s->Name,$s->Revision,$s->RegNum,$s->INN,$s->ShortTitle,$s->Title
		));
	}
	$count_inserted = count($sros);
	echo job_time() . " inserted $count_inserted sros\r\n";
	return $res;
}

function request_efrsb_sros($max_revision, $portion_size)
{
	global $use_efrsb_service_url;
	$url= $use_efrsb_service_url.'/replication-api.php/sro?revision-greater-than='.$max_revision;

	if (null!=$portion_size)
		$url.= '&portion-size='.$portion_size;

	echo job_time() . " request..\r\n$url\r\n";
	$curl = curl_init();
	curl_setopt_array($curl, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
		throw new Exception("can not request sros (code $httpcode for url \"$url\")");

	$sros= json_decode($curl_response);

	if (null===$sros)
	{
		echo "got responce:\r\n".$curl_response."\r\n";
		throw new Exception("can not parse json responce!");
	}

	$count_sros= count($sros);
	echo job_time() . " .. got $count_sros sros.\r\n";

	return $sros;
}

function sync_sros($portion_size= 200, $max_to_insert= null)
{
	$current_datetime_txt= job_time();
	$current_date_time= date_create_from_format('Y-m-d\TH:i:s',$current_datetime_txt);

	echo job_time() . " query for max_revision from sro ..\r\n";
	$rows= execute_query('SELECT MAX(m.Revision) AS max_revision from sro m;',array());
	$max_revision = empty($rows[0]->max_revision) ? 0 : $rows[0]->max_revision;
	echo job_time() . " .. got max_revision=$max_revision.\r\n";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('10 minutes'));

	$inserted= 0;
	for ($sros= request_efrsb_sros($max_revision, $portion_size), $sros_count= count($sros);
		 0!=$sros_count;
		 $sros= request_efrsb_sros($max_revision, $portion_size), $sros_count= count($sros))
	{
		$max_revision= insert_sros($sros);
		$inserted+= $sros_count;
		if (!(date_create() < $time_start_to_stop && (null==$max_to_insert || $inserted<$max_to_insert)))
			break;
	}
	
	if (0==$sros_count)
		echo job_time() . " no sros to insert!\r\n";
}