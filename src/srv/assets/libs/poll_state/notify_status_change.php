<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/actions/backend/voting/alib_voting.php';

function send_voting_email_for_Manager($id_Poll, $событие, $manager_row, $question_rows)
{
	trace("{ dispatch notification for manager $manager_row->FullName");
	$id_Manager = $manager_row->id_Manager;
	if (!isValidEmail($manager_row->EMail))
	{
		trace("Not valid email to $manager_row->EMail");
	}
	else
	{
		$decode_poll= json_decode($manager_row->ExtraPoll);
		$manager_row->ExtraPoll= $decode_poll;
		$id_Vote=null;
		foreach ($question_rows as $question_row)
		{
			$id_Question = $question_row->id_Question;

			$txt_query = "select id_Vote, uncompress(VoteData) ExtraVote
				, VoteTime, Confirmation_code, Confirmation_code_time
			from vote 
			where id_Manager=? and id_Question=? limit 1";

			trace("request for vote with id_Question=$id_Question and id_Manager=$id_Manager..");
			$vote_rows = execute_query($txt_query, array('ss', $id_Manager, $id_Question));
			$count_vote_rows= count($vote_rows);
			trace(".. got $count_vote_rows votes.");
			if ($count_vote_rows==0)
			{
				trace("add vote with id_Question=$id_Question..");
				$txt_query = "insert into vote set id_Manager=?, id_Question=?";
				$id_Vote = execute_query_get_last_insert_id($txt_query, array('ss', $id_Manager, $id_Question));
				trace(".. added vote.");
			}
			else
			{
				$vote_row = $vote_rows[0];
				$id_Vote= $vote_row->id_Vote;
			}
		}
		Направить_email_участнику_Голосования($id_Vote, $manager_row, $событие);
	}
	trace("} dispatched notification for manager $manager_row->FullName");
}

function send_voting_email_by_poll($id_Poll, $событие)
{
	$txt_query = "select
		m.id_Manager
		, m.EMail
		, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) FullName
		, p.Name PollName
		, uncompress(p.ExtraColumns) ExtraPoll
		, p.DateFinish
	from manager m
	left join sro on sro.RegNum=m.SRORegNum
	left join manager_group_manager mgm on mgm.id_Manager=m.id_Manager
	inner join poll p on 
		   p.id_Region=m.id_Region 
		or p.id_SRO=sro.id_SRO 
		or p.id_Manager_group=mgm.id_Manager_group 
		or (p.id_Manager_group is null and p.id_SRO is null and p.id_Region is null)
	where p.id_Poll=? and m.EMail is not null";

	trace('request for managers to send notification..');
	$manager_rows= execute_query($txt_query, array('s', $id_Poll));
	$count_manager_rows= count($manager_rows);
	trace(".. got $count_manager_rows managers to send notification.");

	trace('request for questions..');
	$txt_query="select id_Question from question where id_Poll=?";
	$question_rows= execute_query($txt_query, array('s', $id_Poll));
	$count_question_rows= count($question_rows);
	trace(".. got $count_question_rows questions.");

	foreach ($manager_rows as $manager_row)
	{
		send_voting_email_for_Manager($id_Poll, $событие, $manager_row, $question_rows);
	}
}

function check_poll_change_date()
{
	global $poll_status_description;
	$count_processed_polls= 0;
	$txt_query = "select id_Poll, Name, Status, TimeChange, TimeNotified from poll where Status is not null and (TimeNotified is null or TimeChange >= TimeNotified)";
	$rows = execute_query($txt_query, array());
	$count_rows= count($rows);
	if (0!=$count_rows)
	{
		trace("got $count_rows changed polls");
		foreach ($rows as $row)
		{
			$poll_Name= $row->Name;
			unset($row->Name);
			trace("{ process poll \"$poll_Name\" ".json_encode($row));
			try
			{
				$id_Poll = $row->id_Poll;
				$d = FindDescription($poll_status_description, 'code', $row->Status);
				$событие= $d['descr'];
				send_voting_email_by_poll($id_Poll, $событие);

				trace('fix TimeNotified..');
				$current_time= date_format(safe_date_create(),'Y-m-d\TH:i:s');
				$txt_query="update poll set TimeNotified=? where id_Poll=?";
				execute_query_no_result($txt_query, array('ss', $current_time, $id_Poll));
				trace('.. fixed TimeNotified.');
			}
			catch (Exception $ex)
			{
				trace('Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
			}
			trace('} processed poll');
			$count_processed_polls++;
		}
	}
	
}
