<? 

require_once '../assets/config.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/client.rest.php';

class CryptoAPI extends Rest_client
{
	public $base_url= null;
	public $auth= null;

	function ChainVerificationPolicy()
	{
		return array(
			'RevocationFlag'=>'EntireChain'
			,'RevocationMode'=>'NoCheck'
			,'UrlRetrievalTimeout'=>'00:00:30'
			,'VerificationFlags'=>'NoFlag'
		);
	}

	public function __construct($args)
	{
		parent::__construct();
		$this->base_url= $args->base_url;
		$this->auth= $args->auth;
	}

	function SignAsCapicom($args)
	{
		$post_args= array(
			'auth_token'=>$this->auth
			,'document'=>$args->document
			,'base64_encoded_certificate'=>base64_encode($args->certificate_bytes)
		);
		$postfields= json_encode($post_args);
		$request_result= $this->request(array(
			CURLOPT_URL => $this->base_url."/SignAsCapicom",
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
			CURLOPT_POSTFIELDS => $postfields
		));
		$result_json_decoded= json_decode($request_result);
		return base64_decode($result_json_decoded);
	}

	function Sign($args)
	{
		$post_args= array(
			'auth_token'=>$this->auth
			,'base64_encoded_document'=>base64_encode($args->document_bytes)
			,'base64_encoded_certificate'=>base64_encode($args->certificate_bytes)
		);
		$postfields= json_encode($post_args);
		$request_result= $this->request(array(
			CURLOPT_URL => $this->base_url."/Sign",
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
			CURLOPT_POSTFIELDS => $postfields
		));
		$result_json_decoded= json_decode($request_result);
		return base64_decode($result_json_decoded);
	}

	function VerifyCertificate($args)
	{
		$url= $this->base_url."/VerifyCertificate";
		$base64_encoded_certificate= isset($args->base64_encoded_certificate)
			? $args->base64_encoded_certificate
			: base64_encode($args->certificate_bytes);
		$post_args= array(
			'auth_token'=>$this->auth
			,'base64_encoded_certificate'=>$base64_encoded_certificate
		);
		$postfields= json_encode($post_args);
		try
		{
			$request_result= $this->request(array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
				CURLOPT_POSTFIELDS => $postfields
			));
			$result_json_decoded= json_decode($request_result);
			if (null==$result_json_decoded)
			{
				write_to_log('cryptoapi VerifyCertificate answer:');
				write_to_log($request_result);
				throw new Exception('can not decode cryptoapi VerifyCertificate answer!');
			}
		}
		catch (Exception $ex)
		{
			write_to_log("using url:$url");
			throw $ex;
		}
		return $result_json_decoded;
	}

	function VerifySignature($args)
	{
		$url= $this->base_url."/VerifySignature";
		$base64_encoded_signature= isset($args->base64_encoded_signature)
			? $args->base64_encoded_signature
			: base64_encode($args->signature_bytes);
		$base64_encoded_document= isset($args->base64_encoded_document)
			? $args->base64_encoded_document
			: base64_encode($args->document_bytes);
		$post_args= array(
			'auth_token'=>$this->auth
			,'base64_encoded_document'=>$base64_encoded_document
			,'base64_encoded_signature'=>$base64_encoded_signature
			,'policy'=>$this->ChainVerificationPolicy()
		);
		$postfields= json_encode($post_args);
		try
		{
			$request_result= $this->request(array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
				CURLOPT_POSTFIELDS => $postfields
			));
			$result_json_decoded= json_decode($request_result);
			if (null==$result_json_decoded)
			{
				write_to_log('cryptoapi VerifySignature answer:');
				write_to_log($request_result);
				throw new Exception('can not decode cryptoapi VerifySignature answer!');
			}
		}
		catch (Exception $ex)
		{
			write_to_log("using url:$url");
			throw $ex;
		}
		return $result_json_decoded;
	}

	function FindOutDetailsOfTheCases($cases,$current_unix_time= 0)
	{
		$args= array(
			'auth_token'=>$this->auth
			,'cases'=>$cases
			,'current_unix_time'=>$current_unix_time
		);
		$url= $this->base_url."/FindOutDetailsOfTheCases";
		write_to_log("url=$url");
		$request_result= $this->request(array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
			CURLOPT_POSTFIELDS => json_encode($args)
		));

		return json_decode($request_result);
	}

	function VerifySignatureFromCapicom($args)
	{
		$url= $this->base_url."/VerifySignatureFromCapicom";
		$base64_encoded_signature= isset($args->base64_encoded_signature)
			? $args->base64_encoded_signature
			: base64_encode($args->signature_bytes);
		$post_args= array(
			'auth_token'=>$this->auth
			,'document'=>$args->document
			,'base64_encoded_signature'=>$base64_encoded_signature
			,'policy'=>$this->ChainVerificationPolicy()
		);
		$postfields= json_encode($post_args);
		try
		{
			$request_result= $this->request(array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json','Expect:'),
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
				CURLOPT_POSTFIELDS => $postfields
			));
			$result_json_decoded= json_decode($request_result);
			if (null==$result_json_decoded)
			{
				write_to_log('cryptoapi VerifySignatureFromCapicom answer:');
				write_to_log($request_result);
				throw new Exception('can not decode cryptoapi VerifySignatureFromCapicom answer!');
			}
		}
		catch (Exception $ex)
		{
			write_to_log("using url:$url");
			throw $ex;
		}
		return $result_json_decoded;
	}

	function VerifySignedFromCapicom($args)
	{
		$url= $this->base_url."/VerifySignedFromCapicom";
		$base64_encoded_signed= isset($args->base64_encoded_signed)
			? $args->base64_encoded_signed
			: base64_encode($args->signed);
		$post_args= array(
			'auth_token'=>$this->auth
			,'base64_encoded_signed'=>$base64_encoded_signed
			,'policy'=>$this->ChainVerificationPolicy()
		);
		$postfields= json_encode($post_args);
		try
		{
			$request_result= $this->request(array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
				CURLOPT_POSTFIELDS => $postfields
			));
			$result_json_decoded= json_decode($request_result);
			if (null==$result_json_decoded)
			{
				write_to_log('cryptoapi VerifySignedFromCapicom answer:');
				write_to_log($request_result);
				throw new Exception('can not decode cryptoapi VerifySignedFromCapicom answer!');
			}
		}
		catch (Exception $ex)
		{
			write_to_log("using url:$url");
			throw $ex;
		}
		return $result_json_decoded;
	}

	function GetBase64EncodedCertificateFromCapicomSignature($args)
	{
		$url= $this->base_url."/GetBase64EncodedCertificateFromCapicomSignature";
		$base64_encoded_signed= isset($args->base64_encoded_signed)
			? $args->base64_encoded_signed
			: base64_encode($args->signed);
		$post_args= array(
			'auth_token'=>$this->auth
			,'base64_encoded_signature'=>$base64_encoded_signed
		);
		$postfields= json_encode($post_args);
		try
		{
			return $this->request(array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_HTTPGET => false, CURLOPT_POST => true, CURLOPT_PUT=> false,
				CURLOPT_POSTFIELDS => $postfields
			));
		}
		catch (Exception $ex)
		{
			write_to_log("using url:$url");
			throw $ex;
		}
	}
}
