﻿set names utf8;

select 
LastName
,FirstName
,MiddleName
,EMail
,id_Vote
,vote.id_Manager
,id_Question
,replace(VoteData,"\r\n","\n") VoteData
,replace(BulletinText,"\r\n","\n") BulletinText
,Confirmation_code
,Confirmation_code_time
from vote
inner join manager on vote.id_Manager=manager.id_Manager
\G

select 
id_Vote
,Vote_log_type
,Vote_log_time
,body
from vote_log
\G