﻿select
	email_message.id_Email_Message id_Email_Message
	, RecipientEmail
	, email_sender.id_Email_Sender
	, 'sent!'
from email_message
inner join email_sent on email_message.id_Email_Message=email_sent.id_Email_Message
inner join email_sender on email_sender.id_Email_Sender=email_sent.id_Email_Sender
order by id_Email_Message
\G
select
	email_message.id_Email_Message id_Email_Message
	, RecipientEmail
	, email_sender.id_Email_Sender
	, Description
from email_message
inner join email_error on email_message.id_Email_Message=email_error.id_Email_Message
inner join email_sender on email_sender.id_Email_Sender=email_error.id_Email_Sender
\G
select
	id_Email_Sender
	, SenderServer
	, SenderUser
from email_sender
\G

