﻿select
	email_message.id_Email_Message id_Email_Message
	, RecipientEmail
	, RecipientId
	, RecipientType
	, EmailType
	, replace(uncompress(Details),'\\r\\n','\n') Details
from email_message
inner join email_tosend on email_message.id_Email_Message=email_tosend.id_Email_Message
\G

select
	id_Email_Attachment
	, email_attachment.id_Email_Message
	, FileName
	, length(Content) Content_length
	, if(FileName not like '%.txt','!!!binary!!!',replace(Content,'\r\n','\n')) Content
from email_attachment
inner join email_message on email_attachment.id_Email_Message=email_message.id_Email_Message
inner join email_tosend on email_message.id_Email_Message=email_tosend.id_Email_Message
\G
