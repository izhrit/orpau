﻿insert into mock_efrsb_region set id_Region=333, Name='Мордор';
insert into mock_efrsb_region set id_Region=444, Name='Беломорье';

insert into `mock_efrsb_sro` set 
id_SRO= 1
, OGRN= '1031458335864'
, RegNum= '345'
, INN= '2436706493'
, Name= 'Народная СРО АУ - Народная саморегулируемая'
, ShortTitle= 'Народная СРО АУ'
, Title= 'Народная саморегулируемая'
, UrAdress= 'улица Пушкина, проезд Колотушкина'
, Revision= 1
, Body= ''
, DateLastModif='2020-01-01'
;

insert into `mock_efrsb_debtor` set Revision= 1, `id_Debtor`= 1, `BankruptId`= 1, `ArbitrManagerID`= 1, `Name`= 'ООО Герундий', `INN`= '7907655280', `OGRN`= '1136422185130'
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `mock_efrsb_debtor` set Revision= 2, `id_Debtor`= 2, `BankruptId`= 2, `ArbitrManagerID`= 1, `Name`= 'Колкий Колк Колкович', `INN`= '436364310031', `SNILS`= '79972271685'
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `mock_efrsb_debtor` set Revision= 3, `id_Debtor`= 3, `BankruptId`= 3, `ArbitrManagerID`= 2, `Name`= 'ОАО Порт семи морей', `INN`= '8380289542'
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `mock_efrsb_debtor` set Revision= 4, `id_Debtor`= 4, `BankruptId`= 4, `Name`= 'ООО Снявские майки', `INN`= '5591845539', `ArbitrManagerID`= 2
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `mock_efrsb_debtor` set Revision= 5, `id_Debtor`= 5, `BankruptId`= 5, `Name`= 'ИП Троцкий Л Д', `INN`= '929309724309', `ArbitrManagerID`= 1
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `mock_efrsb_debtor` set Revision= 6, `id_Debtor`= 6, `BankruptId`= 6, `Name`= 'ООО "Новострой-Технология"', `INN`= '322223234', `OGRN`= '234234', `ArbitrManagerID`= 3
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';

insert into `mock_efrsb_manager` set Revision= 1, `id_Manager`= 1, `ArbitrManagerID`= 1, `FirstName`= 'Олег', `LastName`= 'Олегов', `MiddleName`= 'Олегович', `INN`= '055554427038', `RegNum`= 2
, `Body`= compress(''), DateLastModif='2020-01-01', DownloadDate='2020-01-01', id_Region=333;
insert into `mock_efrsb_manager` set Revision= 2, `id_Manager`= 2, `ArbitrManagerID`= 2, `FirstName`= 'Юлия', `LastName`= 'Юльева', `MiddleName`= 'Юльевна', `INN`= '167591839508'
, `Body`= compress(''), DateLastModif='2020-01-01', DownloadDate='2020-01-01';
insert into `mock_efrsb_manager` set Revision= 3, `id_Manager`= 3, `ArbitrManagerID`= 31, `FirstName`= 'Иван', `LastName`= 'Иванов', `MiddleName`= 'Иванович', `INN`= '12345678'
, `Body`= compress(''), DateLastModif='2020-01-01', DownloadDate='2020-01-01', id_Region=444;

insert into `mock_efrsb_debtor_manager` set Revision= 1, `id_Debtor_manager`= 1, `BankruptId`= 1, `ArbitrManagerID`= 1
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `mock_efrsb_debtor_manager` set Revision= 2, `id_Debtor_manager`= 2, `BankruptId`= 2, `ArbitrManagerID`= 2
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `mock_efrsb_debtor_manager` set Revision= 3, `id_Debtor_manager`= 3, `BankruptId`= 3, `ArbitrManagerID`= 3
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `mock_efrsb_debtor_manager` set Revision= 4, `id_Debtor_manager`= 4, `BankruptId`= 4, `ArbitrManagerID`= 1
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `mock_efrsb_debtor_manager` set Revision= 5, `id_Debtor_manager`= 5, `BankruptId`= 5, `ArbitrManagerID`= 2
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `mock_efrsb_debtor_manager` set Revision= 6, `id_Debtor_manager`= 6, `BankruptId`= 6, `ArbitrManagerID`= 3
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `mock_efrsb_debtor_manager` set Revision= 7, `id_Debtor_manager`= 7, `BankruptId`= 2, `ArbitrManagerID`= 1
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
