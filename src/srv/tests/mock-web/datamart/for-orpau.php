<? 

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<style>
		body
		{
			background-color:#D2B4DE;
			text-align:center;
		}
		body span
		{
			font-size:large;
		}
	</style>
	<body>
		<h1>Тестовая имитация информации, загруженной с витрины данных ПАУ</h1>
		<span>
			id_ManagerCertAuth=<b><?= $_GET['id_ManagerCertAuth'] ?></b>, 
			auth=<b><?= $_GET['auth'] ?></b>, 
			widget=<b><?= $_GET['widget'] ?></b>
		</span>
	</body>
</html>