﻿set names utf8;
select 
LastName
,FirstName
,MiddleName
,EMail
,Phone
,replace(AgreementText,"\r\n","\n") AgreementText
,length(AgreementSignature) AgreementSignature_length
,ConfirmationCode
,ConfirmationCodeTime
,replace(ClubAgreementText,"\r\n","\n") ClubAgreementText
from manager 
where id_Manager=2
\G
