﻿execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
include ..\..\..\..\uc\forms\orpau\admin\poll\tests\cases\in.lib.txt quiet
include ..\..\..\..\uc\forms\orpau\admin\group\tests\cases\in.lib.txt quiet
include ..\..\..\..\uc\forms\orpau\admin\question\tests\cases\in.lib.txt quiet

jw !!window.wbt
jw !!window.app.cryptoapi

shot_hide_class q-time
shot_hide_class q-sign-time
shot_hide_class signature-time
shot_hide_class reg
shot_hide_class time
shot_hide_class tim
shot_hide_class seconds
shot_hide_class sms
shot_hide_class email

wait_text "Войти"
js wbt.SetModelFieldValue("login", "v");
js wbt.SetModelFieldValue("password", "1");
wait_click_text "Войти"

wait_click_text "Голосования"
wait_text "Показаны голосования"
shot_check_png ..\..\shots\voting_full_1_login_admin.png

wait_click_text "Зарегистрировать голосование"
wait_text "Голосуют"
play_stored_lines orpau_poll_fields_1
wait_click_text "Добавить вопрос"
wait_text "форма"
play_stored_lines orpau_poll_question_fields_1
wait_click_text "Сохранить параметры вопроса"
wait_text_disappeared "форма"

wait_click_text "Добавить вопрос"
wait_text "форма"
play_stored_lines orpau_poll_question_fields_2
wait_click_text "Сохранить параметры вопроса"
wait_text_disappeared "форма"

shot_check_png ..\..\shots\voting_full_2_new_poll.png
wait_click_text "Сохранить"

exec_check_txt ..\..\sql\trace_votes.bat votes.0.txt

wait_text "4 из 4"
wait_click_text "Выборы руководителя"
wait_text "Голосуют"
play_stored_lines orpau_poll_fields_3
wait_click_full_text "Сохранить"
wait_click_text "сохранить изменения и сбросить данные"
wait_text_disappeared "Вызов ajax запроса"
# wait_click_text "Закрыть"
wait_text_disappeared "Загрузка"
shot_check_png ..\..\shots\voting_full_3_edit_poll.png

exec_check_txt ..\..\sql\trace_votes.bat votes.0.txt

wait_text "4 из 4"
wait_text "О порядке назначения АУ"

je $("td[title='Арбитражный управляющий года'] ~ td > div.actions-button").click();
wait_click_text "Удалить"
wait_click_text "Да, удалить"

wait_text "3 из 3"

shot_check_png ..\..\shots\voting_full_4_delete_poll.png

wait_click_text "О порядке назначения АУ"
wait_text "Настройки голосования"
wait_click_text "Открыть голосование"
wait_text_disappeared "ajax"
wait_text_disappeared "Сохранить"

exec_check_txt ..\..\sql\trace_votes.bat votes.0.txt

wait_click_text "Закрыть"
wait_text_disappeared "Настройки голосования""

# ------------------------ голосование 1 го участника (Иванов)
goto_wait ui.php?use-test-time=2020-01-01T16:00:00
je app.cpw_Now= function(){return new Date(2020,0,1,16,0,0);}
execute_javascript_from_file ..\..\..\..\uc\built\wbt.js

jw !!window.wbt
jw !!window.app.cryptoapi
je app.cryptoapi.SignBase64= function (rawData, detached, cert){return window.btoa('test_signature');}
je app.cryptoapi.ChooseCertificateAnd= function(f){f({IssuerName:'CN=tca',SerialNumber:'1',SubjectName:'ИНН=364484332544'});}

wait_click_text "Вход"
wait_text "Выбор варианта авторизации"
wait_click_text "Вход по ЭЦП"
wait_text_disappeared "Вызов ajax запроса"
wait_click_text "OK"
sleep 50
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_5_manager_login.png
wait_click_text "Подписать"
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_6_manager_login.png
wait_text "Справки"

wait_text "Хотите задать логин?"
wait_click_text "Нет"
wait_click_text "Кабинет АУ"
wait_click_text "Голосование"

wait_click_text "О ситуации в Гондурасе"
wait_text_disappeared "Вызов ajax запроса"
jw wbt.jqgrid_has_highlighted_row('#cpw-orpau-voting-questions-grid');
shot_check_png ..\..\shots\voting_full_7_manager_vote.png

wait_text "Выразить озабоченность"
wait_click_text "За"
shot_check_png ..\..\shots\voting_full_8_manager_answer.png

exec_check_txt ..\..\sql\trace_votes.bat votes.0.txt

wait_click_text "Перейти к подписанию"

exec_check_txt ..\..\sql\trace_votes.bat votes.1.txt

wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_9_manager_signing.png

exec_check_txt ..\..\sql\trace_votes.bat votes.1.txt

wait_click_text "Выслать код подтверждения"

exec_check_txt ..\..\sql\trace_votes.bat votes.2.txt

wait_text "1234"
je $('input.code').val('1234');
wait_click_text "Подписать"

exec_check_txt ..\..\sql\trace_email_tosend.bat email_tosend.voted.1.txt

wait_click_text "OK"

wait_text_disappeared "Вызов ajax запроса"
wait_text_disappeared "Загрузка..."
shot_check_png ..\..\shots\voting_full_10_manager_signing.png

wait_click_text "О ситуации в Гондурасе"
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_11_manager_signing.png

wait_click_text "Иванов"
wait_click_text "Выйти"

# ------------------------ голосование 2 го участника (Афанасьев)
goto_wait ui.php?use-test-time=2020-01-01T20:00:00
je app.cpw_Now= function(){return new Date(2020,0,1,16,0,0);}
execute_javascript_from_file ..\..\..\..\uc\built\wbt.js

jw !!window.wbt
jw !!window.app.cryptoapi
je app.cryptoapi.SignBase64= function (rawData, detached, cert){return window.btoa('test_signature');}
je app.cryptoapi.ChooseCertificateAnd= function(f){f({IssuerName:'CN=tca',SerialNumber:'1',SubjectName:'ИНН=111111111'});}

wait_click_text "Вход"
wait_text "Выбор варианта авторизации"
wait_click_text "Вход по ЭЦП"
wait_text_disappeared "Вызов ajax запроса"
wait_click_text "OK"
sleep 50
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_12_manager_login.png
wait_click_text "Подписать"
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_13_manager_login.png
wait_text "Справки"

wait_text "Хотите задать логин?"
wait_click_text "Нет"

wait_click_text "Кабинет АУ"
wait_click_text "Голосование"
wait_click_text "О ситуации в Гондурасе"
jw wbt.jqgrid_has_highlighted_row('.cpw-form-orpau-voting-questions')
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_14_manager_vote.png

wait_click_text "Выразить озабоченность"
wait_click_text "Против"
shot_check_png ..\..\shots\voting_full_15_manager_answer.png

wait_click_text "Перейти к подписанию"
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_16_manager_signing.png
wait_click_text "Выслать код подтверждения"
wait_text "1234"
je $('input.code').val('3');
wait_click_text "Подписать"
wait_text "не соответствует"
shot_check_png ..\..\shots\voting_full_17_manager_signing_bad.png
wait_click_text "OK"
wait_text_disappeared "не соответствует"

# --- todo разобраться с проверкой смс на позднюю отправку
# wait_click_text "Выслать код подтверждения"
# wait_text_disappeared "Вызов ajax запроса"
# je $.ajax({url:'ui-backend.php?action=test-time&use-test-time=2020-01-01T21:30:00',cache: false,success:function(){$('input.code').val('1234');},error:function(){alert('can not set time');}});
# wait_value "1234"
# wait_click_text "Подписать"
# wait_text_disappeared "Вызов ajax запроса"
# wait_text "слишком поздно"
# shot_check_png ..\..\shots\voting_full_18_manager_signing_late.png
# wait_click_text "OK"
# wait_text_disappeared "слишком поздно"

wait_click_text "Выслать код подтверждения"
wait_text_disappeared "Вызов ajax запроса"
je $('input.code').val('1234');
wait_click_text "Подписать"
wait_click_text "OK"

wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_19_manager_signing.png

wait_click_text "О ситуации в Гондурасе"
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_20_manager_signing.png

wait_click_text "Закрыть"
wait_click_text "Афанасьев"
wait_click_text "Выйти"

wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_21_manager_logout.png

# ------------------------ подведение итогов
goto_wait "admin.php?use-test-time=2020-01-02T00:00:00"

execute_javascript_from_file ..\..\..\..\uc\built\wbt.js
jw !!window.wbt

wait_text "Войти"
js wbt.SetModelFieldValue("login", "v");
js wbt.SetModelFieldValue("password", "1");
wait_click_text "Войти"

wait_click_text "Голосования"
wait_text "Показаны голосования"
shot_check_png ..\..\shots\voting_full_4_delete_poll.png

je $("td[title='О порядке назначения АУ'] ~ td > div.actions-button").click();
wait_click_text "Посмотреть результаты"
wait_text_disappeared "Вызов ajax запроса"
jw wbt.stabilized_dialog_y()
shot_check_png ..\..\shots\voting_full_22_poll_result.png

wait_click_text "1 из 2"
wait_text "Детали"
jw wbt.stabilized_dialog_y()
shot_check_png ..\..\shots\voting_full_23_poll_result_details.png

wait_click_text "Закрыть детали"
wait_click_text "Сохранить результаты"
wait_text "Итоги голосования подведены"
shot_check_png ..\..\shots\voting_full_23_poll_result_details_close.png
wait_click_text "OK"

je $("td[title='О порядке назначения АУ'] ~ td > div.actions-button").click();
wait_click_text "Посмотреть результаты"
wait_text_disappeared "Вызов ajax запроса"
# wait_text "Голосование завершено" todo fix
jw wbt.stabilized_dialog_y()
shot_check_png ..\..\shots\voting_full_24_close_poll_result.png

# ------------------------ проверка 1 го участника (Иванов)
goto_wait ui.php?use-test-time=2020-01-01T16:00:00

jw !!window.app.cryptoapi
je app.cryptoapi.SignBase64= function (rawData, detached, cert){return window.btoa('test_signature');}
je app.cryptoapi.ChooseCertificateAnd= function(f){f({IssuerName:'CN=tca',SerialNumber:'1',SubjectName:'ИНН=364484332544'});}

wait_click_text "Вход"
wait_text "Выбор варианта авторизации"
wait_click_text "Вход по ЭЦП"
wait_text_disappeared "Вызов ajax запроса"
wait_click_text "OK"
sleep 50
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_5_manager_login.png
wait_click_text "Подписать"
wait_text_disappeared "Вызов ajax запроса"
shot_check_png ..\..\shots\voting_full_6_manager_login.png
wait_text "Справки"

wait_text "Хотите задать логин?"
wait_click_text "Нет"

wait_click_text "Кабинет АУ"
wait_click_text "Голосование"
wait_text "решение не принято"
shot_check_png ..\..\shots\voting_full_25_result_manager.png

exec_check_txt ..\..\sql\trace_votes.bat votes.z.txt
exec_check_txt ..\..\sql\trace_email_tosend.bat email_tosend.z.txt

exit