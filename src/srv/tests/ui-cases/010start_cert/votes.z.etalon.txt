*************************** 1. row ***************************
              LastName: Иванов
             FirstName: Иван
            MiddleName: Иванович
                 EMail: IvanovII@someMail.com
               id_Vote: 1
            id_Manager: 1
           id_Question: 3
              VoteData: {
	"Голосование": {
		"Формулировка": "Согласны ли Вы (голосующий) участвовать в съезде АУ 2021 в качестве делегата?",
		"Форма_бюллетеня": "f2",
		"Варианты": [
			"Да, согласен и готов",
			"Нет, НЕ согласен или НЕ готов"
		]
	},
	"Ответ": "Да, согласен и готов",
	"Подписано": "01.01.2020 16:00:11"
}
          BulletinText: ﻿Бюллетень голосования

    Дата голосования: 01.01.2020
    Голосующий: АУ Иванов Иван Иванович (ИНН:364484332544) 

Я, Иванов Иван Иванович, голосую следующим образом:

По вопросу «Согласны ли Вы (голосующий) участвовать в съезде АУ 2021 в качестве делегата?»,
   мне были предложены варианты ответов:
   - «Да, согласен и готов»
   - «Нет, НЕ согласен или НЕ готов»
   - мой собственный вариант в свободной форме
   Я голосую за вариант «Да, согласен и готов»
     Confirmation_code: NULL
Confirmation_code_time: NULL
*************************** 2. row ***************************
              LastName: Семёнов
             FirstName: Семён
            MiddleName: Семёнович
                 EMail: Sem1972@someMail.com
               id_Vote: 2
            id_Manager: 2
           id_Question: 3
              VoteData: NULL
          BulletinText: NULL
     Confirmation_code: NULL
Confirmation_code_time: NULL
*************************** 3. row ***************************
              LastName: Афанасьев
             FirstName: Григорий
            MiddleName: Григорьевич
                 EMail: Sem1972@someMail.com
               id_Vote: 3
            id_Manager: 3
           id_Question: 3
              VoteData: NULL
          BulletinText: NULL
     Confirmation_code: NULL
Confirmation_code_time: NULL
*************************** 1. row ***************************
      id_Vote: 1
Vote_log_type: n
Vote_log_time: 2020-01-01 20:20:22
         body: IvanovII@someMail.com
*************************** 2. row ***************************
      id_Vote: 2
Vote_log_type: n
Vote_log_time: 2020-01-01 20:20:24
         body: Sem1972@someMail.com
*************************** 3. row ***************************
      id_Vote: 3
Vote_log_type: n
Vote_log_time: 2020-01-01 20:20:26
         body: Sem1972@someMail.com
*************************** 4. row ***************************
      id_Vote: 1
Vote_log_type: a
Vote_log_time: 2020-01-01 16:00:05
         body: о согласии участвовать в съезде АУ 2021 делегатом
*************************** 5. row ***************************
      id_Vote: 1
Vote_log_type: b
Vote_log_time: 2020-01-01 16:00:09
         body: IvanovII@someMail.com
