<? 

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../../assets/config.php';

function load_and_test_job_parts($fixed_cmd, $prefix, $argv)
{
	require_once 'test_job_parts.php';
	test_job_parts_group($fixed_cmd, $prefix, $argv);
}

global $prefixed_test_groups;
$prefixed_test_groups= array
(
	  (object)array('prefix'=>'job_parts.', 'tests_func'=>'load_and_test_job_parts')
);

function try_to_use_test_groups($cmd,$argv)
{
	global $prefixed_test_groups;
	foreach ($prefixed_test_groups as $prefixed_test_group)
	{
		$prefix= $prefixed_test_group->prefix;
		if (0===strpos($cmd,$prefix))
		{
			$fixed_test_name= substr($cmd,strlen($prefix));
			$tests_func= $prefixed_test_group->tests_func;
			$tests_func($fixed_test_name,$prefix,$argv);
			return true;
		}
	}
	return false;
}

function Process_command_line_arguments()
{
	global $argv;
	$cmd= $argv[1];
	if (!try_to_use_test_groups($cmd,$argv))
	{
		switch ($cmd)
		{
			default: echo "unknown test command $cmd\r\n";
		}
	}
}

try
{
	Process_command_line_arguments();
}
catch (Exception $ex)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	echo 'catched Exception:';
	print_r($ex);
}
