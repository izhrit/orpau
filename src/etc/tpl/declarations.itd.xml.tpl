<?xml version="1.0" encoding="windows-1251"?>
<declarations>

	<params>
		<par><name>mysql_dir</name><value><%= MYSQL_DIR_BIN %></value></par>
		<par><name>mysql_host</name><value><%= MYSQL_HOST %></value></par>
    	<par><name>mysql_port</name><value><%= MYSQL_PORT %></value></par>
    	<par><name>mysql_db</name><value><%= DBName %></value></par>
    	<par><name>root_proj_dir</name><value><%= ROOT_PROJ_DIR %></value></par>
    	<par><name>base_efrsb_url</name><value><%= base_efrsb_url %></value></par>
	</params>

</declarations>