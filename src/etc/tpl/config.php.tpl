<?

global $default_db_options;
$default_db_options= (object)array(
	'host'=> '<%= MYSQL_HOST %>'
	,'user'=>'<%= DBUser %>'
	,'password'=> '<%= DBPassword %>'
	,'dbname'=> '<%= DBName %>'
	,'charset'=> 'utf8'
);

global $log_file_name;
$log_file_name= '<%= ROOT_PROJ_DIR %>src\srv\log.txt';

global $base_apps_url, $base_cryptoapi_url, $test_cryptoapi_url;
$base_apps_url='<%= base_apps_url %>';
$base_cryptoapi_url='<%= base_mock_url %>cryptoapi/mock.php';
$test_cryptoapi_url='<%= test_cryptoapi_url %>';

global $use_efrsb_service_url;
$use_efrsb_service_url= '<%= base_mock_url %>efrsb/web';

global $ama_datamart_settings;
$ama_datamart_settings= (object)array(
	'use_base_url'=>'<%= base_mock_url %>datamart/'
);
