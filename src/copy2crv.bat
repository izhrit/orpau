
@for /f "eol=# delims== tokens=1,2" %%i in (%~dp0etc\settings.txt) do @set %%i=%%j
@if exist %~dp0etc\settings.local.txt @for /f "eol=# delims== tokens=1,2" %%i in (%~dp0etc\settings.local.txt) do @set %%i=%%j

pushd %~dp0\srv
del "*.png-errors.txt" /S /Q 
popd

copy %~dp0\db\backups\empty_dump.sql %srv_repo_path%\db\backups\
copy %~dp0\db\backups\test_dump.sql  %srv_repo_path%\db\backups\

copy %~dp0\db\migrations\m*.sql %srv_repo_path%\db\migrations\

copy %~dp0\srv\web\js\orpau.js %srv_repo_path%\web\js\
copy %~dp0\srv\web\js\orpau-admin.js %srv_repo_path%\web\js\

copy %~dp0\srv\web\css\orpau.css %srv_repo_path%\web\css\

copy %~dp0\srv\web\img\*.png %srv_repo_path%\web\img\
copy %~dp0\srv\web\img\*.gif %srv_repo_path%\web\img\
copy %~dp0\srv\web\img\*.jpg %srv_repo_path%\web\img\

copy %~dp0\srv\web\ui.php         %srv_repo_path%\web\
copy %~dp0\srv\web\ui-backend.php %srv_repo_path%\web\
copy %~dp0\srv\web\admin.php      %srv_repo_path%\web\
copy %~dp0\srv\web\monitoring.php %srv_repo_path%\web\

copy %~dp0\srv\jobs\*.php %srv_repo_path%\jobs\

xcopy /s /e /Y %~dp0\srv\assets\actions %srv_repo_path%\assets\actions
xcopy /s /e /Y %~dp0\srv\assets\helpers %srv_repo_path%\assets\helpers
xcopy /s /e /Y %~dp0\srv\assets\libs    %srv_repo_path%\assets\libs
