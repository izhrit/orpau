﻿pushd %~dp0\uc
del "*.diff" /S /Q 
del "*.result.*" /S /Q 
del "*.zdiff.*" /S /Q 
del "*.zorig.*" /S /Q 
del "*.tmp" /S /Q 
popd
pushd %~dp0\srv
del "*.png-errors.txt" /S /Q 
popd