--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `Details` text,
  `INN` varchar(12) DEFAULT NULL,
  `IP` varchar(40) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_Log_type` int(11) NOT NULL,
  `id_Log` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Log`),
  KEY `byINN` (`INN`),
  KEY `bySNILS` (`SNILS`),
  KEY `byTime` (`Time`),
  KEY `by_log_type` (`id_Log_type`)
  CONSTRAINT `by_log_type` FOREIGN KEY (`id_Log_type`) REFERENCES `log_type` (`id_Log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `confirmation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirmation` (
  `CaseNumber` varchar(500) DEFAULT NULL,
  `EfrsbMessageNumber` varchar(15) DEFAULT NULL,
  `JudicalActId` varchar(15) DEFAULT NULL,
  `Number` varchar(20) NOT NULL,
  `Signature` longblob,
  `Text` longblob NOT NULL,
  `TimeOfCreation` datetime NOT NULL,
  `id_Confirmation` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_Confirmation`),
  KEY `Ref_Confirmation_Debtor` (`id_Debtor`),
  KEY `Ref_Confirmation_Manager` (`id_Manager`),
  UNIQUE KEY `byNumber` (`Number`)
  CONSTRAINT `Ref_Confirmation_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `Ref_Confirmation_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LegalAdress` varchar(400) DEFAULT NULL,
  `LegalCaseList` varchar(10000) DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `Text` longblob,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) DEFAULT NULL,
  `id_Region` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor`),
  KEY `Ref5580` (`id_Region`),
  KEY `Ref_Debtor_Manager` (`id_Manager`),
  UNIQUE KEY `byBankruptId` (`BankruptId`)
  CONSTRAINT `Ref_Debtor_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Refregion80` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor_manager` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `DateTime_MessageFirst` datetime DEFAULT NULL,
  `DateTime_MessageLast` datetime DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `id_Debtor_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `Ref_Debtor_manager_Debtor` (`BankruptId`),
  KEY `Ref_Debtor_manager_Manager` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_Email_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  PRIMARY KEY (`id_Email_Attachment`),
  KEY `Email_Message_to_Attachment` (`id_Email_Message`)
  CONSTRAINT `Email_Message_to_Attachment` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_error` (
  `Description` text NOT NULL,
  `TimeError` datetime NOT NULL,
  `id_Email_Error` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  PRIMARY KEY (`id_Email_Error`),
  KEY `Email_Message_Error` (`id_Email_Message`),
  KEY `Email_Sender_Error` (`id_Email_Sender`)
  CONSTRAINT `Email_Message_Error` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Error` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_message` (
  `Details` longblob,
  `EmailType` char(1) NOT NULL,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_sender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_sender` (
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  `id_Email_Sender` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_Sender`),
  UNIQUE KEY `byServerUser` (`SenderServer`,`SenderUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_sent` (
  `Message` longblob,
  `TimeSent` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  `id_Email_sent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_sent`),
  KEY `Email_Message_Sent` (`id_Email_Message`),
  KEY `Email_Sender_Sent` (`id_Email_Sender`)
  CONSTRAINT `Email_Message_Sent` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Sent` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_tosend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_tosend` (
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  KEY `Email_Message_ToSend` (`id_Email_Message`),
  KEY `byTimeDispatch` (`TimeDispatch`,`id_Email_Message`)
  CONSTRAINT `Email_Message_ToSend` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_type` (
  `Name` varchar(100) NOT NULL,
  `id_Log_type` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `EMail` varchar(100) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `PostAdress` varchar(80) DEFAULT NULL,
  `Revision` int(11) DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `SRORegNum` varchar(40) DEFAULT NULL,
  `efrsbNumber` varchar(50) DEFAULT NULL,
  `firstName` varchar(50) NOT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `middleName` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Manager`),
  KEY `Ref5581` (`id_Region`),
  KEY `Rem_Manager_SRO` (`id_SRO`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`)
  CONSTRAINT `Refregion81` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`),
  CONSTRAINT `Rem_Manager_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LastMessageDate` datetime DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor`),
  KEY `byArbitrManagerID` (`ArbitrManagerID`),
  UNIQUE KEY `byBankruptId_1` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `id_Debtor_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `byBankruptId` (`BankruptId`),
  KEY `debtor_manager_byArbitrManagerID` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_event` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `EventTime` datetime NOT NULL,
  `MessageInfo_MessageType` char(1) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Event` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Event`),
  KEY `Ref6089` (`ArbitrManagerID`),
  KEY `byEventTime` (`EventTime`),
  KEY `refByEfrsb_id` (`efrsb_id`),
  KEY `refEfrsb_event_Debtor` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `Body` longblob NOT NULL,
  `DateDelete` datetime DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `OGRNIP` varchar(12) DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `SRORegNum` varchar(30) NOT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Manager`),
  KEY `refManager_SRO` (`SRORegNum`),
  UNIQUE KEY `byArbitrManagerID_1` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_message` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `MessageGUID` varchar(32) DEFAULT NULL,
  `MessageInfo_MessageType` char(1) DEFAULT NULL,
  `Number` varchar(30) DEFAULT NULL,
  `OGRN` varchar(20) DEFAULT NULL,
  `PublishDate` datetime NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Message`),
  KEY `refByArbitrManagerID` (`ArbitrManagerID`),
  KEY `refByBankruptId` (`BankruptId`),
  UNIQUE KEY `by_efrsb_id` (`efrsb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `mock_efrsb_sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_sro` (
  `Body` longblob NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAddress` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll` (
  `DateFinish` datetime DEFAULT NULL,
  `DateStart` datetime DEFAULT NULL,
  `ExtraColumns` longblob,
  `Name` varchar(200) DEFAULT NULL,
  `PollType` char(1) DEFAULT NULL,
  `id_Poll` int(11) NOT NULL,
  PRIMARY KEY (`id_Poll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `Name` varchar(250) NOT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sro` (
  `RegNum` varchar(40) DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `SRO_Name` varchar(300) NOT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationName` varchar(250) NOT NULL,
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `BulletinOrpauSignature` longblob,
  `BulletinText` text,
  `BulletinVoterSignature` longblob,
  `VoteData` text,
  `VoteTime` datetime DEFAULT NULL,
  `id_Manager` int(11) DEFAULT NULL,
  `id_Poll` int(11) DEFAULT NULL,
  `id_Vote` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Vote`),
  KEY `Ref_Vote_Manager` (`id_Manager`),
  KEY `Ref_Vote_Poll` (`id_Poll`)
  CONSTRAINT `Ref_Vote_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Ref_Vote_Poll` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

