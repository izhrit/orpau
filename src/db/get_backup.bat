@for /f "eol=# delims== tokens=1,2" %%i in (%~dp0..\etc\settings.txt) do @set %%i=%%j
@if exist %~dp0..\etc\settings.local.txt @for /f "eol=# delims== tokens=1,2" %%i in (%~dp0\..\etc\settings.local.txt) do @set %%i=%%j

call "%MYSQL_DIR_BIN%mysqldump.exe" --defaults-extra-file=%~dp0\..\etc\built\mysql.conf --hex-blob --host=%MYSQL_HOST% --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert --result-file=backup.%date%.%random%.sql %DBName%