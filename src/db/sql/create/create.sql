
CREATE TABLE access_log(
    id_Log         BIGINT         AUTO_INCREMENT,
    id_Log_type    INT            NOT NULL,
    IP             VARCHAR(40),
    INN            VARCHAR(12),
    OGRN           VARCHAR(15),
    SNILS          VARCHAR(11),
    Time           TIMESTAMP      NOT NULL,
    Details        TEXT,
    PRIMARY KEY (id_Log)
)ENGINE=INNODB
;



CREATE TABLE confirmation(
    id_Confirmation       INT             AUTO_INCREMENT,
    id_Manager            INT             NOT NULL,
    id_Debtor             INT             NOT NULL,
    Number                VARCHAR(20)     NOT NULL,
    TimeOfCreation        DATETIME        NOT NULL,
    Text                  LONGBLOB        NOT NULL,
    Signature             LONGBLOB,
    EfrsbMessageNumber    VARCHAR(15),
    JudicalActId          VARCHAR(15),
    CaseNumber            VARCHAR(500),
    PRIMARY KEY (id_Confirmation)
)ENGINE=INNODB
;



CREATE TABLE debtor(
    id_Debtor          INT             AUTO_INCREMENT,
    BankruptId         BIGINT,
    ArbitrManagerID    INT,
    id_Region          INT,
    INN                VARCHAR(12),
    SNILS              VARCHAR(11),
    Name               VARCHAR(152),
    OGRN               VARCHAR(15),
    Revision           BIGINT,
    LegalAdress        VARCHAR(400),
    LegalCaseList      LONGBLOB,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE debtor_manager(
    id_Debtor_Manager        INTEGER     NOT NULL,
    BankruptId               BIGINT,
    ArbitrManagerID          INT,
    DateTime_MessageFirst    DATETIME,
    DateTime_MessageLast     DATETIME,
    Revision                 BIGINT,
    PRIMARY KEY (id_Debtor_Manager)
)ENGINE=INNODB
;



CREATE TABLE email_attachment(
    id_Email_Attachment    INT            AUTO_INCREMENT,
    id_Email_Message       INT            NOT NULL,
    FileName               VARCHAR(50),
    Content                LONGBLOB,
    PRIMARY KEY (id_Email_Attachment)
)ENGINE=INNODB
;



CREATE TABLE email_error(
    id_Email_Error      INT         AUTO_INCREMENT,
    id_Email_Message    INT         NOT NULL,
    id_Email_Sender     INT         NOT NULL,
    TimeError           DATETIME    NOT NULL,
    Description         TEXT        NOT NULL,
    PRIMARY KEY (id_Email_Error)
)ENGINE=INNODB
;



CREATE TABLE email_message(
    id_Email_Message    INT             AUTO_INCREMENT,
    RecipientEmail      VARCHAR(100)    NOT NULL,
    RecipientId         INT             NOT NULL,
    RecipientType       CHAR(1)         NOT NULL,
    EmailType           CHAR(1)         NOT NULL,
    TimeDispatch        DATETIME        NOT NULL,
    Details             LONGBLOB,
    PRIMARY KEY (id_Email_Message)
)ENGINE=INNODB
;



CREATE TABLE email_sender(
    id_Email_Sender    INT            AUTO_INCREMENT,
    SenderServer       VARCHAR(50),
    SenderUser         VARCHAR(50),
    PRIMARY KEY (id_Email_Sender)
)ENGINE=INNODB
;



CREATE TABLE email_sent(
    id_Email_sent       INT         AUTO_INCREMENT,
    id_Email_Message    INT         NOT NULL,
    id_Email_Sender     INT         NOT NULL,
    TimeSent            DATETIME    NOT NULL,
    Message             LONGBLOB,
    PRIMARY KEY (id_Email_sent)
)ENGINE=INNODB
;



CREATE TABLE email_tosend(
    TimeDispatch        DATETIME    NOT NULL,
    id_Email_Message    INT         NOT NULL
)ENGINE=INNODB
;



CREATE TABLE log_type(
    id_Log_type    INT             AUTO_INCREMENT,
    Name           VARCHAR(100)    NOT NULL,
    PRIMARY KEY (id_Log_type)
)ENGINE=INNODB
;



CREATE TABLE manager(
    id_Manager                INT             AUTO_INCREMENT,
    ArbitrManagerID           INT             NOT NULL,
    SRORegNum                 VARCHAR(30),
    id_Region                 INT,
    FirstName                 VARCHAR(50)     NOT NULL,
    MiddleName                VARCHAR(50),
    LastName                  VARCHAR(50)     NOT NULL,
    OGRNIP                    VARCHAR(15),
    INN                       VARCHAR(12)     NOT NULL,
    SRORegDate                DATETIME,
    RegNum                    VARCHAR(30),
    Revision                  BIGINT          NOT NULL,
    CorrespondenceAddress     VARCHAR(300),
    SNILS                     VARCHAR(11),
    Phone                     VARCHAR(12),
    EMail                     VARCHAR(100),
    Password                  VARCHAR(100),
    Salt                      VARCHAR(50),
    Login                     VARCHAR(100),
    AgreementText             LONGBLOB,
    AgreementSignature        LONGBLOB,
    ClubAgreementText         LONGBLOB,
    ClubAgreementSignature    LONGBLOB,
    PhoneInAgreement          VARCHAR(12),
    ConfirmationCode          VARCHAR(10),
    ConfirmationCodeTime      DATETIME,
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE manager_group(
    id_Manager_group    INT            AUTO_INCREMENT,
    Name                VARCHAR(70)    NOT NULL,
    Description         TEXT,
    PRIMARY KEY (id_Manager_group)
)ENGINE=INNODB
;



CREATE TABLE manager_group_manager(
    id_Manager          INT    NOT NULL,
    id_Manager_group    INT    NOT NULL,
    PRIMARY KEY (id_Manager, id_Manager_group)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_debtor(
    id_Debtor          INT             AUTO_INCREMENT,
    BankruptId         BIGINT          NOT NULL,
    ArbitrManagerID    INT,
    id_Region          INT             NOT NULL,
    Body               LONGBLOB        NOT NULL,
    LastMessageDate    DATETIME,
    LastReportDate     DATETIME,
    INN                VARCHAR(12),
    SNILS              VARCHAR(11),
    Name               VARCHAR(152),
    OGRN               VARCHAR(15),
    DateLastModif      DATETIME        NOT NULL,
    Category           VARCHAR(100)    NOT NULL,
    CategoryCode       VARCHAR(50)     NOT NULL,
    Revision           BIGINT          NOT NULL,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_debtor_manager(
    id_Debtor_Manager        INT         AUTO_INCREMENT,
    ArbitrManagerID          INT         NOT NULL,
    BankruptId               BIGINT      NOT NULL,
    DateTime_MessageFirst    DATETIME    NOT NULL,
    DateTime_MessageLast     DATETIME    NOT NULL,
    Revision                 BIGINT      NOT NULL,
    PRIMARY KEY (id_Debtor_Manager)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_manager(
    id_Manager               INT             AUTO_INCREMENT,
    ArbitrManagerID          INT             NOT NULL,
    SRORegNum                VARCHAR(30),
    id_Region                INT,
    FirstName                VARCHAR(50)     NOT NULL,
    MiddleName               VARCHAR(50),
    LastName                 VARCHAR(50)     NOT NULL,
    OGRNIP                   VARCHAR(15),
    INN                      VARCHAR(12)     NOT NULL,
    DateLastModif            DATETIME        NOT NULL,
    DateReg                  DATETIME,
    SRORegDate               DATETIME,
    DateDelete               DATETIME,
    Body                     LONGBLOB        NOT NULL,
    RegNum                   VARCHAR(30),
    DownloadDate             TIMESTAMP       NOT NULL,
    Revision                 BIGINT          NOT NULL,
    CorrespondenceAddress    VARCHAR(300),
    SNILS                    VARCHAR(11),
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_region(
    id_Region    INT             AUTO_INCREMENT,
    Name         VARCHAR(250),
    PRIMARY KEY (id_Region)
)ENGINE=INNODB
;



CREATE TABLE mock_efrsb_sro(
    id_SRO           INT             NOT NULL,
    OGRN             VARCHAR(15),
    RegNum           VARCHAR(30)     NOT NULL,
    Body             LONGBLOB        NOT NULL,
    INN              VARCHAR(10)     NOT NULL,
    DateLastModif    DATETIME        NOT NULL,
    Name             VARCHAR(250),
    ShortTitle       VARCHAR(250),
    Title            VARCHAR(250),
    UrAdress         VARCHAR(250),
    Revision         BIGINT          NOT NULL,
    PRIMARY KEY (id_SRO)
)ENGINE=INNODB
;



CREATE TABLE poll(
    id_Poll             INT             AUTO_INCREMENT,
    id_SRO              INT,
    id_Region           INT,
    id_Manager_group    INT,
    Name                VARCHAR(200),
    ExtraColumns        LONGBLOB,
    DateFinish          DATETIME,
    PollType            CHAR(1),
    Status              CHAR(1),
    TimeChange          DATETIME,
    TimeNotified        DATETIME,
    PRIMARY KEY (id_Poll)
)ENGINE=INNODB
;



CREATE TABLE poll_document(
    id_Poll_document      INT            AUTO_INCREMENT,
    id_Poll               INT            NOT NULL,
    Poll_document_type    CHAR(1)        NOT NULL,
    Poll_document_time    DATETIME       NOT NULL,
    FileName              VARCHAR(70)    NOT NULL,
    Body                  LONGBLOB       NOT NULL,
    Parameters            TEXT           NOT NULL,
    PRIMARY KEY (id_Poll_document)
)ENGINE=INNODB
;



CREATE TABLE question(
    id_Question    INT             AUTO_INCREMENT,
    id_Poll        INT             NOT NULL,
    Title          VARCHAR(250)    NOT NULL,
    Extra          LONGBLOB,
    PRIMARY KEY (id_Question)
)ENGINE=INNODB
;



CREATE TABLE region(
    id_Region    INT             AUTO_INCREMENT,
    Name         VARCHAR(250)    NOT NULL,
    PRIMARY KEY (id_Region)
)ENGINE=INNODB
;



CREATE TABLE sro(
    id_SRO        INT             AUTO_INCREMENT,
    OGRN          VARCHAR(15),
    RegNum        VARCHAR(30)     NOT NULL,
    INN           VARCHAR(10)     NOT NULL,
    Name          VARCHAR(250),
    ShortTitle    VARCHAR(250),
    Title         VARCHAR(250),
    UrAdress      VARCHAR(250),
    Revision      BIGINT          NOT NULL,
    PRIMARY KEY (id_SRO)
)ENGINE=INNODB
;



CREATE TABLE tbl_migration(
    MigrationNumber    VARCHAR(250)    NOT NULL,
    MigrationName      VARCHAR(250)    NOT NULL,
    MigrationTime      TIMESTAMP       NOT NULL,
    PRIMARY KEY (MigrationNumber)
)ENGINE=INNODB
;



CREATE TABLE vote(
    id_Vote                   BIGINT         AUTO_INCREMENT,
    id_Manager                INT,
    id_Question               INT            NOT NULL,
    BulletinText              TEXT,
    BulletinVoterSignature    LONGBLOB,
    BulletinOrpauSignature    LONGBLOB,
    VoteData                  TEXT,
    VoteTime                  DATETIME,
    Confirmation_code         VARCHAR(10),
    Confirmation_code_time    DATETIME,
    PRIMARY KEY (id_Vote)
)ENGINE=INNODB
;



CREATE TABLE vote_document(
    id_Vote_document      INT            AUTO_INCREMENT,
    id_Vote               BIGINT         NOT NULL,
    Vote_document_type    CHAR(1)        NOT NULL,
    Vote_document_time    DATETIME       NOT NULL,
    FileName              VARCHAR(80)    NOT NULL,
    Body                  LONGBLOB       NOT NULL,
    Parameters            TEXT           NOT NULL,
    PRIMARY KEY (id_Vote_document)
)ENGINE=INNODB
;



CREATE TABLE vote_log(
    id_Vote_log      INT         AUTO_INCREMENT,
    id_Vote          BIGINT      NOT NULL,
    Vote_log_type    CHAR(1)     NOT NULL,
    Vote_log_time    DATETIME    NOT NULL,
    body             TEXT,
    PRIMARY KEY (id_Vote_log)
)ENGINE=INNODB
;



CREATE INDEX byTime ON access_log(Time)
;
CREATE INDEX byINN ON access_log(INN)
;
CREATE INDEX bySNILS ON access_log(SNILS)
;
CREATE INDEX by_log_type ON access_log(id_Log_type)
;
CREATE UNIQUE INDEX byNumber ON confirmation(Number)
;
CREATE INDEX Ref_Confirmation_Manager ON confirmation(id_Manager)
;
CREATE INDEX Ref_Confirmation_Debtor ON confirmation(id_Debtor)
;
CREATE UNIQUE INDEX byBankruptId ON debtor(BankruptId)
;
CREATE INDEX byName ON debtor(Name)
;
CREATE INDEX byINN ON debtor(INN)
;
CREATE INDEX bySNILS ON debtor(SNILS)
;
CREATE INDEX byOGRN ON debtor(OGRN)
;
CREATE INDEX byRevision ON debtor(Revision)
;
CREATE INDEX Ref_Debtor_Manager ON debtor(ArbitrManagerID)
;
CREATE INDEX refDebtor_Region ON debtor(id_Region)
;
CREATE INDEX Ref_Debtor_manager_Debtor ON debtor_manager(BankruptId)
;
CREATE INDEX Ref_Debtor_manager_Manager ON debtor_manager(ArbitrManagerID)
;
CREATE INDEX Email_Message_to_Attachment ON email_attachment(id_Email_Message)
;
CREATE INDEX Email_Message_Error ON email_error(id_Email_Message)
;
CREATE INDEX Email_Sender_Error ON email_error(id_Email_Sender)
;
CREATE UNIQUE INDEX byServerUser ON email_sender(SenderServer, SenderUser)
;
CREATE INDEX Email_Message_Sent ON email_sent(id_Email_Message)
;
CREATE INDEX Email_Sender_Sent ON email_sent(id_Email_Sender)
;
CREATE INDEX byTimeDispatch ON email_tosend(TimeDispatch, id_Email_Message)
;
CREATE INDEX Email_Message_ToSend ON email_tosend(id_Email_Message)
;
CREATE UNIQUE INDEX byArbitrManagerID ON manager(ArbitrManagerID)
;
CREATE INDEX byLastName ON manager(LastName)
;
CREATE INDEX byFirstName ON manager(FirstName)
;
CREATE INDEX byMiddleName ON manager(MiddleName)
;
CREATE INDEX byINN ON manager(INN)
;
CREATE INDEX byRevision ON manager(Revision)
;
CREATE INDEX refManager_SRO ON manager(SRORegNum)
;
CREATE INDEX refManager_Region ON manager(id_Region)
;
CREATE INDEX ref_group_manager ON manager_group_manager(id_Manager)
;
CREATE INDEX ref_manager_group ON manager_group_manager(id_Manager_group)
;
CREATE UNIQUE INDEX byBankruptId_1 ON mock_efrsb_debtor(BankruptId)
;
CREATE INDEX refMockEfrsbManager_Debtor ON mock_efrsb_debtor(id_Region)
;
CREATE INDEX byArbitrManagerID ON mock_efrsb_debtor(ArbitrManagerID)
;
CREATE INDEX debtor_manager_byArbitrManagerID ON mock_efrsb_debtor_manager(ArbitrManagerID)
;
CREATE INDEX byBankruptId ON mock_efrsb_debtor_manager(BankruptId)
;
CREATE UNIQUE INDEX byArbitrManagerID_1 ON mock_efrsb_manager(ArbitrManagerID)
;
CREATE INDEX refMockEfrsbManager_Region ON mock_efrsb_manager(id_Region)
;
CREATE INDEX refMockEfrsbManager_SRO ON mock_efrsb_manager(SRORegNum)
;
CREATE UNIQUE INDEX byRegNum ON mock_efrsb_sro(RegNum)
;
CREATE INDEX refPoll_SRO ON poll(id_SRO)
;
CREATE INDEX refPoll_Region ON poll(id_Region)
;
CREATE INDEX ref_poll_manager_group ON poll(id_Manager_group)
;
CREATE INDEX Ref_Poll_Document ON poll_document(id_Poll)
;
CREATE INDEX Ref_Question_Poll ON question(id_Poll)
;
CREATE UNIQUE INDEX byRegNum ON sro(RegNum)
;
CREATE INDEX Ref_Vote_Manager ON vote(id_Manager)
;
CREATE INDEX Ref_Vote_Question ON vote(id_Question)
;
CREATE INDEX Ref_Vote_Document ON vote_document(id_Vote)
;
CREATE INDEX Ref_Vote_Log ON vote_log(id_Vote)
;
ALTER TABLE access_log ADD CONSTRAINT by_log_type 
    FOREIGN KEY (id_Log_type)
    REFERENCES log_type(id_Log_type)
;


ALTER TABLE confirmation ADD CONSTRAINT Ref_Confirmation_Debtor 
    FOREIGN KEY (id_Debtor)
    REFERENCES debtor(id_Debtor)
;

ALTER TABLE confirmation ADD CONSTRAINT Ref_Confirmation_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;


ALTER TABLE debtor ADD CONSTRAINT refDebtor_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE email_attachment ADD CONSTRAINT Email_Message_to_Attachment 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;


ALTER TABLE email_error ADD CONSTRAINT Email_Message_Error 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;

ALTER TABLE email_error ADD CONSTRAINT Email_Sender_Error 
    FOREIGN KEY (id_Email_Sender)
    REFERENCES email_sender(id_Email_Sender)
;


ALTER TABLE email_sent ADD CONSTRAINT Email_Message_Sent 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;

ALTER TABLE email_sent ADD CONSTRAINT Email_Sender_Sent 
    FOREIGN KEY (id_Email_Sender)
    REFERENCES email_sender(id_Email_Sender)
;


ALTER TABLE email_tosend ADD CONSTRAINT Email_Message_ToSend 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;


ALTER TABLE manager ADD CONSTRAINT refManager_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;

ALTER TABLE manager_group_manager ADD CONSTRAINT ref_group_manager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;

ALTER TABLE manager_group_manager ADD CONSTRAINT ref_manager_group 
    FOREIGN KEY (id_Manager_group)
    REFERENCES manager_group(id_Manager_group) ON DELETE CASCADE
;


ALTER TABLE mock_efrsb_debtor ADD CONSTRAINT refMockEfrsbManager_Debtor 
    FOREIGN KEY (id_Region)
    REFERENCES mock_efrsb_region(id_Region)
;


ALTER TABLE mock_efrsb_manager ADD CONSTRAINT refMockEfrsbManager_Region 
    FOREIGN KEY (id_Region)
    REFERENCES mock_efrsb_region(id_Region)
;

ALTER TABLE poll ADD CONSTRAINT ref_poll_manager_group 
    FOREIGN KEY (id_Manager_group)
    REFERENCES manager_group(id_Manager_group)
;

ALTER TABLE poll ADD CONSTRAINT refPoll_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;

ALTER TABLE poll ADD CONSTRAINT refPoll_SRO 
    FOREIGN KEY (id_SRO)
    REFERENCES sro(id_SRO)
;


ALTER TABLE poll_document ADD CONSTRAINT Ref_Poll_Document 
    FOREIGN KEY (id_Poll)
    REFERENCES poll(id_Poll)
;


ALTER TABLE question ADD CONSTRAINT Ref_Question_Poll 
    FOREIGN KEY (id_Poll)
    REFERENCES poll(id_Poll) ON DELETE CASCADE
;


ALTER TABLE vote ADD CONSTRAINT Ref_Vote_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;

ALTER TABLE vote ADD CONSTRAINT Ref_Vote_Question 
    FOREIGN KEY (id_Question)
    REFERENCES question(id_Question) ON DELETE CASCADE
;


ALTER TABLE vote_document ADD CONSTRAINT Ref_Vote_Document 
    FOREIGN KEY (id_Vote)
    REFERENCES vote(id_Vote) ON DELETE CASCADE
;


ALTER TABLE vote_log ADD CONSTRAINT Ref_Vote_Log 
    FOREIGN KEY (id_Vote)
    REFERENCES vote(id_Vote) ON DELETE CASCADE
;


