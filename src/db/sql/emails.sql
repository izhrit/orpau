﻿select.
em.RecipientEmail
, case
  when 'n'=EmailType then 'Начало голосования'
  when 'p'=EmailType then 'смена пароля АУ'
  when 'b'=EmailType then 'блокирование пароля АУ'
  else EmailType
  end as EmailType
, em.TimeDispatch TimeDispatch
, et.TimeDispatch TimeToSend
, ifnull(es.TimeSent,ee.TimeError) TimeSentError
, ee.Description Error
from email_message em
left join email_sent es on em.id_Email_Message=es.id_Email_Message
left join email_error ee on em.id_Email_Message=ee.id_Email_Message
left join email_tosend et on em.id_Email_Message=et.id_Email_Message
order by em.TimeDispatch desc
limit 100;
