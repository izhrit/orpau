﻿insert into email_tosend (id_Email_Message,TimeDispatch)
select em.id_Email_Message, em.TimeDispatch
from email_message em
inner join email_error ee on em.id_Email_Message=ee.id_Email_Message
left join email_sent es on em.id_Email_Message=es.id_Email_Message
where es.id_Email_Message is null
and em.TimeDispatch>'2021-05-01'
limit 400;