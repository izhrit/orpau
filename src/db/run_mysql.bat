@for /f "eol=# delims== tokens=1,2" %%i in (%~dp0..\etc\settings.txt) do @set %%i=%%j
@if exist %~dp0..\etc\settings.local.txt @for /f "eol=# delims== tokens=1,2" %%i in (%~dp0\..\etc\settings.local.txt) do @set %%i=%%j

@rem @echo mysql.exe --host=%MYSQL_HOST% --user=%DBUser% --password=***** %DBName%
@call "%MYSQL_DIR_BIN%mysql.exe" --defaults-extra-file=%~dp0\..\etc\built-local\mysql.conf --host=%MYSQL_HOST% %DBName% 
