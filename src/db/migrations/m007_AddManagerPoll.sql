
CREATE TABLE `manager_group` (
  `Description` text,
  `Name` varchar(70) NOT NULL,
  `id_Manager_group` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Manager_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `manager_group_manager` (
  `id_Manager_group` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_Manager`,`id_Manager_group`),
  KEY `ref_group_manager` (`id_Manager`),
  KEY `ref_manager_group` (`id_Manager_group`),
  CONSTRAINT `ref_group_manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `ref_manager_group` FOREIGN KEY (`id_Manager_group`) REFERENCES `manager_group` (`id_Manager_group`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table poll add `id_Manager_group` int(11) DEFAULT NULL;
alter table poll add KEY `ref_poll_manager_group` (`id_Manager_group`);
alter table poll add CONSTRAINT `ref_poll_manager_group` FOREIGN KEY (`id_Manager_group`) REFERENCES `manager_group` (`id_Manager_group`);

alter table poll_document drop FOREIGN KEY `Ref_Poll_Document`;
alter table vote drop FOREIGN KEY `Ref_Vote_Poll`;
alter table poll change `id_Poll` `id_Poll` int(11) NOT NULL AUTO_INCREMENT;
alter table poll_document add CONSTRAINT `Ref_Poll_Document` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`);
alter table vote add CONSTRAINT `Ref_Vote_Poll` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`);

insert into tbl_migration set MigrationNumber='m007', MigrationName='AddManagerPoll';
