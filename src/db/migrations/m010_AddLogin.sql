
alter table manager add Login varchar(100) DEFAULT NULL;
alter table manager add AgreementText longblob DEFAULT NULL;
alter table manager add AgreementSignature longblob DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m010', MigrationName='AddLogin';
