
alter table poll add `id_Region` int(11) DEFAULT NULL;
alter table poll add `id_SRO` int(11) DEFAULT NULL;

alter table poll add KEY `refPoll_Region` (`id_Region`);
alter table poll add KEY `refPoll_SRO` (`id_SRO`);

alter table poll add CONSTRAINT `refPoll_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);
alter table poll add CONSTRAINT `refPoll_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `sro` (`id_SRO`);

insert into tbl_migration set MigrationNumber='m001', MigrationName='AddPollRegionSro';
