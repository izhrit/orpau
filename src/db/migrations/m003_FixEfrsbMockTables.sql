
CREATE TABLE `mock_efrsb_region` (
  `Name` varchar(250) DEFAULT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table mock_efrsb_manager add `CorrespondenceAddress` varchar(300) DEFAULT NULL;
alter table mock_efrsb_manager add `DateLastModif` datetime NOT NULL;
alter table mock_efrsb_manager add `DateReg` datetime DEFAULT NULL;
alter table mock_efrsb_manager add `Revision` bigint(20) NOT NULL;
alter table mock_efrsb_manager add `SNILS` varchar(11) DEFAULT NULL;
alter table mock_efrsb_manager add `SRORegDate` datetime DEFAULT NULL;
alter table mock_efrsb_manager add `id_Region` int(11) DEFAULT NULL;
alter table mock_efrsb_manager add `DownloadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

alter table mock_efrsb_manager change `OGRNIP` `OGRNIP` varchar(15) DEFAULT NULL;
alter table mock_efrsb_manager change `SRORegNum` `SRORegNum` varchar(30) DEFAULT NULL;

alter table mock_efrsb_manager add KEY `refMockEfrsbManager_Region` (`id_Region`);
alter table mock_efrsb_manager add CONSTRAINT `refMockEfrsbManager_Region` FOREIGN KEY (`id_Region`) REFERENCES `mock_efrsb_region` (`id_Region`);

alter table mock_efrsb_manager drop index `refManager_SRO`;
alter table mock_efrsb_manager add KEY `refMockEfrsbManager_SRO` (`SRORegNum`);

alter table mock_efrsb_debtor add `CategoryCode` varchar(50) NOT NULL;
alter table mock_efrsb_debtor add `Category` varchar(100) NOT NULL;
alter table mock_efrsb_debtor add `DateLastModif` datetime NOT NULL;
alter table mock_efrsb_debtor add `LastReportDate` datetime DEFAULT NULL;
alter table mock_efrsb_debtor add `Revision` bigint(20) NOT NULL;
alter table mock_efrsb_debtor add `id_Region` int(11) NOT NULL;

alter table mock_efrsb_debtor add KEY `refMockEfrsbManager_Debtor` (`id_Region`);
alter table mock_efrsb_debtor add CONSTRAINT `refMockEfrsbManager_Debtor` FOREIGN KEY (`id_Region`) REFERENCES `mock_efrsb_region` (`id_Region`);

alter table mock_efrsb_debtor_manager add `Revision` bigint(20) NOT NULL;
alter table mock_efrsb_debtor_manager add `DateTime_MessageFirst` datetime NOT NULL;
alter table mock_efrsb_debtor_manager add `DateTime_MessageLast` datetime NOT NULL;

alter table mock_efrsb_sro change `UrAddress` `UrAdress` varchar(250) DEFAULT NULL;

alter table manager change PostAdress `CorrespondenceAddress` varchar(300) DEFAULT NULL;
alter table manager change firstName `FirstName` varchar(50) NOT NULL;
alter table manager change lastName `LastName` varchar(50) NOT NULL;
alter table manager change middleName `MiddleName` varchar(50) DEFAULT NULL;
alter table manager change INN `INN` varchar(12) NOT NULL;
alter table manager add `OGRNIP` varchar(15) DEFAULT NULL;
alter table manager change efrsbNumber `RegNum` varchar(30) DEFAULT NULL;
update manager set Revision=-10 where id_Manager=1;
update manager set Revision=-11 where id_Manager=2;
update manager set Revision=-12 where id_Manager=3;
alter table manager change Revision `Revision` bigint(20) NOT NULL;
alter table manager add `SNILS` varchar(11) DEFAULT NULL;
alter table manager change SRORegNum `SRORegNum` varchar(30) DEFAULT NULL;

alter table manager drop foreign key Rem_Manager_SRO;
alter table manager drop index Rem_Manager_SRO;

alter table manager drop column id_SRO;

alter table manager drop foreign key Refregion81;
alter table manager drop key Ref5581;

alter table manager add KEY `refManager_Region` (`id_Region`);
alter table manager add KEY `refManager_SRO` (`SRORegNum`);
alter table manager add CONSTRAINT `refManager_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);

update sro set Revision=-10, RegNum="10" where id_SRO=1;
update sro set Revision=-11, RegNum="11" where id_SRO=2;
alter table sro change RegNum `RegNum` varchar(30) NOT NULL;
alter table sro change Revision `Revision` bigint(20) NOT NULL;
alter table sro add UNIQUE KEY `byRegNum` (`RegNum`);
alter table sro add `INN` varchar(10) NOT NULL;
alter table sro add `OGRN` varchar(15) DEFAULT NULL;
alter table sro add `ShortTitle` varchar(250) DEFAULT NULL;
alter table sro add `Title` varchar(250) DEFAULT NULL;
alter table sro add `UrAdress` varchar(250) DEFAULT NULL;
alter table sro change SRO_Name `Name` varchar(250) DEFAULT NULL;

alter table poll drop foreign key refPoll_SRO;
alter table poll drop key refPoll_SRO;
alter table sro change id_SRO `id_SRO` int(11) NOT NULL AUTO_INCREMENT;
alter table poll add KEY `refPoll_SRO` (`id_SRO`);
alter table poll add CONSTRAINT `refPoll_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `sro` (`id_SRO`);

insert into tbl_migration set MigrationNumber='m003', MigrationName='FixEfrsbMockTables';
