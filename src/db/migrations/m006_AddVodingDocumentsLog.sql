
CREATE TABLE `poll_document` (
  `Body` longblob NOT NULL,
  `FileName` varchar(70) NOT NULL,
  `Parameters` text NOT NULL,
  `Poll_document_time` datetime NOT NULL,
  `Poll_document_type` char(1) NOT NULL,
  `id_Poll_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Poll` int(11) NOT NULL,
  PRIMARY KEY (`id_Poll_document`),
  KEY `Ref_Poll_Document` (`id_Poll`),
  CONSTRAINT `Ref_Poll_Document` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `vote_document` (
  `Body` longblob NOT NULL,
  `FileName` varchar(80) NOT NULL,
  `Parameters` text NOT NULL,
  `Vote_document_time` datetime NOT NULL,
  `Vote_document_type` char(1) NOT NULL,
  `id_Vote_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Vote_document`),
  KEY `Ref_Vote_Document` (`id_Vote`),
  CONSTRAINT `Ref_Vote_Document` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `vote_log` (
  `Vote_log_time` datetime NOT NULL,
  `Vote_log_type` char(1) NOT NULL,
  `body` text,
  `id_Vote_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Vote_log`),
  KEY `Ref_Vote_Log` (`id_Vote`),
  CONSTRAINT `Ref_Vote_Log` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m006', MigrationName='AddVodingDocumentsLog';
