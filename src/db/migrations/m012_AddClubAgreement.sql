alter table manager add ClubAgreementText longblob DEFAULT NULL;
alter table manager add PhoneInAgreement varchar(12) DEFAULT NULL;
alter table manager add ConfirmationCode varchar(10) DEFAULT NULL;
alter table manager add ConfirmationCodeTime datetime DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m012', MigrationName='AddClubAgreement';